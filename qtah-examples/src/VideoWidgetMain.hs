module Main where

import Control.Monad (filterM, when)
import Data.List (isInfixOf)
import Foreign.Hoppy.Runtime (withScopedPtr)
import Graphics.UI.Qtah.Core.HMimeType
import qualified Graphics.UI.Qtah.Core.QCoreApplication as QCoreApplication
import qualified Graphics.UI.Qtah.Core.QDir as QDir
import qualified Graphics.UI.Qtah.Core.QMimeDatabase as QMimeDatabase
import qualified Graphics.UI.Qtah.Core.QMimeType as QMimeType
import qualified Graphics.UI.Qtah.Core.QStandardPaths as QStandardPaths
import Graphics.UI.Qtah.Core.QStandardPaths (QStandardPathsStandardLocation (..))
import qualified Graphics.UI.Qtah.Core.QUrl as QUrl
import Graphics.UI.Qtah.Core.QUrl (QUrl)
import Graphics.UI.Qtah.Core.Types (QtOrientation (..))
import qualified Graphics.UI.Qtah.Multimedia.QMediaContent as QMediaContent
import qualified Graphics.UI.Qtah.Multimedia.QMediaPlayer as QMediaPlayer
import Graphics.UI.Qtah.Multimedia.QMediaPlayer (
  QMediaPlayer,
  QMediaPlayerError (..),
  QMediaPlayerState (..)
  )
import Graphics.UI.Qtah.Multimedia.Types (QMultimediaSupportEstimate (..))
import qualified Graphics.UI.Qtah.Multimedia.Widgets.QVideoWidget as QVideoWidget
import Graphics.UI.Qtah.Signal (connect_)
import qualified Graphics.UI.Qtah.Widgets.QAbstractButton as QAbstractButton
import qualified Graphics.UI.Qtah.Widgets.QAbstractSlider as QAbstractSlider
import qualified Graphics.UI.Qtah.Widgets.QApplication as QApplication
import qualified Graphics.UI.Qtah.Widgets.QBoxLayout as QBoxLayout
import qualified Graphics.UI.Qtah.Widgets.QDialog as QDialog
import qualified Graphics.UI.Qtah.Widgets.QFileDialog as QFileDialog
import Graphics.UI.Qtah.Widgets.QFileDialog (
  QFileDialog,
  QFileDialogAcceptMode (..)
  )
import qualified Graphics.UI.Qtah.Widgets.QHBoxLayout as QHBoxLayout
import qualified Graphics.UI.Qtah.Widgets.QLabel as QLabel
import Graphics.UI.Qtah.Widgets.QLabel (QLabel)
import qualified Graphics.UI.Qtah.Widgets.QPushButton as QPushButton
import Graphics.UI.Qtah.Widgets.QPushButton (QPushButton)
import qualified Graphics.UI.Qtah.Widgets.QSlider as QSlider
import qualified Graphics.UI.Qtah.Widgets.QStyle as QStyle
import Graphics.UI.Qtah.Widgets.QStyle (QStyleStandardPixmap (..))
import qualified Graphics.UI.Qtah.Widgets.QVBoxLayout as QVBoxLayout
import qualified Graphics.UI.Qtah.Widgets.QWidget as QWidget
import Graphics.UI.Qtah.Widgets.QWidget (QWidget)
import System.Environment (getArgs)
import Text.Printf (printf)

data AppData = AppData
  { window :: QWidget
  , dialog :: QFileDialog
  , playButton :: QPushButton
  , mp :: QMediaPlayer
  , errorLabel :: QLabel
  }

main :: IO ()
main = withScopedPtr (getArgs >>= QApplication.new) $ \_ -> do
  window <- makeWindow
  QWidget.show window
  QCoreApplication.exec

makeWindow :: IO QWidget
makeWindow = do
  window <- QWidget.new
  QWidget.setWindowTitle window "A Simple Video Player"
  QWidget.resizeRaw window 480 360
  video <- QVideoWidget.new

  openButton <- QPushButton.newWithText "Open..."

  playButton <- QPushButton.new
  style <- QWidget.style window
  QStyle.standardIcon style QStyleStandardPixmap_SP_MediaPlay >>=
    QAbstractButton.setIcon playButton
  QWidget.setToolTip playButton "Play"
  QWidget.setEnabled playButton False

  slider <- QSlider.newWithOrientation QtOrientation_Horizontal
  QAbstractSlider.setRange slider 0 0

  errorLabel <- QLabel.new

  controlLayout <- QHBoxLayout.new
  QBoxLayout.addWidget controlLayout openButton
  QBoxLayout.addWidget controlLayout playButton
  QBoxLayout.addWidget controlLayout slider

  layout <- QVBoxLayout.new
  QBoxLayout.addWidget layout video
  QBoxLayout.addLayout layout controlLayout
  QBoxLayout.addWidget layout errorLabel

  QWidget.setLayout window layout

  mp <- QMediaPlayer.new
  QMediaPlayer.setVideoOutput mp video

  dialog <- QFileDialog.newWithParent window
  QWidget.setWindowTitle dialog "Open Movie"
  QFileDialog.setAcceptMode dialog QFileDialogAcceptMode_AcceptOpen
  mimes <- getMimes
  QFileDialog.setMimeTypeFilters dialog $ allFiles : map name mimes
  locations <-
    QStandardPaths.standardLocations
      QStandardPathsStandardLocation_MoviesLocation
  location <- if null locations then QDir.homePath
                                else return $ head locations
  QFileDialog.setDirectoryPath dialog location

  let ad = AppData
           window
           dialog
           playButton
           mp
           errorLabel

  connect_ openButton QAbstractButton.clickedSignal $ \_ -> openFile ad
  connect_ playButton QAbstractButton.clickedSignal $ \_ -> play ad
  connect_ mp QMediaPlayer.errorSignal $ handleError ad
  connect_ mp QMediaPlayer.stateChangedSignal $ mediaStateChanged ad
  connect_ mp QMediaPlayer.positionChangedSignal $ (QAbstractSlider.setValue slider) . fromIntegral
  connect_ mp QMediaPlayer.durationChangedSignal $ (QAbstractSlider.setMaximum slider) . fromIntegral
  connect_ slider QAbstractSlider.valueChangedSignal $ (QMediaPlayer.setPosition mp) . fromIntegral

  return window

getMimes :: IO [HMimeType]
getMimes = do
  db <- QMimeDatabase.new
  mimes <- QMimeDatabase.allMimeTypes db
  filterM (\(HMimeType name) -> do
    se <- QMediaPlayer.hasSupport name
    return $ isInfixOf "video" name &&
             QMultimediaSupportEstimate_NotSupported /= se
    ) mimes

allFiles :: String
allFiles = "application/octet-stream"

openFile :: AppData -> IO ()
openFile ad@(AppData _ dialog _ mp _) = do
  res <- QDialog.exec dialog
  when (res == 1) $ do
    (file:_) <- QFileDialog.selectedFiles dialog
    QUrl.fromUserInput file >>= setUrl ad

setUrl :: AppData -> QUrl -> IO ()
setUrl ad@(AppData window _ playButton mp errorLabel) url = do
  QLabel.setText errorLabel ""
  isLocal <- QUrl.isLocalFile url
  if isLocal then QUrl.toLocalFile url >>= QWidget.setWindowFilePath window
             else QWidget.setWindowFilePath window ""
  media <- QMediaContent.newFromUrl url
  QMediaPlayer.setMedia mp media
  QWidget.setEnabled playButton True
  play ad

play :: AppData -> IO ()
play (AppData _ _ _ mp errorLabel) = do
  state <- QMediaPlayer.state mp
  (if state == QMediaPlayerState_PlayingState
   then QMediaPlayer.pause
   else QMediaPlayer.play) mp

mediaStateChanged :: AppData -> QMediaPlayerState -> IO ()
mediaStateChanged (AppData window _ playButton _ errorLabel) state = do
  let (icon, tooltip) = if state == QMediaPlayerState_PlayingState
                        then (QStyleStandardPixmap_SP_MediaPause, "Pause")
                        else (QStyleStandardPixmap_SP_MediaPlay, "Play")
  style <- QWidget.style window
  QStyle.standardIcon style icon >>= QAbstractButton.setIcon playButton
  QWidget.setToolTip playButton tooltip

handleError :: AppData -> QMediaPlayerError -> IO ()
handleError (AppData _ _ playButton mp errorLabel) err = do
  QWidget.setEnabled playButton False
  str <- QMediaPlayer.errorString mp
  QLabel.setText errorLabel (printf "Error #%d: %s" (fromEnum err) str :: String)
