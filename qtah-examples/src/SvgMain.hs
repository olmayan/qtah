module Main where

import Control.Monad (unless)
import Foreign.Hoppy.Runtime (withScopedPtr)
import qualified Graphics.UI.Qtah.Core.QCoreApplication as QCoreApplication
import Graphics.UI.Qtah.Signal (connect_)
import qualified Graphics.UI.Qtah.Svg.QSvgRenderer as QSvgRenderer
import qualified Graphics.UI.Qtah.Svg.QSvgWidget as QSvgWidget
import qualified Graphics.UI.Qtah.Widgets.QAction as QAction
import qualified Graphics.UI.Qtah.Widgets.QApplication as QApplication
import qualified Graphics.UI.Qtah.Widgets.QFileDialog as QFileDialog
import qualified Graphics.UI.Qtah.Widgets.QMainWindow as QMainWindow
import Graphics.UI.Qtah.Widgets.QMainWindow (QMainWindow)
import qualified Graphics.UI.Qtah.Widgets.QMenu as QMenu
import qualified Graphics.UI.Qtah.Widgets.QMenuBar as QMenuBar
import qualified Graphics.UI.Qtah.Widgets.QScrollArea as QScrollArea
import qualified Graphics.UI.Qtah.Widgets.QWidget as QWidget
import System.Environment (getArgs)

main :: IO ()
main = withScopedPtr (getArgs >>= QApplication.new) $ \_ -> do
  mainWindow <- makeMainWindow
  QWidget.show mainWindow
  QCoreApplication.exec

makeMainWindow :: IO QMainWindow
makeMainWindow = do
  window <- QMainWindow.new
  QWidget.setWindowTitle window "A Simple SVG Viewer"
  QWidget.resizeRaw window 480 360

  menu <- QMenuBar.new
  QMainWindow.setMenuBar window menu

  menuFile <- QMenuBar.addNewMenu menu "&File"
  menuFileOpen <- QMenu.addNewAction menuFile "&Open..."
  _ <- QMenu.addSeparator menuFile
  menuFileQuit <- QMenu.addNewAction menuFile "&Quit"

  menuAbout <- QMenuBar.addNewMenu menu "&About"
  menuAboutQt <- QMenu.addNewAction menuAbout "About &Qt"

  scrollArea <- QScrollArea.new
  QMainWindow.setCentralWidget window scrollArea

  svg <- QSvgWidget.new
  QScrollArea.setWidget scrollArea svg
  QMainWindow.setCentralWidget window scrollArea

  connect_ menuFileOpen QAction.triggeredSignal $ \_ -> do
    path <- QFileDialog.getOpenFileName window "Open file" "" fileDialogFilter
    unless (null path) $ do
        QSvgWidget.loadFile svg path
        QSvgWidget.renderer svg >>= QSvgRenderer.defaultSize >>= QWidget.setFixedSize svg

  connect_ menuFileQuit QAction.triggeredSignal $ \_ -> QWidget.close window
  connect_ menuAboutQt QAction.triggeredSignal $ \_ -> QApplication.aboutQt

  return window

fileDialogFilter :: String
fileDialogFilter = "SVG files (*.svg);;All Files (*)"