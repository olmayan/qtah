-- Copyright : olmayan, 2016 <varolmayan@yahoo.com>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Main where

import Control.Monad (when)
import Data.Foldable (forM_)
import Data.IORef
import Foreign.Hoppy.Runtime (withScopedPtr)
import Graphics.UI.Qtah.Core.HPointF (HPointF (HPointF))
import qualified Graphics.UI.Qtah.Core.QAbstractAnimation as QAbstractAnimation
import Graphics.UI.Qtah.Core.QAbstractAnimation (
    QAbstractAnimation,
    QAbstractAnimationDeletionPolicy (
        QAbstractAnimationDeletionPolicy_DeleteWhenStopped
        )
    )
import qualified Graphics.UI.Qtah.Core.QAnimationGroup as QAnimationGroup
import qualified Graphics.UI.Qtah.Core.QCoreApplication as QCoreApplication
import qualified Graphics.UI.Qtah.Core.QEasingCurve as QEasingCurve
import qualified Graphics.UI.Qtah.Core.QObject as QObject
import Graphics.UI.Qtah.Core.QObject (QObject)
import qualified Graphics.UI.Qtah.Core.QParallelAnimationGroup as QParallelAnimationGroup
import qualified Graphics.UI.Qtah.Core.QPropertyAnimation as QPropertyAnimation
import Graphics.UI.Qtah.Core.QPropertyAnimation (QPropertyAnimation)
import qualified Graphics.UI.Qtah.Core.QSequentialAnimationGroup as QSequentialAnimationGroup
import qualified Graphics.UI.Qtah.Core.QSizeF as QSizeF
import qualified Graphics.UI.Qtah.Core.QString as QString
import qualified Graphics.UI.Qtah.Core.QVariant as QVariant
import Graphics.UI.Qtah.Core.QVariant (QVariant)
import Graphics.UI.Qtah.Signal (connect_)
import qualified Graphics.UI.Qtah.Widgets.QAbstractButton as QAbstractButton
import qualified Graphics.UI.Qtah.Widgets.QAction as QAction
import Graphics.UI.Qtah.Widgets.QAction (QAction)
import qualified Graphics.UI.Qtah.Widgets.QApplication as QApplication
import qualified Graphics.UI.Qtah.Widgets.QGraphicsItem as QGraphicsItem
import qualified Graphics.UI.Qtah.Widgets.QGraphicsObject as QGraphicsObject
import qualified Graphics.UI.Qtah.Widgets.QGraphicsProxyWidget as QGraphicsProxyWidget
import qualified Graphics.UI.Qtah.Widgets.QGraphicsWidget as QGraphicsWidget
import qualified Graphics.UI.Qtah.Widgets.QGraphicsScene as QGraphicsScene
import qualified Graphics.UI.Qtah.Widgets.QGraphicsView as QGraphicsView
import qualified Graphics.UI.Qtah.Widgets.QMainWindow as QMainWindow
import Graphics.UI.Qtah.Widgets.QMainWindow (QMainWindow)
import qualified Graphics.UI.Qtah.Widgets.QMenu as QMenu
import qualified Graphics.UI.Qtah.Widgets.QMenuBar as QMenuBar
import qualified Graphics.UI.Qtah.Widgets.QPushButton as QPushButton
import qualified Graphics.UI.Qtah.Widgets.QWidget as QWidget
import System.Environment (getArgs)
import System.Random.Mersenne.Pure64 (PureMT, newPureMT, randomInt)

data AppData = AppData {
    ellipse :: QObject,
    rect :: QObject,
    menuTestCurved :: QAction,
    ref :: IORef PureMT
    }

main :: IO ()
main = withScopedPtr (getArgs >>= QApplication.new) $ \_ -> do
    window <- makeMainWindow
    QWidget.show window
    QCoreApplication.exec

makeMainWindow :: IO QMainWindow
makeMainWindow = do
    window <- QMainWindow.new
    QWidget.setWindowTitle window "Animation Test"

    menu               <- QMainWindow.menuBar window
    menuTest           <- QMenuBar.addNewMenu menu "&Test"
    menuTestSingle     <- QMenu.addNewAction menuTest "&Single animation"
    menuTestParallel   <- QMenu.addNewAction menuTest "&Parallel animation"
    menuTestSequential <- QMenu.addNewAction menuTest "&Sequential animation"
    menuTestComplex    <- QMenu.addNewAction menuTest "Comple&x animation"
    _                  <- QMenu.addSeparator menuTest
    menuTestCurved     <- QMenu.addNewAction menuTest "Add easing &curve"
    QAction.setCheckable menuTestCurved True
    QAction.setStatusTip menuTestCurved
                         "Adds random easing curves to each property animation."
    _                  <- QMenu.addSeparator menuTest
    menuTestButton     <- QMenu.addNewAction menuTest "Show &button"
    QAction.setCheckable menuTestButton True
    _                  <- QMenu.addSeparator menuTest
    menuTestQuit       <- QMenu.addNewAction menuTest "&Quit"

    menuAbout          <- QMenuBar.addNewMenu menu "&About"
    menuAboutQt        <- QMenu.addNewAction menuAbout "About &Qt"

    scene <- QGraphicsScene.newWithSceneRectRaw 0 0 600 600
    view <- QGraphicsView.newWithScene scene
    QWidget.sizeHint view >>= QWidget.setFixedSize view
    QMainWindow.setCentralWidget window view

    ellipse <- QGraphicsScene.addEllipseRaw scene 100 100 100 100
    rect <- QGraphicsScene.addRectRaw scene 400 100 100 100

    -- These proxies are necessary as the shape items do not descend from
    -- the QObject by themselves and therefore can not handle properties
    ellipseProxy <- QGraphicsWidget.new
    rectProxy <- QGraphicsWidget.new
    QGraphicsItem.pos ellipse >>= QGraphicsItem.setPos ellipseProxy
    QGraphicsItem.pos rect >>= QGraphicsItem.setPos rectProxy

    proxyWidget <- QGraphicsProxyWidget.new
    button <- QPushButton.newWithText "Test!"
    QGraphicsProxyWidget.setWidget proxyWidget button
    QGraphicsItem.setVisible proxyWidget False
    sz <- QGraphicsWidget.size proxyWidget
    w <- QSizeF.width sz
    h <- QSizeF.height sz
    QGraphicsItem.setPos proxyWidget (HPointF ((600.0 - w) / 2.0)
                                              ((600.0 - h) / 2.0))
    QGraphicsScene.addItem scene proxyWidget

    g <- newPureMT
    ref <- newIORef g

    let me = AppData (QObject.cast ellipseProxy)
                     (QObject.cast rectProxy)
                     menuTestCurved ref
        resetPositions = do
            QGraphicsItem.setPos ellipseProxy $ HPointF 0 0
            QGraphicsItem.setPos rectProxy $ HPointF 0 0
            QGraphicsItem.setScale ellipseProxy 1.0
            QGraphicsItem.setScale rectProxy 1.0

    forM_ [ (menuTestSingle, testSingle)
          , (menuTestParallel, testParallel)
          , (menuTestSequential, testSequential)
          , (menuTestComplex, testComplex) ] $
        \(item, f) -> connect_ item QAction.triggeredSignal $ \_ -> do
            let itemsToDisable = [ menuTestSingle
                                 , menuTestParallel
                                 , menuTestSequential
                                 , menuTestComplex
                                 , menuTestCurved ]
            forM_ itemsToDisable $ \item -> QAction.setEnabled item False
            anim <- f me
            connect_ anim QAbstractAnimation.finishedSignal $ do
                forM_ itemsToDisable $ \item -> QAction.setEnabled item True
            resetPositions
            QAbstractAnimation.start
                anim
                QAbstractAnimationDeletionPolicy_DeleteWhenStopped

    connect_ menuTestButton QAction.toggledSignal $
        QGraphicsItem.setVisible proxyWidget
    connect_ menuTestQuit QAction.triggeredSignal $ \_ -> QWidget.close window
    connect_ menuAboutQt QAction.triggeredSignal $ \_ -> QApplication.aboutQt

    connect_ ellipseProxy QGraphicsObject.xChangedSignal $
        QGraphicsItem.x ellipseProxy >>= QGraphicsItem.setX ellipse
    connect_ ellipseProxy QGraphicsObject.yChangedSignal $
        QGraphicsItem.y ellipseProxy >>= QGraphicsItem.setY ellipse
    connect_ ellipseProxy QGraphicsObject.scaleChangedSignal $
        QGraphicsItem.scale ellipseProxy >>= QGraphicsItem.setScale ellipse

    connect_ rectProxy QGraphicsObject.xChangedSignal $
        QGraphicsItem.x rectProxy >>= QGraphicsItem.setX rect
    connect_ rectProxy QGraphicsObject.yChangedSignal $
        QGraphicsItem.y rectProxy >>= QGraphicsItem.setY rect
    connect_ rectProxy QGraphicsObject.scaleChangedSignal $
        QGraphicsItem.scale rectProxy >>= QGraphicsItem.setScale rect

    connect_ button QAbstractButton.clickedSignal $ \_ -> do
        QWidget.setEnabled button False
        (HPointF x0 y0) <- QGraphicsItem.pos proxyWidget
        sz <- QGraphicsWidget.size proxyWidget
        w <- QSizeF.width sz
        h <- QSizeF.height sz
        g <- readIORef ref
        let maxX = truncate $ 600 - w + 1
            maxY = truncate $ 600 - h + 1
            (num, g') = randomInt g
            x = num `mod` maxX
            y = (truncate $ fromIntegral num / fromIntegral maxX) `mod` maxY
            dist = sqrt $ (x0 - fromIntegral x) ** 2 +
                          (y0 - fromIntegral y) ** 2
        writeIORef ref g'
        anim <- QVariant.newFromPointF (HPointF (fromIntegral x)
                                                (fromIntegral y)) >>=
            createPropertyAnimation me (QObject.cast button) "pos"
                (truncate $ dist * 2.0)
        connect_ anim QAbstractAnimation.finishedSignal $
            QWidget.setEnabled button True
        QAbstractAnimation.start
            anim
            QAbstractAnimationDeletionPolicy_DeleteWhenStopped

        return ()
    return window

testSingle :: AppData -> IO QAbstractAnimation
testSingle me@(AppData ellipseProxy _ _ _) = do
    anim <- QVariant.newFromPointF (HPointF 300 200) >>=
        createPropertyAnimation me ellipseProxy "pos" 1000
    return $ QAbstractAnimation.cast anim

testParallel :: AppData -> IO QAbstractAnimation
testParallel me@(AppData ellipseProxy rectProxy _ _) = do
    ag <- QParallelAnimationGroup.new

    QVariant.newFromPointF (HPointF 200 200) >>=
        createPropertyAnimation me ellipseProxy "pos" 2000 >>=
            QAnimationGroup.addAnimation ag
    QVariant.newFromFloat 2.0 >>=
        createPropertyAnimation me ellipseProxy "scale" 1000 >>=
            QAnimationGroup.addAnimation ag
    QVariant.newFromPointF (HPointF (-175) 25) >>=
        createPropertyAnimation me rectProxy "pos" 1500 >>=
            QAnimationGroup.addAnimation ag
    QVariant.newFromFloat 0.5 >>=
        createPropertyAnimation me rectProxy "scale" 1500 >>=
            QAnimationGroup.addAnimation ag

    return $ QAbstractAnimation.cast ag

testSequential :: AppData -> IO QAbstractAnimation
testSequential me@(AppData ellipseProxy rectProxy _ _) = do
    ag <- QSequentialAnimationGroup.new

    QVariant.newFromPointF (HPointF (-150) 150) >>=
        createPropertyAnimation me rectProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag
    QVariant.newFromPointF (HPointF 0 300) >>=
        createPropertyAnimation me ellipseProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag
    QVariant.newFromPointF (HPointF 300 300) >>=
        createPropertyAnimation me ellipseProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag
    QVariant.newFromPointF (HPointF 300 0) >>=
        createPropertyAnimation me ellipseProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag
    QVariant.newFromPointF (HPointF 0 0)  >>=
        createPropertyAnimation me ellipseProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag
    QVariant.newFromPointF (HPointF 0 0) >>=
        createPropertyAnimation me rectProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag

    return $ QAbstractAnimation.cast ag

testComplex :: AppData -> IO QAbstractAnimation
testComplex me@(AppData ellipseProxy rectProxy _ _) = do
    ag <- QParallelAnimationGroup.new

    ag1 <- QSequentialAnimationGroup.new
    ag2 <- QSequentialAnimationGroup.new
    QAnimationGroup.addAnimation ag ag1
    QAnimationGroup.addAnimation ag ag2

    QVariant.newFromPointF (HPointF 300 0) >>=
        createPropertyAnimation me ellipseProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag1
    QVariant.newFromPointF (HPointF 300 300) >>=
        createPropertyAnimation me ellipseProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag1
    QVariant.newFromPointF (HPointF 0 300) >>=
        createPropertyAnimation me ellipseProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag1
    QVariant.newFromPointF (HPointF 0 0) >>=
        createPropertyAnimation me ellipseProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag1

    QSequentialAnimationGroup.addPause ag2 200
    QVariant.newFromPointF (HPointF 0 300) >>=
        createPropertyAnimation me rectProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag2
    QVariant.newFromPointF (HPointF (-300) 300) >>=
        createPropertyAnimation me rectProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag2
    QVariant.newFromPointF (HPointF (-300) 0) >>=
        createPropertyAnimation me rectProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag2
    QVariant.newFromPointF (HPointF 0 0) >>=
        createPropertyAnimation me rectProxy "pos" 500 >>=
            QAnimationGroup.addAnimation ag2

    return $ QAbstractAnimation.cast ag

createPropertyAnimation :: AppData
                        -> QObject
                        -> String
                        -> Int
                        -> QVariant
                        -> IO QPropertyAnimation
createPropertyAnimation me target property duration endValue = do
    anim <- QString.toLatin1 property >>=
        QPropertyAnimation.newWithProperty target
    QPropertyAnimation.setDuration anim duration
    QPropertyAnimation.setEndValue anim endValue
    setRandomEasingCurve me anim
    return anim

setRandomEasingCurve :: AppData -> QPropertyAnimation -> IO ()
setRandomEasingCurve me@(AppData _ _ menuTestCurved ref) anim = do
    checked <- QAction.isChecked menuTestCurved
    when checked $ do
        g <- readIORef ref
        let (num, g') = randomInt g
        writeIORef ref g'
        QEasingCurve.newWithType
            (toEnum $ 1 + num `mod` 40)
            >>= QPropertyAnimation.setEasingCurve anim