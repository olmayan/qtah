-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Core.HLineF (
  HLineF (..),
  null,
  isNull,
  translate,
  translateRaw
  ) where

import Graphics.UI.Qtah.Core.Types (QReal)
import Prelude hiding (null)
import Graphics.UI.Qtah.Core.HPointF (HPointF (HPointF))

data HLineF = HLineF
  { p1 :: HPointF
  , p2 :: HPointF
  } deriving (Eq, Show)

null :: HLineF
null = HLineF (HPointF 0 0) (HPointF 0 0)

isNull :: HLineF -> Bool
isNull (HLineF (HPointF 0 0) (HPointF 0 0)) = True
isNull _ = False

translate :: HLineF -> HPointF -> HLineF
translate (HLineF (HPointF x1 y1) (HPointF x2 y2)) (HPointF x y)
  = HLineF (HPointF (x1 + x) (y1 + y)) (HPointF (x2 + x) (y2 + y))

translateRaw :: HLineF -> QReal -> QReal -> HLineF
translateRaw (HLineF (HPointF x1 y1) (HPointF x2 y2)) x y
  = HLineF (HPointF (x1 + x) (y1 + y)) (HPointF (x2 + x) (y2 + y))