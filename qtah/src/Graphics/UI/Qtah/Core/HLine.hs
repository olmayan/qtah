-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Core.HLine (
  HLine (..),
  null,
  isNull,
  translate,
  translateRaw
  ) where

import Prelude hiding (null)
import Graphics.UI.Qtah.Core.HPoint (HPoint (HPoint))

data HLine = HLine
  { p1 :: HPoint
  , p2 :: HPoint
  } deriving (Eq, Show)

null :: HLine
null = HLine (HPoint 0 0) (HPoint 0 0)

isNull :: HLine -> Bool
isNull (HLine (HPoint 0 0) (HPoint 0 0)) = True
isNull _ = False

translate :: HLine -> HPoint -> HLine
translate (HLine (HPoint x1 y1) (HPoint x2 y2)) (HPoint x y)
  = HLine (HPoint (x1 + x) (y1 + y)) (HPoint (x2 + x) (y2 + y))

translateRaw :: HLine -> Int -> Int -> HLine
translateRaw (HLine (HPoint x1 y1) (HPoint x2 y2)) x y
  = HLine (HPoint (x1 + x) (y1 + y)) (HPoint (x2 + x) (y2 + y))