 
-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Multimedia.Widgets.QVideoWidget (
  aModule,
  c_QVideoWidget
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkCtor,
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (enumT, intT, objT, ptrT)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (e_AspectRatioMode)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (c_ListenerBool, c_ListenerInt)
import Graphics.UI.Qtah.Generator.Interface.Multimedia.QMediaObject (c_QMediaObject)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Multimedia", "Widgets", "QVideoWidget"] minVersion $
  QtExport (ExportClass c_QVideoWidget) :
  map QtExportSignal signals

c_QVideoWidget =
  addReqIncludes [includeStd "QVideoWidget"] $
  classSetEntityPrefix "" $
  makeClass (ident "QVideoWidget") Nothing [c_QWidget]
  [ mkCtor "new" []
  , mkCtor "newWithParent" [ptrT $ objT c_QWidget]
  , mkConstMethod "mediaObject" [] $ ptrT $ objT c_QMediaObject
  , mkConstMethod "sizeHint" [] $ objT c_QSize
  , mkProp "aspectRatioMode" $ enumT e_AspectRatioMode
  , mkProp "brightness" intT
  , mkProp "contrast" intT
  , mkProp "hue" intT
  , mkBoolIsProp "fullScreen"
  , mkProp "saturation" intT
  ]

signals =
  [ makeSignal c_QVideoWidget "brightnessChanged" c_ListenerInt
  , makeSignal c_QVideoWidget "contrastChanged" c_ListenerInt
  , makeSignal c_QVideoWidget "fullScreenChanged" c_ListenerBool
  , makeSignal c_QVideoWidget "hueChanged" c_ListenerInt
  , makeSignal c_QVideoWidget "saturationChanged" c_ListenerInt
  ]
