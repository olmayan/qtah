-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-- | Bindings in the @QAudio::@ and @QMultimedia::@ namespacs.
module Graphics.UI.Qtah.Generator.Interface.Multimedia.Types (
  aModule,
  e_Error,
  e_Mode,
  e_Role,
  e_Type,
  e_AvailabilityStatus,
  e_EncodingMode,
  e_EncodingQuality,
  e_SupportEstimate
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportEnum),
  ident1,
  includeStd,
  )
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

{-# ANN module "HLint: ignore Use camelCase" #-}

minVersion = [4, 6]

aModule :: AModule
aModule = AQtModule $
    makeQtModuleWithMinVersion ["Multimedia", "Types"] minVersion exports

exports :: [QtExport]
exports =
  QtExportSpecials :
  (map QtExport . collect)
  [ just $ ExportEnum e_Error
  , just $ ExportEnum e_Mode
  , test (qtVersion >= [5, 6]) $ ExportEnum e_Role
  , just $ ExportEnum e_Type
  , test (qtVersion >= [5, 0]) $ ExportEnum e_AvailabilityStatus
  , test (qtVersion >= [5, 0]) $ ExportEnum e_EncodingMode
  , test (qtVersion >= [5, 0]) $ ExportEnum e_EncodingQuality
  , test (qtVersion >= [5, 0]) $ ExportEnum e_SupportEstimate
  ]

e_Error =
  makeQtEnum (ident1 "QAudio" "Error") [includeStd "QAudio"]
  [ (0, ["no", "error"])
  , (1, ["open", "error"])
  , (2, ["i", "o", "error"])
  , (3, ["underrun", "error"])
  , (4, ["fatal", "error"])
  ]

e_Mode =
  makeQtEnum (ident1 "QAudio" "Mode") [includeStd "QAudio"]
  [ (0, ["audio", "input"])
  , (1, ["audio", "output"])
  ]

e_Role =
  makeQtEnum (ident1 "QAudio" "Role") [includeStd "QAudio"]
  [ (0, ["unknown", "role"])
  , (1, ["music", "role"])
  , (2, ["video", "role"])
  , (3, ["voice", "communication", "role"])
  , (4, ["alarm", "role"])
  , (5, ["notification", "role"])
  , (6, ["ringtone", "role"])
  , (7, ["accessibility", "role"])
  , (8, ["sonification", "role"])
  , (9, ["game", "role"])
  ]

e_Type =
  makeQtEnum (ident1 "QAudio" "Type") [includeStd "QAudio"]
  [ (0, ["active", "state"])
  , (1, ["suspended", "state"])
  , (2, ["stopped", "state"])
  , (3, ["idle", "state"])
  ]

e_AvailabilityStatus =
  makeQtEnum (ident1 "QMultimedia" "AvailabilityStatus") [includeStd "QMultimedia"]
  [ (0, ["available"])
  , (1, ["service", "missing"])
  , (2, ["busy"])
  , (3, ["resource", "error"])
  ]

e_EncodingMode =
  makeQtEnum (ident1 "QMultimedia" "EncodingMode") [includeStd "QMultimedia"]
  [ (0, ["constant", "quality", "encoding"])
  , (1, ["constant", "bit", "rate", "encoding"])
  , (2, ["average", "bit", "rate", "encoding"])
  , (3, ["two", "pass", "encoding"])
  ]

e_EncodingQuality =
  makeQtEnum (ident1 "QMultimedia" "EncodingQuality") [includeStd "QMultimedia"]
  [ (0, ["very", "low", "quality"])
  , (1, ["low", "quality"])
  , (2, ["normal", "quality"])
  , (3, ["high", "quality"])
  , (4, ["very", "high", "quality"])
  ]

e_SupportEstimate =
  makeQtEnum (ident1 "QMultimedia" "SupportEstimate") [includeStd "QMultimedia"]
  [ (0, ["not", "supported"])
  , (1, ["maybe", "supported"])
  , (2, ["probably", "supported"])
  , (3, ["preferred", "supported"])
  ]
