-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Multimedia.QMediaContent (
  aModule,
  c_QMediaContent
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, objT, ptrT)
import Graphics.UI.Qtah.Generator.Interface.Core.QUrl (c_QUrl)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Multimedia", "QMediaContent"] minVersion $
  [ QtExport $ ExportClass c_QMediaContent
  ]

c_QMediaContent =
  addReqIncludes [includeStd "QMediaContent"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QMediaContent") Nothing []
  [ mkCtor "new" []
  , mkCtor "newFromUrl" [objT c_QUrl]
    -- TODO other constructors
    --canonicalRequest
    --canonicalResource
  , mkConstMethod "canonicalUrl" [] $ objT c_QUrl
  , mkConstMethod "isNull" [] boolT
    --playlist
    --resources
  ]
