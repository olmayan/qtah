-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Multimedia.QSound (
  aModule,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkProp,
  mkStaticMethod'
  )
import Foreign.Hoppy.Generator.Types (boolT, intT, objT, ptrT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Multimedia", "QSound"] minVersion $
  [ QtExport $ ExportClass c_QSound
  ]

c_QSound =
  addReqIncludes [includeStd "QSound"] $
  classSetEntityPrefix "" $
  makeClass (ident "QSound") Nothing [c_QObject]
  [ mkCtor "new" [objT c_QString]
  , mkCtor "newWithParent" [objT c_QString, ptrT $ objT c_QObject]
  , mkConstMethod "fileName" [] $ objT c_QString
  , mkConstMethod "isFinished" [] boolT
  , mkConstMethod "loopsRemaining" [] intT
  , mkMethod "play" [] voidT
  , mkMethod "stop" [] voidT
  , mkStaticMethod' "play" "playFile" [objT c_QString] voidT
  , mkProp "loops" intT
  ]
