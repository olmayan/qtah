-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Multimedia.Widgets.QGraphicsVideoItem (
  aModule,
  c_QGraphicsVideoItem
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkProp,
  )

import Foreign.Hoppy.Generator.Types (enumT, objT, ptrT)
import Graphics.UI.Qtah.Generator.Interface.Core.QPointF (c_QPointF)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.QSizeF (c_QSizeF)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (e_AspectRatioMode)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (c_ListenerQSizeF)
import Graphics.UI.Qtah.Generator.Interface.Multimedia.QMediaObject (c_QMediaObject)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsObject (c_QGraphicsObject)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsItem (c_QGraphicsItem)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Multimedia", "Widgets", "QGraphicsVideoItem"] minVersion $
  QtExport (ExportClass c_QGraphicsVideoItem) :
  map QtExportSignal signals

c_QGraphicsVideoItem =
  addReqIncludes [includeStd "QGraphicsVideoItem"] $
  classSetEntityPrefix "" $
  makeClass (ident "QGraphicsVideoItem") Nothing [c_QGraphicsObject]
  [ mkCtor "new" []
  , mkCtor "newWithParent" [ptrT $ objT c_QGraphicsItem]
  , mkConstMethod "boundingRect" [] $ objT c_QRectF
  , mkConstMethod "mediaObject" [] $ ptrT $ objT c_QMediaObject
  , mkConstMethod "nativeSize" [] $ objT c_QSizeF
  --paint
  , mkProp "aspectRatioMode" $ enumT e_AspectRatioMode
  , mkProp "offset" $ objT c_QPointF
  , mkProp "size" $ objT c_QSizeF
  ]

signals =
  [ makeSignal c_QGraphicsVideoItem "nativeSizeChanged" c_ListenerQSizeF
  ]
