module Graphics.UI.Qtah.Generator.Interface.Multimedia.Widgets (modules) where

import qualified Graphics.UI.Qtah.Generator.Interface.Multimedia.Widgets.QCameraViewfinder as QCameraViewfinder
import qualified Graphics.UI.Qtah.Generator.Interface.Multimedia.Widgets.QGraphicsVideoItem as QGraphicsVideoItem
import qualified Graphics.UI.Qtah.Generator.Interface.Multimedia.Widgets.QVideoWidget as QVideoWidget
import Graphics.UI.Qtah.Generator.Module (AModule)

{-# ANN module "HLint: ignore Use camelCase" #-}

modules :: [AModule]
modules =
  [ QCameraViewfinder.aModule
  , QGraphicsVideoItem.aModule
  , QVideoWidget.aModule
  ]
