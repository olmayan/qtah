-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Multimedia.QMediaObject (
  aModule,
  c_QMediaObject
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkMethod,
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, ptrT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.QVariant (c_QVariant)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  c_Listener,
  c_ListenerBool,
  c_ListenerInt
  )
import Graphics.UI.Qtah.Generator.Interface.Multimedia.Types (e_AvailabilityStatus)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Multimedia", "QMediaObject"] minVersion $
  QtExport (ExportClass c_QMediaObject) :
  map QtExportSignal signals

c_QMediaObject =
  addReqIncludes [includeStd "QMediaObject"] $
  classSetEntityPrefix "" $
  makeClass (ident "QMediaObject") Nothing [c_QObject]
  [ mkConstMethod "availability" [] $ enumT e_AvailabilityStatus
  , mkConstMethod "availableMetaData" [] $ objT c_QStringList
  , mkMethod "bind" [ptrT $ objT c_QObject] boolT
  , mkMethod "isAvailable" [] boolT
  , mkMethod "isMetaDataAvailable" [] boolT
  , mkMethod "metaData" [objT c_QString] $ objT c_QVariant
  --service
  , mkMethod "unbind" [ptrT $ objT c_QObject] voidT
  , mkProp "notifyInterval" intT
  ]

signals =
  [ makeSignal c_QMediaObject "availabilityChanged" c_ListenerBool
  --availabilityChanged AvailabilityStatus
  , makeSignal c_QMediaObject "metaDataAvailableChanged" c_ListenerBool
  , makeSignal c_QMediaObject "metaDataChanged" c_Listener
  --metaDataChanged QString QVariant
  , makeSignal c_QMediaObject "notifyIntervalChanged" c_ListenerInt
  ]
