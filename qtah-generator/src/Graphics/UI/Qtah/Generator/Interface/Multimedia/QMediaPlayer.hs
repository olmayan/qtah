-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Multimedia.QMediaPlayer (
  aModule,
  e_Error,
  e_MediaStatus,
  e_State
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  mkStaticMethod,
  mkStaticMethod'
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, ptrT, voidT)
import Graphics.UI.Qtah.Generator.Flag (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QIODevice (c_QIODevice)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qint64, qreal)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  c_ListenerBool,
  c_ListenerInt,
  c_ListenerQInt64,
  c_ListenerQReal,
  c_ListenerQMediaPlayerError,
  c_ListenerQMediaPlayerMediaStatus,
  c_ListenerQMediaPlayerState
  )
import Graphics.UI.Qtah.Generator.Interface.Multimedia.QMediaContent (c_QMediaContent)
import Graphics.UI.Qtah.Generator.Interface.Multimedia.QMediaObject (c_QMediaObject)
import Graphics.UI.Qtah.Generator.Interface.Multimedia.Types (
  e_AvailabilityStatus,
  e_Role,
  e_SupportEstimate
  )
import Graphics.UI.Qtah.Generator.Interface.Multimedia.Widgets.QGraphicsVideoItem (c_QGraphicsVideoItem)
import Graphics.UI.Qtah.Generator.Interface.Multimedia.Widgets.QVideoWidget (c_QVideoWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Multimedia", "QMediaPlayer"] minVersion $
  QtExport (ExportClass c_QMediaPlayer) :
  map QtExport
  [ ExportEnum e_Error
  , ExportEnum e_Flag
  , ExportBitspace bs_Flags
  , ExportEnum e_MediaStatus
  , ExportEnum e_State
  ] ++
  map QtExportSignal signals

c_QMediaPlayer =
  addReqIncludes [includeStd "QMediaPlayer"] $
  classSetEntityPrefix "" $
  makeClass (ident "QMediaPlayer") Nothing [c_QMediaObject] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , just $ mkCtor "newWithParentAndFlags" [ptrT $ objT c_QObject, bitspaceT bs_Flags]
  , just $ mkConstMethod "availability" [] $ enumT e_AvailabilityStatus
  , just $ mkConstMethod "bufferStatus" [] intT
  , just $ mkConstMethod "currentMedia" [] $ objT c_QMediaContent
  --, just $ mkConstMethod "currentNetworkConfiguration" [] $ objT c_QNetworkConfiguration
  , just $ mkConstMethod "duration" [] qint64
  , just $ mkConstMethod "error" [] $ enumT e_Error
  , just $ mkConstMethod "errorString" [] $ objT c_QString
  , just $ mkStaticMethod "hasSupport" [objT c_QString] $ enumT e_SupportEstimate
  , just $ mkStaticMethod' "hasSupport" "hasSupportWithCodecs" [objT c_QString, objT c_QStringList] $ enumT e_SupportEstimate
  , just $ mkStaticMethod' "hasSupport" "hasSupportWithCodecsAndFlags" [objT c_QString, objT c_QStringList, bitspaceT bs_Flags] $ enumT e_SupportEstimate
  , just $ mkConstMethod "isAudioAvailable" [] boolT
  , just $ mkConstMethod "isSeekable" [] boolT
  , just $ mkConstMethod "isVideoAvailable" [] boolT
  , just $ mkConstMethod "mediaStatus" [] $ enumT e_MediaStatus
  --mediaStream
  , just $ mkMethod "pause" [] voidT
  , just $ mkMethod "play" [] voidT
  , just $ mkConstMethod "state" [] $ enumT e_State
  , just $ mkMethod' "setMedia" "setMediaWithIODevice" [objT c_QMediaContent, ptrT $ objT c_QIODevice] voidT
  --setNetworkConfigurations
  , just $ mkMethod "setVideoOutput" [ptrT $ objT c_QVideoWidget] voidT
  , just $ mkMethod' "setVideoOutput" "setVideoOutputGraphicsItem" [ptrT $ objT c_QGraphicsVideoItem] voidT
  --setVideoOutput surface
  , just $ mkMethod "stop" [] voidT
  --(>=5,6)supportedAudioRoles
  , test (qtVersion >= [5, 6]) $ mkProp "audioRole" $ enumT e_Role
  , just $ mkProp "media" $ objT c_QMediaContent
  , just $ mkBoolIsProp "muted"
  , just $ mkProp "playbackRate" qreal
  --, just mkProp "playlist" $ ptrT $ objT c_QMediaPlaylist
  , just $ mkProp "position" qint64
  , just $ mkProp "volume" intT
  ]

signals =
  collect
  [ just $ makeSignal c_QMediaPlayer "audioAvailableChanged" c_ListenerBool
  --, test (qtVersion >= [5, 6]) $ makeSignal c_QMediaPlayer "audioRoleChanged" c_ListenerQAudioRole
  , just $ makeSignal c_QMediaPlayer "bufferStatusChanged" c_ListenerInt
  --, just $ makeSignal c_QMediaPlayer "currentMediaChanged" c_ListenerRefQMediaContent
  , just $ makeSignal c_QMediaPlayer "durationChanged" c_ListenerQInt64
  , just $ makeSignal c_QMediaPlayer "error" c_ListenerQMediaPlayerError
  --, just $ makeSignal c_QMediaPlayer "mediaChanged" c_ListenerRefQMediaContext
  , just $ makeSignal c_QMediaPlayer "mediaStatusChanged" c_ListenerQMediaPlayerMediaStatus
  , just $ makeSignal c_QMediaPlayer "mutedChanged" c_ListenerBool
  --, just $ makeSignal c_QMediaPlayer "networkConfigurationChanged" c_ListenerRefQNetworkConfiguration
  , just $ makeSignal c_QMediaPlayer "playbackRateChanged" c_ListenerQReal
  , just $ makeSignal c_QMediaPlayer "positionChanged" c_ListenerQInt64
  , just $ makeSignal c_QMediaPlayer "seekableChanged" c_ListenerBool
  , just $ makeSignal c_QMediaPlayer "stateChanged" c_ListenerQMediaPlayerState
  , just $ makeSignal c_QMediaPlayer "videoAvailableChanged" c_ListenerBool
  , just $ makeSignal c_QMediaPlayer "volumeChanged" c_ListenerInt
  ]

e_Error =
  makeQtEnum (ident1 "QMediaPlayer" "Error") [includeStd "QMediaPlayer"]
  [ (0, ["no", "error"])
  , (1, ["resource", "error"])
  , (2, ["format", "error"])
  , (3, ["network", "error"])
  , (4, ["access", "denied", "error"])
  , (5, ["service", "missing", "error"])
  ]

(e_Flag, bs_Flags) =
  makeQtEnumBitspace (ident1 "QMediaPlayer" "Flag") "Flags" [includeStd "QMediaPlayer"]
  [ (0x01, ["low", "latency"])
  , (0x02, ["stream", "playback"])
  , (0x04, ["video", "surface"])
  ]

e_MediaStatus =
  makeQtEnum (ident1 "QMediaPlayer" "MediaStatus") [includeStd "QMediaPlayer"]
  [ (0, ["unknown", "media", "status"])
  , (1, ["no", "media"])
  , (2, ["loading", "media"])
  , (3, ["loaded", "media"])
  , (4, ["stalled", "media"])
  , (5, ["buffering", "media"])
  , (6, ["buffered", "media"])
  , (7, ["end", "of", "media"])
  , (8, ["invalid", "media"])
  ]

e_State =
  makeQtEnum (ident1 "QMediaPlayer" "State") [includeStd "QMediaPlayer"]
  [ (0, ["stopped", "state"])
  , (1, ["playing", "state"])
  , (2, ["paused", "state"])
  ]
