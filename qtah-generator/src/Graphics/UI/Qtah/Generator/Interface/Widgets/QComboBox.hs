-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QComboBox (
  aModule,
  c_QComboBox,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkBoolHasProp,
  mkBoolIsProp,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, constT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QAbstractItemModel (c_QAbstractItemModel)
import Graphics.UI.Qtah.Generator.Interface.Core.QModelIndex (c_QModelIndex)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.QVariant (c_QVariant)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_MatchFlags,
  e_InputMethodQuery,
  )
import Graphics.UI.Qtah.Generator.Interface.Gui.QIcon (c_QIcon)
import Graphics.UI.Qtah.Generator.Interface.Gui.QValidator (c_QValidator)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  c_ListenerInt,
  c_ListenerQString,
  )
import Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractItemView (c_QAbstractItemView)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QCompleter (c_QCompleter)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QLineEdit (c_QLineEdit)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

{-# ANN module "HLint: ignore Use camelCase" #-}

aModule =
  AQtModule $
  makeQtModule ["Widgets", "QComboBox"] $
  map QtExport
  [ ExportClass c_QComboBox
  , ExportEnum e_InsertPolicy
  , ExportEnum e_SizeAdjustPolicy
  ] ++
  map QtExportSignal signals

c_QComboBox =
  addReqIncludes [includeStd "QComboBox"] $
  classSetEntityPrefix "" $
  makeClass (ident "QComboBox") Nothing [c_QWidget] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QWidget]
  , just $ mkMethod "addItem" [objT c_QString] voidT
  , just $ mkMethod' "addItem" "addItemWithData" [objT c_QString, objT c_QVariant] voidT
  , just $ mkMethod' "addItem" "addItemWithIcon" [objT c_QIcon, objT c_QString] voidT
  , just $ mkMethod' "addItem" "addItemWithIconAndData" [objT c_QIcon, objT c_QString, objT c_QVariant] voidT
  , just $ mkMethod "addItems" [objT c_QStringList] voidT
  , just $ mkMethod "clear" [] voidT
  , just $ mkMethod "clearEditText" [] voidT
  , just $ mkConstMethod "count" [] intT
  , test (qtVersion >= [5, 2]) $ mkConstMethod "currentData" [] $ objT c_QVariant
  , test (qtVersion >= [5, 2]) $ mkConstMethod' "currentData" "currentDataWithRole" [intT] $ objT c_QVariant
  , just $ mkConstMethod "findData" [objT c_QVariant] intT
  , just $ mkConstMethod' "findData" "findDataWithRole" [objT c_QVariant, intT] intT
  , just $ mkConstMethod' "findData" "findDataWithRoleAndFlags" [objT c_QVariant, intT, bitspaceT bs_MatchFlags] intT
  , just $ mkConstMethod "findText" [objT c_QString] intT
  , just $ mkConstMethod' "findText" "findTextWithFlags" [objT c_QString, bitspaceT bs_MatchFlags] intT
  , just $ mkMethod "hidePopup" [] voidT
  , just $ mkMethod "inputMethodQuery" [enumT e_InputMethodQuery] $ objT c_QVariant
  , just $ mkMethod "insertItem" [intT, objT c_QString] voidT
  , just $ mkMethod' "insertItem" "insertItemWithData" [intT, objT c_QString, objT c_QVariant] voidT
  , just $ mkMethod' "insertItem" "insertItemWithIcon" [intT, objT c_QIcon, objT c_QString] voidT
  , just $ mkMethod' "insertItem" "insertItemWithIconAndData" [intT, objT c_QIcon, objT c_QString, objT c_QVariant] voidT
  , just $ mkMethod "insertItems" [intT, objT c_QStringList] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod "insertSeparator" [intT] voidT
  , just $ mkConstMethod "itemData" [intT] $ objT c_QVariant
  , just $ mkConstMethod' "itemData" "itemDataWithRole" [intT, intT] $ objT c_QVariant
  , just $ mkConstMethod "itemIcon" [intT] $ objT c_QIcon
  , just $ mkConstMethod "itemText" [intT] $ objT c_QString
  , just $ mkMethod "removeItem" [intT] voidT
  , just $ mkMethod "setEditText" [objT c_QString] voidT
  , just $ mkMethod "setItemData" [intT, objT c_QVariant] voidT
  , just $ mkMethod' "setItemData" "setItemDataWithRole" [intT, objT c_QVariant, intT] voidT
  , just $ mkMethod "setItemIcon" [intT, objT c_QIcon] voidT
  , just $ mkMethod "setItemText" [intT, objT c_QString] voidT
  , just $ mkMethod "showPopup" [] voidT
  , test (qtVersion >= [4, 2]) $ mkProp "completer" $ ptrT $ objT c_QCompleter
  , just $ mkProp "currentIndex" intT
  , just $ mkProp "currentText" $ objT c_QString
  , just $ mkProp "duplicatesEnabled" boolT
  , just $ mkBoolIsProp "editable"
  , just $ mkBoolHasProp "frame"
  , just $ mkProp "iconSize" $ objT c_QSize
  , just $ mkProp "insertPolicy" $ enumT e_InsertPolicy
  --, just $ mkProp "itemDelegate" $ ptrT $ objT c_QAbstractItemDelegate
  , just $ mkProp "lineEdit" $ ptrT $ objT c_QLineEdit
  , just $ mkProp "maxCount" intT
  , just $ mkProp "maxVisibleItems" intT
  , just $ mkProp "minimumContentsLength" intT
  , just $ mkProp "model" $ ptrT $ objT c_QAbstractItemModel
  , just $ mkProp "modelColumn" intT
  , just $ mkProp "rootModelIndex" $ objT c_QModelIndex
  , just $ mkProp "sizeAdjustPolicy" $ enumT e_SizeAdjustPolicy
  , just $ mkProp "validator" $ ptrT $ constT $ objT c_QValidator
  , just $ mkProp "view" $ ptrT $ objT c_QAbstractItemView
  ]

signals =
  collect
  [ just $ makeSignal c_QComboBox "activated" c_ListenerInt
  , just $ makeSignal' c_QComboBox "activated" "activatedText" c_ListenerQString
  , test (qtVersion >= [4, 1]) $ makeSignal c_QComboBox "currentIndexChanged" c_ListenerInt
  , test (qtVersion >= [4, 1]) $ makeSignal' c_QComboBox "currentIndexChanged" "currentIndexChangedText" c_ListenerQString
  , test (qtVersion >= [5, 0]) $ makeSignal c_QComboBox "currentTextChanged" c_ListenerQString  
  , just $ makeSignal c_QComboBox "editTextChanged" c_ListenerQString  
  , just $ makeSignal c_QComboBox "highlighted" c_ListenerInt
  , just $ makeSignal' c_QComboBox "highlighted" "highlightedText" c_ListenerQString
  ]

e_InsertPolicy =
  makeQtEnum (ident1 "QComboBox" "InsertPolicy") [includeStd "QComboBox"]
  [ (0, ["no", "insert"])
  , (1, ["insert", "at", "top"])
  , (2, ["insert", "at", "current"])
  , (3, ["insert", "at", "bottom"])
  , (4, ["insert", "after", "current"])
  , (5, ["insert", "before", "current"])
  , (6, ["insert", "alphabetically"])
  ]

e_SizeAdjustPolicy =
  makeQtEnum (ident1 "QComboBox" "SizeAdjustPolicy") [includeStd "QComboBox"]
  [ (0, ["adjust", "to", "contents"])
  , (1, ["adjust", "to", "contents", "on", "first", "show"])
  , (2, ["adjust", "to", "minimum", "contents", "length"])
  , (3, ["adjust", "to", "minimum", "contents", "length", "with", "icon"])
  ]
