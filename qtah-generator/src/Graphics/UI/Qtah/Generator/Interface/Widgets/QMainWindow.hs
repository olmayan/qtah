-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QMainWindow (
  aModule,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkCtor,
  mkMethod,
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_WindowFlags,
  e_DockWidgetArea,
  e_ToolButtonStyle
  )
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (c_ListenerQSize)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QMenu (c_QMenu)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QMenuBar (c_QMenuBar)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QStatusBar (c_QStatusBar)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QTabWidget (
  e_TabPosition,
  e_TabShape
  )
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

{-# ANN module "HLint: ignore Use camelCase" #-}

aModule =
  AQtModule $
  makeQtModule ["Widgets", "QMainWindow"] $
  QtExport (ExportClass c_QMainWindow) :
  map QtExportSignal signals

c_QMainWindow =
  addReqIncludes [includeStd "QMainWindow"] $
  classSetEntityPrefix "" $
  makeClass (ident "QMainWindow") Nothing [c_QWidget] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QWidget]
  , just $ mkCtor "newWithParentAndFlags" [ptrT $ objT c_QWidget, bitspaceT bs_WindowFlags]
    -- TODO addDockWidget
    -- TODO addToolBar
    -- TODO addToolBarBreak
  , just $ mkBoolIsProp "animated"
  , just $ mkProp "centralWidget" $ ptrT $ objT c_QWidget
    -- TODO corner
  , just $ mkMethod "createPopupMenu" [] $ ptrT $ objT c_QMenu
  , test (qtVersion >= [4, 2]) $ mkBoolIsProp "dockNestingEnabled"
    -- TODO dockOptions
    -- TODO dockWidgetArea
  , test (qtVersion >= [4, 5]) $ mkProp "documentMode" boolT
  , just $ mkProp "iconSize" $ objT c_QSize
    -- TODO insertToolBar
    -- TODO insertToolBarBreak
  , just $ mkProp "menuBar" $ ptrT $ objT c_QMenuBar
  , test (qtVersion >= [4, 2]) $ mkProp "menuWidget" $ ptrT $ objT c_QWidget
    -- TODO removeDockWidget
    -- TODO restoreState
    -- TODO saveState
    -- TODO setCorner
  , test (qtVersion >= [4, 5]) $ mkMethod "setTabPosition" [enumT e_DockWidgetArea, enumT e_TabPosition] voidT
    -- TODO splitDockWidget
  , just $ mkProp "statusBar" $ ptrT $ objT c_QStatusBar
    -- TODO tabifiedDockWidgets
    -- TODO tabifyDockWidget
  , test (qtVersion >= [4, 5]) $ mkMethod "tabPosition" [enumT e_DockWidgetArea] $ enumT e_TabPosition
  , test (qtVersion >= [4, 5]) $ mkProp "tabShape" $ enumT e_TabShape
    -- TODO toolBarArea
    -- TODO toolBarBreak
  , just $ mkProp "toolButtonStyle" $ enumT e_ToolButtonStyle
  , test (qtVersion >= [5, 2]) $ mkProp "unifiedTitleAndToolBarOnMac" boolT
  ]

signals =
  [ makeSignal c_QMainWindow "iconSizeChanged" c_ListenerQSize
    -- TODO toolButtonStyleChanged
  ]
