-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QFileIconProvider (
  aModule,
  c_QFileIconProvider
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, enumT, objT)
import Graphics.UI.Qtah.Generator.Flag (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QFileInfo (c_QFileInfo)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Gui.QIcon (c_QIcon)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Widgets", "QFileIconProvider"] $
  (map QtExport . collect)
  [ just $ ExportClass c_QFileIconProvider
  , just $ ExportEnum e_IconType
  , test (qtVersion >= [5, 2]) $ ExportBitspace bs_Options
  , test (qtVersion >= [5, 2]) $ ExportEnum e_Option
  ]

c_QFileIconProvider =
  addReqIncludes [includeStd "QFileIconProvider"] $
  classSetEntityPrefix "" $
  makeClass (ident "QFileIconProvider") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkConstMethod "icon" [enumT e_IconType] $ objT c_QIcon
  , just $ mkConstMethod' "icon" "iconWithFileInfo" [objT c_QFileInfo] $ objT c_QIcon
  , just $ mkConstMethod' "type" "type_" [objT c_QFileInfo] $ objT c_QString
  , test (qtVersion >= [5, 2]) $ mkProp "options" $ bitspaceT bs_Options
  ]

e_IconType =
  makeQtEnum (ident1 "QFileIconProvider" "IconType") [includeStd "QFileIconProvider"]
  [ (0, ["computer"])
  , (1, ["desktop"])
  , (2, ["trashcan"])
  , (3, ["network"])
  , (4, ["drive"])
  , (5, ["folder"])
  , (6, ["file"])
  ]

(e_Option, bs_Options) =
  makeQtEnumBitspace (ident1 "QFileIconProvider" "Option") "Options" [includeStd "QFileIconProvider"]
  [ (0x00000001, ["dont", "use", "custom", "directory", "icons"])
  ]
