-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QHeaderView (
  aModule,
  c_QHeaderView,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )

import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QPoint (c_QPoint)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_Alignment,
  e_Orientation,
  e_SortOrder,
  )
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  c_Listener,
  c_ListenerInt,
  c_ListenerIntInt,
  c_ListenerIntIntInt,
  c_ListenerIntQtSortOrder,
  )
import Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractItemView (c_QAbstractItemView)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Widgets", "QHeaderView"] $
  map QtExport
  [ ExportClass c_QHeaderView
  , ExportEnum e_ResizeMode
  ] ++
  map QtExportSignal signals

c_QHeaderView =
  addReqIncludes [includeStd "QHeaderView"] $
  classSetEntityPrefix "" $
  makeClass (ident "QHeaderView") Nothing [c_QAbstractItemView] $
  collect
  [ just $ mkCtor "new" [enumT e_Orientation]
  , just $ mkCtor "newWithParent" [enumT e_Orientation, ptrT $ objT c_QWidget]
  , just $ mkConstMethod "count" [] intT
  , test (qtVersion >= [4, 1]) $ mkConstMethod "hiddenSectionCount" [] intT
  , just $ mkMethod "hideSection" [intT] voidT 
  , just $ mkConstMethod "isSectionHidden" [intT] intT
  , just $ mkConstMethod "length" [] intT
  , just $ mkConstMethod "logicalIndex" [intT] intT
  , just $ mkConstMethod "logicalIndexAt" [intT] intT
  , just $ mkConstMethod' "logicalIndexAt" "logicalIndexAtPos" [intT, intT] intT
  , just $ mkConstMethod' "logicalIndexAt" "logicalIndexAtPoint" [objT c_QPoint] intT
  , just $ mkMethod "moveSection" [intT, intT] voidT 
  , just $ mkConstMethod "orientation" [] $ enumT e_Orientation
  , just $ mkMethod "resetDefaultSectionSize" [] voidT 
  , just $ mkMethod "resizeSection" [intT, intT] voidT 
  , just $ mkMethod "resizeSections" [enumT e_ResizeMode] voidT 
  , test (qtVersion >= [4, 3]) $ mkMethod "restoreState" [objT c_QByteArray] voidT 
  , test (qtVersion >= [4, 3]) $ mkConstMethod "saveState" [] $ objT c_QByteArray
  , just $ mkConstMethod "sectionPosition" [intT] intT
  , test (qtVersion >= [5, 0]) $ mkConstMethod "sectionResizeMode" [intT] $ enumT e_ResizeMode
  , just $ mkConstMethod "sectionSize" [intT] intT
  , just $ mkConstMethod "sectionSizeHint" [intT] intT
  , just $ mkConstMethod "sectionViewportPosition" [intT] intT
  , test (qtVersion >= [4, 1]) $ mkConstMethod "sectionsHidden" [] boolT
  , just $ mkConstMethod "sectionsMoved" [] boolT
  , test (qtVersion >= [4, 2]) $ mkMethod "setOffsetToLastSection" [] voidT 
  , test (qtVersion >= [4, 2]) $ mkMethod "setOffsetToSectionPosition" [intT] voidT 
  , just $ mkMethod "setSectionHidden" [intT, boolT] voidT 
  , test (qtVersion >= [5, 0]) $ mkMethod' "setSectionResizeMode" "setSectionResizeModeAll" [enumT e_ResizeMode] voidT 
  , test (qtVersion >= [5, 0]) $ mkMethod "setSectionResizeMode" [intT, enumT e_ResizeMode] voidT 
  , just $ mkMethod "setSortIndicator" [intT, enumT e_SortOrder] voidT 
  , just $ mkMethod "showSection" [intT] voidT 
  , just $ mkConstMethod "sortIndicatorOrder" [] $ enumT e_SortOrder
  , test (qtVersion >= [4, 1]) $ mkConstMethod "stretchSectionCount" [] intT
  , test (qtVersion >= [4, 2]) $ mkMethod "swapSections" [intT, intT] voidT 
  , just $ mkConstMethod "visualIndex" [intT] intT
  , just $ mkConstMethod "visualIndexAt" [intT] intT
  , test (qtVersion >= [4, 2]) $ mkProp "cascadingSectionResizes" boolT
  , test (qtVersion >= [4, 1]) $ mkProp "defaultAlignment" $ bitspaceT bs_Alignment
  , just $ mkProp "defaultSectionSize" intT
  , just $ mkProp "highlightSections" boolT
  , test (qtVersion >= [5, 2]) $ mkProp "maximumSectionSize" intT
  , test (qtVersion >= [4, 2]) $ mkProp "minimumSectionSize" intT
  , just $ mkProp "offset" intT
  , test (qtVersion >= [5, 2]) $ mkProp "resizeContentsPrecision" intT
  , test (qtVersion >= [5, 0]) $ mkProp "sectionsClickable" boolT
  , test (qtVersion >= [5, 0]) $ mkProp "sectionsMovable" boolT
  , just $ mkBoolIsProp "sortIndicatorShown"
  , just $ mkProp "stretchLastSection" boolT
  ]

signals =
  collect
  [ test (qtVersion >= [4, 2]) $ makeSignal c_QHeaderView "geometriesChanged" c_Listener
  , just $ makeSignal c_QHeaderView "sectionClicked" c_ListenerInt
  , just $ makeSignal c_QHeaderView "sectionCountChanged" c_ListenerIntInt
  , just $ makeSignal c_QHeaderView "sectionDoubleClicked" c_ListenerInt
  , test (qtVersion >= [4, 3]) $ makeSignal c_QHeaderView "sectionEntered" c_ListenerInt
  , just $ makeSignal c_QHeaderView "sectionHandleDoubleClicked" c_ListenerInt
  , just $ makeSignal c_QHeaderView "sectionMoved" c_ListenerIntIntInt
  , just $ makeSignal c_QHeaderView "sectionPressed" c_ListenerInt
  , just $ makeSignal c_QHeaderView "sectionResized" c_ListenerIntIntInt
  , test (qtVersion >= [4, 3]) $ makeSignal c_QHeaderView "sortIndicatorChanged" c_ListenerIntQtSortOrder
  ]

e_ResizeMode =
  makeQtEnum (ident1 "QHeaderView" "ResizeMode") [includeStd "QHeaderView"]
  [ (0, ["interactive"])
  , (2, ["fixed"])
  , (1, ["stretch"])
  , (3, ["resize", "to", "contents"])
  ]
