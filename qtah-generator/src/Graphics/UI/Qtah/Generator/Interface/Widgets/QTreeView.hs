-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QTreeView (
  aModule,
  c_QTreeView,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkProp,
  )

import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QModelIndex (c_QModelIndex)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  e_SortOrder,
  )
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  --c_ListenerQModelIndex,
  )
import Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractItemView (c_QAbstractItemView)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QHeaderView (c_QHeaderView)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Widgets", "QTreeView"] $
  [ QtExport $ ExportClass c_QTreeView
  ] ++
  map QtExportSignal signals

c_QTreeView =
  addReqIncludes [includeStd "QTreeView"] $
  classSetEntityPrefix "" $
  makeClass (ident "QTreeView") Nothing [c_QAbstractItemView] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QWidget]
  , just $ mkMethod "collapse" [objT c_QModelIndex] voidT
  , just $ mkMethod "collapseAll" [] voidT
  , just $ mkConstMethod "columnAt" [intT] intT
  , just $ mkConstMethod "columnViewportPosition" [intT] intT
  , just $ mkConstMethod "columnWidth" [intT] intT
  , just $ mkMethod "expand" [objT c_QModelIndex] voidT
  , just $ mkMethod "expandAll" [] voidT
  , just $ mkMethod "expandToDepth" [intT] voidT
  , just $ mkMethod "hideColumn" [intT] voidT
  , just $ mkConstMethod "indexAbove" [objT c_QModelIndex] $ objT c_QModelIndex
  , just $ mkConstMethod "indexBelow" [objT c_QModelIndex] $ objT c_QModelIndex
  , just $ mkConstMethod "isColumnHidden" [intT] boolT
  , just $ mkConstMethod "isExpanded" [objT c_QModelIndex] boolT
  , just $ mkConstMethod "isFirstColumnSpanned" [intT, objT c_QModelIndex] boolT
  , just $ mkConstMethod "isRowHidden" [intT, objT c_QModelIndex] boolT
  , just $ mkMethod "resetIndentation" [] voidT
  , just $ mkMethod "resizeColumnToContents" [intT] voidT
  , just $ mkMethod "setColumnHidden" [intT, boolT] voidT
  , just $ mkMethod "setColumnWidth" [intT, intT] voidT
  , just $ mkMethod "setExpanded" [objT c_QModelIndex, boolT] voidT
  , just $ mkMethod "setFirstColumnSpanned" [intT, objT c_QModelIndex, boolT] voidT
  , just $ mkMethod "setRowHidden" [intT, objT c_QModelIndex, boolT] voidT
  , just $ mkMethod "showColumn" [intT] voidT
  , just $ mkMethod "sortByColumn" [intT, enumT e_SortOrder] voidT
  , test (qtVersion >= [4, 2]) $ mkProp "allColumnsShowFocus" boolT
  , test (qtVersion >= [4, 2]) $ mkBoolIsProp "animated"
  , test (qtVersion >= [4, 3]) $ mkProp "autoExpandDelay" intT
  , test (qtVersion >= [4, 4]) $ mkProp "expandsOnDoubleClick" boolT
  , just $ mkProp "header" $ ptrT $ objT c_QHeaderView
  , test (qtVersion >= [4, 4]) $ mkBoolIsProp "headerHidden"
  , just $ mkProp "indentation" intT
  , just $ mkProp "itemsExpandable" boolT
  , just $ mkProp "rootIsDecorated" boolT
  , test (qtVersion >= [4, 2]) $ mkBoolIsProp "sortingEnabled"
  , test (qtVersion >= [5, 2]) $ mkProp "treePosition" intT
  , just $ mkProp "uniformRowHeights" boolT
  , test (qtVersion >= [4, 3]) $ mkProp "wordWrap" boolT
  ]

signals =
  [ --makeSignal c_QTreeView "collapsed" c_ListenerQModelIndex
  --, makeSignal c_QTreeView "expanded" c_ListenerQModelIndex
  ]
