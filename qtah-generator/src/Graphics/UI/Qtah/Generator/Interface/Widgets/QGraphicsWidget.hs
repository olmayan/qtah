-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsWidget (
  aModule,
  c_QGraphicsWidget,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  mkStaticMethod
  )

import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.QSizeF (c_QSizeF)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  e_WindowType,
  bs_WindowFlags,
  qreal
  )
import Graphics.UI.Qtah.Generator.Interface.Gui.QFont (c_QFont)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPalette (c_QPalette)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (c_Listener)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QAction (c_QAction)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsItem (c_QGraphicsItem)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsLayoutItem (c_QGraphicsLayoutItem)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsObject (c_QGraphicsObject)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QStyle (c_QStyle)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Widgets", "QGraphicsWidget"] $
  collect
  [test (qtVersion >= [4, 4]) $ QtExport $ ExportClass c_QGraphicsWidget]
  ++
  map QtExportSignal signals

c_QGraphicsWidget =
  addReqIncludes [includeStd "QGraphicsWidget"] $
  classSetEntityPrefix "" $
  makeClass (ident "QGraphicsWidget") Nothing ancestors $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QGraphicsItem]
  , just $ mkCtor "newWithParentAndWindowFlags" [ptrT $ objT c_QGraphicsItem, bitspaceT bs_WindowFlags]
    --(>= 4, 5)actions
  , test (qtVersion >= [4, 5]) $ mkMethod "addAction" [ptrT $ objT c_QAction] voidT
    --(>= 4, 5)addActions
  , just $ mkMethod "adjustSize" [] voidT
  , just $ mkConstMethod "boundingRect" [] $ objT c_QRectF
  , just $ mkMethod "close" [] boolT
  , just $ mkConstMethod "focusWidget" [] $ ptrT $ objT c_QGraphicsWidget
  , just $ mkConstMethod "getContentsMargins" [ptrT qreal, ptrT qreal, ptrT qreal, ptrT qreal] voidT
  , just $ mkConstMethod "getWindowFrameMargins" [ptrT qreal, ptrT qreal, ptrT qreal, ptrT qreal] voidT
  --grabShortcut
  , test (qtVersion >= [4, 5]) $ mkMethod "insertAction" [ptrT $ objT c_QAction, ptrT $ objT c_QAction] voidT
  --insertActions
  , just $ mkConstMethod "isActiveWindow" [] boolT
  --paint
  --paintWindowFrame
  , just $ mkConstMethod "rect" [] $ objT c_QRectF
  , test (qtVersion >= [4, 5]) $ mkMethod "releaseShortcut" [intT] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod "removeAction" [ptrT $ objT c_QAction] voidT
  , just $ mkMethod "resize" [qreal, qreal] voidT
  --setAttribute
  , just $ mkMethod "setContentsMargins" [qreal, qreal, qreal, qreal] voidT
  , just $ mkMethod' "setGeometry" "setGeometryRaw" [qreal, qreal, qreal, qreal] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod' "setShortcutAutoRepeat" "setShortcutAutoRepeatEnabled" [intT] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod "setShortcutAutoRepeat" [intT, boolT] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod' "setShortcutEnabled" "setShortcutEnabledTrue" [intT] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod "setShortcutEnabled" [intT, boolT] voidT
  , just $ mkStaticMethod "setTabOrder" [ptrT $ objT c_QGraphicsWidget, ptrT $ objT c_QGraphicsWidget] voidT
  , just $ mkMethod "setWindowFrameMargins" [qreal, qreal, qreal, qreal] voidT
  --shape
  , just $ mkConstMethod "size" [] $ objT c_QSizeF
  --testAttribute
  , just $ mkConstMethod' "type" "type_" [] intT
  , just $ mkMethod "unsetLayoutDirection" [] voidT
  , just $ mkMethod "unsetWindowFrameMargins" [] voidT
  , just $ mkConstMethod "windowFrameGeometry" [] $ objT c_QRectF
  , just $ mkConstMethod "windowFrameRect" [] $ objT c_QRectF
  , just $ mkConstMethod "windowType" [] $ enumT e_WindowType
  , test (qtVersion >= [4, 7]) $ mkProp "autoFillBackground" boolT
  --focusPolicy
  , just $ mkProp "font" $ objT c_QFont
  , just $ mkProp "geometry" $ objT c_QRectF
  --layout
  --layoutDirection
  , just $ mkProp "maximumSize" $ objT c_QSizeF
  , just $ mkProp "minimumSize" $ objT c_QSizeF
  , just $ mkProp "palette" $ objT c_QPalette
  , just $ mkProp "preferredSize" $ objT c_QSizeF
  --sizePolicy
  , just $ mkProp "style" $ ptrT $ objT c_QStyle
  , just $ mkProp "windowFlags" $ bitspaceT bs_WindowFlags
  , just $ mkProp "windowTitle" $ objT c_QString
  ]

--QGraphicsObject appears only since 4.6
ancestors =
  collect
  [ just c_QGraphicsLayoutItem
  , test (qtVersion >= [4, 6]) c_QGraphicsObject
  , test (qtVersion < [4, 6]) c_QGraphicsItem
  ]

signals =
  (collect . map (test (qtVersion >= [4, 4])))
  [makeSignal c_QGraphicsWidget "geometryChanged" c_Listener]
