-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsScene (
  aModule,
  bs_SceneLayers,
  c_QGraphicsScene,
  e_ItemIndexMethod
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QEvent (c_QEvent)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QPointF (c_QPointF)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_WindowFlags,
  e_AspectRatioMode,
  e_FocusReason,
  qreal
  )
import Graphics.UI.Qtah.Generator.Interface.Gui.QBrush (c_QBrush)
import Graphics.UI.Qtah.Generator.Interface.Gui.QFont (c_QFont)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPalette (c_QPalette)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPainter (c_QPainter)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPen (c_QPen)
import Graphics.UI.Qtah.Generator.Interface.Gui.QTransform (c_QTransform)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  c_Listener,
  c_ListenerPtrQGraphicsItemPtrQGraphicsItemQtFocusReason,
  c_ListenerQRectF
  )
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsEllipseItem (c_QGraphicsEllipseItem)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsItem (c_QGraphicsItem)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsProxyWidget (c_QGraphicsProxyWidget)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsRectItem (c_QGraphicsRectItem)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsWidget (c_QGraphicsWidget)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QStyle (c_QStyle)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)

import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types
minVersion = [4, 2]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Widgets", "QGraphicsScene"] minVersion $
  QtExport (ExportClass c_QGraphicsScene) :
  map QtExportSignal signals ++
  (map QtExport . collect)
  [ just $ ExportEnum e_ItemIndexMethod
  , test (qtVersion >= [4, 3]) $ ExportEnum e_SceneLayer
  , test (qtVersion >= [4, 3]) $ ExportBitspace bs_SceneLayers
  ]

c_QGraphicsScene =
  addReqIncludes [includeStd "QGraphicsScene"] $
  classSetEntityPrefix "" $
  makeClass (ident "QGraphicsScene") Nothing [c_QObject] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , just $ mkCtor "newWithSceneRect" [objT c_QRectF]
  , just $ mkCtor "newWithSceneRectAndParent" [objT c_QRectF, ptrT $ objT c_QObject]
  , just $ mkCtor "newWithSceneRectRaw" [qreal, qreal, qreal, qreal]
  , just $ mkCtor "newWithSceneRectRawAndParent" [qreal, qreal, qreal, qreal, ptrT $ objT c_QObject]
  , just $ mkMethod "addEllipse" [objT c_QRectF] $ ptrT $ objT c_QGraphicsEllipseItem
  , just $ mkMethod' "addEllipse" "addEllipseWithPen" [objT c_QRectF, objT c_QPen] $ ptrT $ objT c_QGraphicsEllipseItem
  , just $ mkMethod' "addEllipse" "addEllipseWithPenAndBrush" [objT c_QRectF, objT c_QPen, objT c_QBrush] $ ptrT $ objT c_QGraphicsEllipseItem
  , test (qtVersion >= [4, 3]) $ mkMethod' "addEllipse" "addEllipseRaw" [qreal, qreal, qreal, qreal] $ ptrT $ objT c_QGraphicsEllipseItem
  , test (qtVersion >= [4, 3]) $ mkMethod' "addEllipse" "addEllipseRawWithPen" [qreal, qreal, qreal, qreal, objT c_QPen] $ ptrT $ objT c_QGraphicsEllipseItem
  , test (qtVersion >= [4, 3]) $ mkMethod' "addEllipse" "addEllipseRawWithPenAndBrush" [qreal, qreal, qreal, qreal, objT c_QPen, objT c_QBrush] $ ptrT $ objT c_QGraphicsEllipseItem
  , just $ mkMethod "addItem" [ptrT $ objT c_QGraphicsItem] voidT
    --addLine
    --addPath
  --, just $ mkMethod "addPixmap" [objT c_QPixmap] $ ptrT $ objT c_QGraphicsPixmapItem
    --addPolygon
  , just $ mkMethod "addRect" [objT c_QRectF] $ ptrT $ objT c_QGraphicsRectItem
  , just $ mkMethod' "addRect" "addRectWithPen" [objT c_QRectF, objT c_QPen] $ ptrT $ objT c_QGraphicsRectItem
  , just $ mkMethod' "addRect" "addRectWithPenAndBrush" [objT c_QRectF, objT c_QPen, objT c_QBrush] $ ptrT $ objT c_QGraphicsRectItem
  , test (qtVersion >= [4, 3]) $ mkMethod' "addRect" "addRectRaw" [qreal, qreal, qreal, qreal] $ ptrT $ objT c_QGraphicsRectItem
  , just $ mkMethod' "addRect" "addRectRawWithPen" [qreal, qreal, qreal, qreal, objT c_QPen] $ ptrT $ objT c_QGraphicsRectItem
  , just $ mkMethod' "addRect" "addRectRawWithPenAndBrush" [qreal, qreal, qreal, qreal, objT c_QPen, objT c_QBrush] $ ptrT $ objT c_QGraphicsRectItem
    --addSimpleText
    --addText
  , test (qtVersion >= [4, 4]) $ mkMethod "addWidget" [ptrT $ objT c_QWidget] $ ptrT $ objT c_QGraphicsProxyWidget
  , test (qtVersion >= [4, 4]) $ mkMethod' "addWidget" "addWidgetWithFlags" [ptrT $ objT c_QWidget, bitspaceT bs_WindowFlags] $ ptrT $ objT c_QGraphicsProxyWidget
  , just $ mkMethod "advance" [] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod "clear" [] voidT
  , just $ mkMethod "clearFocus" [] voidT
  , just $ mkMethod "clearSelection" [] voidT
    --collidingItems
    --createItemGroup
    --destroyItemGroup
  , just $ mkConstMethod "focusItem" [] $ ptrT $ objT c_QGraphicsItem
  , just $ mkConstMethod "hasFocus" [] boolT
  , just $ mkConstMethod "height" [] qreal
  , just $ mkMethod "invalidate" [] voidT
  , just $ mkMethod' "invalidate" "invalidateRect" [objT c_QRectF] voidT
  , just $ mkMethod' "invalidate" "invalidateRectWithLayers" [objT c_QRectF, bitspaceT bs_SceneLayers] voidT
  , test (qtVersion >= [4, 3]) $ mkMethod' "invalidate" "invalidateRaw" [qreal, qreal, qreal, qreal] voidT
  , test (qtVersion >= [4, 3]) $ mkMethod' "invalidate" "invalidateRawWithLayers" [qreal, qreal, qreal, qreal, bitspaceT bs_SceneLayers] voidT
  , test (qtVersion >= [4, 6]) $ mkConstMethod "isActive" [] boolT
  , test (qtVersion >= [4, 6]) $ mkConstMethod "itemAt" [objT c_QPointF, objT c_QTransform] $ ptrT $ objT c_QGraphicsItem
  , test (qtVersion >= [4, 6]) $ mkConstMethod' "itemAt" "itemAtRaw" [qreal, qreal, objT c_QTransform] $ ptrT $ objT c_QGraphicsItem
    --(>=4,6)items
  , just $ mkConstMethod "itemsBoundingRect" [] $ objT c_QRectF
  , just $ mkConstMethod "mouseGrabberItem" [] $ ptrT $ objT c_QGraphicsItem
  , just $ mkConstMethod "removeItem" [ptrT $ objT c_QGraphicsItem] voidT
  , just $ mkMethod "render" [ptrT $ objT c_QPainter] voidT
  , just $ mkMethod' "render" "renderWithTarget" [ptrT $ objT c_QPainter, objT c_QRectF] voidT
  , just $ mkMethod' "render" "renderWithTargetAndSource" [ptrT $ objT c_QPainter, objT c_QRectF, objT c_QRectF] voidT
  , just $ mkMethod' "render" "renderWithAspectRatioMode" [ptrT $ objT c_QPainter, objT c_QRectF, objT c_QRectF, enumT e_AspectRatioMode] voidT
    --selectedItems
    --(>=4,6)selectionArea
  , test (qtVersion >= [4, 6]) $ mkMethod "sendEvent" [ptrT $ objT c_QGraphicsItem, ptrT $ objT c_QEvent] voidT
  , just $ mkMethod "setFocus" [] voidT
  , just $ mkMethod' "setFocus" "setFocusWithReason" [enumT e_FocusReason] voidT
  , just $ mkMethod "setFocusItem" [ptrT $ objT c_QGraphicsItem] voidT
  , just $ mkMethod' "setFocusItem" "setFocusItemWithReason" [ptrT $ objT c_QGraphicsItem, enumT e_FocusReason] voidT
    --(>=4,6)setSelectionArea / setSelectionAreaWithMode / setSelectionAreaWithModeAndTransform
    --(>=5,5)setSelectionAreaWithOperation ... Mode ... Transform
  , just $ mkMethod "update" [] voidT
  , just $ mkMethod' "update" "updateWithRect" [objT c_QRectF] voidT
  , test (qtVersion >= [4, 6]) $ mkMethod' "update" "updateWithRaw" [qreal, qreal, qreal, qreal] voidT
    -- (>=4,3) views
  , just $ mkConstMethod "width" [] qreal
  , test (qtVersion >= [4, 6]) $ mkProp "activePanel" $ ptrT $ objT c_QGraphicsItem
  , test (qtVersion >= [4, 6]) $ mkProp "activeWindow" $ ptrT $ objT c_QGraphicsWidget
  , just $ mkProp "backgroundBrush" $ objT c_QBrush
  , test (qtVersion >= [4, 3]) $ mkProp "bspTreeDepth" $ intT
  , test (qtVersion >= [4, 4]) $ mkProp "font" $ objT c_QFont
  , just $ mkProp "foregroundBrush" $ objT c_QBrush
  , just $ mkProp "itemIndexMethod" $ enumT e_ItemIndexMethod
  , test (qtVersion >= [5, 4]) $ mkProp "minimumRenderSize" qreal
  , test (qtVersion >= [4, 4]) $ mkProp "palette" $ objT c_QPalette
  , just $ mkProp "sceneRect" $ objT c_QRectF
  , test (qtVersion >= [4, 6]) $ mkProp "stickyFocus" boolT
  , test (qtVersion >= [4, 4]) $ mkProp "style" $ ptrT $ objT c_QStyle
  ]

signals =
  collect
  [ --changed
    just $ makeSignal c_QGraphicsScene "focusItemChanged" c_ListenerPtrQGraphicsItemPtrQGraphicsItemQtFocusReason
  , just $ makeSignal c_QGraphicsScene "sceneRectChanged" c_ListenerQRectF
  , test (qtVersion >= [4, 3]) $ makeSignal c_QGraphicsScene "selectionChanged" c_Listener
  ]

e_ItemIndexMethod =
  makeQtEnum (ident1 "QGraphicsScene" "ItemIndexMethod") [includeStd "QGraphicsScene"]
  [ (0, ["bsp", "tree", "index"])
  , (-1, ["no", "index"])
  ]

(e_SceneLayer, bs_SceneLayers) =
  makeQtEnumBitspace (ident1 "QGraphicsScene" "SceneLayer") "SceneLayers" [includeStd "QGraphicsScene"]
  [ (0x1, ["item", "layer"])
  , (0x2, ["background", "layer"])
  , (0x4, ["foreground", "h", "layer"])
  , (0xffff, ["all", "layers"])
  ]
