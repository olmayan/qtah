-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsObject (
  aModule,
  c_QGraphicsObject,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkMethod,
  mkMethod',
  )

import Foreign.Hoppy.Generator.Types (bitspaceT, enumT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_GestureFlags,
  e_GestureType,
  )
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (c_Listener)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsItem (c_QGraphicsItem)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 6]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Widgets", "QGraphicsObject"] minVersion $
  QtExport (ExportClass c_QGraphicsObject) :
  map QtExportSignal signals

c_QGraphicsObject =
  addReqIncludes [includeStd "QGraphicsObject"] $
  classSetEntityPrefix "" $
  makeClass (ident "QGraphicsObject") Nothing [c_QObject, c_QGraphicsItem]
  [ mkMethod "grabGesture" [enumT e_GestureType] voidT
  , mkMethod' "grabGesture" "grabGestureWithFlags" [enumT e_GestureType, bitspaceT bs_GestureFlags] voidT
  , mkMethod "ungrabGesture" [enumT e_GestureType] voidT
  ]

signals =
  [ makeSignal c_QGraphicsObject "enabledChanged" c_Listener
  , makeSignal c_QGraphicsObject "opacityChanged" c_Listener
  , makeSignal c_QGraphicsObject "parentChanged" c_Listener
  , makeSignal c_QGraphicsObject "rotationChanged" c_Listener
  , makeSignal c_QGraphicsObject "scaleChanged" c_Listener
  , makeSignal c_QGraphicsObject "visibleChanged" c_Listener
  , makeSignal c_QGraphicsObject "xChanged" c_Listener
  , makeSignal c_QGraphicsObject "yChanged" c_Listener
  , makeSignal c_QGraphicsObject "zChanged" c_Listener
  ]
