-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QListView (
  aModule,
  c_QListView,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkProp,
  )

import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  --c_ListenerQModelIndexList,
  )
import Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractItemView (c_QAbstractItemView)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Widgets", "QListView"] $
  map QtExport
  [ ExportClass c_QListView
  , ExportEnum e_Flow
  , ExportEnum e_LayoutMode
  , ExportEnum e_Movement
  , ExportEnum e_ResizeMode
  , ExportEnum e_ViewMode
  ] ++
  map QtExportSignal signals

c_QListView =
  addReqIncludes [includeStd "QListView"] $
  classSetEntityPrefix "" $
  makeClass (ident "QListView") Nothing [c_QAbstractItemView] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QWidget]
  , just $ mkMethod "clearPropertyFlags" [] voidT
  , just $ mkConstMethod "isRowHidden" [intT] boolT
  , just $ mkMethod "setRowHidden" [intT, boolT] voidT
  , test (qtVersion >= [4, 2]) $ mkProp "batchSize" intT
  , just $ mkProp "flow" $ enumT e_Flow
  , just $ mkProp "gridSize" $ objT c_QSize
  , just $ mkProp "layoutMode" $ enumT e_LayoutMode
  , just $ mkProp "modelColumn" intT
  , just $ mkProp "movement" $ enumT e_Movement
  , just $ mkProp "resizeMode" $ enumT e_ResizeMode
  , test (qtVersion >= [4, 3]) $ mkBoolIsProp "selectionRectVisible"
  , just $ mkProp "spacing" intT
  , test (qtVersion >= [4, 1]) $ mkProp "uniformItemSizes" boolT
  , just $ mkProp "viewMode" $ enumT e_ViewMode
  , test (qtVersion >= [4, 2]) $ mkProp "wordWrap" boolT
  , just $ mkBoolIsProp "wrapping"
  ]

signals =
  [ --makeSignal c_QListView "indexesMoved" c_ListenerQModelIndexList
  ]

e_Flow =
  makeQtEnum (ident1 "QListView" "Flow") [includeStd "QListView"]
  [ (0, ["left", "to", "right"])
  , (1, ["top", "to", "bottom"])
  ]

e_LayoutMode =
  makeQtEnum (ident1 "QListView" "LayoutMode") [includeStd "QListView"]
  [ (0, ["single", "pass"])
  , (1, ["batched"])
  ]

e_Movement =
  makeQtEnum (ident1 "QListView" "Movement") [includeStd "QListView"]
  [ (0, ["static"])
  , (1, ["free"])
  , (2, ["snap"])
  ]

e_ResizeMode =
  makeQtEnum (ident1 "QListView" "ResizeMode") [includeStd "QListView"]
  [ (0, ["fixed"])
  , (1, ["adjust"])
  ]

e_ViewMode =
  makeQtEnum (ident1 "QListView" "ViewMode") [includeStd "QListView"]
  [ (0, ["list", "mode"])
  , (1, ["icon", "mode"])
  ]
