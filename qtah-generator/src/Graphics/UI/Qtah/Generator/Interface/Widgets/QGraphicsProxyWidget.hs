-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsProxyWidget (
  aModule,
  c_QGraphicsProxyWidget,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkProp,
  )

import Foreign.Hoppy.Generator.Types (bitspaceT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (bs_WindowFlags)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsItem (c_QGraphicsItem)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsWidget (c_QGraphicsWidget)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 4]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Widgets", "QGraphicsProxyWidget"] minVersion $
  [ QtExport $ ExportClass c_QGraphicsProxyWidget
  ]

c_QGraphicsProxyWidget =
  addReqIncludes [includeStd "QGraphicsProxyWidget"] $
  classSetEntityPrefix "" $
  makeClass (ident "QGraphicsProxyWidget") Nothing [c_QGraphicsWidget] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QGraphicsItem]
  , just $ mkCtor "newWithParentAndWindowFlags" [ptrT $ objT c_QGraphicsItem, bitspaceT bs_WindowFlags]
  , test (qtVersion >= [4, 5]) $ mkMethod "createProxyForChildWidget" [ptrT $ objT c_QWidget] $ ptrT $ objT c_QGraphicsProxyWidget
  --paint
  , just $ mkMethod "setGeometry" [objT c_QRectF] voidT
  , just $ mkConstMethod "subWidgetRect" [ptrT $ objT c_QWidget] $ objT c_QRectF
  , just $ mkConstMethod' "type" "type_" [] intT
  , just $ mkProp "widget" $ ptrT $ objT c_QWidget
  ]
