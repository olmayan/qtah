-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QTableView (
  aModule,
  c_QTableView,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkProp,
  )

import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  e_PenStyle,
  e_SortOrder,
  )
import Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractItemView (c_QAbstractItemView)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QHeaderView (c_QHeaderView)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Widgets", "QTableView"] $
  [ QtExport $ ExportClass c_QTableView
  ]

c_QTableView =
  addReqIncludes [includeStd "QTableView"] $
  classSetEntityPrefix "" $
  makeClass (ident "QTableView") Nothing [c_QAbstractItemView] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QWidget]
  , test (qtVersion >= [4, 4]) $ mkMethod "clearSpans" [] voidT
  , just $ mkConstMethod "columnAt" [intT] intT 
  , test (qtVersion >= [4, 2]) $ mkConstMethod "columnSpan" [intT, intT] intT 
  , just $ mkConstMethod "columnViewportPosition" [intT] intT 
  , just $ mkConstMethod "columnWidth" [intT] intT 
  , just $ mkMethod "hideColumn" [intT] voidT 
  , just $ mkMethod "hideRow" [intT] voidT 
  , just $ mkConstMethod "isColumnHidden" [intT] voidT
  , just $ mkConstMethod "isRowHidden" [intT] voidT
  , just $ mkMethod "resizeColumnToContents" [intT] voidT 
  , just $ mkMethod "resizeColumnsToContents" [] voidT 
  , just $ mkMethod "resizeRowToContents" [intT] voidT 
  , just $ mkMethod "resizeRowsToContents" [] voidT 
  , just $ mkConstMethod "rowAt" [intT] intT 
  , just $ mkConstMethod "rowHeight" [intT] intT 
  , test (qtVersion >= [4, 2]) $ mkConstMethod "rowSpan" [intT, intT] intT 
  , just $ mkConstMethod "rowViewportPosition" [intT] intT 
  , just $ mkMethod "selectColumn" [intT] voidT
  , just $ mkMethod "selectRow" [intT] voidT
  , just $ mkMethod "setColumnHidden" [intT, boolT] voidT
  , test (qtVersion >= [4, 1]) $ mkMethod "setColumnWidth" [intT, intT] voidT
  , test (qtVersion >= [4, 1]) $ mkMethod "setRowHeight" [intT, intT] voidT
  , just $ mkMethod "setRowHidden" [intT, boolT] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "setSpan" [intT, intT, intT, intT] voidT
  , just $ mkMethod "showColumn" [intT] voidT
  , just $ mkMethod "showRow" [intT] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "sortByColumn" [intT, enumT e_SortOrder] voidT
  , test (qtVersion >= [4, 3]) $ mkBoolIsProp "cornerButtonEnabled"
  , just $ mkProp "gridStyle" $ enumT e_PenStyle
  , just $ mkProp "horizontalHeader" $ ptrT $ objT c_QHeaderView
  , just $ mkProp "showGrid" boolT
  , test (qtVersion >= [4, 2]) $ mkBoolIsProp "sortingEnabled"
  , test (qtVersion >= [4, 3]) $ mkProp "wordWrap" boolT
  , just $ mkProp "verticalHeader" $ ptrT $ objT c_QHeaderView
  ]
