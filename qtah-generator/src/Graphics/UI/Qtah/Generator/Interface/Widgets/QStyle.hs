-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QStyle (
  aModule,
  c_QStyle,
  e_PrimitiveElement,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkMethod,
  mkMethod',
  mkStaticMethod,
  mkStaticMethod'
  )
import Foreign.Hoppy.Generator.Types (
  bitspaceT, boolT, enumT, intT,
  objT, ptrT, refT, voidT
  )
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QPoint (c_QPoint)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_Alignment,
  e_LayoutDirection,
  )
import Graphics.UI.Qtah.Generator.Interface.Gui.QIcon (c_QIcon, e_Mode)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPainter (c_QPainter)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Gui.QPixmap (c_QPixmap)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QApplication (c_QApplication)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Widgets.QStyleOption (c_QStyleOption)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

{-# ANN module "HLint: ignore Use camelCase" #-}

aModule =
  AQtModule $
  makeQtModule ["Widgets", "QStyle"] $
  (QtExport $ ExportClass c_QStyle) :
  [ QtExport $ ExportEnum e_ControlElement
  , QtExport $ ExportEnum e_PrimitiveElement
  , QtExport $ ExportEnum e_StandardPixmap
  ]

c_QStyle =
  addReqIncludes [includeStd "QStyle"] $
  classSetEntityPrefix "" $
  makeClass (ident "QStyle") Nothing [c_QObject] $
  collect
  [ --just $ mkConstMethod "combinedLayoutSpacing" [***] intT
    --TODO other non-static members
    just $ mkMethod "drawPrimitive" [enumT e_PrimitiveElement, ptrT $ objT c_QStyleOption, ptrT $ objT c_QPainter] voidT
  , just $ mkMethod' "drawPrimitive" "drawPrimitiveWithWidget" [enumT e_PrimitiveElement, ptrT $ objT c_QStyleOption, ptrT $ objT c_QPainter, ptrT $ objT c_QWidget] voidT
  , just $ mkMethod "generatedIconPixmap" [enumT e_Mode, objT c_QPixmap, ptrT $ objT c_QStyleOption] $ objT c_QPixmap
    --TODO other non-static members
  , test (qtVersion >= [4, 1]) $ mkConstMethod "standardIcon" [enumT e_StandardPixmap] $ objT c_QIcon
  , test (qtVersion >= [4, 1]) $ mkConstMethod' "standardIcon" "standardIconWithOption" [enumT e_StandardPixmap, ptrT $ objT c_QStyleOption] $ objT c_QIcon
  , test (qtVersion >= [4, 1]) $ mkConstMethod' "standardIcon" "standardIconWithOptionAndWidget" [enumT e_StandardPixmap, ptrT $ objT c_QStyleOption, ptrT $ objT c_QWidget] $ objT c_QIcon
    --TODO other non-static members
  , just $ mkMethod "unpolish" [ptrT $ objT c_QWidget] voidT
  , just $ mkMethod' "unpolish" "unpolishApp" [ptrT $ objT c_QApplication] voidT
  , just $ mkStaticMethod "alignedRect" [enumT e_LayoutDirection, bitspaceT bs_Alignment, objT c_QSize, objT c_QRect] $ objT c_QRect
  , just $ mkStaticMethod "sliderPositionFromValue" [intT, intT, intT, intT] intT
  , just $ mkStaticMethod' "sliderPositionFromValue" "sliderPositionFromValueUpsideDown" [intT, intT, intT, intT, boolT] intT
  , just $ mkStaticMethod "sliderValueFromPosition" [intT, intT, intT, intT] intT
  , just $ mkStaticMethod' "sliderValueFromPosition" "sliderValueFromPositionUpsideDown" [intT, intT, intT, intT, boolT] intT
  , just $ mkStaticMethod "visualAlignment" [enumT e_LayoutDirection, bitspaceT bs_Alignment] $ bitspaceT bs_Alignment
  , just $ mkStaticMethod "visualPos" [enumT e_LayoutDirection, objT c_QRect, objT c_QPoint] $ objT c_QPoint
  , just $ mkStaticMethod "visualRect" [enumT e_LayoutDirection, objT c_QRect, objT c_QRect] $ objT c_QRect
  ]

-- TODO add other enums

e_ControlElement =
  makeQtEnum (ident1 "QStyle" "ControlElement") [includeStd "QStyle"] $
  map (\(a, b) -> (a, ["c", "e", "_"] ++ b))
  [ (0, ["push", "button"])
  , (1, ["push", "button", "bevel"])
  , (2, ["push", "button", "label"])
  , (30, ["dock", "widget", "title"])
  , (28, ["splitter"])
  , (3, ["check", "box"])
  , (4, ["check", "box", "label"])
  , (5, ["radio", "button"])
  , (6, ["radio", "button", "label"])
  , (7, ["tab", "bar", "tab"])
  , (8, ["tab", "bar", "tab", "shape"])
  , (9, ["tab", "bar", "tab", "label"])
  , (10, ["progress", "bar"])
  , (11, ["progress", "bar", "groove"])
  , (12, ["progress", "bar", "contents"])
  , (13, ["progress", "bar", "label"])
  , (22, ["tool", "button", "label"])
  , (20, ["menu", "bar", "item"])
  , (21, ["menu", "bar", "empty", "area"])
  , (14, ["menu", "item"])
  , (15, ["menu", "scroller"])
  , (18, ["menu", "tearoff"])
  , (19, ["menu", "empty", "area"])
  , (17, ["menu", "h", "margin"])
  , (16, ["menu", "v", "margin"])
  , (26, ["tool", "box", "tab"])
  , (27, ["size", "grip"])
  , (23, ["header"])
  , (24, ["header", "section"])
  , (25, ["header", "label"])
  , (31, ["scroll", "bar", "add", "line"])
  , (32, ["scroll", "bar", "sub", "line"])
  , (33, ["scroll", "bar", "add", "page"])
  , (34, ["scroll", "bar", "sub", "page"])
  , (35, ["scroll", "bar", "slider"])
  , (36, ["scroll", "bar", "first"])
  , (37, ["scroll", "bar", "last"])
  , (29, ["rubber", "band"])
  , (38, ["focus", "frame"])
  , (45, ["item", "view", "item"])
  , (0xf0000000, ["custom", "base"])
  , (39, ["combo", "box", "label"])
  , (40, ["tool", "bar"])
  , (41, ["tool", "box", "tab", "shape"])
  , (42, ["tool", "box", "tab", "label"])
  , (43, ["header", "empty", "area"])
  , (46, ["shaped", "frame"])
  ]

e_PrimitiveElement =
  makeQtEnum (ident1 "QStyle" "PrimitiveElement") [includeStd "QStyle"] $
  map (\(a, b) -> (a, ["p", "e", "_"] ++ b))
  [ (0, ["frame"])
  , (1, ["frame", "default", "button"])
  , (2, ["frame", "dock", "widget"])
  , (3, ["frame", "focus", "rect"])
  , (4, ["frame", "group", "box"])
  , (5, ["frame", "line", "edit"])
  , (6, ["frame", "menu"])
  , (7, ["frame", "status", "bar", "item"])
  , (8, ["frame", "tab", "widget"])
  , (9, ["frame", "window"])
  , (10, ["frame", "button", "bevel"])
  , (11, ["frame", "button", "tool"])
  , (12, ["frame", "tab", "bar", "base"])
  , (13, ["panel", "button", "command"])
  , (14, ["panel", "button", "bevel"])
  , (15, ["panel", "button", "tool"])
  , (16, ["panel", "menu", "bar"])
  , (17, ["panel", "tool", "bar"])
  , (18, ["panel", "line", "edit"])
  , (19, ["indicator", "arrow", "down"])
  , (20, ["indicator", "arrow", "left"])
  , (21, ["indicator", "arrow", "right"])
  , (22, ["indicator", "arrow", "up"])
  , (23, ["indicator", "branch"])
  , (24, ["indicator", "button", "drop", "down"])
  , (25, ["indicator", "view", "item", "check"])
  , (26, ["indicator", "check", "box"])
  , (27, ["indicator", "dock", "widget", "resize", "handle"])
  , (28, ["indicator", "header", "arrow"])
  , (29, ["indicator", "menu", "check", "mark"])
  , (30, ["indicator", "progress", "chunk"])
  , (31, ["indicator", "radio", "button"])
  , (32, ["indicator", "spin", "down"])
  , (33, ["indicator", "spin", "minus"])
  , (34, ["indicator", "spin", "plus"])
  , (35, ["indicator", "spin", "up"])
  , (36, ["indicator", "tool", "bar", "handle"])
  , (37, ["indicator", "tool", "bar", "separator"])
  , (38, ["panel", "tip", "label"])
  , (39, ["indicator", "tab", "tear"])
  , (40, ["panel", "scroll", "area", "corner"])
  , (41, ["widget"])
  , (42, ["indicator", "column", "view", "arrow"])
  , (43, ["indicator", "item", "view", "item", "drop"])
  , (44, ["panel", "item", "view", "item"])
  , (45, ["panel", "item", "view", "row"])
  , (46, ["panel", "status", "bar"])
  , (47, ["indicator", "tab", "close"])
  , (48, ["panel", "menu"])
  , (0xf000000, ["custom", "base"])
  ]

e_StandardPixmap =
  makeQtEnum (ident1 "QStyle" "StandardPixmap") [includeStd "QStyle"] $
  map (\(a, b) -> (a, ["s", "p", "_"] ++ b))
  [ (1, ["title", "bar", "min", "button"])
  , (0, ["title", "bar", "menu", "button"])
  , (2, ["title", "bar", "max", "button"])
  , (3, ["title", "bar", "close", "button"])
  , (4, ["title", "bar", "normal", "button"])
  , (5, ["title", "bar", "shade", "button"])
  , (6, ["title", "bar", "unshade", "button"])
  , (7, ["title", "bar", "context", "help", "button"])
  , (9, ["message", "box", "information"])
  , (10, ["message", "box", "warning"])
  , (11, ["message", "box", "critical"])
  , (12, ["message", "box", "question"])
  , (13, ["desktop", "icon"])
  , (14, ["trash", "icon"])
  , (15, ["computer", "icon"])
  , (16, ["drive", "f", "d", "icon"])
  , (17, ["drive", "h", "d", "icon"])
  , (18, ["drive", "c", "d", "icon"])
  , (19, ["drive", "d", "v", "dicon"])
  , (20, ["drive", "net", "icon"])
  , (56, ["dir", "home", "icon"])
  , (21, ["dir", "open", "icon"])
  , (22, ["dir", "closed", "icon"])
  , (38, ["dir", "icon"])
  , (23, ["dir", "link", "icon"])
  , (24, ["dir", "link", "open", "icon"])
  , (25, ["file", "icon"])
  , (26, ["file", "link", "icon"])
  , (29, ["file", "dialog", "start"])
  , (30, ["file", "dialog", "end"])
  , (31, ["file", "dialog", "to", "parent"])
  , (32, ["file", "dialog", "new", "folder"])
  , (33, ["file", "dialog", "detailed", "view"])
  , (34, ["file", "dialog", "info", "view"])
  , (35, ["file", "dialog", "contents", "view"])
  , (36, ["file", "dialog", "list", "view"])
  , (37, ["file", "dialog", "back"])
  , (8, ["dock", "widget", "close", "button"])
  , (27, ["tool", "bar", "horizontal", "extension", "button"])
  , (28, ["tool", "bar", "vertical", "extension", "button"])
  , (39, ["dialog", "ok", "button"])
  , (40, ["dialog", "cancel", "button"])
  , (41, ["dialog", "help", "button"])
  , (42, ["dialog", "open", "button"])
  , (43, ["dialog", "save", "button"])
  , (44, ["dialog", "close", "button"])
  , (45, ["dialog", "apply", "button"])
  , (46, ["dialog", "reset", "button"])
  , (47, ["dialog", "discard", "button"])
  , (48, ["dialog", "yes", "button"])
  , (49, ["dialog", "no", "button"])
  , (50, ["arrow", "up"])
  , (51, ["arrow", "down"])
  , (52, ["arrow", "left"])
  , (53, ["arrow", "right"])
  , (54, ["arrow", "back"])
  , (55, ["arrow", "forward"])
  , (57, ["command", "link"])
  , (58, ["vista", "shield"])
  , (59, ["browser", "reload"])
  , (60, ["browser", "stop"])
  , (61, ["media", "play"])
  , (62, ["media", "stop"])
  , (63, ["media", "pause"])
  , (64, ["media", "skip", "forward"])
  , (65, ["media", "skip", "backward"])
  , (66, ["media", "seek", "forward"])
  , (67, ["media", "seek", "backward"])
  , (68, ["media", "volume"])
  , (69, ["media", "volume", "muted"])
  , (70, ["line", "edit", "clear", "button"])
  , (0xf0000000, ["custom", "base"])
  ]
