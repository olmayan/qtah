-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsEllipseItem (
  aModule,
  c_QGraphicsEllipseItem,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkCtor,
  mkMethod',
  mkProp,
  )

import Foreign.Hoppy.Generator.Types (intT, objT, ptrT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qreal)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractGraphicsShapeItem (c_QAbstractGraphicsShapeItem)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsItem (c_QGraphicsItem)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 2]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Widgets", "QGraphicsEllipseItem"] minVersion $
  [ QtExport $ ExportClass c_QGraphicsEllipseItem
  ]

c_QGraphicsEllipseItem =
  addReqIncludes [includeStd "QGraphicsEllipseItem"] $
  classSetEntityPrefix "" $
  makeClass (ident "QGraphicsEllipseItem") Nothing [c_QAbstractGraphicsShapeItem]
  [ mkCtor "new" []
  , mkCtor "newWithParent" [ptrT $ objT c_QGraphicsItem]
  , mkCtor "newWithRect" [objT c_QRectF]
  , mkCtor "newWithRectAndParent" [objT c_QRectF, ptrT $ objT c_QGraphicsItem]
  , mkCtor "newWithRectRaw" [qreal, qreal, qreal, qreal]
  , mkCtor "newWithRectRawAndParent" [qreal, qreal, qreal, qreal, ptrT $ objT c_QGraphicsItem]  
  , mkMethod' "setRect" "setRectRaw" [qreal, qreal, qreal, qreal] voidT
  , mkProp "rect" $ objT c_QRectF
  , mkProp "spanAngle" intT
  , mkProp "startAngle" intT
  ]
