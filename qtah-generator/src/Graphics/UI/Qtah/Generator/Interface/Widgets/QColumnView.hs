-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QColumnView (
  aModule,
  c_QColumnView,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkCtor,
  mkProp,
  )

import Foreign.Hoppy.Generator.Types (boolT, objT, ptrT)
import Graphics.UI.Qtah.Generator.Interface.Core.QList (c_QListInt)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  --c_ListenerQModelIndex,
  )
import Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractItemView (c_QAbstractItemView)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 3]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Widgets", "QColumnView"] minVersion $
  [ QtExport $ ExportClass c_QColumnView
  ] ++
  map QtExportSignal signals

c_QColumnView =
  addReqIncludes [includeStd "QColumnView"] $
  classSetEntityPrefix "" $
  makeClass (ident "QColumnView") Nothing [c_QAbstractItemView]
  [ mkCtor "new" []
  , mkCtor "newWithParent" [ptrT $ objT c_QWidget]
  , mkProp "columnWidths" $ objT c_QListInt
  , mkProp "previewWidget" $ ptrT $ objT c_QWidget
  , mkProp "resizeGripsVisible" boolT
  ]

signals =
  [ --makeSignal c_QColumnView "updatePreviewWidget" c_ListenerQModelIndex
  ]
