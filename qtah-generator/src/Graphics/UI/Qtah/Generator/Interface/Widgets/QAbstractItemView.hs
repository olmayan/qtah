-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractItemView (
  aModule,
  c_QAbstractItemView,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkBoolHasProp,
  mkConstMethod,
  mkMethod,
  mkMethod',
  mkProp,
  )

import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QAbstractItemModel (c_QAbstractItemModel)
import Graphics.UI.Qtah.Generator.Interface.Core.QModelIndex (c_QModelIndex)
import Graphics.UI.Qtah.Generator.Interface.Core.QPoint (c_QPoint)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  e_DropAction,
  e_TextElideMode,
  )
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  c_Listener,
  --c_ListenerQModelIndex,
  c_ListenerQSize,
  )
import Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractScrollArea (c_QAbstractScrollArea)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Widgets", "QAbstractItemView"] $
  (map QtExport . collect)
  [ just $ ExportClass c_QAbstractItemView
  , test (qtVersion >= [4, 2]) $ ExportEnum e_DragDropMode
  , just $ ExportEnum e_DropIndicatorPosition
  , just $ ExportEnum e_EditTrigger
  , just $ ExportBitspace bs_EditTriggers
  , just $ ExportEnum e_ScrollHint
  , test (qtVersion >= [4, 2]) $ ExportEnum e_ScrollMode
  , just $ ExportEnum e_SelectionBehavior
  , just $ ExportEnum e_SelectionMode
  , just $ ExportEnum e_State
  ] ++
  map QtExportSignal signals

c_QAbstractItemView =
  addReqIncludes [includeStd "QAbstractItemView"] $
  classSetEntityPrefix "" $
  makeClass (ident "QAbstractItemView") Nothing [c_QAbstractScrollArea] $
  collect
  [ just $ mkMethod "closePersistentEditor" [objT c_QModelIndex] voidT
  , just $ mkMethod "clearSelection" [] voidT
  , just $ mkMethod "edit" [objT c_QModelIndex] voidT
  , just $ mkConstMethod "indexAt" [objT c_QPoint] $ objT c_QModelIndex
  , test (qtVersion >= [4, 1]) $ mkConstMethod "indexWidget" [objT c_QModelIndex] $ ptrT $ objT c_QWidget
  --, just $ mkConstMethod' "itemDelegate" "itemDelegateWithIndex" [objT c_QModelIndex] $ ptrT $ objT c_QAbstractItemDelegate
  --, test (qtVersion >= [4, 2]) $ mkConstMethod "itemDelegateForColumn" [intT] $ ptrT $ objT c_QAbstractItemDelegate
  --, test (qtVersion >= [4, 2]) $ mkConstMethod "itemDelegateForRow" [intT] $ ptrT $ objT c_QAbstractItemDelegate
  , just $ mkMethod "keyboardSearch" [objT c_QString] voidT
  , just $ mkMethod "openPersistentEditor" [objT c_QModelIndex] voidT
  , just $ mkMethod "reset" [] voidT
  --, test (qtVersion >= [4, 2]) $ mkMethod "resetHorizontalScrollMode" [] voidT
  --, test (qtVersion >= [4, 2]) $ mkMethod "resetVerticalScrollMode" [] voidT
  , just $ mkMethod' "scrollTo" "scrollToEnsureVisible" [objT c_QModelIndex] voidT
  , just $ mkMethod "scrollTo" [objT c_QModelIndex, enumT e_ScrollHint] voidT
  , test (qtVersion >= [4, 1]) $ mkMethod "scrollToBottom" [] voidT
  , test (qtVersion >= [4, 1]) $ mkMethod "scrollToTop" [] voidT
  , just $ mkMethod "selectAll" [] voidT
  , just $ mkConstMethod "setDropIndicatorShown" [boolT] voidT
  , test (qtVersion >= [4, 1]) $ mkConstMethod "setIndexWidget" [objT c_QModelIndex, ptrT $ objT c_QWidget] voidT
  --, test (qtVersion >= [4, 2]) $ mkMethod "setItemDelegateForColumn" [intT, ptrT $ objT c_QAbstractItemDelegate] voidT
  --, test (qtVersion >= [4, 2]) $ mkMethod "setItemDelegateForRow" [intT, ptrT $ objT c_QAbstractItemDelegate] voidT
  , just $ mkConstMethod "showDropIndicator" [] boolT
  , just $ mkConstMethod "sizeHintForColumn" [intT] intT
  , just $ mkConstMethod "sizeHintForIndex" [objT c_QModelIndex] $ objT c_QSize
  , just $ mkConstMethod "sizeHintForRow" [intT] intT
  , test (qtVersion >= [4, 3]) $ mkMethod "update" [objT c_QModelIndex] voidT
  , just $ mkConstMethod "visualRect" [objT c_QModelIndex] $ objT c_QRect
  , just $ mkProp "alternatingRowColors" boolT
  , just $ mkBoolHasProp "autoScroll"
  , test (qtVersion >= [4, 4]) $ mkProp "autoScrollMargin" intT
  , just $ mkProp "currentIndex" $ objT c_QModelIndex
  , test (qtVersion >= [4, 6]) $ mkProp "defaultDropAction" $ enumT e_DropAction
  , test (qtVersion >= [4, 2]) $ mkProp "dragDropMode" $ enumT e_DragDropMode
  , test (qtVersion >= [4, 2]) $ mkProp "dragDropOverwriteMode" boolT
  , just $ mkProp "dragEnabled" boolT
  , just $ mkProp "editTriggers" $ bitspaceT bs_EditTriggers
  , test (qtVersion >= [4, 2]) $ mkProp "horizontalScrollMode" $ enumT e_ScrollMode
  , just $ mkProp "iconSize" $ objT c_QSize
  --, just $ mkProp "itemDelegate" $ ptrT $ objT c_QAbstractItemDelegate
  , just $ mkProp "model" $ ptrT $ objT c_QAbstractItemModel
  , just $ mkProp "rootIndex" $ objT c_QModelIndex
  , just $ mkProp "selectionBehavior" $ enumT e_SelectionBehavior
  , just $ mkProp "selectionMode" $ enumT e_SelectionMode
  --, just $ mkProp "selectionModel" $ ptrT $ objT c_QItemSelectionModel
  , just $ mkProp "tabKeyNavigation" boolT
  , just $ mkProp "textElideMode" $ enumT e_TextElideMode
  , test (qtVersion >= [4, 2]) $ mkProp "verticalScrollMode" $ enumT e_ScrollMode
  ]

signals =
  [ --makeSignal c_QAbstractItemView "activated" c_ListenerQModelIndex
  --, makeSignal c_QAbstractItemView "clicked" c_ListenerQModelIndex
  --, makeSignal c_QAbstractItemView "doubleClicked" c_ListenerQModelIndex
  --, makeSignal c_QAbstractItemView "entered" c_ListenerQModelIndex
    makeSignal c_QAbstractItemView "iconSizeChanged" c_ListenerQSize
  --, makeSignal c_QAbstractItemView "pressed" c_ListenerQModelIndex
  , makeSignal c_QAbstractItemView "viewportEntered" c_Listener
  ]

e_DragDropMode =
  makeQtEnum (ident1 "QAbstractItemView" "DragDropMode") [includeStd "QAbstractItemView"]
  [ (0, ["no", "drag", "drop"])
  , (1, ["drag", "only"])
  , (2, ["drop", "only"])
  , (3, ["drag", "drop"])
  , (4, ["internal", "move"])
  ]

e_DropIndicatorPosition =
  makeQtEnum (ident1 "QAbstractItemView" "DropIndicatorPosition") [includeStd "QAbstractItemView"]
  [ (0, ["on", "item"])
  , (1, ["above", "item"])
  , (2, ["below", "item"])
  , (3, ["on", "viewport"])
  ]

(e_EditTrigger, bs_EditTriggers) =
  makeQtEnumBitspace (ident1 "QAbstractItemView" "EditTrigger") "EditTriggers" [includeStd "QAbstractItemView"]
  [ (0x00, ["no", "edit", "triggers"])
  , (0x01, ["current", "changed"])
  , (0x02, ["double", "clicked"])
  , (0x04, ["selected", "clicked"])
  , (0x08, ["edit", "key", "pressed"])
  , (0x10, ["any", "key", "pressed"])
  , (0xff, ["all", "edit", "triggers"])
  ]

e_ScrollHint =
  makeQtEnum (ident1 "QAbstractItemView" "ScrollHint") [includeStd "QAbstractItemView"]
  [ (0, ["ensure", "visible"])
  , (1, ["position", "at", "top"])
  , (2, ["position", "at", "bottom"])
  , (3, ["position", "at", "center"])
  ]

e_ScrollMode =
  makeQtEnum (ident1 "QAbstractItemView" "ScrollMode") [includeStd "QAbstractItemView"]
  [ (0, ["scroll", "per", "item"])
  , (1, ["scroll", "per", "pixel"])
  ]

e_SelectionBehavior =
  makeQtEnum (ident1 "QAbstractItemView" "SelectionBehavior") [includeStd "QAbstractItemView"]
  [ (0, ["select", "items"])
  , (1, ["select", "rows"])
  , (2, ["select", "columns"])
  ]

e_SelectionMode =
  makeQtEnum (ident1 "QAbstractItemView" "SelectionMode") [includeStd "QAbstractItemView"]
  [ (1, ["single", "selection"])
  , (4, ["contiguous", "selection"])
  , (3, ["extended", "selection"])
  , (2, ["multi", "selection"])
  , (0, ["no", "selection"])
  ]

e_State =
  makeQtEnum (ident1 "QAbstractItemView" "State") [includeStd "QAbstractItemView"]
  [ (0, ["no", "state"])
  , (1, ["dragging", "state"])
  , (2, ["drag", "selecting", "state"])
  , (3, ["editing", "state"])
  , (4, ["expanding", "state"])
  , (5, ["collapsing", "state"])
  , (6, ["animating", "state"])
  ]
