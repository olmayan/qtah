-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QCompleter (
  aModule,
  c_QCompleter,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QAbstractItemModel (c_QAbstractItemModel)
import Graphics.UI.Qtah.Generator.Interface.Core.QModelIndex (c_QModelIndex)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_MatchFlags,
  e_CaseSensitivity,
  )
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  --c_ListenerQModelIndex,
  c_ListenerQString,
  )
import Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractItemView (c_QAbstractItemView)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

{-# ANN module "HLint: ignore Use camelCase" #-}

minVersion = [4, 2]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Widgets", "QCompleter"] minVersion $
  map QtExport
  [ ExportClass c_QCompleter
  , ExportEnum e_CompletionMode
  , ExportEnum e_ModelSorting
  ] ++
  map QtExportSignal signals

c_QCompleter =
  addReqIncludes [includeStd "QCompleter"] $
  classSetEntityPrefix "" $
  makeClass (ident "QCompleter") Nothing [c_QObject] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , just $ mkCtor "newWithModel" [ptrT $ objT c_QAbstractItemModel]
  , just $ mkCtor "newWithModelAndParent" [ptrT $ objT c_QAbstractItemModel, ptrT $ objT c_QObject]
  , just $ mkCtor "newWithStringList" [objT c_QStringList]
  , just $ mkCtor "newWithStringListAndParent" [objT c_QStringList, ptrT $ objT c_QObject]
  , just $ mkMethod "complete" [] voidT
  , just $ mkMethod' "complete" "completeRect" [objT c_QRect] voidT
  , just $ mkConstMethod "completionCount" [] intT
  , just $ mkConstMethod "completionModel" [] $ ptrT $ objT c_QAbstractItemModel
  , just $ mkConstMethod "currentCompletion" [] $ objT c_QString
  , just $ mkConstMethod "currentIndex" [] $ objT c_QModelIndex
  , just $ mkConstMethod "currentRow" [] intT
  , just $ mkConstMethod "pathFromIndex" [objT c_QModelIndex] $ objT c_QString
  , just $ mkMethod "setCurrentRow" [intT] boolT
  , just $ mkConstMethod "splitPath" [objT c_QString] $ objT c_QStringList
  , just $ mkProp "caseSensitivity" $ enumT e_CaseSensitivity
  , just $ mkProp "completionColumn" intT
  , just $ mkProp "completionMode" $ enumT e_CompletionMode
  , just $ mkProp "completionPrefix" $ objT c_QString
  , just $ mkProp "completionRole" intT
  , test (qtVersion >= [5, 2]) $ mkProp "filterMode" $ bitspaceT bs_MatchFlags
  , test (qtVersion >= [4, 7]) $ mkProp "maxVisibleItems" intT
  , just $ mkProp "model" $ ptrT $ objT c_QAbstractItemModel
  , just $ mkProp "modelSorting" $ enumT e_ModelSorting
  , just $ mkProp "popup" $ ptrT $ objT c_QAbstractItemView
  , just $ mkProp "widget" $ ptrT $ objT c_QWidget
  , test (qtVersion >= [4, 3]) $ mkProp "wrapAround" boolT
  ]

signals =
  [ makeSignal c_QCompleter "activated" c_ListenerQString
  --, makeSignal' c_QCompleter "activated" "activatedIndex" c_ListenerQModelIndex
  , makeSignal c_QCompleter "highlighted" c_ListenerQString
  --, makeSignal' c_QCompleter "highlighted" "highlightedIndex" c_ListenerQModelIndex
  ]

e_CompletionMode =
  makeQtEnum (ident1 "QCompleter" "CompletionMode") [includeStd "QCompleter"]
  [ (0, ["popup", "completion"])
  , (1, ["unfiltered", "popup", "completion"])
  , (2, ["inline", "completion"])
  ]

e_ModelSorting =
  makeQtEnum (ident1 "QCompleter" "ModelSorting") [includeStd "QCompleter"]
  [ (0, ["unsorted", "model"])
  , (1, ["case", "sensitively", "sorted", "model"])
  , (2, ["case", "insensitively", "sorted", "model"])
  ]
