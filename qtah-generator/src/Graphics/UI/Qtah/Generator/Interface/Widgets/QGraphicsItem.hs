-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsItem (
  aModule,
  c_QGraphicsItem,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkConstMethod',
  mkMethod,
  mkMethod',
  mkProp,
  )

import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QPointF (c_QPointF)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.QVariant (c_QVariant)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_MouseButtons,
  qreal
  )
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsScene (c_QGraphicsScene)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 2]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Widgets", "QGraphicsItem"] minVersion $
  QtExport (ExportClass c_QGraphicsItem) :
  (map QtExport . collect)
  [ test (qtVersion >= [4, 4]) $ ExportEnum e_CacheMode
  , just $ ExportEnum e_GraphicsItemChange
  , just $ ExportEnum e_GraphicsItemFlag
  , just $ ExportBitspace bs_GraphicsItemFlags
  , test (qtVersion >= [4, 6]) $ ExportEnum e_PanelModality
  ]

c_QGraphicsItem =
  addReqIncludes [includeStd "QGraphicsItem"] $
  classSetEntityPrefix "" $
  makeClass (ident "QGraphicsItem") Nothing [] $
  collect
  [ just $ mkMethod "advance" [intT] voidT
  , just $ mkConstMethod "boundingRect" [] $ objT c_QRectF
  --(>=4, 4)boundingRegion
  --childItems
  --childrenBoundingRect
  , just $ mkMethod "clearFocus" [] voidT
  --(>=4, 5)clipPath
  , just $ mkConstMethod "collidesWithItem" [ptrT $ objT c_QGraphicsItem] boolT
  --collidesWithItem ItemSelectionMode
  --collidesWithPath QPainterPath
  --collidingItems
  , test (qtVersion >= [4, 4]) $ mkConstMethod "commonAncestorItem" [ptrT $ objT c_QGraphicsItem] $ ptrT $ objT c_QGraphicsItem
  , just $ mkConstMethod "contains" [objT c_QPointF] boolT
  --cursor
  , just $ mkConstMethod' "data" "dataVariant" [intT] $ objT c_QVariant
  --deviceTransform
  , test (qtVersion >= [4, 5]) $ mkConstMethod "effectiveOpacity" [] qreal
  , just $ mkMethod "ensureVisible" [objT c_QRectF] voidT
  , just $ mkMethod' "ensureVisible" "ensureVisibleWithMargins" [objT c_QRectF, qreal, qreal] voidT
  , just $ mkMethod' "ensureVisible" "ensureVisibleRaw" [qreal, qreal, qreal, qreal] voidT
  , just $ mkMethod' "ensureVisible" "ensureVisibleRawWithMargins" [qreal, qreal, qreal, qreal, qreal, qreal] voidT
  , test (qtVersion >= [4, 6]) $ mkConstMethod "focusItem" [] $ ptrT $ objT c_QGraphicsItem
  , test (qtVersion >= [4, 6]) $ mkConstMethod "focusProxy" [] $ ptrT $ objT c_QGraphicsItem
  , test (qtVersion >= [4, 4]) $ mkMethod "grabKeyboard" [] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod "grabMouse" [] voidT
  , just $ mkConstMethod "hasCursor" [] boolT
  , just $ mkConstMethod "hasFocus" [] boolT
  , just $ mkMethod "hide" [] voidT
  , just $ mkMethod "installSceneEventFilter" [ptrT $ objT c_QGraphicsItem] voidT
  , just $ mkConstMethod "isActive" [] boolT
  , just $ mkConstMethod "isAncestorOf" [ptrT $ objT c_QGraphicsItem] boolT
  --TODO other methods
  , just $ mkConstMethod "scene" [] $ ptrT $ objT c_QGraphicsScene
  --TODO other methods
  , just $ mkMethod "setFocus" [] voidT
  --setFocusWithReason
  --TODO other methods
  , just $ mkProp "acceptDrops" boolT
  , test (qtVersion >= [4, 4]) $ mkProp "acceptHoverEvents" boolT
  , test (qtVersion >= [4, 6]) $ mkProp "acceptTouchEvents" boolT
  , just $ mkProp "acceptedMouseButtons" $ bitspaceT bs_MouseButtons
  , test (qtVersion >= [4, 4]) $ mkProp "boundingRegionGranularity" qreal
  , test (qtVersion >= [4, 4]) $ mkProp "cacheMode" $ enumT e_CacheMode
  , test (qtVersion >= [4, 6]) $ mkProp "filtersChildEvents" $ boolT
  , just $ mkProp "flags" $ bitspaceT bs_GraphicsItemFlags
  --(>= 4, 6)graphicsEffect
  --group
  --(>= 4, 6)inputMethodHints
  --TODO other props
  , just $ mkProp "pos" $ objT c_QPointF
  --TODO other props
  , just $ mkProp "scale" $ qreal
  --TODO other props
  , just $ mkBoolIsProp "visible"
  , just $ mkProp "x" $ qreal
  , just $ mkProp "y" $ qreal
  , just $ mkProp "zValue" $ qreal
  ]

e_CacheMode =
  makeQtEnum (ident1 "QGraphicsItem" "CacheMode") [includeStd "QGraphicsItem"]
  [ (0, ["no", "cache"])
  , (1, ["item", "coordinate", "cache"])
  , (2, ["device", "coordinate", "cache"])
  ]

e_GraphicsItemChange =
  makeQtEnum (ident1 "QGraphicsItem" "GraphicsItemChange") [includeStd "QGraphicsItem"]
  [ (3,  ["item", "enabled", "change"])
  , (13, ["item", "enabled", "has", "changed"])
  , (1,  ["item", "matrix", "change"])
  , (0,  ["item", "position", "change"])
  , (9,  ["item", "position", "has", "changed"])
  , (8,  ["item", "transform", "change"])
  , (10, ["item", "transform", "has", "changed"])
  , (28, ["item", "rotation", "change"])
  , (29, ["item", "rotation", "has", "changed"])
  , (30, ["item", "scale", "change"])
  , (31, ["item", "scale", "has", "changed"])
  , (32, ["item", "transform", "origin", "point", "change"])
  , (33, ["item", "transform", "origin", "point", "has", "changed"])
  , (4,  ["item", "selected", "change"])
  , (14, ["item", "selected", "has", "changed"])
  , (2,  ["item", "visible", "change"])
  , (12, ["item", "visible", "has", "changed"])
  , (5,  ["item", "parent", "change"])
  , (15, ["item", "parent", "has", "changed"])
  , (6,  ["item", "child", "added", "change"])
  , (7,  ["item", "child", "removed", "change"])
  , (11, ["item", "scene", "change"])
  , (16, ["item", "scene", "has", "changed"])
  , (17, ["item", "cursor", "change"])
  , (18, ["item", "cursor", "has", "changed"])
  , (19, ["item", "tool", "tip", "change"])
  , (20, ["item", "tool", "tip", "has", "changed"])
  , (21, ["item", "flags", "change"])
  , (22, ["item", "flags", "has", "changed"])
  , (23, ["item", "z", "value", "change"])
  , (24, ["item", "z", "value", "has", "changed"])
  , (25, ["item", "opacity", "change"])
  , (26, ["item", "opacity", "has", "changed"])
  , (27, ["item", "scene", "position", "has", "changed"])
  ]

(e_GraphicsItemFlag, bs_GraphicsItemFlags) =
  makeQtEnumBitspace (ident1 "QGraphicsItem" "GraphicsItemFlag") "GraphicsItemFlags" [includeStd "QGraphicsItem"]
  [ (0x1, ["item", "is", "movable"])
  , (0x2, ["item", "is", "selectable"])
  , (0x4, ["item", "is", "focusable"])
  , (0x8, ["item", "clips", "to", "shape"])
  , (0x10, ["item", "clips", "children", "to", "shape"])
  , (0x20, ["item", "ignores", "transformations"])
  , (0x40, ["item", "ignores", "parent", "opacity"])
  , (0x80, ["item", "doesnt", "propagate", "opacity", "to", "children"])
  , (0x100, ["item", "stacks", "behind", "parent"])
  , (0x200, ["item", "uses", "extended", "style", "option"])
  , (0x400, ["item", "has", "no", "contents"])
  , (0x800, ["item", "sends", "geometry", "changes"])
  , (0x1000, ["item", "accepts", "input", "method"])
  , (0x2000, ["item", "negatize", "z", "stacks", "behind", "parent"])
  , (0x4000, ["item", "is", "panel"])
  , (0x10000, ["item", "sends", "scene", "position", "changes"])
  , (0x80000, ["item", "contains", "children", "in", "shape"])
  ]

e_PanelModality =
  makeQtEnum (ident1 "QGraphicsItem" "PanelModality") [includeStd "QGraphicsItem"]
  [ (0, ["non", "modal"])
  , (1, ["panel", "modal"])
  , (2, ["scene", "modal"])
  ]
