-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsView (
  aModule,
  c_QGraphicsView,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkBoolIsProp,
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QPoint (c_QPoint)
import Graphics.UI.Qtah.Generator.Interface.Core.QPointF (c_QPointF)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_Alignment,
  e_AspectRatioMode,
  e_ItemSelectionMode,
  qreal
  )
import Graphics.UI.Qtah.Generator.Interface.Gui.QBrush (c_QBrush)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPainter (
  bs_RenderHints,
  c_QPainter,
  e_RenderHint,
  )
import Graphics.UI.Qtah.Generator.Interface.Gui.QPolygon (c_QPolygon)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPolygonF (c_QPolygonF)
import Graphics.UI.Qtah.Generator.Interface.Gui.QTransform (c_QTransform)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractScrollArea (c_QAbstractScrollArea)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsItem (c_QGraphicsItem)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsScene (
  bs_SceneLayers,
  c_QGraphicsScene
  )
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener ({-c_Listener-})
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 2]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Widgets", "QGraphicsView"] minVersion $
  QtExport (ExportClass c_QGraphicsView) :
  map QtExportSignal signals ++
  (map QtExport . collect)
  [ just $ ExportEnum e_CacheModeFlag
  , just $ ExportBitspace bs_CacheMode
  , just $ ExportEnum e_DragMode
  , test (qtVersion >= [4, 3]) $ ExportEnum e_OptimizationFlag
  , test (qtVersion >= [4, 3]) $ ExportBitspace bs_OptimizationFlags
  , just $ ExportEnum e_ViewportAnchor
  , test (qtVersion >= [4, 3]) $ ExportEnum e_ViewportUpdateMode
  ]

c_QGraphicsView =
  addReqIncludes [includeStd "QGraphicsView"] $
  classSetEntityPrefix "" $
  makeClass (ident "QGraphicsView") Nothing [c_QAbstractScrollArea] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QWidget]
  , just $ mkCtor "newWithScene" [ptrT $ objT c_QGraphicsScene]
  , just $ mkCtor "newWithSceneAndParent" [ptrT $ objT c_QGraphicsScene, ptrT $ objT c_QWidget]
  , just $ mkMethod "centerOn" [objT c_QPointF] voidT
  , just $ mkMethod' "centerOn" "centerOnRaw" [qreal, qreal] voidT
  , just $ mkMethod' "centerOn" "centerOnItem" [ptrT $ objT c_QGraphicsItem] voidT
  , just $ mkMethod "ensureVisible" [objT c_QRectF] voidT
  , just $ mkMethod' "ensureVisible" "ensureVisibleWithMargins" [objT c_QRectF, qreal, qreal] voidT
  , just $ mkMethod' "ensureVisible" "ensureVisibleRaw" [qreal, qreal, qreal, qreal] voidT
  , just $ mkMethod' "ensureVisible" "ensureVisibleRawWithMargins" [qreal, qreal, qreal, qreal, qreal, qreal] voidT
  , just $ mkMethod' "ensureVisible" "ensureVisibleItem" [ptrT $ objT c_QGraphicsItem] voidT
  , just $ mkMethod' "ensureVisible" "ensureVisibleItemWithMargins" [ptrT $ objT c_QGraphicsItem, qreal, qreal] voidT
  , just $ mkMethod "fitInView" [objT c_QRectF] voidT
  , just $ mkMethod' "fitInView" "finInViewWithAspectRatio" [objT c_QRectF, enumT e_AspectRatioMode] voidT
  , just $ mkMethod' "fitInView" "finInViewRaw" [qreal, qreal, qreal, qreal] voidT
  , just $ mkMethod' "fitInView" "finInViewRawWithAspectRatio" [qreal, qreal, qreal, enumT e_AspectRatioMode] voidT
  , just $ mkMethod' "fitInView" "finInViewItem" [ptrT $ objT c_QGraphicsItem] voidT
  , just $ mkMethod' "fitInView" "finInViewItemWithAspectRatio" [ptrT $ objT c_QGraphicsItem, enumT e_AspectRatioMode] voidT
  --inputMethodQuery
  , just $ mkMethod "invalidateScene" [] voidT
  , just $ mkMethod' "invalidateScene" "invalidateSceneWithRect" [objT c_QRectF] voidT
  , just $ mkMethod' "invalidateScene" "invalidateSceneWithRectAndLayers" [objT c_QRectF, bitspaceT bs_SceneLayers] voidT
  , test (qtVersion >= [4, 6]) $ mkConstMethod "isTransformed" [] boolT
  , just $ mkConstMethod "itemAt" [objT c_QPoint] $ ptrT $ objT c_QGraphicsItem
  , just $ mkConstMethod' "itemAt" "itemAtRaw" [qreal, qreal] $ ptrT $ objT c_QGraphicsItem
  --items
  , just $ mkConstMethod "mapFromScene" [objT c_QPointF] $ objT c_QPoint
  , just $ mkConstMethod' "mapFromScene" "mapFromSceneWithRect" [objT c_QRectF] $ objT c_QPolygon
  , just $ mkConstMethod' "mapFromScene" "mapFromSceneWithPolygon" [objT c_QPolygon] $ objT c_QPolygon
  --mapFromScene QPainterPath
  , just $ mkConstMethod' "mapFromScene" "mapFromSceneRaw" [qreal, qreal] $ objT c_QPoint
  , just $ mkConstMethod' "mapFromScene" "mapFromSceneWithRectRaw" [qreal, qreal, qreal, qreal] $ objT c_QPolygon
  , just $ mkConstMethod "mapToScene" [objT c_QPoint] $ objT c_QPointF
  , just $ mkConstMethod' "mapToScene" "mapToSceneWithRect" [objT c_QRect] $ objT c_QPolygonF
  , just $ mkConstMethod' "mapToScene" "mapToSceneWithPolygon" [objT c_QPolygon] $ objT c_QPolygonF
  --mapToScene QPainterPath
  , just $ mkConstMethod' "mapToScene" "mapToSceneRaw" [intT, intT] $ objT c_QPointF
  , just $ mkConstMethod' "mapToScene" "mapToSceneWithRectRaw" [intT, intT, intT, intT] $ objT c_QPolygonF
  --matrix
  , just $ mkMethod "render" [ptrT $ objT c_QPainter] voidT
  , just $ mkMethod' "render" "renderWithTarget" [ptrT $ objT c_QPainter, objT c_QRectF] voidT
  , just $ mkMethod' "render" "renderWithTargetAndSource" [ptrT $ objT c_QPainter, objT c_QRectF, objT c_QRect] voidT
  , just $ mkMethod' "render" "renderWithAspectRatioMode" [ptrT $ objT c_QPainter, objT c_QRectF, objT c_QRect, enumT e_AspectRatioMode] voidT
  , just $ mkMethod "resetCachedContent" [] voidT
  , just $ mkMethod "resetMatrix" [] voidT
  , test (qtVersion >= [4, 3]) $ mkMethod "resetTransform" [] voidT
  , just $ mkMethod "rotate" [qreal] voidT
  , just $ mkMethod "rubberBandRect" [] $ objT c_QRect
  , just $ mkMethod "scale" [qreal, qreal] voidT
  --setMatrix
  , just $ mkMethod' "setOptimizationFlag" "setOptimizationFlagEnabled" [enumT e_OptimizationFlag] voidT
  , just $ mkMethod "setOptimizationFlag" [enumT e_OptimizationFlag, boolT] voidT
  , just $ mkMethod' "setRenderHint" "setRenderHintEnabled" [enumT e_RenderHint] voidT
  , just $ mkMethod "setRenderHint" [enumT e_RenderHint, boolT] voidT
  , just $ mkMethod' "setSceneRect" "setSceneRectRaw" [qreal, qreal, qreal, qreal] voidT
  , test (qtVersion >= [4, 3]) $ mkMethod' "setTransform" "setTransformCombine" [objT c_QTransform] voidT
  , test (qtVersion >= [4, 3]) $ mkMethod "setTransform" [objT c_QTransform, boolT] voidT
  , just $ mkMethod "shear" [qreal, qreal] voidT
  , just $ mkConstMethod "sizeHint" [] $ objT c_QSize
  , test (qtVersion >= [4, 3]) $ mkConstMethod "transform" [] $ objT c_QTransform
  , just $ mkMethod "translate" [qreal, qreal] voidT
  --updateScene
  , just $ mkMethod "updateSceneRect" [objT c_QRectF] voidT
  , test (qtVersion >= [4, 3]) $ mkConstMethod "viewportTransform" [] $ objT c_QTransform
  , just $ mkProp "alignment" $ bitspaceT bs_Alignment
  , just $ mkProp "backgroundBrush" $ objT c_QBrush
  , just $ mkProp "cacheMode" $ bitspaceT bs_CacheMode
  , just $ mkProp "dragMode" $ enumT e_DragMode
  , just $ mkProp "foregroundBrush" $ objT c_QBrush
  , just $ mkBoolIsProp "interactive"
  , test (qtVersion >= [4, 3]) $ mkProp "optimizationFlags" $ bitspaceT bs_OptimizationFlags
  , just $ mkProp "renderHints" $ bitspaceT bs_RenderHints
  , just $ mkProp "resizeAnchor" $ enumT e_ViewportAnchor
  , test (qtVersion >= [4, 3]) $ mkProp "rubberBandSelectionMode" $ enumT e_ItemSelectionMode
  , just $ mkProp "scene" $ ptrT $ objT c_QGraphicsScene
  , just $ mkProp "sceneRect" $ objT c_QRectF
  , just $ mkProp "transformationAnchor" $ enumT e_ViewportAnchor
  , test (qtVersion >= [4, 3]) $ mkProp "viewportUpdateMode" $ enumT e_ViewportUpdateMode
  ]

signals =
  [ --rubberBandChanged
  ]

(e_CacheModeFlag, bs_CacheMode) =
  makeQtEnumBitspace (ident1 "QGraphicsView" "CacheModeFlag") "CacheMode" [includeStd "QGraphicsView"]
  [ (0x0, ["cache", "none"])
  , (0x1, ["cache", "background"])
  ]

e_DragMode =
  makeQtEnum (ident1 "QGraphicsView" "DragMode") [includeStd "QGraphicsView"]
  [ (0, ["no", "drag"])
  , (1, ["scroll", "hand", "drag"])
  , (2, ["rubber", "band", "drag"])
  ]

(e_OptimizationFlag, bs_OptimizationFlags) =
  makeQtEnumBitspace (ident1 "QGraphicsView" "OptimizationFlag") "OptimizationFlags" [includeStd "QGraphicsView"]
  [ (0x1, ["dont", "clip", "painter"])
  , (0x2, ["dont", "save", "painter", "state"])
  , (0x4, ["dont", "adjust", "for", "antialiasing"])
  , (0x8, ["indirect", "painting"])
  ]

e_ViewportAnchor =
  makeQtEnum (ident1 "QGraphicsView" "ViewportAnchor") [includeStd "QGraphicsView"]
  [ (0, ["no", "anchor"])
  , (1, ["anchor", "view", "center"])
  , (2, ["anchor", "under", "mouse"])
  ]

e_ViewportUpdateMode =
  makeQtEnum (ident1 "QGraphicsView" "ViewportUpdateMode") [includeStd "QGraphicsView"]
  [ (0, ["full", "viewport", "update"])
  , (1, ["minimal", "viewport", "update"])
  , (2, ["smart", "viewport", "update"])
  , (4, ["bounding", "rect", "viewport", "update"])
  , (3, ["no", "viewport", "update"])
  ]
