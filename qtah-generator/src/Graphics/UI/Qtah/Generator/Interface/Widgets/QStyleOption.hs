-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Widgets.QStyleOption (
  aModule,
  c_QStyleOption,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkCtor,
  mkMethod
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

{-# ANN module "HLint: ignore Use camelCase" #-}

aModule =
  AQtModule $
  makeQtModule ["Widgets", "QStyleOption"] $
  [ QtExport $ ExportClass c_QStyleOption
  , QtExport $ ExportEnum e_OptionType
  , QtExport $ ExportEnum e_StyleOptionType
  , QtExport $ ExportEnum e_StyleOptionVersion
  ]

c_QStyleOption =
  addReqIncludes [includeStd "QStyleOption"] $
  classAddFeatures [Assignable, Copyable] $
  classSetEntityPrefix "" $
  makeClass (ident "QStyleOption") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithVersion" [intT]
  , just $ mkCtor "newWithVersionAndType" [intT, intT]
  , test (qtVersion >= [4, 1]) $ mkMethod "initFrom" [ptrT $ objT c_QWidget] voidT
  ]

-- TODO add public variables

soDefault = 0

e_OptionType =
  makeQtEnum (ident1 "QStyleOption" "OptionType") [includeStd "QStyleOption"] $
  map (\(a, b) -> (a, ["s", "o", "_"] ++ b))
  [ (soDefault, ["default"])
  , (1, ["focus", "rect"])
  , (2, ["button"])
  , (3, ["tab"])
  , (4, ["menu", "item"])
  , (5, ["frame"])
  , (6, ["progress", "bar"])
  , (7, ["tool", "box"])
  , (8, ["header"])
  , (9, ["dock", "widget"])
  , (10, ["view", "item"])
  , (11, ["tab", "widget", "frame"])
  , (12, ["tab", "bar", "base"])
  , (13, ["rubber", "band"])
  , (14, ["tool", "bar"])
  , (15, ["graphics", "item"])
  , (16, ["slider"])
  , (17, ["spin", "box"])
  , (18, ["tool", "button"])
  , (19, ["combo", "box"])
  , (20, ["title", "bar"])
  , (21, ["group", "box"])
  , (22, ["size", "grip"])
  , (0xf00, ["custom", "base"])
  , (0xf0000, ["complex"])
  , (0xf000000, ["complex", "custom", "base"])
  ]

e_StyleOptionType =
  makeQtEnum (ident1 "QStyleOption" "StyleOptionType") [includeStd "QStyleOption"]
  [ (soDefault, ["type"])
  ]

e_StyleOptionVersion =
  makeQtEnum (ident1 "QStyleOption" "StyleOptionVersion") [includeStd "QStyleOption"]
  [ (1, ["version"])
  ]
