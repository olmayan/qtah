module Graphics.UI.Qtah.Generator.Interface.Multimedia (modules) where

import qualified Graphics.UI.Qtah.Generator.Interface.Multimedia.QMediaContent as QMediaContent
import qualified Graphics.UI.Qtah.Generator.Interface.Multimedia.QMediaObject as QMediaObject
import qualified Graphics.UI.Qtah.Generator.Interface.Multimedia.QMediaPlayer as QMediaPlayer
import qualified Graphics.UI.Qtah.Generator.Interface.Multimedia.QSound as QSound
import qualified Graphics.UI.Qtah.Generator.Interface.Multimedia.Types as Types
import qualified Graphics.UI.Qtah.Generator.Interface.Multimedia.Widgets as Widgets
import Graphics.UI.Qtah.Generator.Module (AModule)

{-# ANN module "HLint: ignore Use camelCase" #-}

modules :: [AModule]
modules =
  [ QMediaContent.aModule
  , QMediaObject.aModule
  , QMediaPlayer.aModule
  , QSound.aModule
  , Types.aModule
  ] ++
  Widgets.modules
