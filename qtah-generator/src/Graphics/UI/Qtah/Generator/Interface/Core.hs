-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core (modules) where

import qualified Graphics.UI.Qtah.Generator.Interface.Core.QAbstractAnimation as QAbstractAnimation
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QAbstractItemModel as QAbstractItemModel
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QAbstractProxyModel as QAbstractProxyModel
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QAnimationGroup as QAnimationGroup
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QBuffer as QBuffer
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QByteArray as QByteArray
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QChar as QChar
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QChildEvent as QChildEvent
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QCoreApplication as QCoreApplication
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QDate as QDate
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QDateTime as QDateTime
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QDir as QDir
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QEasingCurve as QEasingCurve
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QEvent as QEvent
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QFile as QFile
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QFileDevice as QFileDevice
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QFileInfo as QFileInfo
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QFileSystemModel as QFileSystemModel
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QIdentityProxyModel as QIdentityProxyModel
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QIODevice as QIODevice
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QLine as QLine
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QLineF as QLineF
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QList as QList
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QLocale as QLocale
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QMargins as QMargins
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QMarginsF as QMarginsF
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QMimeData as QMimeData
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QMimeDatabase as QMimeDatabase
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QMimeType as QMimeType
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QModelIndex as QModelIndex
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QObject as QObject
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QParallelAnimationGroup as QParallelAnimationGroup
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QPauseAnimation as QPauseAnimation
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QPoint as QPoint
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QPointF as QPointF
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QProcess as QProcess
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QPropertyAnimation as QPropertyAnimation
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QRect as QRect
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QRectF as QRectF
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QRegExp as QRegExp
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QRegularExpression as QRegularExpression
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QRegularExpressionMatch as QRegularExpressionMatch
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QRegularExpressionMatchIterator as QRegularExpressionMatchIterator
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QSaveFile as QSaveFile
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QSequentialAnimationGroup as QSequentialAnimationGroup
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QSize as QSize
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QSizeF as QSizeF
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QSortFilterProxyModel as QSortFilterProxyModel
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QStandardPaths as QStandardPaths
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QString as QString
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QStringList as QStringList
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QStringListModel as QStringListModel
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QTemporaryFile as QTemporaryFile
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QTime as QTime
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QTimerEvent as QTimerEvent
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QTimeZone as QTimeZone
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QUrl as QUrl
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QUrlQuery as QUrlQuery
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QUuid as QUuid
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QVector as QVector
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QVariant as QVariant
import qualified Graphics.UI.Qtah.Generator.Interface.Core.QVariantAnimation as QVariantAnimation
import qualified Graphics.UI.Qtah.Generator.Interface.Core.Types as Types
import Graphics.UI.Qtah.Generator.Module (AModule)

{-# ANN module "HLint: ignore Use camelCase" #-}

modules :: [AModule]
modules =
  concat
  [ [ QAbstractAnimation.aModule
    , QAbstractItemModel.aModule
    , QAbstractProxyModel.aModule
    , QAnimationGroup.aModule
    , QBuffer.aModule
    , QByteArray.aModule
    , QChar.aModule
    , QChildEvent.aModule
    , QCoreApplication.aModule
    , QDate.aModule
    , QDateTime.aModule
    , QDir.aModule
    , QEasingCurve.aModule
    , QEvent.aModule
    , QFile.aModule
    , QFileDevice.aModule
    , QFileInfo.aModule
    , QFileSystemModel.aModule
    , QIdentityProxyModel.aModule
    , QIODevice.aModule
    , QLine.aModule
    , QLineF.aModule
    , QLocale.aModule
    , QMargins.aModule
    , QMarginsF.aModule
    , QMimeData.aModule
    , QMimeDatabase.aModule
    , QMimeType.aModule
    , QModelIndex.aModule
    , QObject.aModule
    , QParallelAnimationGroup.aModule
    , QPauseAnimation.aModule
    , QPoint.aModule
    , QPointF.aModule
    , QProcess.aModule
    , QPropertyAnimation.aModule
    , QRect.aModule
    , QRectF.aModule
    , QRegExp.aModule
    , QRegularExpression.aModule
    , QRegularExpressionMatch.aModule
    , QRegularExpressionMatchIterator.aModule
    , QSequentialAnimationGroup.aModule
    , QSaveFile.aModule
    , QSize.aModule
    , QSizeF.aModule
    , QSortFilterProxyModel.aModule
    , QStandardPaths.aModule
    , QString.aModule
    , QStringList.aModule
    , QStringListModel.aModule
    , QTemporaryFile.aModule
    , QTime.aModule
    , QTimerEvent.aModule
    , QTimeZone.aModule
    , QUrl.aModule
    , QUrlQuery.aModule
    , QUuid.aModule
    , QVariant.aModule
    , QVariantAnimation.aModule
    , Types.aModule
    ]
  , QList.allModules
  , QVector.allModules
  ]
