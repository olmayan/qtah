-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Internal.Callback where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportCallback),
  makeCallback,
  makeModule,
  moduleAddExports,
  moduleAddHaskellName,
  moduleModify',
  toExtName,
  )
import Foreign.Hoppy.Generator.Types (boolT, doubleT, enumT, intT, objT, ptrT, voidT)
import {-# SOURCE #-} qualified Graphics.UI.Qtah.Generator.Interface.Core.QAbstractAnimation as QAbstractAnimation
import Graphics.UI.Qtah.Generator.Interface.Core.QEvent (c_QEvent)
--import Graphics.UI.Qtah.Generator.Interface.Core.QModelIndex (c_QModelIndex)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Core.QProcess (
  e_ExitStatus,
  e_ProcessError,
  e_ProcessState,
  )
import Graphics.UI.Qtah.Generator.Interface.Core.QPoint (c_QPoint)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QSizeF (c_QSizeF)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  e_FocusReason,
  e_SortOrder,
  qint64,
  qreal
  )
import {-# SOURCE #-} qualified Graphics.UI.Qtah.Generator.Interface.Gui.QClipboard as QClipboard
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Gui.QMovie (e_MovieState)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Gui.QStandardItem (c_QStandardItem)
import {-# SOURCE #-} qualified Graphics.UI.Qtah.Generator.Interface.Multimedia.QMediaPlayer as QMediaPlayer
import Graphics.UI.Qtah.Generator.Interface.Multimedia.Types (e_Role)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractButton (
  c_QAbstractButton,
  )
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Widgets.QAbstractSlider (
  e_SliderAction,
  )
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Widgets.QAction (c_QAction)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsItem (c_QGraphicsItem)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AHoppyModule))

{-# ANN module "HLint: ignore Use camelCase" #-}

aModule =
  AHoppyModule $
  moduleModify' (makeModule "callback" "b_callback.hpp" "b_callback.cpp") $ do
    moduleAddHaskellName ["Internal", "Callback"]
    moduleAddExports
      [ ExportCallback cb_BoolVoid
      , ExportCallback cb_DoubleVoid
      , ExportCallback cb_IntVoid
      , ExportCallback cb_IntBoolVoid
      , ExportCallback cb_IntIntVoid
      , ExportCallback cb_IntIntIntVoid
      , ExportCallback cb_IntQProcessExitStatusVoid
      , ExportCallback cb_IntQtSortOrderVoid
      , ExportCallback cb_PtrQAbstractButtonVoid
      , ExportCallback cb_PtrQAbstractButtonBoolVoid
      , ExportCallback cb_PtrQActionVoid
      , ExportCallback cb_PtrQGraphicsItemPtrQGraphicsItemQtFocusReasonVoid
      , ExportCallback cb_PtrQObjectPtrQEventBool
      , ExportCallback cb_PtrQObjectVoid
      , ExportCallback cb_PtrQStandardItemVoid
      , ExportCallback cb_PtrQWidgetPtrQWidgetVoid
      , ExportCallback cb_QAbstractAnimationDirectionVoid
      , ExportCallback cb_QAbstractAnimationStateVoid
      , ExportCallback cb_QAbstractSliderActionVoid
      , ExportCallback cb_QClipboardModeVoid
      , ExportCallback cb_QInt64Void
      --, ExportCallback cb_QAudioRoleVoid
      , ExportCallback cb_QMediaPlayerErrorVoid
      , ExportCallback cb_QMediaPlayerMediaStatusVoid
      , ExportCallback cb_QMediaPlayerStateVoid
      --, ExportCallback cb_QModelIndexVoid
      , ExportCallback cb_QMovieStateVoid
      , ExportCallback cb_QPointVoid
      , ExportCallback cb_QProcessErrorVoid
      , ExportCallback cb_QProcessStateVoid
      , ExportCallback cb_QRealVoid
      , ExportCallback cb_QRectVoid
      , ExportCallback cb_QRectFVoid
      , ExportCallback cb_QSizeVoid
      , ExportCallback cb_QSizeFVoid
      , ExportCallback cb_QStringVoid
      , ExportCallback cb_QStringQStringQStringVoid
      , ExportCallback cb_Void
      ]

cb_BoolVoid =
  makeCallback (toExtName "CallbackBoolVoid")
  [boolT] voidT

cb_DoubleVoid =
  makeCallback (toExtName "CallbackDoubleVoid")
  [doubleT] voidT

cb_IntVoid =
  makeCallback (toExtName "CallbackIntVoid")
  [intT] voidT

cb_IntBoolVoid =
  makeCallback (toExtName "CallbackIntBoolVoid")
  [intT, boolT] voidT

cb_IntIntVoid =
  makeCallback (toExtName "CallbackIntIntVoid")
  [intT, intT] voidT

cb_IntIntIntVoid =
  makeCallback (toExtName "CallbackIntIntIntVoid")
  [intT, intT, intT] voidT

cb_IntQProcessExitStatusVoid =
  makeCallback (toExtName "CallbackIntQProcessExitStatusVoid")
  [intT, enumT e_ExitStatus] voidT

cb_IntQtSortOrderVoid =
  makeCallback (toExtName "CallbackIntQtSortOrderVoid")
  [intT, enumT e_SortOrder] voidT

cb_PtrQAbstractButtonVoid =
  makeCallback (toExtName "CallbackPtrQAbstractButtonVoid")
  [ptrT $ objT c_QAbstractButton] voidT

cb_PtrQAbstractButtonBoolVoid =
  makeCallback (toExtName "CallbackPtrQAbstractButtonBoolVoid")
  [ptrT $ objT c_QAbstractButton, boolT] voidT

cb_PtrQActionVoid =
  makeCallback (toExtName "CallbackPtrQActionVoid")
  [ptrT $ objT c_QAction] voidT

cb_PtrQGraphicsItemPtrQGraphicsItemQtFocusReasonVoid =
  makeCallback (toExtName "CallbackPtrQGraphicsItemPtrQGraphicsItemQtFocusReasonVoid")
  [ptrT $ objT c_QGraphicsItem, ptrT $ objT c_QGraphicsItem, enumT e_FocusReason] boolT

cb_PtrQObjectPtrQEventBool =
  makeCallback (toExtName "CallbackPtrQObjectPtrQEventBool")
  [ptrT $ objT c_QObject, ptrT $ objT c_QEvent] boolT

cb_PtrQObjectVoid =
  makeCallback (toExtName "CallbackPtrQObjectVoid")
  [ptrT $ objT c_QObject] voidT

cb_PtrQStandardItemVoid =
  makeCallback (toExtName "CallbackPtrQStandardItemVoid")
  [ptrT $ objT c_QStandardItem] voidT

cb_PtrQWidgetPtrQWidgetVoid =
  makeCallback (toExtName "CallbackPtrQWidgetPtrQWidgetVoid")
  [ptrT $ objT c_QWidget, ptrT $ objT c_QWidget] voidT

cb_QAbstractAnimationDirectionVoid =
  makeCallback (toExtName "CallbackQAbstractAnimationDirectionVoid")
  [enumT QAbstractAnimation.e_Direction] voidT

cb_QAbstractAnimationStateVoid =
  makeCallback (toExtName "CallbackQAbstractAnimationStateVoid")
  [enumT QAbstractAnimation.e_State] voidT

cb_QAbstractSliderActionVoid =
  makeCallback (toExtName "CallbackQAbstractSliderActionVoid")
  [enumT e_SliderAction] voidT

cb_QClipboardModeVoid =
  makeCallback (toExtName "CallbackQClipboardModeVoid")
  [enumT QClipboard.e_Mode] voidT

cb_QInt64Void =
  makeCallback (toExtName "CallbackQInt64Void")
  [qint64] voidT

cb_QAudioRoleVoid =
  makeCallback (toExtName "CallbackQAudioRoleVoid")
  [enumT e_Role] voidT

cb_QMediaPlayerErrorVoid =
  makeCallback (toExtName "CallbackQMediaPlayerErrorVoid")
  [enumT QMediaPlayer.e_Error] voidT

cb_QMediaPlayerMediaStatusVoid =
  makeCallback (toExtName "CallbackQMediaPlayerMediaStatusVoid")
  [enumT QMediaPlayer.e_MediaStatus] voidT

cb_QMediaPlayerStateVoid =
  makeCallback (toExtName "CallbackQMediaPlayerStateVoid")
  [enumT QMediaPlayer.e_State] voidT

--cb_QModelIndexVoid =
  --makeCallback (toExtName "CallbackQModelIndexVoid")
  --[objT c_QModelIndex] voidT

cb_QMovieStateVoid =
  makeCallback (toExtName "CallbackQMovieStateVoid")
  [enumT e_MovieState] voidT

cb_QPointVoid =
  makeCallback (toExtName "CallbackQPointVoid")
  [objT c_QPoint] voidT

cb_QProcessErrorVoid =
  makeCallback (toExtName "CallbackQProcessErrorVoid")
  [enumT e_ProcessError] voidT

cb_QProcessStateVoid =
  makeCallback (toExtName "CallbackQProcessStateVoid")
  [enumT e_ProcessState] voidT

cb_QRealVoid =
  makeCallback (toExtName "CallbackQRealVoid")
  [qreal] voidT

cb_QRectVoid =
  makeCallback (toExtName "CallbackQRectVoid")
  [objT c_QRect] voidT

cb_QRectFVoid =
  makeCallback (toExtName "CallbackQRectFVoid")
  [objT c_QRectF] voidT

cb_QSizeVoid =
  makeCallback (toExtName "CallbackQSizeVoid")
  [objT c_QSize] voidT

cb_QSizeFVoid =
  makeCallback (toExtName "CallbackQSizeFVoid")
  [objT c_QSizeF] voidT

cb_QStringVoid =
  makeCallback (toExtName "CallbackQStringVoid")
  [objT c_QString] voidT

cb_QStringQStringQStringVoid =
  makeCallback (toExtName "CallbackQStringQStringQStringVoid")
  [objT c_QString, objT c_QString, objT c_QString] voidT

cb_Void =
  makeCallback (toExtName "CallbackVoid")
  [] voidT
