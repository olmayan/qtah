module Graphics.UI.Qtah.Generator.Interface.Svg.QGraphicsSvgItem (
  aModule,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (intT, objT, ptrT, voidT)
import Graphics.UI.Qtah.Generator.Flag (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Svg.QSvgRenderer (c_QSvgRenderer)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsItem (c_QGraphicsItem)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QGraphicsObject (c_QGraphicsObject)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 2]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Svg", "QGraphicsSvgItem"] minVersion $
  [ QtExport $ ExportClass c_QGraphicsSvgItem
  ]
  -- This class appears since Qt 4.2, but its ancestor QGraphicsObject
  -- appears since Qt 4.6

c_QGraphicsSvgItem =
  addReqIncludes [includeStd "QGraphicsSvgItem"] $
  classSetEntityPrefix "" $
  makeClass (ident "QGraphicsSvgItem") Nothing ancestors $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QGraphicsItem]
  , just $ mkCtor "newFromFile" [objT c_QString]
  , just $ mkCtor "newFromFileWithParent" [objT c_QString, ptrT $ objT c_QGraphicsItem]
  , just $ mkConstMethod "boundingRect" [] $ objT c_QRectF
  --paint
  , just $ mkConstMethod "renderer" [] $ ptrT $ objT c_QSvgRenderer
  , just $ mkMethod "setSharedRenderer" [ptrT $ objT c_QSvgRenderer] voidT
  , just $ mkConstMethod' "type" "type_" [] intT
  , test (qtVersion >= [4, 6]) $ mkProp "elementId" $ objT c_QString
  , test (qtVersion >= [4, 6]) $ mkProp "maximumCacheSize" $ objT c_QSize
  ]

ancestors =
  collect
  [ test (qtVersion >= [4, 6]) c_QGraphicsObject
  , test (qtVersion < [4, 6]) c_QGraphicsItem
  ]
