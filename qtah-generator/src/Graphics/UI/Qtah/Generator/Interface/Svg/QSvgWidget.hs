module Graphics.UI.Qtah.Generator.Interface.Svg.QSvgWidget (
  aModule,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkMethod'
  )
import Foreign.Hoppy.Generator.Types (objT, ptrT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Svg.QSvgRenderer (c_QSvgRenderer)
import Graphics.UI.Qtah.Generator.Interface.Widgets.QWidget (c_QWidget)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 1]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Svg", "QSvgWidget"] minVersion $
  [ QtExport $ ExportClass c_QSvgWidget
  ]

c_QSvgWidget =
  addReqIncludes [includeStd "QSvgWidget"] $
  classSetEntityPrefix "" $
  makeClass (ident "QSvgWidget") Nothing [c_QWidget]
  [ mkCtor "new" []
  , mkCtor "newWithParent" [ptrT $ objT c_QWidget]
  , mkCtor "newFromFile" [objT c_QString]
  , mkCtor "newFromFileWithParent" [objT c_QString, ptrT $ objT c_QWidget]
  , mkMethod' "load" "loadFile" [objT c_QString] voidT
  , mkMethod "load" [objT c_QByteArray] voidT
  , mkConstMethod "renderer" [] $ ptrT $ objT c_QSvgRenderer
  , mkConstMethod "sizeHint" [] $ objT c_QSize
  ]
