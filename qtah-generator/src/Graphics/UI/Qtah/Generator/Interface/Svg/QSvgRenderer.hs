module Graphics.UI.Qtah.Generator.Interface.Svg.QSvgRenderer (
  aModule,
  c_QSvgRenderer
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (boolT, intT, objT, ptrT, voidT)
import Graphics.UI.Qtah.Generator.Flag (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPainter (c_QPainter)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (c_Listener)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 1]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Svg", "QSvgRenderer"] minVersion $
  QtExport (ExportClass c_QSvgRenderer) :
  map QtExportSignal signals

c_QSvgRenderer =
  addReqIncludes [includeStd "QSvgRenderer"] $
  classSetEntityPrefix "" $
  makeClass (ident "QSvgRenderer") Nothing [c_QObject] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , just $ mkCtor "newFromByteArray" [objT c_QByteArray]
  , just $ mkCtor "newFromByteArrayWithParent" [objT c_QByteArray, ptrT $ objT c_QObject]
  --(>=4, 5)newFromXml
  , just $ mkConstMethod "animated" [] boolT
  , test (qtVersion >= [4, 2]) $ mkConstMethod "boundsOnElement" [objT c_QString] $ objT c_QRectF
  , just $ mkConstMethod "defaultSize" [] $ objT c_QSize
  , test (qtVersion >= [4, 2]) $ mkConstMethod "elementExists" [objT c_QString] boolT
  , just $ mkConstMethod "isValid" [] boolT
  , just $ mkMethod' "load" "loadFile" [objT c_QString] voidT
  , just $ mkMethod "load" [objT c_QByteArray] voidT
  --(>=4, 5)load loadXml
  --(>=4, 2)matrixForElement
  , just $ mkMethod "render" [ptrT $ objT c_QPainter] voidT
  , just $ mkMethod' "render" "renderBounded" [ptrT $ objT c_QPainter, objT c_QRectF] voidT
  , just $ mkMethod' "render" "renderElement" [ptrT $ objT c_QPainter, objT c_QString] voidT
  , just $ mkMethod' "render" "renderElementBounded" [ptrT $ objT c_QPainter, objT c_QString, objT c_QRectF] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod' "setViewBox" "setViewBoxF" [objT c_QRectF] voidT
  , test (qtVersion >= [4, 2]) $ mkConstMethod "viewBoxF" [] $ objT c_QRectF
  , just $ mkProp "framesPerSecond" intT
  , test (qtVersion >= [4, 2]) $ mkProp "viewBox" $ objT c_QRect
  ]

signals =
  [ makeSignal c_QSvgRenderer "repaintNeeded" c_Listener
  ]
