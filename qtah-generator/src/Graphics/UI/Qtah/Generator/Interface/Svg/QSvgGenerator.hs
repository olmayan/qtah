module Graphics.UI.Qtah.Generator.Interface.Svg.QSvgGenerator (
  aModule,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (intT, objT, voidT)
import Graphics.UI.Qtah.Generator.Flag (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPaintDevice (c_QPaintDevice)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 3]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Svg", "QSvgGenerator"] minVersion $
  [ QtExport $ ExportClass c_QSvgGenerator
  ]

c_QSvgGenerator =
  addReqIncludes [includeStd "QSvgGenerator"] $
  classSetEntityPrefix "" $
  makeClass (ident "QSvgGenerator") Nothing [c_QPaintDevice] $
  collect
  [ just $ mkCtor "new" []
  , test (qtVersion >= [4, 5]) $ mkMethod' "setViewBox" "setViewBoxF" [objT c_QRectF] voidT
  , test (qtVersion >= [4, 5]) $ mkConstMethod "viewBoxF" [] $ objT c_QRectF
  , test (qtVersion >= [4, 5]) $ mkProp "description" $ objT c_QString
  , test (qtVersion >= [4, 5]) $ mkProp "fileName" $ objT c_QString
  --test (qtVersion >= [4, 5]) outputDevice
  , test (qtVersion >= [4, 5]) $ mkProp "resolution" intT
  , test (qtVersion >= [4, 5]) $ mkProp "size" $ objT c_QSize
  , test (qtVersion >= [4, 5]) $ mkProp "title" $ objT c_QString
  , test (qtVersion >= [4, 5]) $ mkProp "viewBox" $ objT c_QRect
  ]
