-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QFileInfo (
  aModule,
  c_QFileInfo
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  mkStaticMethod',
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, objT, refT, uintT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QDateTime (c_QDateTime)
import Graphics.UI.Qtah.Generator.Interface.Core.QDir (c_QDir)
import Graphics.UI.Qtah.Generator.Interface.Core.QFile (
  bs_Permissions,
  c_QFile,
  )
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qint64)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QFileInfo"]
  [ QtExport $ ExportClass c_QFileInfo
  ]

c_QFileInfo =
  addReqIncludes [includeStd "QFileInfo"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QFileInfo") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newFromFilePath" [objT c_QString]
  , just $ mkCtor "newFromFile" [objT c_QFile]
  , just $ mkCtor "newFromDirAndFileName" [objT c_QDir, objT c_QString]
  , just $ mkConstMethod "absoluteDir" [] $ objT c_QDir
  , just $ mkConstMethod "absoluteFilePath" [] $ objT c_QString
  , just $ mkConstMethod "absolutePath" [] $ objT c_QString
  , just $ mkConstMethod "baseName" [] $ objT c_QString
  , test (qtVersion >= [4, 3]) $ mkConstMethod "bundleName" [] $ objT c_QString
  , just $ mkConstMethod "canonicalFilePath" [] $ objT c_QString
  , just $ mkConstMethod "canonicalPath" [] $ objT c_QString
  , just $ mkConstMethod "completeBaseName" [] $ objT c_QString
  , just $ mkConstMethod "completeSuffix" [] $ objT c_QString
  , just $ mkConstMethod "created" [] $ objT c_QDateTime
  , just $ mkConstMethod "dir" [] $ objT c_QDir
  , just $ mkConstMethod "exists" [] boolT
  , just $ mkConstMethod "fileName" [] $ objT c_QString
  , just $ mkConstMethod "filePath" [] $ objT c_QString
  , just $ mkConstMethod "group" [] $ objT c_QString
  , just $ mkConstMethod "groupId" [] uintT
  , just $ mkConstMethod "isAbsolute" [] boolT
  , test (qtVersion >= [4, 3]) $ mkConstMethod "isBundle" [] boolT
  , just $ mkConstMethod "isDir" [] boolT
  , just $ mkConstMethod "isExecutable" [] boolT
  , just $ mkConstMethod "isFile" [] boolT
  , just $ mkConstMethod "isHidden" [] boolT
  , test (qtVersion >= [5, 0]) $ mkConstMethod "isNativePath" [] boolT
  , just $ mkConstMethod "isReadable" [] boolT
  , just $ mkConstMethod "isRelative" [] boolT
  , just $ mkConstMethod "isRoot" [] boolT
  , just $ mkConstMethod "isSymLink" [] boolT
  , just $ mkConstMethod "isWritable" [] boolT
  , just $ mkConstMethod "lastModified" [] $ objT c_QDateTime
  , just $ mkConstMethod "lastRead" [] $ objT c_QDateTime
  , just $ mkMethod "makeAbsolute" [] boolT
  , just $ mkConstMethod "owner" [] $ objT c_QString
  , just $ mkConstMethod "ownerId" [] uintT
  , just $ mkConstMethod "path" [] $ objT c_QString
  , just $ mkConstMethod "permission" [bitspaceT bs_Permissions] boolT
  , just $ mkConstMethod "permissions" [] $ bitspaceT bs_Permissions
  , just $ mkMethod "refresh" [] voidT
  , just $ mkMethod' "setFile" "setFilePath" [objT c_QString] voidT
  , just $ mkMethod "setFile" [objT c_QFile] voidT
  , just $ mkMethod' "setFile" "setFileDirAndName" [objT c_QDir, objT c_QString] voidT
  , just $ mkConstMethod "size" [] qint64
  , just $ mkConstMethod "suffix" [] $ objT c_QString
  , test (qtVersion >= [5, 0]) $ mkMethod "swap" [refT $ objT c_QFileInfo] voidT
  , test (qtVersion >= [4, 2]) $ mkConstMethod "symLinkTarget" [] $ objT c_QString
  , test (qtVersion >= [5, 2]) $ mkStaticMethod' "exists" "fileExists" [objT c_QString] boolT
  , just $ mkProp "caching" boolT
  ]
