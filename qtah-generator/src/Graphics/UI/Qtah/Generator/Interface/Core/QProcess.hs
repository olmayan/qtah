-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QProcess (
  aModule,
  c_QProcess,
  e_ExitStatus,
  e_ProcessError,
  e_ProcessState,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  mkStaticMethod,
  mkStaticMethod',
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QIODevice (
  bs_OpenMode,
  c_QIODevice,
  )
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qint64)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  c_Listener,
  )
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types


aModule =
  AQtModule $
  makeQtModule ["Core", "QProcess"] $
  map QtExport
  [ ExportClass c_QProcess
  , ExportEnum e_ExitStatus
  , ExportEnum e_InputChannelMode
  , ExportEnum e_ProcessChannel
  , ExportEnum e_ProcessChannelMode
  , ExportEnum e_ProcessError
  , ExportEnum e_ProcessState
  ] ++
  map QtExportSignal signals

c_QProcess =
  addReqIncludes [includeStd "QProcess"] $
  classSetEntityPrefix "" $
  makeClass (ident "QProcess") Nothing [c_QIODevice] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , test (qtVersion >= [5, 0]) $ mkConstMethod "arguments" [] $ objT c_QStringList
  , just $ mkMethod "closeReadChannel" [enumT e_ProcessChannel] voidT
  , just $ mkMethod "closeWriteChannel" [] voidT
  , just $ mkConstMethod "error" [] $ enumT e_ProcessError
  , just $ mkConstMethod "exitCode" [] intT
  , test (qtVersion >= [4, 1]) $ mkConstMethod "exitStatus" [] $ enumT e_ExitStatus
  , just $ mkMethod "kill" [] voidT
  , test (qtVersion >= [5, 3]) $ mkConstMethod "processId" [] qint64
  , test (qtVersion >= [5, 0]) $ mkConstMethod "program" [] $ objT c_QString
  , just $ mkMethod "readAllStandardError" [] $ objT c_QByteArray
  , just $ mkMethod "readAllStandardOutput" [] $ objT c_QByteArray
  , test (qtVersion >= [5, 1]) $ mkMethod "setArguments" [objT c_QStringList] voidT
  , test (qtVersion >= [5, 1]) $ mkMethod "setProgram" [objT c_QString] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "setStandardErrorFile" [objT c_QString] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod' "setStandardErrorFile" "setStandardErrorFileWithMode" [objT c_QString, bitspaceT bs_OpenMode] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "setStandardInputFile" [objT c_QString] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "setStandardOutputFile" [objT c_QString] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod' "setStandardOutputFile" "setStandardOutputFileWithMode" [objT c_QString, bitspaceT bs_OpenMode] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "setStandardOutputProcess" [ptrT $ objT c_QProcess] voidT
  , just $ mkMethod' "start" "startWithProgram" [objT c_QString, objT c_QStringList] voidT
  , just $ mkMethod' "start" "startWithProgramAndMode" [objT c_QString, objT c_QStringList, bitspaceT bs_OpenMode] voidT
  , just $ mkMethod' "start" "startWithCommand" [objT c_QString] voidT
  , just $ mkMethod' "start" "startWithCommandAndMode" [objT c_QString, bitspaceT bs_OpenMode] voidT
  , test (qtVersion >= [5, 1]) $ mkMethod' "start" "startWithMode" [bitspaceT bs_OpenMode] voidT
  , test (qtVersion >= [5, 1]) $ mkMethod "start" [] voidT
  , just $ mkConstMethod "state" [] $ enumT e_ProcessState
  , just $ mkMethod "terminate" [] voidT
  , just $ mkMethod "waitForFinished" [intT] boolT
  , just $ mkMethod "waitForStarted" [intT] boolT
  , just $ mkStaticMethod' "execute" "executeProgram" [objT c_QString, objT c_QStringList] intT
  , just $ mkStaticMethod "execute" [objT c_QString] intT
  , test (qtVersion >= [5, 2]) $ mkStaticMethod "nullDevice" [] $ objT c_QString
  , just $ mkStaticMethod' "startDetached" "startDetachedProgram" [objT c_QString, objT c_QStringList] boolT
  , just $ mkStaticMethod' "startDetached" "startDetachedProgramWithWorkDir" [objT c_QString, objT c_QStringList, objT c_QString] boolT
  , just $ mkStaticMethod' "startDetached" "startDetachedProgramWithWorkDirAndPid" [objT c_QString, objT c_QStringList, objT c_QString, ptrT qint64] boolT
  , just $ mkStaticMethod "startDetached" [objT c_QString] boolT
  , test (qtVersion >= [4, 1]) $ mkStaticMethod "systemEnvironment" [] $ objT c_QStringList
  --(>=5,7)createProcessArgumentsModifier
  , test (qtVersion >= [5, 2]) $ mkProp "inputChannelMode" $ enumT e_InputChannelMode
  --, test (qtVersion >= [4, 7]) $ mkProp "nativeArguments" $ objT c_QString --ONLY ON WINDOWS OS
  , test (qtVersion >= [4, 2]) $ mkProp "processChannelMode" $ enumT e_ProcessChannelMode
  --, test (qtVersion >= [4, 6]) $ mkProp "processEnvironment" $ objT c_QProcessEnvironment
  , just $ mkProp "readChannel" $ enumT e_ProcessChannel
  , just $ mkProp "workingDirectory" $ objT c_QString
  ]

signals =
  collect
  [ --test (qtVersion < [5, 6]) $ makeSignal c_QProcess "error" c_ListenerQProcessError
  --test (qtVersion >= [5, 6]) $ makeSignal c_QProcess "errorOccurred" c_ListenerQProcessError
  --, just $ makeSignal c_QProcess "finished" c_ListenerIntQProcessExitStatus
    just $ makeSignal c_QProcess "readyReadStandardError" c_Listener
  , just $ makeSignal c_QProcess "readyReadStandardOutput" c_Listener
  , just $ makeSignal c_QProcess "started" c_Listener
  --, just $ makeSignal c_QProcess "stateChanged" c_ListenerQProcessState
  ]

e_ExitStatus =
  makeQtEnum (ident1 "QProcess" "ExitStatus") [includeStd "QProcess"]
  [ (0, ["normal", "exit"])
  , (1, ["crash", "exit"])
  ]

e_InputChannelMode =
  makeQtEnum (ident1 "QProcess" "InputChannelMode") [includeStd "QProcess"]
  [ (0, ["managed", "input", "channel"])
  , (1, ["forwarded", "input", "channel"])
  ]

e_ProcessChannel =
  makeQtEnum (ident1 "QProcess" "ProcessChannel") [includeStd "QProcess"]
  [ (0, ["standard", "output"])
  , (1, ["standard", "error"])
  ]

e_ProcessChannelMode =
  makeQtEnum (ident1 "QProcess" "ProcessChannelMode") [includeStd "QProcess"]
  [ (0, ["separate", "channels"])
  , (1, ["merged", "channels"])
  , (2, ["forwarded", "channels"])
  , (4, ["forwarded", "error", "channel"])
  , (3, ["forwarded", "output", "channel"])
  ]

e_ProcessError =
  makeQtEnum (ident1 "QProcess" "ProcessError") [includeStd "QProcess"]
  [ (0, ["failed", "to", "start"])
  , (1, ["crashed"])
  , (2, ["timedout"])
  , (4, ["write", "error"])
  , (3, ["read", "error"])
  , (5, ["unknown", "error"])
  ]

e_ProcessState =
  makeQtEnum (ident1 "QProcess" "ProcessState") [includeStd "QProcess"]
  [ (0, ["NotRunning"])
  , (1, ["Starting"])
  , (2, ["Running"])
  ]
