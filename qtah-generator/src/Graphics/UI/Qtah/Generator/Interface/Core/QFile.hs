-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QFile (
  aModule,
  c_QFile,
  bs_Permissions,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  mkStaticMethod,
  mkStaticMethod',
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, charT, constT, intT, objT, ptrT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QFileDevice (
  bs_FileHandleFlags,
  bs_Permissions,
  c_QFileDevice,
  )
import Graphics.UI.Qtah.Generator.Interface.Core.QIODevice (
  bs_OpenMode,
  c_QIODevice,
  )
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qint64)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QFile"]
  [ QtExport $ ExportClass c_QFile
  ]

c_QFile =
  addReqIncludes [includeStd "QFile"] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QFile") Nothing ancestors $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithFileName" [objT c_QString]
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , just $ mkCtor "newWithFileNameAndParent" [objT c_QString, ptrT $ objT c_QObject]
  , just $ mkMethod "copy" [objT c_QString] boolT
  , just $ mkMethod "exists" [] boolT
  , just $ mkMethod "link" [objT c_QString] boolT
  -- open FILE*
  , just $ mkMethod' "open" "openFD" [intT, bitspaceT bs_OpenMode] boolT
  , just $ mkMethod' "open" "openFDWithFlags" [intT, bitspaceT bs_OpenMode, bitspaceT bs_FileHandleFlags] boolT
  , just $ mkMethod "remove" [] boolT
  , just $ mkMethod "rename" [objT c_QString] boolT
  , test (qtVersion >= [4, 2]) $ mkConstMethod "symLinkTarget" [] $ objT c_QString
  , just $ mkStaticMethod' "copy" "copy_" [objT c_QString, objT c_QString] boolT
  , just $ mkStaticMethod "decodeName" [objT c_QByteArray] $ objT c_QString
  , just $ mkStaticMethod' "decodeName" "decodeNameRaw" [ptrT $ constT charT] $ objT c_QString
  , just $ mkStaticMethod "encodeName" [objT c_QString] $ objT c_QByteArray
  , just $ mkStaticMethod' "exists" "exists_" [objT c_QString] boolT
  , just $ mkStaticMethod' "link" "link_" [objT c_QString, objT c_QString] boolT
  , test (qtVersion >= [5, 0]) $ mkStaticMethod' "permissions" "permissions_" [objT c_QString] $ bitspaceT bs_Permissions
  , just $ mkStaticMethod' "remove" "remove_" [objT c_QString] boolT
  , just $ mkStaticMethod "resize" [objT c_QString, qint64] boolT
  , test (qtVersion >= [5, 0]) $ mkStaticMethod' "setPermissions" "setPermissions_" [objT c_QString, bitspaceT bs_Permissions] boolT
  , test (qtVersion >= [4, 2]) $ mkStaticMethod' "symLinkTarget" "symLinkTarget_" [objT c_QString] $ objT c_QString
  , just $ mkProp "fileName" $ objT c_QString
  ]

ancestors =
  collect
  [ test (qtVersion >= [5, 0]) c_QFileDevice
  , test (qtVersion < [5, 0]) c_QIODevice
  ]
