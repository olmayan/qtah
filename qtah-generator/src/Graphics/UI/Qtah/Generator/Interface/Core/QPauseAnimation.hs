-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QPauseAnimation (
  aModule,
  c_QPauseAnimation
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkCtor,
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (intT, objT, ptrT)
import Graphics.UI.Qtah.Generator.Interface.Core.QAbstractAnimation (c_QAbstractAnimation)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 6]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QPauseAnimation"] minVersion $
  [ QtExport $ ExportClass c_QPauseAnimation ]

c_QPauseAnimation =
  addReqIncludes [includeStd "QPauseAnimation"] $
  classSetEntityPrefix "" $
  makeClass (ident "QPauseAnimation") Nothing [c_QAbstractAnimation]
  [ mkCtor "new" []
  , mkCtor "newWithDuration" [intT]
  , mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , mkCtor "newWithDurationAndParent" [intT, ptrT $ objT c_QObject]
  , mkProp "duration" intT
  ]
