-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QRegularExpressionMatch (
  aModule,
  c_QRegularExpressionMatch,
  bs_MatchOptions,
  e_MatchType,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Core.QRegularExpression (
  c_QRegularExpression,
  bs_MatchOptions,
  e_MatchType,
  )
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QRegularExpressionMatch"] minVersion $
  [ QtExport $ ExportClass c_QRegularExpressionMatch
  ]

c_QRegularExpressionMatch =
  addReqIncludes [includeStd "QRegularExpressionMatch"] $
  classAddFeatures [Assignable, Copyable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QRegularExpressionMatch") Nothing [] $
  collect
  [ test (qtVersion >= [5, 1]) $ mkCtor "new" []
  , just $ mkConstMethod' "captured" "capturedFirst" [] $ objT c_QString
  , just $ mkConstMethod "captured" [intT] $ objT c_QString
  , just $ mkConstMethod' "captured" "capturedByName" [objT c_QString] $ objT c_QString
  , just $ mkConstMethod' "capturedEnd" "capturedEndFirst" [] intT
  , just $ mkConstMethod "capturedEnd" [intT] intT
  , just $ mkConstMethod' "capturedEnd" "capturedEndByName" [objT c_QString] intT
  , just $ mkConstMethod' "capturedLength" "capturedLengthFirst" [] intT
  , just $ mkConstMethod "capturedLength" [intT] intT
  , just $ mkConstMethod' "capturedLength" "capturedLengthByName" [objT c_QString] intT
  --capturedRef
  , just $ mkConstMethod' "capturedStart" "capturedStartFirst" [] intT
  , just $ mkConstMethod "capturedStart" [intT] intT
  , just $ mkConstMethod' "capturedStart" "capturedStartByName" [objT c_QString] intT
  , just $ mkConstMethod "capturedTexts" [] $ objT c_QStringList
  , just $ mkConstMethod "hasMatch" [] boolT
  , just $ mkConstMethod "hasPartialMatch" [] boolT
  , just $ mkConstMethod "isValid" [] boolT
  , just $ mkConstMethod "lastCapturedIndex" [] intT
  , just $ mkConstMethod "matchOptions" [] $ bitspaceT bs_MatchOptions
  , just $ mkConstMethod "matchType" [] $ enumT e_MatchType
  , just $ mkConstMethod "regularExpression" [] $ objT c_QRegularExpression
  , just $ mkMethod "swap" [refT $ objT c_QRegularExpressionMatch] voidT
  ]
