-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QTimeZone (
  aModule,
  c_QTimeZone
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkStaticMethod,
  mkStaticMethod',
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Core.QDateTime (c_QDateTime)
import Graphics.UI.Qtah.Generator.Interface.Core.QList (c_QListQByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QLocale (
  c_QLocale,
  e_Country,
  )
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 2]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QTimeZone"] minVersion $
  map QtExport
  [ ExportClass c_QTimeZone
  , ExportEnum e_NameType
  , ExportEnum e_TimeType
  ]

c_QTimeZone =
  addReqIncludes [includeStd "QTimeZone"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QTimeZone") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithId" [objT c_QByteArray]
  , just $ mkCtor "newWithOffset" [intT]
  , just $ mkCtor "newWithIdAndOffsetAndName" [objT c_QByteArray, intT, objT c_QString, objT c_QString]
  , just $ mkCtor "newFull" [objT c_QByteArray, intT, objT c_QString, objT c_QString, enumT e_Country, objT c_QString]
  , just $ mkConstMethod "abbreviation" [objT c_QDateTime] $ objT c_QString
  , just $ mkConstMethod "comment" [] $ objT c_QString
  , just $ mkConstMethod "country" [] $ enumT e_Country
  , just $ mkConstMethod "daylightTimeOffset" [objT c_QDateTime] intT
  , just $ mkConstMethod "displayName" [objT c_QDateTime] $ objT c_QString
  , just $ mkConstMethod' "displayName" "displayNameWithNameType" [objT c_QDateTime, enumT e_NameType] $ objT c_QString
  , just $ mkConstMethod' "displayName" "displayNameWithNameTypeAndLocale" [objT c_QDateTime, enumT e_NameType, objT c_QLocale] $ objT c_QString
  , just $ mkConstMethod' "displayName" "displayNameForTimeType" [enumT e_TimeType] $ objT c_QString
  , just $ mkConstMethod' "displayName" "displayNameForTimeTypeWithNameType" [enumT e_TimeType, enumT e_NameType] $ objT c_QString
  , just $ mkConstMethod' "displayName" "displayNameForTimeTypeWithNameTypeAndLocale" [enumT e_TimeType, enumT e_NameType, objT c_QLocale] $ objT c_QString
  , just $ mkConstMethod "hasDaylightTime" [] boolT
  , just $ mkConstMethod "hasTransitions" [] boolT
  , just $ mkConstMethod "id" [] $ objT c_QByteArray
  , just $ mkConstMethod "isDaylightTime" [objT c_QDateTime] boolT
  , just $ mkConstMethod "isValid" [] boolT
  --, just $ mkConstMethod "nextTransition" [objT c_QDateTime] $ objT c_OffsetData
  --, just $ mkConstMethod "offsetData" [objT c_QDateTime] $ objT c_OffsetData
  , just $ mkConstMethod "offsetFromUtc" [objT c_QDateTime] intT
  --, just $ mkConstMethod "previousTransition" [objT c_QDateTime] $ objT c_OffsetData
  , just $ mkConstMethod "standardTimeOffset" [objT c_QDateTime] intT
  , just $ mkMethod "swap" [refT $ objT c_QTimeZone] voidT
  --transitions
  , just $ mkStaticMethod "availableTimeZoneIds" [] $ objT c_QListQByteArray
    , just $ mkStaticMethod' "availableTimeZoneIds" "availableTimeZoneIdsWithLocale" [enumT e_Country] $ objT c_QListQByteArray
  , just $ mkStaticMethod' "availableTimeZoneIds" "availableTimeZoneIdsWithOffset" [intT] $ objT c_QListQByteArray
  , just $ mkStaticMethod "isTimeZoneIdAvailable" [objT c_QByteArray] boolT
  , just $ mkStaticMethod "ianaIdToWindowsId" [objT c_QByteArray] $ objT c_QByteArray
  , test (qtVersion >= [5, 5]) $ mkStaticMethod "systemTimeZone" [] $ objT c_QTimeZone
  , just $ mkStaticMethod "systemTimeZoneId" [] $ objT c_QTimeZone
  , test (qtVersion >= [5, 5]) $ mkStaticMethod "utc" [] $ objT c_QTimeZone
  , just $ mkStaticMethod "windowsIdToDefaultIanaId" [objT c_QByteArray] $ objT c_QByteArray
  , just $ mkStaticMethod' "windowsIdToDefaultIanaId" "windowsIdToDefaultIanaIdWithCountry" [objT c_QByteArray, enumT e_Country] $ objT c_QByteArray
  , just $ mkStaticMethod "windowsIdToIanaIds" [objT c_QByteArray] $ objT c_QListQByteArray
  , just $ mkStaticMethod' "windowsIdToIanaIds" "windowsIdToIanaIdsWithCountry" [objT c_QByteArray, enumT e_Country] $ objT c_QListQByteArray
  ]

e_NameType =
  makeQtEnum (ident1 "QTimeZone" "NameType") [includeStd "QTimeZone"]
  [ (0, ["default", "name"])
  , (1, ["long", "name"])
  , (2, ["short", "name"])
  , (3, ["offset", "name"])
  ]

e_TimeType =
  makeQtEnum (ident1 "QTimeZone" "TimeType") [includeStd "QTimeZone"]
  [ (0, ["standard", "time"])
  , (1, ["daylight", "time"])
  , (2, ["generic", "time"])
  ]
