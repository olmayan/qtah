-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QUrl (
  aModule,
  bs_ComponentFormattingOptions,
  c_QUrl,
  ) where

import Data.Bits ((.|.))
import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkProp,
  mkStaticMethod,
  mkStaticMethod',
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.QUrlQuery (c_QUrlQuery)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QUrl"] $
  (map QtExport . collect)
  [ just $ ExportClass c_QUrl
  , just $ ExportEnum e_ComponentFormattingOption
  , just $ ExportBitspace bs_ComponentFormattingOptions
  , just $ ExportEnum e_ParsingMode
  , just $ ExportEnum e_UrlFormattingOption
  , just $ ExportBitspace bs_FormattingOptions
  , test (qtVersion >= [5, 4]) $ ExportEnum e_UserInputResolutionOption
  , test (qtVersion >= [5, 4]) $ ExportBitspace bs_UserInputResolutionOptions
  ]

c_QUrl =
  addReqIncludes [includeStd "QUrl"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QUrl") Nothing [] $
  collect
  [ just $mkCtor "newNull" []
  , just $ mkCtor "new" [objT c_QString]
  , just $ mkCtor "newWithParsingMode" [objT c_QString, enumT e_ParsingMode]
  , test (qtVersion >= [5, 2]) $ mkConstMethod "adjusted" [bitspaceT bs_FormattingOptions] $ objT c_QUrl
  , just $ mkConstMethod' "authority" "authorityPrettyDecoded" [] $ objT c_QString
  , just $ mkConstMethod "authority" [bitspaceT bs_ComponentFormattingOptions] $ objT c_QString
  , just $ mkMethod "clear" [] voidT
  , test (qtVersion >= [4, 2]) $ mkConstMethod "errorString" [] $ objT c_QString
  , test (qtVersion >= [5, 2]) $ mkConstMethod' "fileName" "fileNameFullyDecoded" [] $ objT c_QString
  , test (qtVersion >= [5, 2]) $ mkConstMethod "fileName" [bitspaceT bs_ComponentFormattingOptions] $ objT c_QString
  , just $ mkConstMethod' "fragment" "fragmentPrettyDecoded" [] $ objT c_QString
  , just $ mkConstMethod "fragment" [bitspaceT bs_ComponentFormattingOptions] $ objT c_QString
  , test (qtVersion >= [4, 2]) $ mkConstMethod "hasFragment" [] boolT
  , test (qtVersion >= [4, 2]) $ mkConstMethod "hasQuery" [] boolT
  , just $ mkConstMethod' "host" "hostFullyDecoded" [] $ objT c_QString
  , just $ mkConstMethod "host" [bitspaceT bs_ComponentFormattingOptions] $ objT c_QString
  , just $ mkConstMethod "isEmpty" [] boolT
  , test (qtVersion >= [4, 8]) $ mkConstMethod "isLocalFile" [] boolT
  , just $ mkConstMethod "isParentOf" [objT c_QUrl] boolT
  , just $ mkConstMethod "isRelative" [] boolT
  , just $ mkConstMethod "isValid" [] boolT
  , test (qtVersion >= [5, 2]) $ mkConstMethod "matches" [objT c_QUrl, bitspaceT bs_FormattingOptions] boolT
  , just $ mkConstMethod' "password" "passwordFullyDecoded" [] $ objT c_QString
  , just $ mkConstMethod "password" [bitspaceT bs_ComponentFormattingOptions] $ objT c_QString
  , just $ mkConstMethod' "path" "pathFullyDecoded" [] $ objT c_QString
  , just $ mkConstMethod "path" [bitspaceT bs_ComponentFormattingOptions] $ objT c_QString
  , test (qtVersion >= [4, 1]) $ mkConstMethod' "port" "portWithDefault" [intT] intT
  , test (qtVersion >= [4, 1]) $ mkConstMethod "port" [] intT
  , just $ mkConstMethod' "query" "queryPrettyDecoded" [] $ objT c_QString
  , just $ mkConstMethod "query" [bitspaceT bs_ComponentFormattingOptions] $ objT c_QString
  , just $ mkConstMethod "resolved" [objT c_QUrl] $ objT c_QUrl
  , just $ mkMethod "setPort" [intT] voidT
  , just $ mkConstMethod' "setQuery" "setQueryStringTolerant" [objT c_QString] voidT
  , just $ mkConstMethod' "setQuery" "setQueryString" [objT c_QString, enumT e_ParsingMode] voidT
  , test (qtVersion >= [5, 0]) $ mkConstMethod "setQuery" [objT c_QUrlQuery] voidT
  , just $ mkConstMethod' "setUrl" "setUrlTolerant" [objT c_QString] voidT
  , just $ mkConstMethod "setUrl" [objT c_QString, enumT e_ParsingMode] voidT
  , just $ mkConstMethod' "setUserInfo" "setUserInfoTolerant" [objT c_QString] voidT
  , just $ mkConstMethod "setUserInfo" [objT c_QString, enumT e_ParsingMode] voidT
  , just $ mkConstMethod' "setUserName" "setUserNameDecoded" [objT c_QString] voidT
  , just $ mkConstMethod "setUserName" [objT c_QString, enumT e_ParsingMode] voidT
  , test (qtVersion >= [4, 8]) $ mkMethod "swap" [refT $ objT c_QUrl] voidT
  --(>=5,2)toCFURL
  , test (qtVersion >= [5, 0]) $ mkConstMethod' "toDisplayString" "toDisplayStringPrettyDecoded" [] $ objT c_QString
  , test (qtVersion >= [5, 0]) $ mkConstMethod "toDisplayString" [bitspaceT bs_FormattingOptions] $ objT c_QString
  , just $ mkConstMethod' "toEncoded" "toFullyEncoded" [] $ objT c_QByteArray
  , just $ mkConstMethod "toEncoded" [bitspaceT bs_FormattingOptions] $ objT c_QByteArray
  , just $ mkConstMethod "toLocalFile" [] $ objT c_QString
  --(>=5,2)toNSURL
  , just $ mkConstMethod' "toString" "toStringPrettyDecoded" [] $ objT c_QString
  , just $ mkConstMethod "toString" [bitspaceT bs_FormattingOptions] $ objT c_QString
  , test (qtVersion >= [4, 8]) $ mkConstMethod' "topLevelDomain" "topLevelDomainFullyDecoded" [] $ objT c_QString
  , test (qtVersion >= [4, 8]) $ mkConstMethod "topLevelDomain" [bitspaceT bs_ComponentFormattingOptions] $ objT c_QString
  , just $ mkConstMethod' "url" "urlPrettyDecoded" [] $ objT c_QString
  , just $ mkConstMethod "url" [bitspaceT bs_FormattingOptions] $ objT c_QString
  , just $ mkConstMethod' "userInfo" "userInfoPrettyDecoded" [] $ objT c_QString
  , just $ mkConstMethod "userInfo" [bitspaceT bs_ComponentFormattingOptions] $ objT c_QString
  , just $ mkConstMethod' "userName" "userNameFullyDecoded" [] $ objT c_QString
  , just $ mkConstMethod "userName" [bitspaceT bs_ComponentFormattingOptions] $ objT c_QString
  , test (qtVersion >= [4, 2]) $ mkStaticMethod "fromAce" [objT c_QByteArray] $ objT c_QString
  --(>=5,2)fromCFURL
  , just $ mkStaticMethod' "fromEncoded" "fromEncodedTolerant" [objT c_QByteArray] $ objT c_QUrl
  , just $ mkStaticMethod "fromEncoded" [objT c_QByteArray, enumT e_ParsingMode] $ objT c_QUrl
  , just $ mkStaticMethod "fromLocalFile" [objT c_QString] $ objT c_QUrl
  --(>=5,2)fromNSURL
  , just $ mkStaticMethod "fromPercentEncoding" [objT c_QByteArray] $ objT c_QString
  --(>=5,1)fromStringList
  , test (qtVersion >= [4, 6]) $ mkStaticMethod "fromUserInput" [objT c_QString] $ objT c_QUrl
  , test (qtVersion >= [5, 4]) $ mkStaticMethod' "fromUserInput" "fromUserInputWithWD" [objT c_QString, objT c_QString] $ objT c_QUrl
  , test (qtVersion >= [5, 4]) $ mkStaticMethod' "fromUserInput"
                                                 "fromUserInputWithWDAndDefaultRes"
                                                 [objT c_QString, objT c_QString, bitspaceT bs_UserInputResolutionOptions] $
                                                 objT c_QUrl
  , test (qtVersion >= [4, 2]) $ mkStaticMethod "idnWhitelist" [] $ objT c_QStringList
  , test (qtVersion >= [4, 2]) $ mkStaticMethod "setIdnWhitelist" [objT c_QStringList] voidT 
  , test (qtVersion >= [4, 2]) $ mkStaticMethod "toAce" [objT c_QString] $ objT c_QByteArray
  , just $ mkStaticMethod "toPercentEncoding" [objT c_QString] $ objT c_QByteArray
  , just $ mkStaticMethod' "toPercentEncoding" "toPercentEncodingWithExclude" [objT c_QString, objT c_QByteArray] $ objT c_QByteArray
  , just $ mkStaticMethod' "toPercentEncoding" "toPercentEncodingWithExcludeAndInclude" [objT c_QString, objT c_QByteArray, objT c_QByteArray] $ objT c_QByteArray
  --(>=5,1)toStringList
  , just $ mkProp "scheme" $ objT c_QString
  ]

(e_ComponentFormattingOption, bs_ComponentFormattingOptions) =
  makeQtEnumBitspace (ident1 "QUrl" "ComponentFormattingOption") "ComponentFormattingOptions" [includeStd "QUrl"] $
  let prettyDecoded = 0x000000
      encodeSpaces = 0x100000
      encodeUnicode = 0x200000
      encodeDelimiters = 0x400000 .|. 0x800000
      encodeReserved = 0x1000000
      decodeReserved = 0x2000000
      fullyEncoded =
        encodeSpaces .|. encodeUnicode .|. encodeDelimiters .|. encodeReserved
      fullyDecoded =
        fullyEncoded .|. decodeReserved .|. 0x4000000
  in
  [ (prettyDecoded, ["pretty", "decoded"])
  , (encodeSpaces, ["encode", "spaces"])
  , (encodeUnicode, ["encode", "unicode"])
  , (encodeDelimiters, ["encode", "delimiters"])
  , (encodeReserved, ["encode", "reserved"])
  , (decodeReserved, ["decode", "reserved"])
  , (fullyEncoded, ["fully", "encoded"])
  , (fullyDecoded, ["fully", "decoded"])
  ]

e_ParsingMode =
  makeQtEnum (ident1 "QUrl" "ParsingMode") [includeStd "QUrl"]
  [ (0, ["tolerant", "mode"])
  , (1, ["strict", "mode"])
  , (2, ["decoded", "mode"])
  ]

(e_UrlFormattingOption, bs_FormattingOptions) =
  makeQtEnumBitspace (ident1 "QUrl" "UrlFormattingOption") "FormattingOptions" [includeStd "QUrl"] $
  let removePassword = 0x2
      removeUserInfo = removePassword .|. 0x4
      removePort = 0x8
      removeAuthority = removeUserInfo .|. removePort .|. 0x10
  in
  [ (0x0, ["none"])
  , (0x1, ["remove", "scheme"])
  , (removePassword, ["remove", "password"])
  , (removeUserInfo, ["remove", "user", "info"])
  , (removePort, ["remove", "port"])
  , (removeAuthority, ["remove", "authority"])
  , (0x20, ["remove", "path"])
  , (0x40, ["remove", "query"])
  , (0x80, ["remove", "fragment"])
  , (0x800, ["remove", "filename"])
  , (0x200, ["prefer", "local", "file"])
  , (0x400, ["strip", "trailing", "slash"])
  , (0x1000, ["normalize", "path", "segments"])
  ]

(e_UserInputResolutionOption, bs_UserInputResolutionOptions) =
  makeQtEnumBitspace (ident1 "QUrl" "UserInputResolutionOption") "UserInputResolutionOptions" [includeStd "QUrl"]
  [ (0, ["default", "resolution"])
  , (1, ["assume", "local", "file"])
  ]
