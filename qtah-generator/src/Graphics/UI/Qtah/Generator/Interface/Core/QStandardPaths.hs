-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QStandardPaths (
  aModule,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetDtorPrivate,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkStaticMethod,
  mkStaticMethod'
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, objT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

{-# ANN module "HLint: ignore Use camelCase" #-}

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QStandardPaths"] minVersion $
  (QtExport $ ExportClass c_QStandardPaths) :
  [ QtExport $ ExportBitspace bs_LocateOptions
  , QtExport $ ExportEnum e_LocateOption
  , QtExport $ ExportEnum e_StandardLocation
  ]

c_QStandardPaths =
  addReqIncludes [includeStd "QStandardPaths"] $
  classSetDtorPrivate $ 
  classSetEntityPrefix "" $
  makeClass (ident "QStandardPaths") Nothing []
  [ mkStaticMethod "displayName" [enumT e_StandardLocation] $ objT c_QString
  , mkStaticMethod "findExecutable" [objT c_QString] $ objT c_QString
  , mkStaticMethod' "findExecutable" "findExecutableWithPaths" [objT c_QString, objT c_QStringList] $ objT c_QString
  , mkStaticMethod "locate" [enumT e_StandardLocation, objT c_QString] $ objT c_QString
  , mkStaticMethod' "locate" "locateWithOptions" [enumT e_StandardLocation, objT c_QString, bitspaceT bs_LocateOptions] $ objT c_QString
  , mkStaticMethod "locateAll" [enumT e_StandardLocation, objT c_QString] $ objT c_QStringList
  , mkStaticMethod' "locateAll" "locateAllWithOptions" [enumT e_StandardLocation, objT c_QString, bitspaceT bs_LocateOptions] $ objT c_QStringList
  , mkStaticMethod "setTestModeEnabled" [boolT] voidT
  , mkStaticMethod "standardLocations" [enumT e_StandardLocation] $ objT c_QStringList
  , mkStaticMethod "writableLocation" [enumT e_StandardLocation] $ objT c_QString
  ]

(e_LocateOption, bs_LocateOptions) =
  makeQtEnumBitspace (ident1 "QStandardPaths" "LocateOption") "LocateOptions" [includeStd "QStandardPaths"]
  [ (0x0, ["locate", "file"])
  , (0x1, ["locate", "directory"])
  ]

e_StandardLocation =
  makeQtEnum (ident1 "QStandardPaths" "StandardLocation") [includeStd "QStandardPaths"] $
  [ (0, ["desktop", "location"])
  , (1, ["documents", "location"])
  , (2, ["fonts", "location"])
  , (3, ["applications", "location"])
  , (4, ["music", "location"])
  , (5, ["movies", "location"])
  , (6, ["pictures", "location"])
  , (7, ["temp", "location"])
  , (8, ["home", "location"])
  , (9, ["data", "location"])
  , (10, ["cache", "location"])
  , (15, ["generic", "cache", "location"])
  , (11, ["generic", "data", "location"])
  , (12, ["runtime", "location"])
  , (13, ["config", "location"])
  , (14, ["download", "location"])
  , (16, ["generic", "config", "location"])
  , (17, ["app", "data", "location"])
  , (18, ["app", "config", "location"])
  ]
