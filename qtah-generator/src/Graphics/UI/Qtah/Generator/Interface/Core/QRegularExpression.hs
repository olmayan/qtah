-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QRegularExpression (
  aModule,
  c_QRegularExpression,
  bs_MatchOptions,
  e_MatchType,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkProp,
  mkStaticMethod,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QRegularExpressionMatch (c_QRegularExpressionMatch)
import Graphics.UI.Qtah.Generator.Interface.Core.QRegularExpressionMatchIterator (c_QRegularExpressionMatchIterator)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QRegularExpression"] minVersion $
  map QtExport
  [ ExportClass c_QRegularExpression
  , ExportEnum e_MatchOption
  , ExportBitspace bs_MatchOptions
  , ExportEnum e_MatchType
  , ExportEnum e_PatternOption
  , ExportBitspace bs_PatternOptions
  ]

c_QRegularExpression =
  addReqIncludes [includeStd "QRegularExpression"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QRegularExpression") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithPattern" [objT c_QString]
  , just $ mkCtor "newWithPatternAndOptions" [objT c_QString, bitspaceT bs_PatternOptions]
  , just $ mkConstMethod "captureCount" [] intT
  , just $ mkStaticMethod "escape" [objT c_QString] $ objT c_QString
  , just $ mkConstMethod "globalMatch" [objT c_QString] $ objT c_QRegularExpressionMatchIterator
  , just $ mkConstMethod' "globalMatch" "globalMatchWithOffset" [objT c_QString, intT] $ objT c_QRegularExpressionMatchIterator
  , just $ mkConstMethod' "globalMatch" "globalMatchWithOffsetAndType" [objT c_QString, intT, enumT e_MatchType] $ objT c_QRegularExpressionMatchIterator
  , just $ mkConstMethod' "globalMatch" "globalMatchWithOffsetAndTypeAndOptions"
                          [objT c_QString, intT, enumT e_MatchType, bitspaceT bs_MatchOptions] $
                          objT c_QRegularExpressionMatchIterator
  -- TODO QStringRef?
  , just $ mkConstMethod "isValid" [] boolT
  , just $ mkConstMethod "match" [objT c_QString] $ objT c_QRegularExpressionMatch
  , just $ mkConstMethod' "match" "matchWithOffset" [objT c_QString, intT] $ objT c_QRegularExpressionMatch
  , just $ mkConstMethod' "match" "matchWithOffsetAndType" [objT c_QString, intT, enumT e_MatchType] $ objT c_QRegularExpressionMatch
  , just $ mkConstMethod' "match" "matchWithOffsetAndTypeAndOptions"
                          [objT c_QString, intT, enumT e_MatchType, bitspaceT bs_MatchOptions] $
                          objT c_QRegularExpressionMatch
  , test (qtVersion >= [5, 1]) $ mkConstMethod "namedCaptureGroups" [] $ objT c_QStringList
  , test (qtVersion >= [5, 4]) $ mkMethod "optimize" [] voidT
  , just $ mkConstMethod "patternErrorOffset" [] intT
  , just $ mkMethod "swap" [refT $ objT c_QRegularExpression] voidT
  , just $ mkProp "pattern" $ objT c_QString
  , just $ mkProp "patternOptions" $ bitspaceT bs_PatternOptions
  ]

(e_MatchOption, bs_MatchOptions) =
  makeQtEnumBitspace (ident1 "QRegularExpression" "MatchOption") "MatchOptions" [includeStd "QRegularExpression"]
  [ (0x0000, ["no", "match", "option"])
  , (0x0001, ["anchored", "match", "option"])
  , (0x0002, ["dont", "check", "subject", "string", "match", "option"])
  ]

e_MatchType =
  makeQtEnum (ident1 "QRegularExpression" "MatchType") [includeStd "QRegularExpression"]
  [ (0, ["normal", "match"])
  , (1, ["partial", "prefer", "complete", "match"])
  , (2, ["partial", "prefer", "first", "match"])
  , (3, ["no", "match"])
  ]

(e_PatternOption, bs_PatternOptions) =
  makeQtEnumBitspace (ident1 "QRegularExpression" "PatternOption") "PatternOptions" [includeStd "QRegularExpression"]
  [ (0x0000, ["no", "pattern", "option"])
  , (0x0001, ["case", "insensitive", "option"])
  , (0x0002, ["dot", "matches", "everything", "option"])
  , (0x0004, ["multiline", "option"])
  , (0x0008, ["extended", "pattern", "syntax", "option"])
  , (0x0010, ["inverted", "greediness", "option"])
  , (0x0020, ["dont", "capture", "option"])
  , (0x0040, ["use", "unicode", "properties", "option"])
  , (0x0080, ["optimize", "on", "first", "usage", "option"])
  , (0x0100, ["dont", "automatically", "optimize", "option"])
  ]
