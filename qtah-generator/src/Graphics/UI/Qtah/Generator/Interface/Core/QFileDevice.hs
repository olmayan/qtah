-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QFileDevice (
  aModule,
  bs_FileHandleFlags,
  bs_Permissions,
  c_QFileDevice,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (
  bitspaceT, boolT, enumT, intT,
  objT, ptrT, ucharT, voidT
  )
import Graphics.UI.Qtah.Generator.Interface.Core.QIODevice (c_QIODevice)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qint64)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QFileDevice"] minVersion $
  map QtExport
  [ ExportClass c_QFileDevice
  , ExportEnum e_FileError
  , ExportEnum e_FileHandleFlag
  , ExportBitspace bs_FileHandleFlags
  , ExportEnum e_MemoryMapFlags
  , ExportEnum e_Permission
  , ExportBitspace bs_Permissions
  ]

c_QFileDevice =
  addReqIncludes [includeStd "QFileDevice"] $
  classSetEntityPrefix "" $
  makeClass (ident "QFileDevice") Nothing [c_QIODevice]
  [ mkConstMethod "error" [] $ enumT e_FileError
  , mkConstMethod "fileName" [] $ objT c_QString
  , mkMethod "flush" [] boolT
  , mkConstMethod "handle" [] intT
  , mkMethod "map" [qint64, qint64] $ ptrT ucharT
  , mkMethod' "map" "mapWithFlahs" [qint64, qint64, enumT e_MemoryMapFlags] $ ptrT ucharT
  , mkMethod "resize" [qint64] boolT
  , mkMethod "unmap" [ptrT ucharT] boolT
  , mkMethod "unsetError" [] voidT
  , mkProp "permissions" $ bitspaceT bs_Permissions
  ]

e_FileError =
  makeQtEnum (ident1 "QFileDevice" "FileError") [includeStd "QFileDevice"]
  [ (0, ["no", "error"])
  , (1, ["read", "error"])
  , (2, ["write", "error"])
  , (3, ["fatal", "error"])
  , (4, ["resource", "error"])
  , (5, ["open", "error"])
  , (6, ["abort", "error"])
  , (7, ["time", "out", "error"])
  , (8, ["unspecified", "error"])
  , (9, ["remove", "error"])
  , (10, ["rename", "error"])
  , (11, ["position", "error"])
  , (12, ["resize", "error"])
  , (13, ["permissions", "error"])
  , (14, ["copy", "error"])
  ]

(e_FileHandleFlag, bs_FileHandleFlags) =
  makeQtEnumBitspace (ident1 "QFileDevice" "FileHandleFlag") "FileHandleFlags" [includeStd "QFileDevice"]
  [ (0x0001, ["auto", "close", "handle"])
  , (0, ["dont", "close", "handle"])
  ]

e_MemoryMapFlags =
  makeQtEnum (ident1 "QFileDevice" "MemoryMapFlags") [includeStd "QFileDevice"]
  [ (0, ["no", "options"])
  , (1, ["map", "private", "option"])
  ]

(e_Permission, bs_Permissions) =
  makeQtEnumBitspace (ident1 "QFileDevice" "Permission") "Permissions" [includeStd "QFileDevice"]
  [ (0x4000, ["read", "owner"])
  , (0x2000, ["write", "owner"])
  , (0x1000, ["exe", "owner"])
  , (0x0400, ["read", "user"])
  , (0x0200, ["write", "user"])
  , (0x0100, ["exe", "user"])
  , (0x0040, ["read", "group"])
  , (0x0020, ["write", "group"])
  , (0x0010, ["exe", "group"])
  , (0x0004, ["read", "other"])
  , (0x0002, ["write", "other"])
  , (0x0001, ["exe", "other"])
  ]
