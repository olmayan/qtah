-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QVariant (
  aModule,
  c_QVariant
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkStaticMethod
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, charT, constT, doubleT, floatT, intT, objT, ptrT, refT, uintT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QEasingCurve (c_QEasingCurve)
import Graphics.UI.Qtah.Generator.Interface.Core.QChar (c_QChar)
import Graphics.UI.Qtah.Generator.Interface.Core.QDate (c_QDate)
import Graphics.UI.Qtah.Generator.Interface.Core.QDateTime (c_QDateTime)
import Graphics.UI.Qtah.Generator.Interface.Core.QLine (c_QLine)
import Graphics.UI.Qtah.Generator.Interface.Core.QLineF (c_QLineF)
import Graphics.UI.Qtah.Generator.Interface.Core.QLocale (c_QLocale)
import Graphics.UI.Qtah.Generator.Interface.Core.QModelIndex (c_QModelIndex)
import Graphics.UI.Qtah.Generator.Interface.Core.QPoint (c_QPoint)
import Graphics.UI.Qtah.Generator.Interface.Core.QPointF (c_QPointF)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.QRegExp (c_QRegExp)
import Graphics.UI.Qtah.Generator.Interface.Core.QRegularExpression (c_QRegularExpression)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QSizeF (c_QSizeF)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.QTime (c_QTime)
import Graphics.UI.Qtah.Generator.Interface.Core.QUrl (c_QUrl)
import Graphics.UI.Qtah.Generator.Interface.Core.QUuid (c_QUuid)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qlonglong, qreal, qulonglong)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QVariant"] $
  [QtExport (ExportClass c_QVariant)]

c_QVariant =
  addReqIncludes [includeStd "QVariant"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QVariant") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  --, just $ mkCtor "newFromType" [???]
  , test (qtVersion >= [5, 0]) $ mkCtor "newFromRegularExpression" [objT c_QRegularExpression]
  , just $ mkCtor "newFromUrl" [objT c_QUrl]
  , test (qtVersion >= [4, 7]) $ mkCtor "newFromEasingCurve" [objT c_QEasingCurve]
  , test (qtVersion >= [5, 0]) $ mkCtor "newFromUuid" [objT c_QUuid]
  , test (qtVersion >= [5, 0]) $ mkCtor "newFromModelIndex" [objT c_QModelIndex]
  --, test (qtVersion >= [5, 5]) $ mkCtor "newFromPersistentModelIndex" [objT c_QPersistentModelIndex]
  --, test (qtVersion >= [5, 0]) $ mkCtor "newFromJsonValue" [objT c_QJsonValue]
  --, test (qtVersion >= [5, 0]) $ mkCtor "newFromJsonObject" [objT c_QJsonObject]
  --, test (qtVersion >= [5, 0]) $ mkCtor "newFromJsonArray" [objT c_QJsonArray]
  --, test (qtVersion >= [5, 0]) $ mkCtor "newFromJsonDocument" [objT c_QJsonDocument]
  , test (qtVersion >= [5, 2]) $ mkCtor "newFromVariant" [objT c_QVariant]
  , just $ mkCtor "newFromTypeId" [intT, ptrT $ constT voidT]
  --, just $ mkCtor "newFromDataStream" [objT c_QDataStream]
  , just $ mkCtor "newFromInt" [intT]
  , just $ mkCtor "newFromUInt" [uintT]
  , just $ mkCtor "newFromLL" [qlonglong]
  , just $ mkCtor "newFromULL" [qulonglong]
  , just $ mkCtor "newFromBool" [boolT]
  , just $ mkCtor "newFromDouble" [doubleT]
  , test (qtVersion >= [4, 6]) $ mkCtor "newFromFloat" [floatT]
  , just $ mkCtor "newFromByteArray" [objT c_QByteArray]
  --, just $ mkCtor "newFromBitArray" [objT c_QBitArray]
  , just $ mkCtor "newFromString" [objT c_QString]
  --, just $ mkCtor "newFromLatin1String" [objT c_QLatin1String]
  , just $ mkCtor "newFromStringList" [objT c_QStringList]
  , just $ mkCtor "newFromChar" [objT c_QChar]
  , just $ mkCtor "newFromDate" [objT c_QDate]
  , just $ mkCtor "newFromTime" [objT c_QTime]
  , just $ mkCtor "newFromDateTime" [objT c_QDateTime]
  -- newFromList
  -- newFromMap
  -- newFromHash
  , just $ mkCtor "newFromSize" [objT c_QSize]
  , just $ mkCtor "newFromSizeF" [objT c_QSizeF]
  , just $ mkCtor "newFromPoint" [objT c_QPoint]
  , just $ mkCtor "newFromPointF" [objT c_QPointF]
  , just $ mkCtor "newFromLine" [objT c_QLine]
  , just $ mkCtor "newFromLineF" [objT c_QLineF]
  , just $ mkCtor "newFromRect" [objT c_QRect]
  , just $ mkCtor "newFromRectF" [objT c_QRectF]
  , just $ mkCtor "newFromLocale" [objT c_QLocale]
  , just $ mkCtor "newFromRegExp" [objT c_QRegExp]
  --just $ mkConstMethod' "canConvert" "canConvertByTypeId" [intT] boolT
  --, just $ mkConstMethod  "canConvert" [] boolT
  , just $ mkConstMethod  "canConvert" [intT] boolT
  , just $ mkMethod "clear" [] voidT
  , just $ mkMethod "convert" [intT] boolT
  , just $ mkConstMethod "isNull" [] boolT
  , just $ mkConstMethod "isValid" [] boolT
  -- setValue
  , test (qtVersion >= [4, 8]) $ mkMethod "swap" [refT $ objT c_QVariant] voidT
  --, just $ mkConstMethod "toBitArray" [] $ objT c_QBitArray
  , just $ mkConstMethod "toBool" [] boolT
  , just $ mkConstMethod "toByteArray" [] $ objT c_QByteArray
  , just $ mkConstMethod "toChar" [] $ objT c_QChar
  , just $ mkConstMethod "toDate" [] $ objT c_QDate
  , just $ mkConstMethod "toDateTime" [] $ objT c_QDateTime
  , just $ mkConstMethod "toDouble" [] doubleT
  , just $ mkConstMethod' "toDouble" "toDoubleCheck" [ptrT boolT] doubleT
  , test (qtVersion >= [4, 7]) $ mkConstMethod "toEasingCurve" [] $ objT c_QEasingCurve
  , test (qtVersion >= [4, 6]) $ mkConstMethod "toFloat" [] floatT
  , test (qtVersion >= [4, 6]) $ mkConstMethod' "toFloat" "toFloatCheck" [ptrT boolT] floatT
  -- toHash
  , just $ mkConstMethod "toInt" [] $ intT
  , just $ mkConstMethod' "toInt" "toIntCheck" [ptrT boolT] intT
  --, test (qtVersion >= [5, 0]) $ mkConstMethod "toJsonArray" [] $ objT c_QJsonArray
  --, test (qtVersion >= [5, 0]) $ mkConstMethod "toJsonDocument" [] $ objT c_QJsonDocument
  --, test (qtVersion >= [5, 0]) $ mkConstMethod "toJsonObject" [] $ objT c_QJsonObject
  --, test (qtVersion >= [5, 0]) $ mkConstMethod "toJsonValue" [] $ objT c_QJsonValue
  , just $ mkConstMethod "toLine" [] $ objT c_QLine
  , just $ mkConstMethod "toLineF" [] $ objT c_QLineF
  -- toList
  , just $ mkConstMethod "toLocale" [] $ objT c_QLocale
  , just $ mkConstMethod "toLongLong" [] $ qlonglong
  , just $ mkConstMethod' "toLongLong" "toLongLongCheck" [ptrT boolT] $ qlonglong
  -- toMap
  , test (qtVersion >= [5, 0]) $ mkConstMethod "toModelIndex" [] $ objT c_QModelIndex
  --, test (qtVersion >= [5, 5]) $ mkConstMethod "toPersistentModelIndex" [] $ objT c_QPersistentModelIndex
  , just $ mkConstMethod "toPoint" [] $ objT c_QPoint
  , just $ mkConstMethod "toPointF" [] $ objT c_QPointF
  , test (qtVersion >= [4, 6]) $ mkConstMethod "toReal" [] $ qreal
  , test (qtVersion >= [4, 6]) $ mkConstMethod' "toReal" "toRealCheck" [ptrT boolT] qreal
  , just $ mkConstMethod "toRect" [] $ objT c_QRect
  , just $ mkConstMethod "toRectF" [] $ objT c_QRectF
  , test (qtVersion >= [4, 1]) $ mkConstMethod "toRegExp" [] $ objT c_QRegExp
  , test (qtVersion >= [5, 0]) $ mkConstMethod "toRegularExpression" [] $ objT c_QRegularExpression
  , just $ mkConstMethod "toSize" [] $ objT c_QSize
  , just $ mkConstMethod "toSizeF" [] $ objT c_QSizeF
  , just $ mkConstMethod "toString" [] $ objT c_QString
  , just $ mkConstMethod "toStringList" [] $ objT c_QStringList
  , just $ mkConstMethod "toTime" [] $ objT c_QTime
  , just $ mkConstMethod "toUInt" [] $ uintT
  , just $ mkConstMethod' "toUInt" "toUIntCheck" [ptrT boolT] uintT
  , just $ mkConstMethod "toULongLong" [] $ qulonglong
  , just $ mkConstMethod' "toULongLong" "toULongLongCheck" [ptrT boolT] $ qulonglong
  , just $ mkConstMethod "toUrl" [] $ objT c_QUrl
  , test (qtVersion >= [5, 0]) $ mkConstMethod "toUuid" [] $ objT c_QUuid
  -- type
  , just $ mkConstMethod "typeName" [] $ ptrT $ constT charT
  , just $ mkConstMethod "userType" [] intT
  -- value
  -- TODO : operator overloads
  -- just $ mkStaticMethod "fromValue"
  -- just $ mkStaticMethod "nameToType"
  , just $ mkStaticMethod "typeToName" [intT] $ ptrT $ constT charT
  ]
