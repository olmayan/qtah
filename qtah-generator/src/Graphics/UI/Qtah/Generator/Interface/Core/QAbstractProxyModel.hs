-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QAbstractProxyModel (
  aModule,
  c_QAbstractProxyModel,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (objT, ptrT)
import Graphics.UI.Qtah.Generator.Interface.Core.QAbstractItemModel (c_QAbstractItemModel)
import Graphics.UI.Qtah.Generator.Interface.Core.QModelIndex (c_QModelIndex)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  c_Listener
  )
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 1]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QAbstractProxyModel"] minVersion $
  [ QtExport $ ExportClass c_QAbstractProxyModel
  ] ++
  map QtExportSignal signals

c_QAbstractProxyModel =
  addReqIncludes [includeStd "QAbstractProxyModel"] $
  classSetEntityPrefix "" $
  makeClass (ident "QAbstractProxyModel") Nothing [c_QAbstractItemModel]
  [ mkConstMethod "mapFromSource" [objT c_QModelIndex] $ objT c_QModelIndex
  --, mkConstMethod "mapSelectionFromSource" [objT c_QItemSelection] $ objT c_QItemSelection
  --, mkConstMethod "mapSelectionToSource" [objT c_QItemSelection] $ objT c_QItemSelection
  , mkConstMethod "mapToSource" [objT c_QModelIndex] $ objT c_QModelIndex
  , mkProp "sourceModel" $ ptrT $ objT c_QAbstractItemModel
  ]

signals =
  [ makeSignal c_QAbstractProxyModel "sourceModelChanged" c_Listener
  ]
