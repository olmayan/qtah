-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QMimeData (
  aModule,
  c_QMimeData,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (boolT, objT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.QVariant (c_QVariant)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QMimeData"] $
  map QtExport
  [ ExportClass c_QMimeData
  ]

c_QMimeData =
  addReqIncludes [includeStd "QMimeData"] $
  classSetEntityPrefix "" $
  makeClass (ident "QMimeData") Nothing [c_QObject] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkMethod "clear" [] voidT
  , just $ mkConstMethod' "data" "data_" [objT c_QString] $ objT c_QByteArray
  , just $ mkConstMethod "formats" [] $ objT c_QStringList
  , just $ mkConstMethod "hasColor" [] boolT
  , just $ mkConstMethod "hasFormat" [objT c_QString] boolT
  , just $ mkConstMethod "hasHtml" [] boolT
  , just $ mkConstMethod "hasImage" [] boolT
  , just $ mkConstMethod "hasText" [] boolT
  , just $ mkConstMethod "hasUrls" [] boolT
  , test (qtVersion >= [4, 2]) $ mkMethod "removeFormat" [objT c_QString] voidT
  , just $ mkMethod "setData" [objT c_QString, objT c_QByteArray] voidT
  , just $ mkProp "colorData" $ objT c_QVariant
  , just $ mkProp "html" $ objT c_QString
  , just $ mkProp "imageData" $ objT c_QVariant
  , just $ mkProp "text" $ objT c_QString
  --urls
  ]
