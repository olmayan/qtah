-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QDate (
  aModule,
  c_QDate
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkStaticMethod,
  mkStaticMethod',
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Comparable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  e_DateFormat,
  qint64,
  )
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QDate"] $
  (map QtExport . collect)
  [ just $ ExportClass c_QDate
  , test (qtVersion >= [4, 5]) $ ExportEnum e_MonthNameType
  ]

c_QDate =
  addReqIncludes [includeStd "QDate"] $
  classAddFeatures [Assignable, Comparable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QDate") Nothing [] $
  collect
  [ just $ mkCtor "newNull" []
  , just $ mkCtor "new" [intT, intT, intT]
  , just $ mkConstMethod "addDays" [qint64] $ objT c_QDate
  , just $ mkConstMethod "addMonths" [intT] $ objT c_QDate
  , just $ mkConstMethod "addYears" [intT] $ objT c_QDate
  , just $ mkConstMethod "day" [] intT
  , just $ mkConstMethod "dayOfWeek" [] intT
  , just $ mkConstMethod "dayOfYear" [] intT
  , just $ mkConstMethod "daysInMonth" [] intT
  , just $ mkConstMethod "daysInYear" [] intT
  , just $ mkConstMethod "daysTo" [objT c_QDate] qint64
  , test (qtVersion >= [4, 5]) $ mkConstMethod "getDate" [ptrT intT, ptrT intT, ptrT intT] voidT
  , just $ mkConstMethod "isNull" [] boolT
  , just $ mkConstMethod "isValid" [] boolT
  , just $ mkConstMethod "month" [] intT
  , test (qtVersion >= [4, 2]) $ mkMethod "setDate" [intT, intT, intT] voidT
  , just $ mkConstMethod "toJulianDay" [] qint64
  , just $ mkConstMethod' "toString" "toStringWithStringFormat" [objT c_QString] $ objT c_QString
  , just $ mkConstMethod' "toString" "toStringTextFormat" [] $ objT c_QString
  , just $ mkConstMethod "toString" [enumT e_DateFormat] $ objT c_QString
  , just $ mkConstMethod "weekNumber" [] intT
  , just $ mkConstMethod' "weekNumber" "weekNumberWithYear" [ptrT intT] intT
  , just $ mkConstMethod "year" [] intT
  , just $ mkStaticMethod "currentDate" [] $ objT c_QDate
  , just $ mkStaticMethod "fromJulianDay" [qint64] $ objT c_QDate
  , just $ mkStaticMethod' "fromString" "fromStringTextDate" [objT c_QString] $ objT c_QDate
  , just $ mkStaticMethod "fromString" [objT c_QString, enumT e_DateFormat] $ objT c_QDate
  , just $ mkStaticMethod' "fromString" "fromStringTextFormat" [objT c_QString, objT c_QString] $ objT c_QDate
  , just $ mkStaticMethod "isLeapYear" [intT] boolT
  , just $ mkStaticMethod' "isValid" "isValidYMD" [intT, intT, intT] boolT
  , test (qtVersion >= [4, 5]) $ mkStaticMethod' "longDayName" "longDayNameDateFormat" [intT] $ objT c_QString
  , test (qtVersion >= [4, 5]) $ mkStaticMethod "longDayName" [intT, enumT e_MonthNameType] $ objT c_QString
  , test (qtVersion >= [4, 5]) $ mkStaticMethod' "longMonthName" "longMonthNameDateFormat" [intT] $ objT c_QString
  , test (qtVersion >= [4, 5]) $ mkStaticMethod "longMonthName" [intT, enumT e_MonthNameType] $ objT c_QString
  , test (qtVersion >= [4, 5]) $ mkStaticMethod' "shortDayName" "shortDayNameDateFormat" [intT] $ objT c_QString
  , test (qtVersion >= [4, 5]) $ mkStaticMethod "shortDayName" [intT, enumT e_MonthNameType] $ objT c_QString
  , test (qtVersion >= [4, 5]) $ mkStaticMethod' "shortMonthName" "shortMonthNameDateFormat" [intT] $ objT c_QString
  , test (qtVersion >= [4, 5]) $ mkStaticMethod "shortMonthName" [intT, enumT e_MonthNameType] $ objT c_QString
  ]

e_MonthNameType =
  makeQtEnum (ident1 "QDate" "MonthNameType") [includeStd "QDate"]
  [ (0, ["date", "format"])
  , (1, ["standalone", "format"])
  ]
