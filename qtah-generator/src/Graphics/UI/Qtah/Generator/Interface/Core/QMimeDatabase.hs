-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QMimeDatabase (
  aModule,
  c_QMimeDatabase
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  )
import Foreign.Hoppy.Generator.Types (enumT, objT, ptrT)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QFileInfo (c_QFileInfo)
import Graphics.UI.Qtah.Generator.Interface.Core.QIODevice (c_QIODevice)
--import Graphics.UI.Qtah.Generator.Interface.Core.QList (c_QListQMimeType)
import Graphics.UI.Qtah.Generator.Interface.Core.QMimeType (c_QMimeType)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QUrl (c_QUrl)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QMimeDatabase"] minVersion
  [ QtExport $ ExportClass c_QMimeDatabase
  , QtExport $ ExportEnum e_MatchMode
  ]

c_QMimeDatabase  =
  addReqIncludes [includeStd "QMimeDatabase"] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QMimeDatabase") Nothing []
  [ mkCtor "new" []
  --, mkConstMethod "allMimeTypes" [] $ objT c_QListQMimeType
  , mkConstMethod "mimeTypeForData" [objT c_QByteArray] $ objT c_QMimeType
  , mkConstMethod' "mimeTypeForData" "mimeTypeForDataFromIODevice" [ptrT $ objT c_QIODevice] $ objT c_QMimeType
  , mkConstMethod' "mimeTypeForFile" "mimeTypeForFileFromFI" [objT c_QFileInfo] $ objT c_QMimeType
  , mkConstMethod' "mimeTypeForFile" "mimeTypeForFileFromFIWithMatchMode" [objT c_QFileInfo, enumT e_MatchMode] $ objT c_QMimeType
  , mkConstMethod "mimeTypeForFile" [objT c_QString] $ objT c_QMimeType
  , mkConstMethod' "mimeTypeForFile" "mimeTypeForFileWithMatchMode" [objT c_QString, enumT e_MatchMode] $ objT c_QMimeType
  , mkConstMethod' "mimeTypeForFileNameAndData" "mimeTypeForFileNameAndDataFromIODevice" [objT c_QString, ptrT $ objT c_QIODevice] $ objT c_QMimeType
  , mkConstMethod "mimeTypeForFileNameAndData" [objT c_QString, objT c_QByteArray] $ objT c_QMimeType
  , mkConstMethod "mimeTypeForName" [objT c_QString] $ objT c_QMimeType
  , mkConstMethod "mimeTypeForUrl" [objT c_QUrl] $ objT c_QMimeType
  --, mkConstMethod "mimeTypesForFileName" [objT c_QString] $ objT c_QListQMimeType
  , mkConstMethod "suffixForFileName" [objT c_QString] $ objT c_QString
  ]

e_MatchMode =
  makeQtEnum (ident1 "QMimeDatabase" "MatchMode") [includeStd "QMimeDatabase"]
  [ (0, ["match", "default"])
  , (1, ["match", "extension"])
  , (2, ["match", "content"])
  ]
