-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QRegularExpressionMatchIterator (
  aModule,
  c_QRegularExpressionMatchIterator,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, objT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Core.QRegularExpression (
  c_QRegularExpression,
  bs_MatchOptions,
  e_MatchType,
  )
import Graphics.UI.Qtah.Generator.Interface.Core.QRegularExpressionMatch (c_QRegularExpressionMatch)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QRegularExpressionMatchIterator"] minVersion $
  [ QtExport $ ExportClass c_QRegularExpressionMatchIterator
  ]

c_QRegularExpressionMatchIterator =
  addReqIncludes [includeStd "QRegularExpressionMatchIterator"] $
  classAddFeatures [Assignable, Copyable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QRegularExpressionMatchIterator") Nothing [] $
  collect
  [ test (qtVersion >= [5, 1]) $ mkCtor "new" []
  , just $ mkConstMethod "hasNext" [] boolT
  , just $ mkConstMethod "isValid" [] boolT
  , just $ mkConstMethod "matchOptions" [] $ bitspaceT bs_MatchOptions
  , just $ mkConstMethod "matchType" [] $ enumT e_MatchType
  , just $ mkMethod "next" [] $ objT c_QRegularExpressionMatch
  , just $ mkConstMethod "peekNext" [] $ objT c_QRegularExpressionMatch
  , just $ mkConstMethod "regularExpression" [] $ objT c_QRegularExpression
  , just $ mkMethod "swap" [refT $ objT c_QRegularExpressionMatchIterator] voidT
  ]
