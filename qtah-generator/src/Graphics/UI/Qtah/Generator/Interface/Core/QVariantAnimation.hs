-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QVariantAnimation (
  aModule,
  c_QVariantAnimation
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (intT, objT, ptrT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QAbstractAnimation (c_QAbstractAnimation)
import Graphics.UI.Qtah.Generator.Interface.Core.QEasingCurve (c_QEasingCurve)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QVariant (c_QVariant)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qreal)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 6]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QVariantAnimation"] minVersion $
  [QtExport $ ExportClass c_QVariantAnimation
  ]

c_QVariantAnimation =
  addReqIncludes [includeStd "QVariantAnimation"] $
  classSetEntityPrefix "" $
  makeClass (ident "QVariantAnimation") Nothing [c_QAbstractAnimation]
  [ mkCtor "new" []
  , mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , mkConstMethod "currentValue" [] $ objT c_QVariant
  , mkConstMethod "keyValueAt" [qreal] $ objT c_QVariant
  -- keyValues
  -- setKeyValues
  , mkMethod "setKeyValueAt" [qreal, objT c_QVariant] $ voidT
  , mkProp "duration" intT
  , mkProp "easingCurve" $ objT c_QEasingCurve
  , mkProp "endValue" $ objT c_QVariant
  , mkProp "startValue" $ objT c_QVariant
  ]
