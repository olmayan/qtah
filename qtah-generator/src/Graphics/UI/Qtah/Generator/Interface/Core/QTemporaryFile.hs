-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QTemporaryFile (
  aModule,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkCtor,
  mkMethod,
  mkProp,
  mkStaticMethod,
  mkStaticMethod',
  )
import Foreign.Hoppy.Generator.Types (boolT, objT, ptrT, refT)
import Graphics.UI.Qtah.Generator.Interface.Core.QFile (c_QFile)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QTemporaryFile"]
  [ QtExport $ ExportClass c_QTemporaryFile
  ]

c_QTemporaryFile =
  addReqIncludes [includeStd "QTemporaryFile"] $
  classSetEntityPrefix "" $
  makeClass (ident "QTemporaryFile") Nothing [c_QFile]
  [ mkCtor "new" []
  , mkCtor "newWithTemplateName" [objT c_QString]
  , mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , mkCtor "newWithTemplateNameAndParent" [objT c_QString, ptrT $ objT c_QObject]
  , mkMethod "open" [] boolT
  , mkStaticMethod "createNativeFile" [refT $ objT c_QFile] $ ptrT $ objT c_QTemporaryFile
  , mkStaticMethod' "createNativeFile" "createNativeFileWithName" [objT c_QString] $ ptrT $ objT c_QTemporaryFile
  , mkProp "autoRemove" boolT
  , mkProp "fileTemplate" $ objT c_QString
  ]
