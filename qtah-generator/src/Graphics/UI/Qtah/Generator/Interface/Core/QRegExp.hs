-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QRegExp (
  aModule,
  c_QRegExp,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkProp,
  mkStaticMethod,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (e_CaseSensitivity)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QRegExp"] $
  map QtExport
  [ ExportClass c_QRegExp
  , ExportEnum e_CaretMode
  , ExportEnum e_PatternSyntax
  ]

c_QRegExp =
  addReqIncludes [includeStd "QRegExp"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QRegExp") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithPattern" [objT c_QString]
  , just $ mkCtor "newWithPatternAndCS" [objT c_QString, enumT e_CaseSensitivity]
  , just $ mkCtor "newWithPatternAndCSAndSyntax" [objT c_QString, enumT e_CaseSensitivity, enumT e_PatternSyntax]
  , just $ mkConstMethod' "cap" "capFirst" [] $ objT c_QString
  , just $ mkConstMethod "cap" [intT] $ objT c_QString
  , test (qtVersion >= [4, 6]) $ mkConstMethod "captureCount" [] intT
  , just $ mkConstMethod "capturedTexts" [] $ objT c_QStringList
  , just $ mkConstMethod "errorString" [] $ objT c_QString
  , just $ mkStaticMethod "escape" [objT c_QString] $ objT c_QString
  , just $ mkConstMethod "exactMatch" [objT c_QString] boolT
  , just $ mkConstMethod "indexIn" [objT c_QString] intT
  , just $ mkConstMethod' "indexIn" "indexInWithOffset" [objT c_QString, intT] intT
  , just $ mkConstMethod' "indexIn" "indexInWithOffsetAndCaretMode" [objT c_QString, intT, enumT e_CaretMode] intT
  , just $ mkConstMethod "isEmpty" [] boolT
  , just $ mkConstMethod "isValid" [] boolT
  , just $ mkConstMethod "lastIndexIn" [objT c_QString] intT
  , just $ mkConstMethod' "lastIndexIn" "lastIndexInWithOffset" [objT c_QString, intT] intT
  , just $ mkConstMethod' "lastIndexIn" "lastIndexInWithOffsetAndCaretMode" [objT c_QString, intT, enumT e_CaretMode] intT
  , just $ mkConstMethod "matchedLength" [] intT
  , just $ mkConstMethod' "pos" "posFirst" [] intT
  , just $ mkConstMethod "pos" [intT] intT
  , test (qtVersion >= [4, 8]) $ mkMethod "swap" [refT $ objT c_QRegExp] voidT
  , just $ mkBoolIsProp "minimal"
  , just $ mkProp "pattern" $ objT c_QString
  , just $ mkProp "patternSyntax" $ enumT e_PatternSyntax
  ]

e_CaretMode =
  makeQtEnum (ident1 "QRegExp" "CaretMode") [includeStd "QRegExp"]
  [ (0, ["caret", "at", "zero"])
  , (1, ["caret", "at", "offset"])
  , (2, ["caret", "wont", "match"])
  ]

e_PatternSyntax =
  makeQtEnum (ident1 "QRegExp" "PatternSyntax") [includeStd "QRegExp"]
  [ (0, ["reg", "exp"])
  , (3, ["reg", "exp2"])
  , (1, ["wildcard"])
  , (4, ["wildcard", "unix"])
  , (2, ["fixed", "string"])
  , (5, ["w3", "c", "xml", "schema11"])
  ]
