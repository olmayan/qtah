-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QTime (
  aModule,
  c_QTime
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkStaticMethod,
  mkStaticMethod',
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Comparable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (e_DateFormat)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QTime"]
  [ QtExport $ ExportClass c_QTime
  ]

c_QTime =
  addReqIncludes [includeStd "QTime"] $
  classAddFeatures [Assignable, Comparable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QTime") Nothing []
  [ mkCtor "newNull" []
  , mkCtor "newHM" [intT, intT]
  , mkCtor "newHMS" [intT, intT, intT]
  , mkCtor "new" [intT, intT, intT, intT]
  , mkConstMethod "addMSecs" [intT] $ objT c_QTime
  , mkConstMethod "addSecs" [intT] $ objT c_QTime
  , mkConstMethod "elapsed" [] intT
  , mkConstMethod "isNull" [] boolT
  , mkConstMethod "isValid" [] boolT
  , mkConstMethod "minute" [] intT
  , mkConstMethod "msec" [] intT
  , mkConstMethod "msecsSinceStartOfDay" [] intT
  , mkConstMethod "msecsTo" [objT c_QTime] intT
  , mkMethod "restart" [] intT
  , mkConstMethod "second" [] intT
  , mkConstMethod "secsTo" [objT c_QTime] intT
  , mkConstMethod "setHMS" [intT, intT, intT] boolT
  , mkConstMethod' "setHMS" "setHMSWithMilliseconds" [intT, intT, intT, intT] boolT
  , mkMethod "start" [] voidT
  , mkConstMethod' "toString" "toStringWithStringFormat" [objT c_QString] $ objT c_QString
  , mkConstMethod' "toString" "toStringTextFormat" [] $ objT c_QString
  , mkConstMethod "toString" [enumT e_DateFormat] $ objT c_QString
  , mkStaticMethod "currentTime" [] $ objT c_QTime
  , mkStaticMethod "fromMSecsSinceStartOfDay" [intT] $ objT c_QTime
  , mkStaticMethod' "fromString" "fromStringTextDate" [objT c_QString] $ objT c_QTime
  , mkStaticMethod "fromString" [objT c_QString, enumT e_DateFormat] $ objT c_QTime
  , mkStaticMethod' "isValid" "isValidHMS" [intT, intT, intT] boolT
  , mkStaticMethod' "isValid" "isValidHMSWithMilliseconds" [intT, intT, intT, intT] boolT
  ]
