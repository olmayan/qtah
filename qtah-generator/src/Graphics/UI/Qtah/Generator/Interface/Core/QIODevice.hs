-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QIODevice (
  aModule,
  c_QIODevice,
  bs_OpenMode,
  ) where

import Data.Bits ((.|.))
import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, constT, charT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qint64)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  c_Listener,
  c_ListenerInt,
  c_ListenerQInt64,
  )
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QIODevice"] $
  map QtExport
  [ ExportClass c_QIODevice
  , ExportEnum e_OpenModeFlag
  , ExportBitspace bs_OpenMode
  ] ++
  map QtExportSignal signals

c_QIODevice =
  addReqIncludes [includeStd "QIODevice"] $
  classSetEntityPrefix "" $
  makeClass (ident "QIODevice") Nothing [c_QObject] $
  collect
  [ just $ mkConstMethod "atEnd" [] boolT
  , just $ mkConstMethod "bytesAvailable" [] qint64
  , just $ mkConstMethod "bytesToWrite" [] qint64
  , just $ mkConstMethod "canReadLine" [] boolT
  , just $ mkMethod "close" [] voidT
  , test (qtVersion >= [5, 7]) $ mkMethod "commitTransaction" [] voidT
  , just $ mkConstMethod "errorString" [] $ objT c_QString
  , just $ mkMethod "getChar" [ptrT charT] boolT
  , just $ mkConstMethod "isOpen" [] boolT
  , just $ mkConstMethod "isReadable" [] boolT
  , just $ mkConstMethod "isSequential" [] boolT
  , test (qtVersion >= [5, 7]) $ mkConstMethod "isTransactionStarted" [] boolT
  , just $ mkConstMethod "isWritable" [] boolT
  , just $ mkMethod "open" [bitspaceT bs_OpenMode] boolT
  , just $ mkConstMethod "openMode" [] $ bitspaceT bs_OpenMode
  , test (qtVersion >= [4, 1]) $ mkMethod' "peek" "peekRaw" [ptrT charT, qint64] qint64
  , test (qtVersion >= [4, 1]) $ mkMethod "peek" [qint64] $ objT c_QByteArray
  , just $ mkConstMethod "pos" [] qint64
  , just $ mkMethod "putChar" [charT] boolT
  , just $ mkMethod' "read" "readRaw" [ptrT charT, qint64] qint64
  , just $ mkMethod "read" [qint64] $ objT c_QByteArray
  , just $ mkMethod "readAll" [] $ objT c_QByteArray
  , test (qtVersion >= [5, 7]) $ mkConstMethod "readChannelCount" [] intT
  , just $ mkMethod' "readLine" "readLineRaw" [ptrT charT, qint64] qint64
  , just $ mkMethod "readLine" [] $ objT c_QByteArray
  , just $ mkMethod' "readLine" "readLineWithMaxSize" [qint64] $ objT c_QByteArray
  , just $ mkMethod "reset" [] boolT
  , test (qtVersion >= [5, 7]) $ mkMethod "rollbackTransaction" [] voidT
  , just $ mkMethod "seek" [qint64] boolT
  , just $ mkConstMethod "size" [] qint64
  , test (qtVersion >= [5, 7]) $ mkMethod "startTransaction" [] voidT
  , just $ mkMethod "ungetChar" [charT] voidT
  , just $ mkMethod "waitForBytesWritten" [intT] boolT
  , just $ mkMethod "waitForReadyRead" [intT] boolT
  , test (qtVersion >= [4, 5]) $ mkMethod' "write" "writeRaw" [ptrT $ constT charT] qint64
  , just $ mkMethod' "write" "writeRawWithSize" [ptrT $ constT charT, qint64] qint64
  , just $ mkMethod "write" [objT c_QByteArray] qint64
  , test (qtVersion >= [5, 7]) $ mkConstMethod "writeChannelCount" [] intT
  , test (qtVersion >= [5, 7]) $ mkProp "currentReadChannel" intT
  , test (qtVersion >= [5, 7]) $ mkProp "currentWriteChannel" intT
  , just $ mkBoolIsProp "textModeEnabled"
  ]

signals =
  collect
  [ just $ makeSignal c_QIODevice "aboutToClose" c_Listener
  , just $ makeSignal c_QIODevice "bytesWritten" c_ListenerQInt64
  --, test (qtVersion >= [5, 7]) $ makeSignal c_QIODevice "channelBytesWritten" c_ListenerIntQInt64
  , test (qtVersion >= [5, 7]) $ makeSignal c_QIODevice "channelReadyRead" c_ListenerInt
  , just $ makeSignal c_QIODevice "readChannelFinished" c_Listener
  , just $ makeSignal c_QIODevice "readyRead" c_Listener
  ]

(e_OpenModeFlag, bs_OpenMode) =
  makeQtEnumBitspace (ident1 "QIODevice" "OpenModeFlag") "OpenMode" [includeStd "QIODevice"] $
  let readOnly = 0x0001
      writeOnly = 0x0002
      readWrite = readOnly .|. writeOnly
  in
  [ (0x0000, ["not", "open"])
  , (readOnly, ["read", "only"])
  , (writeOnly, ["write", "only"])
  , (readWrite, ["read", "write"])
  , (0x0004, ["append"])
  , (0x0008, ["truncate"])
  , (0x0010, ["text"])
  , (0x0020, ["unbuffered"])
  ]
