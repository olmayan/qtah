-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QParallelAnimationGroup (
  aModule,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor
  )
import Foreign.Hoppy.Generator.Types (intT, objT, ptrT)
import Graphics.UI.Qtah.Generator.Interface.Core.QAnimationGroup(c_QAnimationGroup)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 6]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QParallelAnimationGroup"] minVersion $
  [ QtExport $ ExportClass c_QParallelAnimationGroup ]

c_QParallelAnimationGroup =
  addReqIncludes [includeStd "QParallelAnimationGroup"] $
  classSetEntityPrefix "" $
  makeClass (ident "QParallelAnimationGroup") Nothing [c_QAnimationGroup]
  [ mkCtor "new" []
  , mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , mkConstMethod "duration" [] intT
  ]
