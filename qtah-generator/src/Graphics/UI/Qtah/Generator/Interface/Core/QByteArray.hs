-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

{-# LANGUAGE CPP #-}

module Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (
  aModule,
  c_QByteArray
  ) where

#if !MIN_VERSION_base(4,8,0)
import Data.Monoid (mconcat)
#endif
import Foreign.Hoppy.Generator.Language.Haskell (
  addImports,
  indent,
  sayLn,
  )
import Foreign.Hoppy.Generator.Spec (
  ClassHaskellConversion (
    ClassHaskellConversion,
    classHaskellConversionFromCppFn,
    classHaskellConversionToCppFn,
    classHaskellConversionType
  ),
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetHaskellConversion,
  hsImports,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Comparable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (charT, constT, intT, objT, ptrT, refT, voidT)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Imports
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types
import Language.Haskell.Syntax (
  HsName (HsIdent),
  HsQName (UnQual),
  HsType (HsTyCon),
  )

aModule =
  AQtModule $
  makeQtModule ["Core", "QByteArray"]
  [ QtExport $ ExportClass c_QByteArray
  ]

c_QByteArray =
  addReqIncludes [includeStd "QByteArray"] $
  classAddFeatures [Assignable, Comparable, Copyable, Equatable] $
  classSetHaskellConversion
    ClassHaskellConversion
    { classHaskellConversionType = do
      addImports $ hsImports "Data.ByteString" ["ByteString"]
      return $ HsTyCon $ UnQual $ HsIdent "ByteString"
    , classHaskellConversionToCppFn = do
      addImports $ mconcat [hsImports "Data.ByteString" ["useAsCStringLen"],
                            importForPrelude]
      sayLn "QtahP.flip useAsCStringLen (QtahP.uncurry newWithSize)"
    , classHaskellConversionFromCppFn = do
      addImports $ hsImports "Data.ByteString" ["packCStringLen"]
      sayLn "\\q -> do"
      indent $ do
        sayLn "bytes <- data_ q"
        sayLn "size <- size q"
        sayLn "packCStringLen (bytes, size)"
    } $
  classSetEntityPrefix "" $
  makeClass (ident "QByteArray") Nothing []
  [ mkCtor "newEmpty" []
  , mkCtor "new" [ptrT $ constT charT]
  , mkCtor "newWithSize" [ptrT $ constT charT, intT]
  , mkMethod "append" [objT c_QByteArray] $ refT $ objT c_QByteArray
  --, mkMethod' "append" "appendChars" [intT, charT] $ refT $ objT c_QByteArray
  , mkMethod' "append" "appendConstChar" [ptrT $ constT charT] $ refT $ objT c_QByteArray
  --, mkMethod' "append" "appendConstCharWithLen" [ptrT $ constT charT, intT] $ refT $ objT c_QByteArray
  , mkMethod' "append" "appendChar" [charT] $ refT $ objT c_QByteArray
  , mkMethod' "append" "appendString" [objT c_QString] $ refT $ objT c_QByteArray
  , mkConstMethod "at" [intT] charT
  , mkConstMethod "capacity" [] intT
  , mkMethod "chop" [intT] voidT
  , mkConstMethod "constData" [] $ ptrT $ constT charT
  , mkConstMethod' "data" "data_" [] $ ptrT charT
  , mkConstMethod "size" [] intT
  ]
