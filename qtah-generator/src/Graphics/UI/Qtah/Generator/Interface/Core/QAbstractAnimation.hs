-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QAbstractAnimation (
  aModule,
  c_QAbstractAnimation,
  e_Direction,
  e_State
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (enumT, intT, objT, ptrT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QAnimationGroup (c_QAnimationGroup)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  c_Listener,
  c_ListenerInt,
  c_ListenerQAbstractAnimationDirection,
  c_ListenerQAbstractAnimationState
  )
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 6]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QAbstractAnimation"] minVersion $
  map QtExport
  [ ExportClass c_QAbstractAnimation
  , ExportEnum e_DeletionPolicy
  , ExportEnum e_Direction
  , ExportEnum e_State
  ] ++
  map QtExportSignal signals

c_QAbstractAnimation =
  addReqIncludes [includeStd "QAbstractAnimation"] $
  classSetEntityPrefix "" $
  makeClass (ident "QAbstractAnimation") Nothing [c_QObject]
  [ mkMethod "pause" [] voidT
  , mkMethod "resume" [] voidT
  , mkMethod "setCurrentTime" [intT] voidT
  , mkMethod "setPaused" [intT] voidT
  , mkMethod' "start" "startKeep" [] voidT
  , mkMethod "start" [enumT e_DeletionPolicy] voidT
  , mkMethod "stop" [] voidT
  , mkConstMethod "currentLoop" [] intT
  , mkConstMethod "currentLoopTime" [] intT
  , mkConstMethod "currentTime" [] intT
  , mkConstMethod "duration" [] intT
  , mkConstMethod "group" [] $ ptrT $ objT c_QAnimationGroup
  , mkConstMethod "state" [] $ enumT e_State
  , mkConstMethod "totalDuration" [] voidT
  , mkProp "direction" $ enumT e_Direction
  , mkProp "loopCount" intT
  ]

signals =
  [ makeSignal c_QAbstractAnimation "currentLoopChanged" c_ListenerInt
  , makeSignal c_QAbstractAnimation "directionChanged" c_ListenerQAbstractAnimationDirection
  , makeSignal c_QAbstractAnimation "finished" c_Listener
  , makeSignal c_QAbstractAnimation "stateChanged" c_ListenerQAbstractAnimationState
  ]

e_DeletionPolicy =
  makeQtEnum (ident1 "QAbstractAnimation" "DeletionPolicy") [includeStd "QAbstractAnimation"]
  [ (0, ["keep", "when", "stopped"])
  , (1, ["delete", "when", "stopped"])
  ]

e_Direction =
  makeQtEnum (ident1 "QAbstractAnimation" "Direction") [includeStd "QAbstractAnimation"]
  [ (0, ["forward"])
  , (1, ["backward"])
  ]

e_State =
  makeQtEnum (ident1 "QAbstractAnimation" "State") [includeStd "QAbstractAnimation"]
  [ (0, ["stopped"])
  , (1, ["paused"])
  , (2, ["running"])
  ]
