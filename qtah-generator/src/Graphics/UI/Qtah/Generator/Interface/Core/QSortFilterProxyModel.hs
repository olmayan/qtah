-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QSortFilterProxyModel (
  aModule,
  c_QSortFilterProxyModel,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QAbstractProxyModel (c_QAbstractProxyModel)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QRegExp (c_QRegExp)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  e_CaseSensitivity,
  e_SortOrder,
  )
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 1]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QSortFilterProxyModel"] minVersion
  [ QtExport $ ExportClass c_QSortFilterProxyModel
  ]

c_QSortFilterProxyModel =
  addReqIncludes [includeStd "QSortFilterProxyModel"] $
  classSetEntityPrefix "" $
  makeClass (ident "QSortFilterProxyModel") Nothing [c_QAbstractProxyModel] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , just $ mkMethod "invalidate" [] voidT
  , just $ mkMethod "setFilterFixedString" [objT c_QString] voidT
  , just $ mkMethod' "setFilterRegExp" "setFilterRegExpPattern" [objT c_QString] voidT
  , just $ mkMethod "setFilterWildcard" [objT c_QString] voidT
  , test (qtVersion >= [4, 5]) $ mkConstMethod "sortColumn" [] intT
  , test (qtVersion >= [4, 5]) $ mkConstMethod "sortOrder" [] $ enumT e_SortOrder
  , test (qtVersion >= [4, 2]) $ mkProp "dynamicSortFilter" boolT
  , just $ mkProp "filterCaseSensitivity" $ enumT e_CaseSensitivity
  , just $ mkProp "filterKeyColumn" intT
  , just $ mkProp "filterRegExp" $ objT c_QRegExp
  , test (qtVersion >= [4, 2]) $ mkProp "filterRole" intT
  , test (qtVersion >= [4, 3]) $ mkBoolIsProp "sortLocaleAware"
  , test (qtVersion >= [4, 2]) $ mkProp "sortCaseSensitivity" $ enumT e_CaseSensitivity
  , test (qtVersion >= [4, 2]) $ mkProp "sortRole" intT
  ]
