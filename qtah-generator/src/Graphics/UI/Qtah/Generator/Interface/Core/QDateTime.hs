-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QDateTime (
  aModule,
  c_QDateTime
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkProp,
  mkStaticMethod,
  mkStaticMethod',
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Comparable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, uintT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QDate (c_QDate)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QTime (c_QTime)
import Graphics.UI.Qtah.Generator.Interface.Core.QTimeZone (c_QTimeZone)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  e_DateFormat,
  e_TimeSpec,
  qint64,
  )
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QDateTime"]
  [ QtExport $ ExportClass c_QDateTime
  ]

c_QDateTime =
  addReqIncludes [includeStd "QDateTime"] $
  classAddFeatures [Assignable, Comparable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QDateTime") Nothing [] $
  collect
  [ just $ mkCtor "newNull" []
  , just $ mkCtor "newFromDate" [objT c_QDate]
  , just $ mkCtor "newFromDateTime" [objT c_QDate, objT c_QTime]
  , just $ mkCtor "newFromDateTimeWithSpec" [objT c_QDate, objT c_QTime, enumT e_TimeSpec]
  , test (qtVersion >= [5, 2]) $ mkCtor "newFromDateTimeWithSpecAndOffset" [objT c_QDate, objT c_QTime, enumT e_TimeSpec, intT]
  , test (qtVersion >= [5, 2]) $ mkCtor "newFromDateTimeWithTimeZone" [objT c_QDate, objT c_QTime, objT c_QTimeZone]
  , just $ mkConstMethod "addDays" [qint64] $ objT c_QDateTime
  , just $ mkConstMethod "addMSecs" [qint64] $ objT c_QDateTime
  , just $ mkConstMethod "addMonths" [intT] $ objT c_QDateTime
  , just $ mkConstMethod "addSecs" [qint64] $ objT c_QDateTime
  , just $ mkConstMethod "addYears" [intT] $ objT c_QDateTime
  , just $ mkConstMethod "daysTo" [objT c_QDateTime] qint64
  , test (qtVersion >= [5, 2]) $ mkConstMethod "isDaylightTime" [] boolT
  , just $ mkConstMethod "isNull" [] boolT
  , just $ mkConstMethod "isValid" [] boolT
  , just $ mkConstMethod "msecsTo" [objT c_QDateTime] qint64
  , just $ mkConstMethod "secsTo" [objT c_QDateTime] qint64
  , test (qtVersion >= [4, 7]) $ mkMethod "setMSecsSinceEpoch" [qint64] voidT
  , just $ mkMethod "setTime_t" [uintT] voidT
  , test (qtVersion >= [5, 9]) $ mkMethod "swap" [objT c_QDateTime] voidT
  , test (qtVersion >= [5, 2]) $ mkConstMethod "timeZoneAbbreviation" [] $ objT c_QString
  --(>=5.5)toCFDate
  , just $ mkConstMethod "toLocalTime" [] $ objT c_QDateTime
  , test (qtVersion >= [4, 7]) $ mkConstMethod "toMSecsSinceEpoch" [] qint64
  --(>=5.5)toNSDate
  , test (qtVersion >= [5, 2]) $ mkConstMethod "toOffsetFromUtc" [intT] $ objT c_QDateTime
  , just $ mkConstMethod' "toString" "toStringWithStringFormat" [objT c_QString] $ objT c_QString
  , just $ mkConstMethod' "toString" "toStringTextFormat" [] $ objT c_QString
  , just $ mkConstMethod "toString" [enumT e_DateFormat] $ objT c_QString
  , just $ mkConstMethod "toTimeSpec" [enumT e_TimeSpec] $ objT c_QDateTime
  , test (qtVersion >= [4, 7]) $ mkConstMethod "toTimeZone" [objT c_QTimeZone] $ objT c_QDateTime
  , just $ mkConstMethod "toTime_t" [] uintT
  , just $ mkConstMethod "toUTC" [] $ objT c_QDateTime
  , just $ mkStaticMethod "currentDateTime" [] $ objT c_QDateTime
  , test (qtVersion >= [4, 7]) $ mkStaticMethod "currentDateTimeUtc" [] $ objT c_QDateTime
  , test (qtVersion >= [4, 7]) $ mkStaticMethod "currentMSecsSinceEpoch" [] qint64
  --(>=5.5)fromCFDate
  , test (qtVersion >= [4, 7]) $ mkStaticMethod "fromMSecsSinceEpoch" [qint64] $ objT c_QDateTime
  , test (qtVersion >= [5, 2]) $ mkStaticMethod' "fromMSecsSinceEpoch" "fromMSecsSinceEpochWithSpec" [qint64, enumT e_TimeSpec, intT] $ objT c_QDateTime
  , test (qtVersion >= [5, 2]) $ mkStaticMethod' "fromMSecsSinceEpoch" "fromMSecsSinceEpochWithTimeZone" [qint64, objT c_QTimeZone] $ objT c_QDateTime
  --(>=5.5)fromNSDate
  , just $ mkStaticMethod' "fromString" "fromStringTextDate" [objT c_QString] $ objT c_QDateTime
  , just $ mkStaticMethod "fromString" [objT c_QString, enumT e_DateFormat] $ objT c_QDateTime
  , just $ mkStaticMethod' "fromString" "fromStringTextFormat" [objT c_QString, objT c_QString] $ objT c_QDateTime
  , test (qtVersion >= [4, 2]) $ mkStaticMethod "fromTime_t" [uintT] $ objT c_QDateTime
  , test (qtVersion >= [5, 2]) $ mkStaticMethod' "fromTime_t" "fromTime_tWithSpec" [uintT, enumT e_TimeSpec, intT] $ objT c_QDateTime
  , test (qtVersion >= [5, 2]) $ mkStaticMethod' "fromTime_t" "fromTime_tWithTimeZon" [uintT, objT c_QTimeZone] $ objT c_QDateTime
  , just $ mkProp "date" $ objT c_QDate
  , test (qtVersion >= [5, 2]) $ mkProp "offsetFromUtc" intT
  , just $ mkProp "time" $ objT c_QTime
  , just $ mkProp "timeSpec" $ enumT e_TimeSpec
  , test (qtVersion >= [5, 2]) $ mkProp "timeZone" $ objT c_QTimeZone
  ]
