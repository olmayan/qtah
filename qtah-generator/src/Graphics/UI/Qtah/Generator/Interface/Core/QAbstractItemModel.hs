-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QAbstractItemModel (
  aModule,
  c_QAbstractItemModel,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkMethod,
  mkMethod',
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, ptrT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QMimeData (c_QMimeData)
import Graphics.UI.Qtah.Generator.Interface.Core.QModelIndex (c_QModelIndex)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.QVariant (c_QVariant)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_DropActions,
  bs_ItemFlags,
  e_DropAction,
  e_Orientation,
  e_SortOrder,
  )
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  c_Listener,
  )
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QAbstractItemModel"] $
  map QtExport
  [ ExportClass c_QAbstractItemModel
  , ExportEnum e_DeletionPolicy
  ] ++
  map QtExportSignal signals

c_QAbstractItemModel =
  addReqIncludes [includeStd "QAbstractItemModel"] $
  classSetEntityPrefix "" $
  makeClass (ident "QAbstractItemModel") Nothing [c_QObject]
  [ mkConstMethod "buddy" [objT c_QModelIndex] $ objT c_QModelIndex
  , mkConstMethod "canDropMimeData" [ptrT $ objT c_QMimeData, enumT e_DropAction, intT, intT, objT c_QModelIndex] boolT
  , mkConstMethod "canFetchMore" [objT c_QModelIndex] boolT
  , mkConstMethod "columnCount" [objT c_QModelIndex] intT
  , mkConstMethod' "columnCount" "columnCountWithIndex" [objT c_QModelIndex] intT
  , mkConstMethod' "data" "data_" [objT c_QModelIndex] $ objT c_QVariant
  , mkConstMethod' "data" "dataWithRole" [objT c_QModelIndex, intT] $ objT c_QVariant
  , mkMethod "dropMimeData" [ptrT $ objT c_QMimeData, enumT e_DropAction, intT, intT, objT c_QModelIndex] boolT
  , mkMethod "fetchMore" [objT c_QModelIndex] voidT
  , mkConstMethod "flags" [objT c_QModelIndex] $ bitspaceT bs_ItemFlags
  , mkConstMethod "hasChildren" [] boolT
  , mkConstMethod' "hasChildren" "hasChildrenWithParent" [objT c_QModelIndex] boolT
  , mkConstMethod "hasIndex" [intT, intT] boolT
  , mkConstMethod' "hasIndex" "hasIndexWithParent" [intT, intT, objT c_QModelIndex] boolT
  , mkConstMethod "headerData" [intT, enumT e_Orientation] $ objT c_QVariant
  , mkConstMethod' "headerData" "headerDataWithRole" [intT, enumT e_Orientation, intT] $ objT c_QVariant
  , mkConstMethod "index" [intT, intT] $ objT c_QModelIndex
  , mkConstMethod' "index" "indexWithParent" [intT, intT, objT c_QModelIndex] $ objT c_QModelIndex
  , mkMethod "insertColumn" [intT] boolT
  , mkMethod' "insertColumn" "insertColumnWithParent" [intT, objT c_QModelIndex] boolT
  , mkMethod "insertColumns" [intT, intT] boolT
  , mkMethod' "insertColumns" "insertColumnsWithParent" [intT, intT, objT c_QModelIndex] boolT
  , mkMethod "insertRow" [intT] boolT
  , mkMethod' "insertRow" "insertRowWithParent" [intT, objT c_QModelIndex] boolT
  , mkMethod "insertRows" [intT, intT] boolT
  , mkMethod' "insertRows" "insertRowsWithParent" [intT, intT, objT c_QModelIndex] boolT
  --itemData
  --match
  --mimeData
  , mkConstMethod "mimeTypes" [] $ objT c_QStringList
  , mkMethod "moveColumn" [objT c_QModelIndex, intT, objT c_QModelIndex, intT] boolT
  , mkMethod "moveColumns" [objT c_QModelIndex, intT, intT, objT c_QModelIndex, intT] boolT
  , mkMethod "moveRow" [objT c_QModelIndex, intT, objT c_QModelIndex, intT] boolT
  , mkMethod "moveRows" [objT c_QModelIndex, intT, intT, objT c_QModelIndex, intT] boolT
  , mkConstMethod "parent" [objT c_QModelIndex] $ objT c_QModelIndex
  , mkMethod "removeColumn" [intT] boolT
  , mkMethod' "removeColumn" "removeColumnWithParent" [intT, objT c_QModelIndex] boolT
  , mkMethod "removeColumns" [intT, intT] boolT
  , mkMethod' "removeColumns" "removeColumnsWithParent" [intT, intT, objT c_QModelIndex] boolT
  , mkMethod "removeRow" [intT] boolT
  , mkMethod' "removeRow" "removeRowWithParent" [intT, objT c_QModelIndex] boolT
  , mkMethod "removeRows" [intT, intT] boolT
  , mkMethod' "removeRows" "removeRowsWithParent" [intT, intT, objT c_QModelIndex] boolT
  , mkMethod "revert" [] voidT
  --roleNames
  , mkConstMethod "rowCount" [] intT
  , mkConstMethod' "rowCount" "rowCountWithParent" [objT c_QModelIndex] intT
  , mkMethod "setData" [objT c_QModelIndex, objT c_QVariant] voidT
  , mkMethod' "setData" "setDataWithRole" [objT c_QModelIndex, objT c_QVariant, intT] voidT
  , mkMethod "setHeaderData" [intT, enumT e_Orientation, objT c_QVariant] voidT
  , mkMethod' "setHeaderData" "setHeaderDataWithRole" [intT, enumT e_Orientation, objT c_QVariant, intT] voidT
  --setItemData
  , mkConstMethod "sibling" [intT, intT, objT c_QModelIndex] $ objT c_QModelIndex
  , mkMethod' "sort" "sortAscending" [intT] voidT
  , mkMethod "sort" [intT, enumT e_SortOrder] voidT
  , mkConstMethod "span" [objT c_QModelIndex] $ objT c_QSize
  , mkMethod "submit" [] voidT
  , mkConstMethod "supportedDragActions" [] $ bitspaceT bs_DropActions
  , mkConstMethod "supportedDropActions" [] $ bitspaceT bs_DropActions
  ]

signals =
  [ --columnsAboutToBeInserted
    --columnsAboutToBeMoved
    --columnsAboutToBeRemoved
    --columnsInserted
    --columnsMoved
    --columnsRemoved
    --dataChanged
    --headerDataChanged
    --layoutAboutToBeChanged
    --layoutChanged
    makeSignal c_QAbstractItemModel "modelAboutToBeReset" c_Listener
  , makeSignal c_QAbstractItemModel "modelReset" c_Listener
    --rowsAboutToBeInserted
    --rowsAboutToBeMoved
    --rowsAboutToBeRemoved
    --rowsInserted
    --rowsMoved
    --rowsRemoved
  ]

e_DeletionPolicy =
  makeQtEnum (ident1 "QAbstractItemModel" "LayoutChangeHint") [includeStd "QAbstractItemModel"]
  [ (0, ["no", "layout", "change", "hint"])
  , (1, ["vertical", "sort", "hint"])
  , (2, ["horizontal", "sort", "hint"])
  ]
