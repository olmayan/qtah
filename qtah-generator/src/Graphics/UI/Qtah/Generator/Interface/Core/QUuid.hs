-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QUuid (
  aModule,
  c_QUuid,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkStaticMethod,
  mkStaticMethod',
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Comparable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, objT, ucharT, uintT, ushortT)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QUuid"] $
  map QtExport
  [ ExportClass c_QUuid
  , ExportEnum e_Variant
  , ExportEnum e_Version
  ]

c_QUuid =
  addReqIncludes [includeStd "QUuid"] $
  classAddFeatures [Assignable, Comparable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QUuid") Nothing []
  [ mkCtor "newNull" []
  , mkCtor "new" $ [uintT, ushortT, ushortT] ++ replicate 8 ucharT
  , mkCtor "newFromString" [objT c_QString]
  , mkCtor "newFromBytes" [objT c_QByteArray]
  --newFromGUID
  , mkConstMethod "isNull" [] boolT
  , mkConstMethod "toByteArray" [] $ objT c_QByteArray
  --toCFUUID
  --toNSUUID
  , mkConstMethod "toRfc4122" [] $ objT c_QByteArray
  , mkConstMethod "toString" [] $ objT c_QString
  , mkConstMethod "variant" [] $ enumT e_Variant
  , mkConstMethod "version" [] $ enumT e_Version
  --operator GUID
  , mkStaticMethod "createUuid" [] $ objT c_QUuid
  , mkStaticMethod' "createUuidV3" "createUuidV3WithBytes" [objT c_QUuid, objT c_QByteArray] $ objT c_QUuid
  , mkStaticMethod "createUuidV3" [objT c_QUuid, objT c_QString] $ objT c_QUuid
  , mkStaticMethod' "createUuidV5" "createUuidV5WithBytes" [objT c_QUuid, objT c_QByteArray] $ objT c_QUuid
  , mkStaticMethod "createUuidV5" [objT c_QUuid, objT c_QString] $ objT c_QUuid
  --fromCFUUID
  --fromNSUUID
  , mkStaticMethod "fromRfc4122" [objT c_QByteArray] $ objT c_QUuid
  ]

e_Variant =
  makeQtEnum (ident1 "QUuid" "Variant") [includeStd "QUuid"]
  [ (-1, ["var", "unknown"])
  , (0, ["n", "c", "s"])
  , (2, ["d", "c", "e"])
  , (6, ["microsoft"])
  , (7, ["reserved"])
  ]

e_Version =
  makeQtEnum (ident1 "QUuid" "Version") [includeStd "QUuid"]
  [ (-1, ["ver", "unknown"])
  , (1, ["time"])
  , (2, ["embedded", "p", "o", "s", "i", "x"])
  , (3, ["md5"])
  , (4, ["random"])
  , (5, ["sha1"])
  ]
