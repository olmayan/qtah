-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QUrlQuery (
  aModule,
  c_QUrlQuery
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkStaticMethod,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, objT, refT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QChar (c_QChar)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Core.QUrl (
  bs_ComponentFormattingOptions,
  c_QUrl,
  )
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QUrlQuery"] minVersion $
  [ QtExport $ ExportClass c_QUrlQuery
  ]

c_QUrlQuery =
  addReqIncludes [includeStd "QUrlQuery"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QUrlQuery") Nothing []
  [ mkCtor "newEmpty" []
  , mkCtor "newWithUrl" [objT c_QUrl]
  , mkCtor "new" [objT c_QString]
  , mkMethod "addQueryItem" [objT c_QString, objT c_QString] voidT
  , mkConstMethod' "allQueryItemValues" "allQueryItemValuesPrettyDecoded" [objT c_QString] $ objT c_QStringList
  , mkConstMethod "allQueryItemValues" [objT c_QString, bitspaceT bs_ComponentFormattingOptions] $ objT c_QStringList
  , mkMethod "clear" [] voidT
  , mkConstMethod "hasQueryItem" [objT c_QString] boolT
  , mkConstMethod "isEmpty" [] boolT
  , mkConstMethod' "query" "queryPrettyDecoded" [] $ objT c_QString
  , mkConstMethod "query" [bitspaceT bs_ComponentFormattingOptions] $ objT c_QString
  , mkConstMethod' "queryItemValue" "queryItemValuePrettyDecoded" [objT c_QString] $ objT c_QString
  , mkConstMethod "queryItemValue" [objT c_QString, bitspaceT bs_ComponentFormattingOptions] $ objT c_QString
  --queryItems
  , mkConstMethod "queryPairDelimiter" [] $ objT c_QChar
  , mkConstMethod "queryValueDelimiter" [] $ objT c_QChar
  , mkMethod "removeAllQueryItems" [objT c_QString] voidT
  , mkMethod "removeQueryItem" [objT c_QString] voidT
  , mkMethod "setQuery" [objT c_QString] voidT
  , mkMethod "setQueryDelimiters" [objT c_QChar, objT c_QChar] voidT
  --setQueryItems
  , mkMethod "swap" [refT $ objT c_QUrlQuery] voidT
  , mkConstMethod' "toString" "toStringPrettyDecoded" [] $ objT c_QString
  , mkConstMethod "toString" [bitspaceT bs_ComponentFormattingOptions] $ objT c_QString
  , mkStaticMethod "defaultQueryPairDelimiter" [] $ objT c_QChar
  , mkStaticMethod "defaultQueryValueDelimiter" [] $ objT c_QChar
  ]
