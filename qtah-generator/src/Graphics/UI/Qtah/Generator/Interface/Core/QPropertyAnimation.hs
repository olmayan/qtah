-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QPropertyAnimation (
  aModule,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkCtor,
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (intT, objT, ptrT)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QEasingCurve (c_QEasingCurve)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QVariant (c_QVariant)
import Graphics.UI.Qtah.Generator.Interface.Core.QVariantAnimation (c_QVariantAnimation)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 6]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QPropertyAnimation"] minVersion $
  [ QtExport $ ExportClass c_QPropertyAnimation
  ]

c_QPropertyAnimation =
  addReqIncludes [includeStd "QPropertyAnimation"] $
  classSetEntityPrefix "" $
  makeClass (ident "QPropertyAnimation") Nothing [c_QVariantAnimation]
  [ mkCtor "new" []
  , mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , mkCtor "newWithProperty" [ptrT $ objT c_QObject, objT c_QByteArray]
  , mkCtor "newWithPropertyAndParent" [ptrT $ objT c_QObject, objT c_QByteArray, ptrT $ objT c_QObject]
  , mkProp "duration" $ intT
  , mkProp "easingCurve" $ objT c_QEasingCurve
  , mkProp "endValue" $ objT c_QVariant
  , mkProp "propertyName" $ objT c_QByteArray
  , mkProp "startValue" $ objT c_QVariant
  , mkProp "targetObject" $ ptrT $ objT c_QObject
  ]
