-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QFileSystemModel (
  aModule,
  c_QFileSystemModel,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, intT, objT, ptrT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QAbstractItemModel (c_QAbstractItemModel)
import Graphics.UI.Qtah.Generator.Interface.Core.QDateTime (c_QDateTime)
import Graphics.UI.Qtah.Generator.Interface.Core.QDir (
  bs_Filters,
  c_QDir,
  )
import Graphics.UI.Qtah.Generator.Interface.Core.QFile (bs_Permissions)
import Graphics.UI.Qtah.Generator.Interface.Core.QFileInfo (c_QFileInfo)
import Graphics.UI.Qtah.Generator.Interface.Core.QModelIndex (c_QModelIndex)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.QVariant (c_QVariant)
import Graphics.UI.Qtah.Generator.Interface.Gui.QIcon (c_QIcon)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  c_ListenerQString,
  c_ListenerQStringQStringQString,
  )
import Graphics.UI.Qtah.Generator.Interface.Widgets.QFileIconProvider (c_QFileIconProvider)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 4]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QFileSystemModel"] minVersion $
  [ QtExport $ ExportClass c_QFileSystemModel
  ] ++
  map QtExportSignal signals

c_QFileSystemModel =
  addReqIncludes [includeStd "QFileSystemModel"] $
  classSetEntityPrefix "" $
  makeClass (ident "QFileSystemModel") Nothing [c_QAbstractItemModel]
  [ mkCtor "new" []
  , mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , mkConstMethod "fileIcon" [objT c_QModelIndex] $ objT c_QIcon
  , mkConstMethod "fileInfo" [objT c_QModelIndex] $ objT c_QFileInfo
  , mkConstMethod "fileName" [objT c_QModelIndex] $ objT c_QString
  , mkConstMethod "filePath" [objT c_QModelIndex] $ objT c_QString
  , mkConstMethod "index" [objT c_QString] $ objT c_QModelIndex
  , mkConstMethod' "index" "indexWithColumn" [objT c_QString, intT] $ objT c_QModelIndex
  , mkConstMethod "isDir" [objT c_QModelIndex] boolT
  , mkConstMethod "lastModified" [objT c_QModelIndex] $ objT c_QDateTime
  , mkMethod "mkdir" [objT c_QModelIndex, objT c_QString] $ objT c_QModelIndex
  , mkConstMethod "myComputer" [] $ objT c_QVariant
  , mkConstMethod' "myComputer" "myComputerWithRole" [intT] $ objT c_QVariant
  , mkConstMethod "permissions" [objT c_QModelIndex] $ bitspaceT bs_Permissions
  , mkMethod "remove" [objT c_QModelIndex] boolT
  , mkMethod "rmdir" [objT c_QModelIndex] boolT
  , mkConstMethod "rootDirectory" [] $ objT c_QDir
  , mkConstMethod "rootPath" [] $ objT c_QString
  , mkMethod "setRootPath" [objT c_QString] $ objT c_QModelIndex
  , mkConstMethod' "type" "type_" [objT c_QModelIndex] $ objT c_QString
  , mkProp "filter" $ bitspaceT bs_Filters
  , mkProp "iconProvider" $ ptrT $ objT c_QFileIconProvider
  , mkProp "nameFilters" $ objT c_QStringList
  , mkBoolIsProp "readOnly"
  ]

signals =
  collect
  [ test (qtVersion >= [4, 7]) $ makeSignal c_QFileSystemModel "directoryLoaded" c_ListenerQString
  , just $ makeSignal c_QFileSystemModel "fileRenamed" c_ListenerQStringQStringQString
  , just $ makeSignal c_QFileSystemModel "rootPathChanged" c_ListenerQString
  ]
