-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QMimeType (
  aModule,
  c_QMimeType
  ) where

import Foreign.Hoppy.Generator.Language.Haskell (
  addImports,
  indent,
  sayLn,
  )
import Foreign.Hoppy.Generator.Spec (
  ClassHaskellConversion (
    ClassHaskellConversion,
    classHaskellConversionFromCppFn,
    classHaskellConversionToCppFn,
    classHaskellConversionType
  ),
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  classSetHaskellConversion,
  hsImports,
  hsImportSetMakeSource,
  hsQualifiedImport,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, objT, refT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Imports
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types
import Language.Haskell.Syntax (
  HsName (HsIdent),
  HsQName (UnQual),
  HsType (HsTyCon),
  )

minVersion = [5, 0]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QMimeType"] minVersion
  [ QtExport $ ExportClass c_QMimeType
  ]

c_QMimeType =
  addReqIncludes [includeStd "QMimeType"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  --TODO revert this to `toGc` after bug fix.
  classSetConversionToGc $
  classSetHaskellConversion
    ClassHaskellConversion
    { classHaskellConversionType = do
      addImports $ hsQualifiedImport "Graphics.UI.Qtah.Core.HMimeType" "HMimeType"
      return $ HsTyCon $ UnQual $ HsIdent "HMimeType.HMimeType"
    , classHaskellConversionToCppFn = do
      addImports $ mconcat [importForPrelude, hsImports "Prelude" ["(>>=)"],
                            --hsImportSetMakeSource $ -- to avoid circular import
                            hsImports "Graphics.UI.Qtah.Generated.Core.QMimeDatabase" [],
                            hsQualifiedImport
                            "Graphics.UI.Qtah.Core.QMimeDatabase" "QMimeDatabase",
                            hsQualifiedImport
                            "Graphics.UI.Qtah.Core.HMimeType" "HMimeType"]
      sayLn "\\(HMimeType.HMimeType s) -> do"
      indent $ do
        sayLn "db <- QMimeDatabase.new"
        sayLn "QMimeDatabase.mimeTypeForName db s >>= newCopy"
    , classHaskellConversionFromCppFn = do
      addImports $ mconcat [importForPrelude, hsQualifiedImport
                            "Graphics.UI.Qtah.Core.HMimeType" "HMimeType"]
      sayLn "\\q -> qMimeType_name q >>= (QtahP.return . HMimeType.HMimeType)"
    } $
  classSetEntityPrefix "" $
  makeClass (ident "QMimeType") Nothing []
  [ mkCtor "new" []
  , mkConstMethod "aliases" [] $ objT c_QStringList
  , mkConstMethod "allAncestors" [] $ objT c_QStringList
  , mkConstMethod "comment" [] $ objT c_QString
  , mkConstMethod "filterString" [] $ objT c_QString
  , mkConstMethod "genericIconName" [] $ objT c_QString
  , mkConstMethod "globPatterns" [] $ objT c_QStringList
  , mkConstMethod "iconName" [] $ objT c_QString
  , mkConstMethod "inherits" [objT c_QString] boolT
  , mkConstMethod "isDefault" [] boolT
  , mkConstMethod "isValid" [] boolT
  , mkConstMethod "name" [] $ objT c_QString
  , mkConstMethod "parentMimeTypes" [] $ objT c_QStringList
  , mkConstMethod "preferredSuffix" [] $ objT c_QString
  , mkConstMethod "suffixes" [] $ objT c_QStringList
  , mkMethod "swap" [refT $ objT c_QMimeType] voidT
  ]
