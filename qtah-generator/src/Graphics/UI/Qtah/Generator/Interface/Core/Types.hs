-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

-- | Top-level bindings and bindings in the @Qt::@ namespace.
module Graphics.UI.Qtah.Generator.Interface.Core.Types (
  aModule,
  qint8,
  qint16,
  qint32,
  qint64,
  qlonglong,
  qptrdiff,
  qreal,
  quint8,
  quint16,
  quint32,
  quint64,
  qulonglong,
  quintptr,
  e_AlignmentFlag,
  bs_Alignment,
  e_AnchorPoint,
  e_ApplicationAttribute,
  e_ApplicationState,
  bs_ApplicationStates,
  e_ArrowType,
  e_AspectRatioMode,
  e_Axis,
  e_BGMode,
  e_BrushStyle,
  e_CaseSensitivity,
  e_CheckState,
  e_ClipOperation,
  e_ConnectionType,
  e_ContextMenuPolicy,
  e_CoordinateSystem,
  e_Corner,
  e_CursorMoveStyle,
  e_CursorShape,
  e_DateFormat,
  e_DayOfWeek,
  e_DockWidgetArea,
  bs_DockWidgetAreas,
  e_DropAction,
  bs_DropActions,
  e_Edge,
  bs_Edges,
  e_EnterKeyType,
  e_EventPriority,
  e_FillRule,
  e_FindChildOption,
  bs_FindChildOptions,
  e_FocusPolicy,
  e_FocusReason,
  e_GestureFlag,
  bs_GestureFlags,
  e_GestureState,
  e_GestureType,
  e_GlobalColor,
  e_HitTestAccuracy,
  e_ImageConversionFlag,
  bs_ImageConversionFlags,
  e_InputMethodHint,
  bs_InputMethodHints,
  e_InputMethodQuery,
  bs_InputMethodQueries,
  e_ItemDataRole,
  e_ItemFlag,
  bs_ItemFlags,
  e_ItemSelectionMode,
  e_ItemSelectionOperation,
  e_Key,
  e_KeyboardModifier,
  bs_KeyboardModifiers,
  e_LayoutDirection,
  e_MaskMode,
  e_MatchFlag,
  bs_MatchFlags,
  e_Modifier,
  e_MouseButton,
  bs_MouseButtons,
  e_MouseEventFlag,
  bs_MouseEventFlags,
  e_MouseEventSource,
  e_NativeGestureType,
  e_NavigationMode,
  e_Orientation,
  bs_Orientations,
  e_PenCapStyle,
  e_PenJoinStyle,
  e_PenStyle,
  e_ScreenOrientation,
  bs_ScreenOrientations,
  e_ScrollBarPolicy,
  e_ScrollPhase,
  e_ShortcutContext,
  e_SizeHint,
  e_SizeMode,
  e_SortOrder,
  e_TabFocusBehavior,
  e_TextElideMode,
  e_TextFlag,
  e_TextFormat,
  e_TextInteractionFlag,
  bs_TextInteractionFlags,
  e_TileRule,
  e_TimeSpec,
  e_TimerType,
  e_ToolBarArea,
  bs_ToolBarAreas,
  e_ToolButtonStyle,
  e_TouchPointState,
  bs_TouchPointStates,
  e_TransformationMode,
  e_UIEffect,
  e_WhiteSpaceMode,
  e_WidgetAttribute,
  e_WindowFrameSection,
  e_WindowModality,
  e_WindowState,
  bs_WindowStates,
  e_WindowType,
  bs_WindowFlags,
  ) where

import Data.Bits ((.|.), finiteBitSize)
import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportEnum, ExportFn),
  Include,
  Purity (Nonpure),
  Type,
  addReqIncludes,
  ident1,
  includeStd,
  makeFn,
  )
import Foreign.Hoppy.Generator.Types (
    charT, doubleT, floatT, intT,
    llongT, objT, ptrdiffT, ptrT,
    shortT, ucharT, uintT, ullongT,
    ushortT, voidT
    )
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qrealFloat, qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

{-# ANN module "HLint: ignore Use camelCase" #-}

aModule :: AModule
aModule = AQtModule $ makeQtModule ["Core", "Types"] exports

exports :: [QtExport]
exports =
  QtExportSpecials :
  (map QtExport . collect)
  [ just $ ExportEnum e_AlignmentFlag
  , just $ ExportBitspace bs_Alignment
  , just $ ExportEnum e_AnchorPoint
  , just $ ExportEnum e_ApplicationAttribute
  , just $ ExportEnum e_ApplicationState
  , just $ ExportBitspace bs_ApplicationStates
  , just $ ExportEnum e_ArrowType
  , just $ ExportEnum e_AspectRatioMode
  , just $ ExportEnum e_Axis
  , just $ ExportEnum e_BGMode
  , just $ ExportEnum e_BrushStyle
  , just $ ExportEnum e_CaseSensitivity
  , just $ ExportEnum e_CheckState
  , just $ ExportEnum e_ClipOperation
  , just $ ExportEnum e_ConnectionType
  , just $ ExportEnum e_ContextMenuPolicy
  , just $ ExportEnum e_CoordinateSystem
  , just $ ExportEnum e_Corner
  , just $ ExportEnum e_CursorMoveStyle
  , just $ ExportEnum e_CursorShape
  , just $ ExportEnum e_DateFormat
  , just $ ExportEnum e_DayOfWeek
  , just $ ExportEnum e_DockWidgetArea
  , just $ ExportBitspace bs_DockWidgetAreas
  , just $ ExportEnum e_DropAction
  , just $ ExportBitspace bs_DropActions
  , test (qtVersion >= e_Edge_version) $ ExportEnum e_Edge
  , test (qtVersion >= e_Edge_version) $ ExportBitspace bs_Edges
  , test (qtVersion >= e_EnterKeyType_version) $ ExportEnum e_EnterKeyType
  , just $ ExportEnum e_EventPriority
  , just $ ExportEnum e_FillRule
  , just $ ExportEnum e_FindChildOption
  , just $ ExportBitspace bs_FindChildOptions
  , just $ ExportEnum e_FocusPolicy
  , just $ ExportEnum e_FocusReason
  , test (qtVersion >= e_GestureFlag_version) $ ExportEnum e_GestureFlag
  , test (qtVersion >= e_GestureFlag_version) $ ExportBitspace bs_GestureFlags
  , test (qtVersion >= e_GestureState_version) $ ExportEnum e_GestureState
  , test (qtVersion >= e_GestureType_version) $ ExportEnum e_GestureType
  , just $ ExportEnum e_GlobalColor
  , just $ ExportEnum e_HitTestAccuracy
  , just $ ExportEnum e_ImageConversionFlag
  , just $ ExportBitspace bs_ImageConversionFlags
  , just $ ExportEnum e_InputMethodHint
  , just $ ExportBitspace bs_InputMethodHints
  , just $ ExportEnum e_InputMethodQuery
  , just $ ExportBitspace bs_InputMethodQueries
  , just $ ExportEnum e_ItemDataRole
  , just $ ExportEnum e_ItemFlag
  , just $ ExportBitspace bs_ItemFlags
  , test (qtVersion >= e_ItemSelectionMode_version) $ ExportEnum e_ItemSelectionMode
  , test (qtVersion >= e_ItemSelectionOperation_version) $ ExportEnum e_ItemSelectionOperation
  , just $ ExportEnum e_Key
  , just $ ExportEnum e_KeyboardModifier
  , just $ ExportBitspace bs_KeyboardModifiers
  , just $ ExportEnum e_LayoutDirection
  , just $ ExportEnum e_MaskMode
  , just $ ExportEnum e_MatchFlag
  , just $ ExportBitspace bs_MatchFlags
  , just $ ExportEnum e_Modifier
  , just $ ExportEnum e_MouseButton
  , just $ ExportBitspace bs_MouseButtons
  , test (qtVersion >= e_MouseEventFlag_version) $ ExportEnum e_MouseEventFlag
  , test (qtVersion >= e_MouseEventFlag_version) $ ExportBitspace bs_MouseEventFlags
  , test (qtVersion >= e_MouseEventSource_version) $ ExportEnum e_MouseEventSource
  , test (qtVersion >= e_NativeGestureType_version) $ ExportEnum e_NativeGestureType
  , test (qtVersion >= e_NavigationMode_version) $ ExportEnum e_NavigationMode
  , just $ ExportEnum e_Orientation
  , just $ ExportBitspace bs_Orientations
  , just $ ExportEnum e_PenCapStyle
  , just $ ExportEnum e_PenJoinStyle
  , just $ ExportEnum e_PenStyle
  , just $ ExportEnum e_ScreenOrientation
  , just $ ExportBitspace bs_ScreenOrientations
  , just $ ExportEnum e_ScrollBarPolicy
  , test (qtVersion >= e_ScrollPhase_version) $ ExportEnum e_ScrollPhase
  , just $ ExportEnum e_ShortcutContext
  , test (qtVersion >= e_SizeHint_version) $ ExportEnum e_SizeHint
  , test (qtVersion >= e_SizeMode_version) $ ExportEnum e_SizeMode
  , just $ ExportEnum e_SortOrder
  , test (qtVersion >= e_TabFocusBehavior_version) $ ExportEnum e_TabFocusBehavior
  , just $ ExportEnum e_TextElideMode
  , just $ ExportEnum e_TextFlag
  , just $ ExportEnum e_TextFormat
  , just $ ExportEnum e_TextInteractionFlag
  , just $ ExportBitspace bs_TextInteractionFlags
  , test (qtVersion >= e_TileRule_version) $ ExportEnum e_TileRule
  , just $ ExportEnum e_TimeSpec
  , just $ ExportEnum e_TimerType
  , just $ ExportEnum e_ToolBarArea
  , just $ ExportBitspace bs_ToolBarAreas
  , just $ ExportEnum e_ToolButtonStyle
  , test (qtVersion >= e_TouchPointState_version) $ ExportEnum e_TouchPointState
  , test (qtVersion >= e_TouchPointState_version) $ ExportBitspace bs_TouchPointStates
  , just $ ExportEnum e_TransformationMode
  , just $ ExportEnum e_UIEffect
  , just $ ExportEnum e_WhiteSpaceMode
  , just $ ExportEnum e_WidgetAttribute
  , test (qtVersion >= e_WindowFrameSection_version) $ ExportEnum e_WindowFrameSection
  , just $ ExportEnum e_WindowModality
  , just $ ExportEnum e_WindowState
  , just $ ExportBitspace bs_WindowStates
  , just $ ExportEnum e_WindowType
  , just $ ExportBitspace bs_WindowFlags
  , test (qtVersion < [5, 0]) $ ExportFn f_escape
  ]

qtInclude :: [Include]
qtInclude = [includeStd "Qt"]

qint8 :: Type
qint8 = charT

qint16 :: Type
qint16 = shortT

qint32 :: Type
qint32 = intT

qint64 :: Type
qint64 = llongT

qlonglong :: Type
qlonglong = llongT

qptrdiff :: Type
qptrdiff = ptrdiffT

qreal :: Type
qreal = if qrealFloat then floatT else doubleT

quint8 :: Type
quint8 = ucharT

quint16 :: Type
quint16 = ushortT

quint32 :: Type
quint32 = uintT

quint64 :: Type
quint64 = ullongT

qulonglong :: Type
qulonglong = ullongT

-- Not sure about it
quintptr :: Type
quintptr = if finiteBitSize (undefined :: Int) == 64 then ullongT else uintT

(e_AlignmentFlag, bs_Alignment) =
  makeQtEnumBitspace (ident1 "Qt" "AlignmentFlag") "Alignment" qtInclude
  [ -- Horizontal flags.
    (0x01, ["align", "left"])
  , (0x02, ["align", "right"])
  , (0x04, ["align", "h", "center"])
  , (0x08, ["align", "justify"])
    -- Vertical flags.
  , (0x20, ["align", "top"])
  , (0x40, ["align", "bottom"])
  , (0x80, ["align", "v", "center"])
    -- Useful in right-to-left mode.
  , (0x10, ["align", "absolute"])
  ]

e_AnchorPoint =
  makeQtEnum (ident1 "Qt" "AnchorPoint") qtInclude
  [ (0, ["anchor", "left"])
  , (1, ["anchor", "horizontal", "center"])
  , (2, ["anchor", "right"])
  , (3, ["anchor", "top"])
  , (4, ["anchor", "vertical", "center"])
  , (5, ["anchor", "bottom"])
  ]

e_ApplicationAttribute =
  makeQtEnum (ident1 "Qt" "ApplicationAttribute") qtInclude $
  collect
  [ just $ (0, ["a", "a_", "immediate", "widget", "creation"])
  , test (qtVersion < [5, 0]) $ (1, ["a", "a_", "ms", "windows", "use", "direct3", "dby", "default"])
  , just $ (2, ["a", "a_", "dont", "show", "icons", "in", "menus"])
  , just $ (3, ["a", "a_", "native", "windows"])
  , just $ (4, ["a", "a_", "dont", "create", "native", "widget", "siblings"])
  , just $ (5, ["a", "a_", "plugin", "application"])
  --, just $ (5, ["a", "a_", "mac", "plugin", "application"])
  , just $ (6, ["a", "a_", "dont", "use", "native", "menu", "bar"])
  , just $ (7, ["a", "a_", "mac", "dont", "swap", "ctrl", "and", "meta"])
  , just $ (8, ["a", "a_", "use", "96", "dpi"])
  , just $ (10, ["a", "a_", "x11", "init", "threads"])
  , just $ (11, ["a", "a_", "synthesize", "touch", "for", "unhandled", "mouse", "events"])
  , just $ (12, ["a", "a_", "synthesize", "mouse", "for", "unhandled", "touch", "events"])
  , just $ (13, ["a", "a_", "use", "high", "dpi", "pixmaps"])
  , just $ (14, ["a", "a_", "force", "raster", "widgets"])
  , just $ (15, ["a", "a_", "use", "desktop", "open", "g", "l"])
  , just $ (16, ["a", "a_", "use", "open", "g", "l", "e", "s"])
  , just $ (17, ["a", "a_", "use", "software", "open", "g", "l"])
  , just $ (18, ["a", "a_", "share", "open", "g", "l", "contexts"])
  , just $ (19, ["a", "a_", "set", "palette"])
  , just $ (20, ["a", "a_", "enable", "high", "dpi", "scaling"])
  , just $ (21, ["a", "a_", "disable", "high", "dpi", "scaling"])
  , just $ (22, ["a", "a_", "use", "style", "sheet", "propagation", "in", "widget", "styles"])
  , just $ (23, ["a", "a_", "dont", "use", "native", "dialogs"])
  , just $ (24, ["a", "a_", "synthesize", "mouse", "for", "unhandled", "tablet", "events"])
  , just $ (25, ["a", "a_", "compress", "high", "frequency", "events"])
  ]

(e_ApplicationState, bs_ApplicationStates) =
  makeQtEnumBitspace (ident1 "Qt" "ApplicationState") "ApplicationStates" qtInclude
  [ (0x0, ["application", "suspended"])
  , (0x1, ["application", "hidden"])
  , (0x2, ["application", "inactive"])
  , (0x4, ["application", "active"])
  ]

e_ArrowType =
  makeQtEnum (ident1 "Qt" "ArrowType") qtInclude
  [ (0, ["no", "arrow"])
  , (1, ["up", "arrow"])
  , (2, ["down", "arrow"])
  , (3, ["left", "arrow"])
  , (4, ["right", "arrow"])
  ]

e_AspectRatioMode =
  makeQtEnum (ident1 "Qt" "AspectRatioMode") qtInclude
  [ (0, ["ignore", "aspect", "ratio"])
  , (1, ["keep", "aspect", "ratio"])
  , (2, ["keep", "aspect", "ratio", "by", "expanding"])
  ]

e_Axis =
  makeQtEnum (ident1 "Qt" "Axis") qtInclude
  [ (0, ["x", "axis"])
  , (1, ["y", "axis"])
  , (2, ["z", "axis"])
  ]

e_BGMode =
  makeQtEnum (ident1 "Qt" "BGMode") qtInclude
  [ (0, ["transparent", "mode"])
  , (1, ["opaque", "mode"])
  ]

e_BrushStyle =
  makeQtEnum (ident1 "Qt" "BrushStyle") qtInclude
  [ (0, ["no", "brush"])
  , (1, ["solid", "pattern"])
  , (2, ["dense", "1", "pattern"])
  , (3, ["dense", "2", "pattern"])
  , (4, ["dense", "3", "pattern"])
  , (5, ["dense", "4", "pattern"])
  , (6, ["dense", "5", "pattern"])
  , (7, ["dense", "6", "pattern"])
  , (8, ["dense", "7", "pattern"])
  , (9, ["hor", "pattern"])
  , (10, ["ver", "pattern"])
  , (11, ["cross", "pattern"])
  , (12, ["b", "diag", "pattern"])
  , (13, ["f", "diag", "pattern"])
  , (14, ["diag", "cross", "pattern"])
  , (15, ["linear", "gradient", "pattern"])
  , (16, ["radial", "gradient", "pattern"])
  , (17, ["conical", "gradient", "pattern"])
  , (24, ["texture", "pattern"])
  ]

e_CaseSensitivity =
  makeQtEnum (ident1 "Qt" "CaseSensitivity") qtInclude
  [ (0, ["case", "insensitive"])
  , (1, ["case", "sensitive"])
  ]

e_CheckState =
  makeQtEnum (ident1 "Qt" "CheckState") qtInclude
  [ (0, ["unchecked"])
  , (1, ["partially", "checked"])
  , (2, ["checked"])
  ]

e_ClipOperation =
  makeQtEnum (ident1 "Qt" "ClipOperation") qtInclude
  [ (0, ["no", "clip"])
  , (1, ["replace", "clip"])
  , (2, ["intersect", "clip"])
  ]

e_ConnectionType =
  makeQtEnum (ident1 "Qt" "ConnectionType") qtInclude
  [ (0, ["auto", "connection"])
  , (1, ["direct", "connection"])
  , (2, ["queued", "connection"])
  , (3, ["blocking", "queued", "connection"])
  , (0x80, ["unique", "connection"])
  ]

e_ContextMenuPolicy =
  makeQtEnum (ident1 "Qt" "ContextMenuPolicy") qtInclude
  [ (0, ["no", "context", "menu"])
  , (1, ["default", "context", "menu"])
  , (2, ["actions", "context", "menu"])
  , (3, ["custom", "context", "menu"])
  , (4, ["prevent", "context", "menu"])
  ]

e_CoordinateSystem =
  makeQtEnum (ident1 "Qt" "CoordinateSystem") qtInclude
  [ (0, ["device", "coordinates"])
  , (1, ["logical", "coordinates"])
  ]

e_Corner =
  makeQtEnum (ident1 "Qt" "Corner") qtInclude
  [ (0x00000, ["top", "left", "corner"])
  , (0x00001, ["top", "right", "corner"])
  , (0x00002, ["bottom", "left", "corner"])
  , (0x00003, ["bottom", "right", "corner"])
  ]

e_CursorMoveStyle =
  makeQtEnum (ident1 "Qt" "CursorMoveStyle") qtInclude
  [ (0, ["logical", "move", "style"])
  , (1, ["visual", "move", "style"])
  ]

e_CursorShape =
  makeQtEnum (ident1 "Qt" "CursorShape") qtInclude
  [ (0, ["arrow", "cursor"])
  , (1, ["up", "arrow", "cursor"])
  , (2, ["cross", "cursor"])
  , (3, ["wait", "cursor"])
  , (4, ["i", "beam", "cursor"])
  , (5, ["size", "ver", "cursor"])
  , (6, ["size", "hor", "cursor"])
  , (7, ["size", "b", "diag", "cursor"])
  , (8, ["size", "f", "diag", "cursor"])
  , (9, ["size", "all", "cursor"])
  , (10, ["blank", "cursor"])
  , (11, ["split", "v", "cursor"])
  , (12, ["split", "h", "cursor"])
  , (13, ["pointing", "hand", "cursor"])
  , (14, ["forbidden", "cursor"])
  , (15, ["whats", "this", "cursor"])
  , (16, ["busy", "cursor"])
  , (17, ["open", "hand", "cursor"])
  , (18, ["closed", "hand", "cursor"])
  , (19, ["drag", "copy", "cursor"])
  , (20, ["drag", "move", "cursor"])
  , (21, ["drag", "link", "cursor"])
  , (24, ["bitmap", "cursor"])
  ]

-- These values may be incorrect
e_DateFormat =
  makeQtEnum (ident1 "Qt" "DateFormat") qtInclude
  [ (0, ["text", "date"])
  , (1, ["i", "s", "o", "date"])
  --, (2, ["system", "locale", "date"])
  , (2, ["local", "date"])
  , (3, ["locale", "date"])
  , (4, ["system", "locale", "short", "date"])
  , (5, ["system", "locale", "long", "date"])
  , (6, ["default", "locale", "short", "date"])
  , (7, ["default", "locale", "long", "date"])
  , (8, ["r", "f", "c", "2822", "date"])
  ]

e_DayOfWeek =
  makeQtEnum (ident1 "Qt" "DayOfWeek") qtInclude
  [ (1, ["monday"])
  , (2, ["tuesday"])
  , (3, ["wednesday"])
  , (4, ["thursday"])
  , (5, ["friday"])
  , (6, ["saturday"])
  , (7, ["sunday"])
  ]

(e_DockWidgetArea, bs_DockWidgetAreas) =
  makeQtEnumBitspace (ident1 "Qt" "DockWidgetArea") "DockWidgetAreas" qtInclude $
  let left = 0x1
      right = 0x2
      top = 0x4
      bottom = 0x8
      all = left .|. right .|. top .|. bottom
      no = 0
  in
  [ (left, ["left", "dock", "widget", "area"])
  , (right, ["right", "dock", "widget", "area"])
  , (top, ["top", "dock", "widget", "area"])
  , (bottom, ["bottom", "dock", "widget", "area"])
  , (all, ["all", "dock", "widget", "areas"])
  , (no, ["no", "dock", "widget", "area"])
  ]

(e_DropAction, bs_DropActions) =
  makeQtEnumBitspace (ident1 "Qt" "DropAction") "DropActions" qtInclude
  [ (0x1, ["copy", "action"])
  , (0x2, ["move", "action"])
  , (0x4, ["link", "action"])
  , (0xff, ["action", "mask"])
  , (0x0, ["ignore", "action"])
  , (0x8002, ["target", "move", "action"])
  ]

e_Edge_version = [5, 1]

(e_Edge, bs_Edges) =
  makeQtEnumBitspace (ident1 "Qt" "Edge") "Edges" qtInclude
  [ (0x1, ["top", "edge"])
  , (0x2, ["left", "edge"])
  , (0x4, ["right", "edge"])
  , (0x8, ["bottom", "edge"])
  ]

e_EnterKeyType_version = [5, 6]

e_EnterKeyType =
  makeQtEnum (ident1 "Qt" "EnterKeyType") qtInclude
  [ (0, ["EnterKeyDefault"])
  , (1, ["EnterKeyReturn"])
  , (2, ["EnterKeyDone"])
  , (3, ["EnterKeyGo"])
  , (4, ["EnterKeySend"])
  , (5, ["EnterKeySearch"])
  , (6, ["EnterKeyNext"])
  , (7, ["EnterKeyPrevious"])
  ]

e_EventPriority =
  makeQtEnum (ident1 "Qt" "EventPriority") qtInclude
  [ (1, ["high", "event", "priority"])
  , (0, ["normal", "event", "priority"])
  , (-1, ["low", "event", "priority"])
  ]

e_FillRule =
  makeQtEnum (ident1 "Qt" "FillRule") qtInclude
  [ (0, ["odd", "even", "fill"])
  , (1, ["winding", "fill"])
  ]

(e_FindChildOption, bs_FindChildOptions) =
  makeQtEnumBitspace (ident1 "Qt" "FindChildOption") "FindChildOptions" qtInclude
  [ (0x0, ["find", "direct", "children", "only"])
  , (0x1, ["find", "children", "recursively"])
  ]

e_FocusPolicy =
  makeQtEnum (ident1 "Qt" "FocusPolicy") qtInclude $
  let tabFocus = 0x1
      clickFocus = 0x2
      strongFocus = tabFocus .|. clickFocus .|. 0x8
      wheelFocus = strongFocus .|. 0x4
      noFocus = 0
  in
  [ (tabFocus, ["tab", "focus"])
  , (clickFocus, ["click", "focus"])
  , (strongFocus, ["strong", "focus"])
  , (wheelFocus, ["wheel", "focus"])
  , (noFocus, ["no", "focus"])
  ]

e_FocusReason =
  makeQtEnum (ident1 "Qt" "FocusReason") qtInclude
  [ (0, ["mouse", "focus", "reason"])
  , (1, ["tab", "focus", "reason"])
  , (2, ["backtab", "focus", "reason"])
  , (3, ["active", "window", "focus", "reason"])
  , (4, ["popup", "focus", "reason"])
  , (5, ["shortcut", "focus", "reason"])
  , (6, ["menu", "bar", "focus", "reason"])
  , (7, ["other", "focus", "reason"])
  ]

e_GestureFlag_version = [4, 6]

(e_GestureFlag, bs_GestureFlags) =
  makeQtEnumBitspace (ident1 "Qt" "GestureFlag") "GestureFlags" qtInclude
  [ (0x1, ["dont", "start", "gesture", "on", "children"])
  , (0x2, ["receive", "partial", "gestures"])
  , (0x4, ["ignored", "gestures", "propagate", "to", "parent"])
  ]

e_GestureState_version = [4, 6]

e_GestureState =
  makeQtEnum (ident1 "Qt" "GestureState") qtInclude
  [ (0, ["no", "gesture"])
  , (1, ["gesture", "started"])
  , (2, ["gesture", "updated"])
  , (3, ["gesture", "finished"])
  , (4, ["gesture", "canceled"])
  ]

e_GestureType_version = [4, 6]

e_GestureType =
  makeQtEnum (ident1 "Qt" "GestureType") qtInclude
  [ (1, ["tap", "gesture"])
  , (2, ["tap", "and", "hold", "gesture"])
  , (3, ["pan", "gesture"])
  , (4, ["pinch", "gesture"])
  , (5, ["swipe", "gesture"])
  , (0x0100, ["custom", "gesture"])
  ]

e_GlobalColor =
  makeQtEnum (ident1 "Qt" "GlobalColor") qtInclude
  [ (3, ["white"])
  , (2, ["black"])
  , (7, ["red"])
  , (13, ["dark", "red"])
  , (8, ["green"])
  , (14, ["dark", "green"])
  , (9, ["blue"])
  , (15, ["dark", "blue"])
  , (10, ["cyan"])
  , (16, ["dark", "cyan"])
  , (11, ["magenta"])
  , (17, ["dark", "magenta"])
  , (12, ["yellow"])
  , (18, ["dark", "yellow"])
  , (5, ["gray"])
  , (4, ["dark", "gray"])
  , (6, ["light", "gray"])
  , (19, ["transparent"])
  , (0, ["color0"])
  , (1, ["color1"])
  ]

e_HitTestAccuracy =
  makeQtEnum (ident1 "Qt" "HitTestAccuracy") qtInclude
  [ (0, ["exact", "hit"])
  , (1, ["fuzzy", "hit"])
  ]

(e_ImageConversionFlag, bs_ImageConversionFlags) =
  makeQtEnumBitspace (ident1 "Qt" "ImageConversionFlag") "ImageConversionFlags" qtInclude
  [ -- Color/Mono preference.
    (0x00000000, ["auto", "color"])
  , (0x00000003, ["color", "only"])
  , (0x00000002, ["mono", "only"])
    -- Dithering mode preference for RGB channels.
  --, (0x00000000, ["diffuse", "dither"])
  , (0x00000010, ["ordered", "dither"])
  , (0x00000020 , ["threshold", "dither"])
    -- Dithering mode preference for alpha channel.
  --, (0x00000000, ["threshold", "alpha", "dither"])
  , (0x00000004, ["ordered", "alpha", "dither"])
  , (0x00000008, ["diffuse", "alpha", "dither"])
    -- Dithering mode preference for alpha channel.
  , (0x00000040, ["prefer", "dither"])
  , (0x00000080, ["avoid", "dither"])
  , (0x00000100, ["no", "opaque", "detection"])
  , (0x00000200, ["no", "format", "conversion"])
  ]

(e_InputMethodHint, bs_InputMethodHints) =
  makeQtEnumBitspace (ident1 "Qt" "InputMethodHint") "InputMethodHints" qtInclude
  [ (0x0, ["imh", "none"])
  , (0x1, ["imh", "hidden", "text"])
  , (0x2, ["imh", "sensitive", "data"])
  , (0x4, ["imh", "no", "auto", "uppercase"])
  , (0x8, ["imh", "prefer", "numbers"])
  , (0x10, ["imh", "prefer", "uppercase"])
  , (0x20, ["imh", "prefer", "lowercase"])
  , (0x40, ["imh", "no", "predictive", "text"])
  , (0x80, ["imh", "date"])
  , (0x100, ["imh", "time"])
  , (0x200, ["imh", "prefer", "latin"])
  , (0x400, ["imh", "multi", "line"])
  , (0x10000, ["imh", "digits", "only"])
  , (0x20000, ["imh", "formatted", "numbers", "only"])
  , (0x40000, ["imh", "uppercase", "only"])
  , (0x80000, ["imh", "lowercase", "only"])
  , (0x100000, ["imh", "dialable", "characters", "only"])
  , (0x200000, ["imh", "email", "characters", "only"])
  , (0x400000, ["imh", "url", "characters", "only"])
  , (0x800000, ["imh", "latin", "only"])
  , (0xffff0000, ["imh", "exclusive", "input", "mask"])
  ]

(e_InputMethodQuery, bs_InputMethodQueries) =
  makeQtEnumBitspace (ident1 "Qt" "InputMethodQuery") "InputMethodQueries" qtInclude $
  let imCursorRectangle = 0x2
      imCursorPosition = 0x8
      imSurroundingText = 0x10
      imCurrentSelection = 0x20
      imAnchorRectangle = 0x4000
      imAnchorPosition = 0x80
      imQueryInput = imCursorRectangle .|. imCursorPosition .|. imSurroundingText .|.
                     imCurrentSelection .|. imAnchorRectangle .|. imAnchorPosition
  in
  [ (0x1, ["im", "enabled"])
  --, (imCursorRectangle, ["im", "micro", "focus"])
  , (imCursorRectangle, ["im", "cursor", "rectangle"])
  , (0x4, ["im", "font"])
  , (imCursorPosition, ["im", "cursor", "position"])
  , (imSurroundingText, ["im", "surrounding", "text"])
  , (imCurrentSelection, ["im", "current", "selection"])
  , (0x40, ["im", "maximum", "text", "length"])
  , (imAnchorPosition, ["im", "anchor", "position"])
  , (0x100, ["im", "hints"])
  , (0x200, ["im", "preferred", "language"])
  , (0x80000000, ["im", "platform", "data"])
  , (0x400, ["im", "absolute", "position"])
  , (0x800, ["im", "text", "before", "cursor"])
  , (0x1000, ["im", "text", "after", "cursor"])
  , (0x2000, ["im", "enter", "key", "type"])
  , (imAnchorRectangle, ["im", "anchor", "rectangle"])
  , (0x8000, ["im", "input", "item", "clip", "rectangle"])
  , (imQueryInput, ["im", "query", "input"])
  , (0xffffffff, ["im", "query", "all"])
  ]

e_ItemDataRole =
  makeQtEnum (ident1 "Qt" "ItemDataRole") qtInclude
  [ (0, ["display", "role"])
  , (1, ["decoration", "role"])
  , (2, ["edit", "role"])
  , (3, ["tool", "tip", "role"])
  , (4, ["status", "tip", "role"])
  , (5, ["whats", "this", "role"])
  , (13, ["size", "hint", "role"])
  , (6, ["font", "role"])
  , (7, ["text", "alignment", "role"])
  , (8, ["background", "role"])
  --, (8, ["background", "color", "role"])
  , (9, ["foreground", "role"])
  , (9, ["text", "color", "role"])
  , (10, ["check", "state", "role"])
  , (14, ["initial", "sort", "order", "role"])
  , (11, ["accessible", "text", "role"])
  , (12, ["accessible", "description", "role"])
  , (0x0100, ["user", "role"])
  ]

(e_ItemFlag, bs_ItemFlags) =
  makeQtEnumBitspace (ident1 "Qt" "ItemFlag") "ItemFlags" qtInclude
  [ (0, ["no", "item", "flags"])
  , (1, ["item", "is", "selectable"])
  , (2, ["item", "is", "editable"])
  , (4, ["item", "is", "drag", "enabled"])
  , (8, ["item", "is", "drop", "enabled"])
  , (16, ["item", "is", "user", "checkable"])
  , (32, ["item", "is", "enabled"])
  , (64, ["item", "is", "auto", "tristate"])
  --, (64, ["item", "is", "tristate"])
  , (128, ["item", "never", "has", "children"])
  , (256, ["item", "is", "user", "tristate"])
  ]

e_ItemSelectionMode_version = [4, 2]

e_ItemSelectionMode =
  makeQtEnum (ident1 "Qt" "ItemSelectionMode") qtInclude
  [ (0x0, ["contains", "item", "shape"])
  , (0x1, ["intersects", "item", "shape"])
  , (0x2, ["contains", "item", "bounding", "rect"])
  , (0x3, ["intersects", "item", "bounding", "rect"])
  ]

e_ItemSelectionOperation_version = [4, 2]

e_ItemSelectionOperation =
  makeQtEnum (ident1 "Qt" "ItemSelectionOperation") qtInclude
  [ (0, ["replace", "selection"])
  , (1, ["add", "to", "selection"])
  ]

e_Key =
  makeQtEnum (ident1 "Qt" "Key") qtInclude
  [ (0x01000000, ["key_", "escape"])
  , (0x01000001, ["key_", "tab"])
  , (0x01000002, ["key_", "backtab"])
  , (0x01000003, ["key_", "backspace"])
  , (0x01000004, ["key_", "return"])
  , (0x01000005, ["key_", "enter"])
  , (0x01000006, ["key_", "insert"])
  , (0x01000007, ["key_", "delete"])
  , (0x01000008, ["key_", "pause"])
  , (0x01000009, ["key_", "print"])
  , (0x0100000a, ["key_", "sys", "req"])
  , (0x0100000b, ["key_", "clear"])
  , (0x01000010, ["key_", "home"])
  , (0x01000011, ["key_", "end"])
  , (0x01000012, ["key_", "left"])
  , (0x01000013, ["key_", "up"])
  , (0x01000014, ["key_", "right"])
  , (0x01000015, ["key_", "down"])
  , (0x01000016, ["key_", "page", "up"])
  , (0x01000017, ["key_", "page", "down"])
  , (0x01000020, ["key_", "shift"])
  , (0x01000021, ["key_", "control"])
  , (0x01000022, ["key_", "meta"])
  , (0x01000023, ["key_", "alt"])
  , (0x01001103, ["key_", "alt", "gr"])
  , (0x01000024, ["key_", "caps", "lock"])
  , (0x01000025, ["key_", "num", "lock"])
  , (0x01000026, ["key_", "scroll", "lock"])
  , (0x01000030, ["key_", "f1"])
  , (0x01000031, ["key_", "f2"])
  , (0x01000032, ["key_", "f3"])
  , (0x01000033, ["key_", "f4"])
  , (0x01000034, ["key_", "f5"])
  , (0x01000035, ["key_", "f6"])
  , (0x01000036, ["key_", "f7"])
  , (0x01000037, ["key_", "f8"])
  , (0x01000038, ["key_", "f9"])
  , (0x01000039, ["key_", "f10"])
  , (0x0100003a, ["key_", "f11"])
  , (0x0100003b, ["key_", "f12"])
  , (0x0100003c, ["key_", "f13"])
  , (0x0100003d, ["key_", "f14"])
  , (0x0100003e, ["key_", "f15"])
  , (0x0100003f, ["key_", "f16"])
  , (0x01000040, ["key_", "f17"])
  , (0x01000041, ["key_", "f18"])
  , (0x01000042, ["key_", "f19"])
  , (0x01000043, ["key_", "f20"])
  , (0x01000044, ["key_", "f21"])
  , (0x01000045, ["key_", "f22"])
  , (0x01000046, ["key_", "f23"])
  , (0x01000047, ["key_", "f24"])
  , (0x01000048, ["key_", "f25"])
  , (0x01000049, ["key_", "f26"])
  , (0x0100004a, ["key_", "f27"])
  , (0x0100004b, ["key_", "f28"])
  , (0x0100004c, ["key_", "f29"])
  , (0x0100004d, ["key_", "f30"])
  , (0x0100004e, ["key_", "f31"])
  , (0x0100004f, ["key_", "f32"])
  , (0x01000050, ["key_", "f33"])
  , (0x01000051, ["key_", "f34"])
  , (0x01000052, ["key_", "f35"])
  , (0x01000053, ["key_", "super_", "l"])
  , (0x01000054, ["key_", "super_", "r"])
  , (0x01000055, ["key_", "menu"])
  , (0x01000056, ["key_", "hyper_", "l"])
  , (0x01000057, ["key_", "hyper_", "r"])
  , (0x01000058, ["key_", "help"])
  , (0x01000059, ["key_", "direction_", "l"])
  , (0x01000060, ["key_", "direction_", "r"])
  , (0x20, ["key_", "space"])
  --, (0x20, ["key_", "any"])
  , (0x21, ["key_", "exclam"])
  , (0x22, ["key_", "quote", "dbl"])
  , (0x23, ["key_", "number", "sign"])
  , (0x24, ["key_", "dollar"])
  , (0x25, ["key_", "percent"])
  , (0x26, ["key_", "ampersand"])
  , (0x27, ["key_", "apostrophe"])
  , (0x28, ["key_", "paren", "left"])
  , (0x29, ["key_", "paren", "right"])
  , (0x2a, ["key_", "asterisk"])
  , (0x2b, ["key_", "plus"])
  , (0x2c, ["key_", "comma"])
  , (0x2d, ["key_", "minus"])
  , (0x2e, ["key_", "period"])
  , (0x2f, ["key_", "slash"])
  , (0x30, ["key_0"])
  , (0x31, ["key_1"])
  , (0x32, ["key_2"])
  , (0x33, ["key_3"])
  , (0x34, ["key_4"])
  , (0x35, ["key_5"])
  , (0x36, ["key_6"])
  , (0x37, ["key_7"])
  , (0x38, ["key_8"])
  , (0x39, ["key_9"])
  , (0x3a, ["key_", "colon"])
  , (0x3b, ["key_", "semicolon"])
  , (0x3c, ["key_", "less"])
  , (0x3d, ["key_", "equal"])
  , (0x3e, ["key_", "greater"])
  , (0x3f, ["key_", "question"])
  , (0x40, ["key_", "at"])
  , (0x41, ["key_", "a"])
  , (0x42, ["key_", "b"])
  , (0x43, ["key_", "c"])
  , (0x44, ["key_", "d"])
  , (0x45, ["key_", "e"])
  , (0x46, ["key_", "f"])
  , (0x47, ["key_", "g"])
  , (0x48, ["key_", "h"])
  , (0x49, ["key_", "i"])
  , (0x4a, ["key_", "j"])
  , (0x4b, ["key_", "k"])
  , (0x4c, ["key_", "l"])
  , (0x4d, ["key_", "m"])
  , (0x4e, ["key_", "n"])
  , (0x4f, ["key_", "o"])
  , (0x50, ["key_", "p"])
  , (0x51, ["key_", "q"])
  , (0x52, ["key_", "r"])
  , (0x53, ["key_", "s"])
  , (0x54, ["key_", "t"])
  , (0x55, ["key_", "u"])
  , (0x56, ["key_", "v"])
  , (0x57, ["key_", "w"])
  , (0x58, ["key_", "x"])
  , (0x59, ["key_", "y"])
  , (0x5a, ["key_", "z"])
  , (0x5b, ["key_", "bracket", "left"])
  , (0x5c, ["key_", "backslash"])
  , (0x5d, ["key_", "bracket", "right"])
  , (0x5e, ["key_", "ascii", "circum"])
  , (0x5f, ["key_", "underscore"])
  , (0x60, ["key_", "quote", "left"])
  , (0x7b, ["key_", "brace", "left"])
  , (0x7c, ["key_", "bar"])
  , (0x7d, ["key_", "brace", "right"])
  , (0x7e, ["key_", "ascii", "tilde"])
  , (0x0a0, ["key_nobreakspace"])
  , (0x0a1, ["key_exclamdown"])
  , (0x0a2, ["key_cent"])
  , (0x0a3, ["key_sterling"])
  , (0x0a4, ["key_currency"])
  , (0x0a5, ["key_yen"])
  , (0x0a6, ["key_brokenbar"])
  , (0x0a7, ["key_section"])
  , (0x0a8, ["key_diaeresis"])
  , (0x0a9, ["key_copyright"])
  , (0x0aa, ["key_ordfeminine"])
  , (0x0ab, ["key_guillemotleft"])
  , (0x0ac, ["key_notsign"])
  , (0x0ad, ["key_hyphen"])
  , (0x0ae, ["key_registered"])
  , (0x0af, ["key_macron"])
  , (0x0b0, ["key_degree"])
  , (0x0b1, ["key_plusminus"])
  , (0x0b2, ["key_twosuperior"])
  , (0x0b3, ["key_threesuperior"])
  , (0x0b4, ["key_acute"])
  , (0x0b5, ["key_mu"])
  , (0x0b6, ["key_paragraph"])
  , (0x0b7, ["key_periodcentered"])
  , (0x0b8, ["key_cedilla"])
  , (0x0b9, ["key_onesuperior"])
  , (0x0ba, ["key_masculine"])
  , (0x0bb, ["key_guillemotright"])
  , (0x0bc, ["key_onequarter"])
  , (0x0bd, ["key_onehalf"])
  , (0x0be, ["key_threequarters"])
  , (0x0bf, ["key_questiondown"])
  , (0x0c0, ["key_", "agrave"])
  , (0x0c1, ["key_", "aacute"])
  , (0x0c2, ["key_", "acircumflex"])
  , (0x0c3, ["key_", "atilde"])
  , (0x0c4, ["key_", "adiaeresis"])
  , (0x0c5, ["key_", "aring"])
  , (0x0c6, ["key_", "ae"])
  , (0x0c7, ["key_", "ccedilla"])
  , (0x0c8, ["key_", "egrave"])
  , (0x0c9, ["key_", "eacute"])
  , (0x0ca, ["key_", "ecircumflex"])
  , (0x0cb, ["key_", "ediaeresis"])
  , (0x0cc, ["key_", "igrave"])
  , (0x0cd, ["key_", "iacute"])
  , (0x0ce, ["key_", "icircumflex"])
  , (0x0cf, ["key_", "idiaeresis"])
  , (0x0d0, ["key_", "et", "h"])
  , (0x0d1, ["key_", "ntilde"])
  , (0x0d2, ["key_", "ograve"])
  , (0x0d3, ["key_", "oacute"])
  , (0x0d4, ["key_", "ocircumflex"])
  , (0x0d5, ["key_", "otilde"])
  , (0x0d6, ["key_", "odiaeresis"])
  , (0x0d7, ["key_multiply"])
  , (0x0d8, ["key_", "ooblique"])
  , (0x0d9, ["key_", "ugrave"])
  , (0x0da, ["key_", "uacute"])
  , (0x0db, ["key_", "ucircumflex"])
  , (0x0dc, ["key_", "udiaeresis"])
  , (0x0dd, ["key_", "yacute"])
  , (0x0de, ["key_", "t", "h", "o", "r", "n"])
  , (0x0df, ["key_ssharp"])
  , (0x0f7, ["key_division"])
  , (0x0ff, ["key_ydiaeresis"])
  , (0x01001120, ["key_", "multi_key"])
  , (0x01001137, ["key_", "codeinput"])
  , (0x0100113c, ["key_", "single", "candidate"])
  , (0x0100113d, ["key_", "multiple", "candidate"])
  , (0x0100113e, ["key_", "previous", "candidate"])
  , (0x0100117e, ["key_", "mode_switch"])
  , (0x01001121, ["key_", "kanji"])
  , (0x01001122, ["key_", "muhenkan"])
  , (0x01001123, ["key_", "henkan"])
  , (0x01001124, ["key_", "romaji"])
  , (0x01001125, ["key_", "hiragana"])
  , (0x01001126, ["key_", "katakana"])
  , (0x01001127, ["key_", "hiragana_", "katakana"])
  , (0x01001128, ["key_", "zenkaku"])
  , (0x01001129, ["key_", "hankaku"])
  , (0x0100112a, ["key_", "zenkaku_", "hankaku"])
  , (0x0100112b, ["key_", "touroku"])
  , (0x0100112c, ["key_", "massyo"])
  , (0x0100112d, ["key_", "kana_", "lock"])
  , (0x0100112e, ["key_", "kana_", "shift"])
  , (0x0100112f, ["key_", "eisu_", "shift"])
  , (0x01001130, ["key_", "eisu_toggle"])
  , (0x01001131, ["key_", "hangul"])
  , (0x01001132, ["key_", "hangul_", "start"])
  , (0x01001133, ["key_", "hangul_", "end"])
  , (0x01001134, ["key_", "hangul_", "hanja"])
  , (0x01001135, ["key_", "hangul_", "jamo"])
  , (0x01001136, ["key_", "hangul_", "romaja"])
  , (0x01001138, ["key_", "hangul_", "jeonja"])
  , (0x01001139, ["key_", "hangul_", "banja"])
  , (0x0100113a, ["key_", "hangul_", "pre", "hanja"])
  , (0x0100113b, ["key_", "hangul_", "post", "hanja"])
  , (0x0100113f, ["key_", "hangul_", "special"])
  , (0x01001250, ["key_", "dead_", "grave"])
  , (0x01001251, ["key_", "dead_", "acute"])
  , (0x01001252, ["key_", "dead_", "circumflex"])
  , (0x01001253, ["key_", "dead_", "tilde"])
  , (0x01001254, ["key_", "dead_", "macron"])
  , (0x01001255, ["key_", "dead_", "breve"])
  , (0x01001256, ["key_", "dead_", "abovedot"])
  , (0x01001257, ["key_", "dead_", "diaeresis"])
  , (0x01001258, ["key_", "dead_", "abovering"])
  , (0x01001259, ["key_", "dead_", "doubleacute"])
  , (0x0100125a, ["key_", "dead_", "caron"])
  , (0x0100125b, ["key_", "dead_", "cedilla"])
  , (0x0100125c, ["key_", "dead_", "ogonek"])
  , (0x0100125d, ["key_", "dead_", "iota"])
  , (0x0100125e, ["key_", "dead_", "voiced_", "sound"])
  , (0x0100125f, ["key_", "dead_", "semivoiced_", "sound"])
  , (0x01001260, ["key_", "dead_", "belowdot"])
  , (0x01001261, ["key_", "dead_", "hook"])
  , (0x01001262, ["key_", "dead_", "horn"])
  , (0x01000061, ["key_", "back"])
  , (0x01000062, ["key_", "forward"])
  , (0x01000063, ["key_", "stop"])
  , (0x01000064, ["key_", "refresh"])
  , (0x01000070, ["key_", "volume", "down"])
  , (0x01000071, ["key_", "volume", "mute"])
  , (0x01000072, ["key_", "volume", "up"])
  , (0x01000073, ["key_", "bass", "boost"])
  , (0x01000074, ["key_", "bass", "up"])
  , (0x01000075, ["key_", "bass", "down"])
  , (0x01000076, ["key_", "treble", "up"])
  , (0x01000077, ["key_", "treble", "down"])
  , (0x01000080, ["key_", "media", "play"])
  , (0x01000081, ["key_", "media", "stop"])
  , (0x01000082, ["key_", "media", "previous"])
  , (0x01000083, ["key_", "media", "next"])
  , (0x01000084, ["key_", "media", "record"])
  , (0x1000085, ["key_", "media", "pause"])
  , (0x1000086, ["key_", "media", "toggle", "play", "pause"])
  , (0x01000090, ["key_", "home", "page"])
  , (0x01000091, ["key_", "favorites"])
  , (0x01000092, ["key_", "search"])
  , (0x01000093, ["key_", "standby"])
  , (0x01000094, ["key_", "open", "url"])
  , (0x010000a0, ["key_", "launch", "mail"])
  , (0x010000a1, ["key_", "launch", "media"])
  , (0x010000a2, ["key_", "launch0"])
  , (0x010000a3, ["key_", "launch1"])
  , (0x010000a4, ["key_", "launch2"])
  , (0x010000a5, ["key_", "launch3"])
  , (0x010000a6, ["key_", "launch4"])
  , (0x010000a7, ["key_", "launch5"])
  , (0x010000a8, ["key_", "launch6"])
  , (0x010000a9, ["key_", "launch7"])
  , (0x010000aa, ["key_", "launch8"])
  , (0x010000ab, ["key_", "launch9"])
  , (0x010000ac, ["key_", "launch", "a"])
  , (0x010000ad, ["key_", "launch", "b"])
  , (0x010000ae, ["key_", "launch", "c"])
  , (0x010000af, ["key_", "launch", "d"])
  , (0x010000b0, ["key_", "launch", "e"])
  , (0x010000b1, ["key_", "launch", "f"])
  , (0x0100010e, ["key_", "launch", "g"])
  , (0x0100010f, ["key_", "launch", "h"])
  , (0x010000b2, ["key_", "mon", "brightness", "up"])
  , (0x010000b3, ["key_", "mon", "brightness", "down"])
  , (0x010000b4, ["key_", "keyboard", "light", "on", "off"])
  , (0x010000b5, ["key_", "keyboard", "brightness", "up"])
  , (0x010000b6, ["key_", "keyboard", "brightness", "down"])
  , (0x010000b7, ["key_", "power", "off"])
  , (0x010000b8, ["key_", "wake", "up"])
  , (0x010000b9, ["key_", "eject"])
  , (0x010000ba, ["key_", "screen", "saver"])
  , (0x010000bb, ["key_", "w", "w", "w"])
  , (0x010000bc, ["key_", "memo"])
  , (0x010000bd, ["key_", "light", "bulb"])
  , (0x010000be, ["key_", "shop"])
  , (0x010000bf, ["key_", "history"])
  , (0x010000c0, ["key_", "add", "favorite"])
  , (0x010000c1, ["key_", "hot", "links"])
  , (0x010000c2, ["key_", "brightness", "adjust"])
  , (0x010000c3, ["key_", "finance"])
  , (0x010000c4, ["key_", "community"])
  , (0x010000c5, ["key_", "audio", "rewind"])
  , (0x010000c6, ["key_", "back", "forward"])
  , (0x010000c7, ["key_", "application", "left"])
  , (0x010000c8, ["key_", "application", "right"])
  , (0x010000c9, ["key_", "book"])
  , (0x010000ca, ["key_", "c", "d"])
  , (0x010000cb, ["key_", "calculator"])
  , (0x010000cc, ["key_", "to", "do", "list"])
  , (0x010000cd, ["key_", "clear", "grab"])
  , (0x010000ce, ["key_", "close"])
  , (0x010000cf, ["key_", "copy"])
  , (0x010000d0, ["key_", "cut"])
  , (0x010000d1, ["key_", "display"])
  , (0x010000d2, ["key_", "d", "o", "s"])
  , (0x010000d3, ["key_", "documents"])
  , (0x010000d4, ["key_", "excel"])
  , (0x010000d5, ["key_", "explorer"])
  , (0x010000d6, ["key_", "game"])
  , (0x010000d7, ["key_", "go"])
  , (0x010000d8, ["key_i", "touch"])
  , (0x010000d9, ["key_", "log", "off"])
  , (0x010000da, ["key_", "market"])
  , (0x010000db, ["key_", "meeting"])
  , (0x010000dc, ["key_", "menu", "k", "b"])
  , (0x010000dd, ["key_", "menu", "p", "b"])
  , (0x010000de, ["key_", "my", "sites"])
  , (0x010000df, ["key_", "news"])
  , (0x010000e0, ["key_", "office", "home"])
  , (0x010000e1, ["key_", "option"])
  , (0x010000e2, ["key_", "paste"])
  , (0x010000e3, ["key_", "phone"])
  , (0x010000e4, ["key_", "calendar"])
  , (0x010000e5, ["key_", "reply"])
  , (0x010000e6, ["key_", "reload"])
  , (0x010000e7, ["key_", "rotate", "windows"])
  , (0x010000e8, ["key_", "rotation", "p", "b"])
  , (0x010000e9, ["key_", "rotation", "k", "b"])
  , (0x010000ea, ["key_", "save"])
  , (0x010000eb, ["key_", "send"])
  , (0x010000ec, ["key_", "spell"])
  , (0x010000ed, ["key_", "split", "screen"])
  , (0x010000ee, ["key_", "support"])
  , (0x010000ef, ["key_", "task", "pane"])
  , (0x010000f0, ["key_", "terminal"])
  , (0x010000f1, ["key_", "tools"])
  , (0x010000f2, ["key_", "travel"])
  , (0x010000f3, ["key_", "video"])
  , (0x010000f4, ["key_", "word"])
  , (0x010000f5, ["key_", "xfer"])
  , (0x010000f6, ["key_", "zoom", "in"])
  , (0x010000f7, ["key_", "zoom", "out"])
  , (0x010000f8, ["key_", "away"])
  , (0x010000f9, ["key_", "messenger"])
  , (0x010000fa, ["key_", "web", "cam"])
  , (0x010000fb, ["key_", "mail", "forward"])
  , (0x010000fc, ["key_", "pictures"])
  , (0x010000fd, ["key_", "music"])
  , (0x010000fe, ["key_", "battery"])
  , (0x010000ff, ["key_", "bluetooth"])
  , (0x01000100, ["key_", "w", "l", "a", "n"])
  , (0x01000101, ["key_", "u", "w", "b"])
  , (0x01000102, ["key_", "audio", "forward"])
  , (0x01000103, ["key_", "audio", "repeat"])
  , (0x01000104, ["key_", "audio", "random", "play"])
  , (0x01000105, ["key_", "subtitle"])
  , (0x01000106, ["key_", "audio", "cycle", "track"])
  , (0x01000107, ["key_", "time"])
  , (0x01000108, ["key_", "hibernate"])
  , (0x01000109, ["key_", "view"])
  , (0x0100010a, ["key_", "top", "menu"])
  , (0x0100010b, ["key_", "power", "down"])
  , (0x0100010c, ["key_", "suspend"])
  , (0x0100010d, ["key_", "contrast", "adjust"])
  , (0x01000110, ["key_", "touchpad", "toggle"])
  , (0x01000111, ["key_", "touchpad", "on"])
  , (0x01000112, ["key_", "touchpad", "off"])
  , (0x01000113, ["key_", "mic", "mute"])
  , (0x01000114, ["key_", "red"])
  , (0x01000115, ["key_", "green"])
  , (0x01000116, ["key_", "yellow"])
  , (0x01000117, ["key_", "blue"])
  , (0x01000118, ["key_", "channel", "up"])
  , (0x01000119, ["key_", "channel", "down"])
  , (0x0100011a, ["key_", "guide"])
  , (0x0100011b, ["key_", "info"])
  , (0x0100011c, ["key_", "settings"])
  , (0x0100011d, ["key_", "mic", "volume", "up"])
  , (0x0100011e, ["key_", "mic", "volume", "down"])
  , (0x01000120, ["key_", "new"])
  , (0x01000121, ["key_", "open"])
  , (0x01000122, ["key_", "find"])
  , (0x01000123, ["key_", "undo"])
  , (0x01000124, ["key_", "redo"])
  , (0x0100ffff, ["key_", "media", "last"])
  , (0x01ffffff, ["key_unknown"])
  , (0x01100004, ["key_", "call"])
  , (0x01100020, ["key_", "camera"])
  , (0x01100021, ["key_", "camera", "focus"])
  , (0x01100000, ["key_", "context1"])
  , (0x01100001, ["key_", "context2"])
  , (0x01100002, ["key_", "context3"])
  , (0x01100003, ["key_", "context4"])
  , (0x01100006, ["key_", "flip"])
  , (0x01100005, ["key_", "hangup"])
  , (0x01010002, ["key_", "no"])
  , (0x01010000, ["key_", "select"])
  , (0x01010001, ["key_", "yes"])
  , (0x01100007, ["key_", "toggle", "call", "hangup"])
  , (0x01100008, ["key_", "voice", "dial"])
  , (0x01100009, ["key_", "last", "number", "redial"])
  , (0x01020003, ["key_", "execute"])
  , (0x01020002, ["key_", "printer"])
  , (0x01020005, ["key_", "play"])
  , (0x01020004, ["key_", "sleep"])
  , (0x01020006, ["key_", "zoom"])
  , (0x0102000a, ["key_", "exit"])
  , (0x01020001, ["key_", "cancel"])
  ]

(e_KeyboardModifier, bs_KeyboardModifiers) =
  makeQtEnumBitspace (ident1 "Qt" "KeyboardModifier") "KeyboardModifiers" qtInclude
  [ (0x00000000, ["no", "modifier"])
  , (0x02000000, ["shift", "modifier"])
  , (0x04000000, ["control", "modifier"])
  , (0x08000000, ["alt", "modifier"])
  , (0x10000000, ["meta", "modifier"])
  , (0x20000000, ["keypad", "modifier"])
  , (0x40000000, ["group", "switch", "modifier"])
  ]

e_LayoutDirection =
  makeQtEnum (ident1 "Qt" "LayoutDirection") qtInclude
  [ (0, ["left", "to", "right"])
  , (1, ["right", "to", "left"])
  , (2, ["layout", "direction", "auto"])
  ]

e_MaskMode =
  makeQtEnum (ident1 "Qt" "MaskMode") qtInclude
  [ (0, ["mask", "in", "color"])
  , (1, ["mask", "out", "color"])
  ]

(e_MatchFlag, bs_MatchFlags) =
  makeQtEnumBitspace (ident1 "Qt" "MatchFlag") "MatchFlags" qtInclude
  [ (0, ["match", "exactly"])
  , (8, ["match", "fixed", "string"])
  , (1, ["match", "contains"])
  , (2, ["match", "starts", "with"])
  , (3, ["match", "ends", "with"])
  , (16, ["match", "case", "sensitive"])
  , (4, ["match", "reg", "exp"])
  , (5, ["match", "wildcard"])
  , (32, ["match", "wrap"])
  , (64, ["match", "recursive"])
  ]

e_Modifier =
  makeQtEnum (ident1 "Qt" "Modifier") qtInclude
  [ (0x02000000, ["SHIFT"])
  , (0x10000000, ["META"])
  , (0x04000000, ["CTRL"])
  , (0x08000000, ["ALT"])
  , (0x00000000, ["UNICODE_ACCEL"])
  ]

(e_MouseButton, bs_MouseButtons) =
  makeQtEnumBitspace (ident1 "Qt" "MouseButton") "MouseButtons" qtInclude
  [ (0x00000000, ["no", "button"])
  , (0x07ffffff, ["all", "buttons"])
  , (0x00000001, ["left", "button"])
  , (0x00000002, ["right", "button"])
  , (0x00000004, ["middle", "button"])
    -- TODO Other mouse buttons.  Lots of synonyms here which Hoppy doesn't support.
  ]

(e_MouseEventFlag, bs_MouseEventFlags) =
  makeQtEnumBitspace (ident1 "Qt" "MouseEventFlag") "MouseEventFlags" qtInclude
  [ (0x01, ["mouse", "event", "created", "double", "click"])
  ]

e_MouseEventFlag_version = [5, 3]

e_MouseEventSource =
  makeQtEnum (ident1 "Qt" "MouseEventSource") qtInclude
  [ (0, ["mouse", "event", "not", "synthesized"])
  , (1, ["mouse", "event", "synthesized", "by", "system"])
  , (2, ["mouse", "event", "synthesized", "by", "qt"])
  ]

e_MouseEventSource_version = [5, 3]

e_NativeGestureType =
  makeQtEnum (ident1 "Qt" "NativeGestureType") qtInclude
  [ (0, ["begin", "native", "gesture"])
  , (1, ["end", "native", "gesture"])
  , (2, ["pan", "native", "gesture"])
  , (3, ["zoom", "native", "gesture"])
  , (4, ["smart", "zoom", "native", "gesture"])
  , (5, ["rotate", "native", "gesture"])
  , (6, ["swipe", "native", "gesture"])
  ]

e_NativeGestureType_version = [5, 2]

e_NavigationMode =
  makeQtEnum (ident1 "Qt" "NavigationMode") qtInclude
  [ (0, ["navigation", "mode", "none"])
  , (1, ["navigation", "mode", "keypad", "tab", "order"])
  , (2, ["navigation", "mode", "keypad", "directional"])
  , (3, ["navigation", "mode", "cursor", "auto"])
  , (4, ["navigation", "mode", "cursor", "force", "visible"])
  ]

e_NavigationMode_version = [4, 6]

(e_Orientation, bs_Orientations) =
  makeQtEnumBitspace (ident1 "Qt" "Orientation") "Orientations" qtInclude
  [ (1, ["horizontal"])
  , (2, ["vertical"])
  ]

e_PenCapStyle =
  makeQtEnum (ident1 "Qt" "PenCapStyle") qtInclude
  [ (0x00, ["flat", "cap"])
  , (0x10, ["square", "cap"])
  , (0x20, ["round", "cap"])
  ]

e_PenJoinStyle =
  makeQtEnum (ident1 "Qt" "PenJoinStyle") qtInclude
  [ (0x00, ["miter", "join"])
  , (0x40, ["bevel", "join"])
  , (0x80, ["round", "join"])
  , (0x100, ["svg", "miter", "join"])
  ]

e_PenStyle =
  makeQtEnum (ident1 "Qt" "PenStyle") qtInclude
  [ (0, ["no", "pen"])
  , (1, ["solid", "line"])
  , (2, ["dash", "line"])
  , (3, ["dot", "line"])
  , (4, ["dash", "dot", "line"])
  , (5, ["dash", "dot", "dot", "line"])
  , (6, ["custom", "dash", "line"])
  ]

(e_ScreenOrientation, bs_ScreenOrientations) =
  makeQtEnumBitspace (ident1 "Qt" "ScreenOrientation") "ScreenOrientations" qtInclude
  [ (0x00000000, ["PrimaryOrientation"])
  , (0x00000002, ["LandscapeOrientation"])
  , (0x00000001, ["PortraitOrientation"])
  , (0x00000008, ["InvertedLandscapeOrientation"])
  , (0x00000004, ["InvertedPortraitOrientation"])
  ]

e_ScrollBarPolicy =
  makeQtEnum (ident1 "Qt" "ScrollBarPolicy") qtInclude
  [ (0, ["scroll", "bar", "as", "needed"])
  , (1, ["scroll", "bar", "always", "off"])
  , (2, ["scroll", "bar", "always", "on"])
  ]

e_ScrollPhase =
  makeQtEnum (ident1 "Qt" "ScrollPhase") qtInclude
  [ (1, ["scroll", "begin"])
  , (2, ["scroll", "update"])
  , (3, ["scroll", "end"])
  ]

e_ScrollPhase_version = [5, 2]

e_ShortcutContext =
  makeQtEnum (ident1 "Qt" "ShortcutContext") qtInclude
  [ (0, ["widget", "shortcut"])
  , (1, ["window", "shortcut"])
  , (2, ["application", "shortcut"])
  , (3, ["widget", "with", "children", "shortcut"])
  ]

e_SizeHint =
  makeQtEnum (ident1 "Qt" "SizeHint") qtInclude
  [ (0, ["minimum", "size"])
  , (1, ["preferred", "size"])
  , (2, ["maximum", "size"])
  , (3, ["minimum", "descent"])
  ]

e_SizeHint_version = [4, 4]

e_SizeMode =
  makeQtEnum (ident1 "Qt" "SizeMode") qtInclude
  [ (0, ["absolute", "size"])
  , (1, ["relative", "size"])
  ]

e_SizeMode_version = [4, 4]

e_SortOrder =
  makeQtEnum (ident1 "Qt" "SortOrder") qtInclude
  [ (0, ["ascending", "order"])
  , (1, ["descending", "order"])
  ]

e_TabFocusBehavior =
  makeQtEnum (ident1 "Qt" "TabFocusBehavior") qtInclude
  [ (0x00, ["no", "tab", "focus"])
  , (0x01, ["tab", "focus", "text", "controls"])
  , (0x02, ["tab", "focus", "list", "controls"])
  , (0xff, ["tab", "focus", "all", "controls"])
  ]

e_TabFocusBehavior_version = [5, 5]

e_TextElideMode =
  makeQtEnum (ident1 "Qt" "TextElideMode") qtInclude
  [ (0, ["elide", "left"])
  , (1, ["elide", "right"])
  , (2, ["elide", "middle"])
  , (3, ["elide", "none"])
  ]

e_TextFlag =
  makeQtEnum (ident1 "Qt" "TextFlag") qtInclude
  [ (0x0100, ["text", "single", "line"])
  , (0x0200, ["text", "dont", "clip"])
  , (0x0400, ["text", "expand", "tabs"])
  , (0x0800, ["text", "show", "mnemonic"])
  , (0x1000, ["text", "word", "wrap"])
  , (0x2000, ["text", "wrap", "anywhere"])
  , (0x8000, ["text", "hide", "mnemonic"])
  , (0x4000, ["text", "dont", "print"])
  , (0x08000000, ["text", "include", "trailing", "spaces"])
  , (0x10000, ["text", "justification", "forced"])
  ]


e_TextFormat =
  makeQtEnum (ident1 "Qt" "TextFormat") qtInclude
  [ (0, ["plain", "text"])
  , (1, ["rich", "text"])
  , (2, ["auto", "text"])
  , (3, ["log", "text"])
  ]

(e_TextInteractionFlag, bs_TextInteractionFlags) =
  makeQtEnumBitspace (ident1 "Qt" "TextInteractionFlag") "TextInteractionFlags" qtInclude $
  let noTextInteraction = 0
      textSelectableByMouse = 1
      textSelectableByKeyboard = 2
      linksAccessibleByMouse = 4
      linksAccessibleByKeyboard = 8
      textEditable = 16
      textEditorInteraction = textSelectableByMouse .|. textSelectableByKeyboard .|. textEditable
      textBrowserInteraction =
        textSelectableByMouse .|. linksAccessibleByMouse .|. linksAccessibleByKeyboard
  in [ (noTextInteraction, ["no", "text", "interaction"])
     , (textSelectableByMouse, ["text", "selectable", "by", "mouse"])
     , (textSelectableByKeyboard, ["text", "selectable", "by", "keyboard"])
     , (linksAccessibleByMouse, ["links", "accessible", "by", "mouse"])
     , (linksAccessibleByKeyboard, ["links", "accessible", "by", "keyboard"])
     , (textEditable, ["text", "editable"])
     , (textEditorInteraction, ["text", "editor", "interaction"])
     , (textBrowserInteraction, ["text", "browser", "interaction"])
     ]

e_TileRule =
  makeQtEnum (ident1 "Qt" "TileRule") qtInclude
  [ (0, ["stretch", "tile"])
  , (1, ["repeat", "tile"])
  , (2, ["round", "tile"])
  ]

e_TileRule_version = [4, 6]

e_TimeSpec =
  makeQtEnum (ident1 "Qt" "TimeSpec") qtInclude
  [ (0, ["local", "time"])
  , (1, ["UTC"])
  , (2, ["offset", "from", "UTC"])
  , (3, ["time", "zone"])
  ]

e_TimerType =
  makeQtEnum (ident1 "Qt" "TimerType") qtInclude
  [ (0, ["precise", "timer"])
  , (1, ["coarse", "timer"])
  , (2, ["very", "coarse", "timer"])
  ]

(e_ToolBarArea, bs_ToolBarAreas) =
  makeQtEnumBitspace (ident1 "Qt" "ToolBarArea") "ToolBarAreas" qtInclude $
  let leftToolBarArea = 0x1
      rightToolBarArea = 0x2
      topToolBarArea = 0x4
      bottomToolBarArea = 0x8
      allToolBarAreas =
        leftToolBarArea .|. rightToolBarArea .|. topToolBarArea .|. bottomToolBarArea
      noToolBarArea = 0
  in [ (leftToolBarArea, ["left", "tool", "bar", "area"])
     , (rightToolBarArea, ["right", "tool", "bar", "area"])
     , (topToolBarArea, ["top", "tool", "bar", "area"])
     , (bottomToolBarArea, ["bottom", "tool", "bar", "area"])
     , (allToolBarAreas, ["all", "tool", "bar", "areas"])
     , (noToolBarArea, ["no", "tool", "bar", "area"])
     ]

e_ToolButtonStyle =
  makeQtEnum (ident1 "Qt" "ToolButtonStyle") qtInclude
  [ (0, ["tool", "button", "icon", "only"])
  , (1, ["tool", "button", "text", "only"])
  , (2, ["tool", "button", "text", "beside", "icon"])
  , (3, ["tool", "button", "text", "under", "icon"])
  , (4, ["tool", "button", "follow", "style"])
  ]

(e_TouchPointState, bs_TouchPointStates) =
  makeQtEnumBitspace (ident1 "Qt" "TouchPointState") "TouchPointStates" qtInclude $
  [ (0x01, ["touch", "point", "pressed"])
  , (0x02, ["touch", "point", "moved"])
  , (0x04, ["touch", "point", "stationary"])
  , (0x08, ["touch", "point", "released"])
  ]

e_TouchPointState_version = [4, 6]

e_TransformationMode =
  makeQtEnum (ident1 "Qt" "TransformationMode") qtInclude
  [ (0, ["fast", "transformation"])
  , (1, ["smooth", "transformation"])
  ]

e_UIEffect =
  makeQtEnum (ident1 "Qt" "UIEffect") qtInclude
  [ (1, ["UI_", "animate", "menu"])
  , (2, ["UI_", "fade", "menu"])
  , (3, ["UI_", "animate", "combo"])
  , (4, ["UI_", "animate", "tooltip"])
  , (5, ["UI_", "fade", "tooltip"])
  , (6, ["UI_", "animate", "tool", "box"])
  ]

e_WhiteSpaceMode =
  makeQtEnum (ident1 "Qt" "WhiteSpaceMode") qtInclude
  [ (0, ["white", "space", "normal"])
  , (1, ["white", "space", "pre"])
  , (2, ["white", "space", "no", "wrap"])
  ]

e_WidgetAttribute =
  makeQtEnum (ident1 "Qt" "WidgetAttribute") qtInclude
  [ (078, ["WA_", "accept", "drops"])
  , (121, ["WA_", "accept", "touch", "events"])
  , (084, ["WA_", "always", "show", "tool", "tips"])
  , (128, ["WA_", "always", "stack", "on", "top"])
  , (003, ["WA_", "contents", "propagated"])
  , (047, ["WA_", "custom", "whats", "this"])
  , (055, ["WA_", "delete", "on", "close"])
  , (000, ["WA_", "disabled"])
  , (101, ["WA_", "dont", "create", "native", "ancestors"])
  , (103, ["WA_", "dont", "show", "on", "screen"])
  , (032, ["WA_", "force", "disabled"])
  , (059, ["WA_", "force", "updates", "disabled"])
  , (072, ["WA_", "group", "leader"])
  , (074, ["WA_", "hover"])
  , (014, ["WA_", "input", "method", "enabled"])
  , (077, ["WA_", "keyboard", "focus", "change"])
  , (033, ["WA_", "key", "compression"])
  , (048, ["WA_", "layout", "on", "entire", "rect"])
  , (092, ["WA_", "layout", "uses", "widget", "rect"])
  , (096, ["WA_", "mac", "always", "show", "tool", "window"])
  , (046, ["WA_", "mac", "brushed", "metal"])
  , (117, ["WA_", "mac", "framework", "scaled"])
  , (091, ["WA_", "mac", "mini", "size"])
  , (012, ["WA_", "mac", "no", "click", "through"])
  , (089, ["WA_", "mac", "normal", "size"])
  , (085, ["WA_", "mac", "opaque", "size", "grip"])
  , (088, ["WA_", "mac", "show", "focus", "rect"])
  , (090, ["WA_", "mac", "small", "size"])
  , (102, ["WA_", "mac", "variable", "size"])
  , (011, ["WA_", "mapped"])
  , (071, ["WA_", "mouse", "no", "mask"])
  , (002, ["WA_", "mouse", "tracking"])
  , (043, ["WA_", "moved"])
  , (094, ["WA_", "m", "s", "windows", "use", "direct3", "d"])
  , (100, ["WA_", "native", "window"])
  , (058, ["WA_", "no", "child", "events", "for", "parent"])
  , (039, ["WA_", "no", "child", "events", "from", "children"])
  , (073, ["WA_", "no", "mouse", "propagation"])
  , (054, ["WA_", "no", "mouse", "replay"])
  , (009, ["WA_", "no", "system", "background"])
  , (004, ["WA_", "opaque", "paint", "event"])
  , (049, ["WA_", "outside", "w", "s", "range"])
  , (008, ["WA_", "paint", "on", "screen"])
  , (052, ["WA_", "paint", "unclipped"])
  , (034, ["WA_", "pending", "move", "event"])
  , (035, ["WA_", "pending", "resize", "event"])
  , (076, ["WA_", "quit", "on", "close"])
  , (042, ["WA_", "resized"])
  , (056, ["WA_", "right", "to", "left"])
  , (038, ["WA_", "set", "cursor"])
  , (037, ["WA_", "set", "font"])
  , (087, ["WA_", "set", "locale"])
  , (036, ["WA_", "set", "palette"])
  , (086, ["WA_", "set", "style"])
  , (070, ["WA_", "show", "modal"])
  , (098, ["WA_", "show", "without", "activating"])
  , (005, ["WA_", "static", "contents"])
  , (093, ["WA_", "styled", "background"])
  , (097, ["WA_", "style", "sheet"])
  , (123, ["WA_", "touch", "pad", "accept", "single", "touch", "events"])
  , (120, ["WA_", "translucent", "background"])
  , (051, ["WA_", "transparent", "for", "mouse", "events"])
  , (001, ["WA_", "under", "mouse"])
  , (010, ["WA_", "updates", "disabled"])
  , (041, ["WA_", "window", "modified"])
  , (080, ["WA_", "window", "propagation"])
  , (126, ["WA_", "x11", "do", "not", "accept", "focus"])
  , (115, ["WA_", "x11", "net", "wm", "window", "type", "combo"])
  , (104, ["WA_", "x11", "net", "wm", "window", "type", "desktop"])
  , (110, ["WA_", "x11", "net", "wm", "window", "type", "dialog"])
  , (116, ["WA_", "x11", "net", "wm", "window", "type", "d", "n", "d"])
  , (105, ["WA_", "x11", "net", "wm", "window", "type", "dock"])
  , (111, ["WA_", "x11", "net", "wm", "window", "type", "drop", "down", "menu"])
  , (107, ["WA_", "x11", "net", "wm", "window", "type", "menu"])
  , (114, ["WA_", "x11", "net", "wm", "window", "type", "notification"])
  , (112, ["WA_", "x11", "net", "wm", "window", "type", "popup", "menu"])
  , (109, ["WA_", "x11", "net", "wm", "window", "type", "splash"])
  , (106, ["WA_", "x11", "net", "wm", "window", "type", "tool", "bar"])
  , (113, ["WA_", "x11", "net", "wm", "window", "type", "tool", "tip"])
  , (108, ["WA_", "x11", "net", "wm", "window", "type", "utility"])
  ]

e_WindowFrameSection =
  makeQtEnum (ident1 "Qt" "WindowFrameSection") qtInclude
  [ (0, ["no", "section"])
  , (1, ["left", "section"])
  , (2, ["top", "left", "section"])
  , (3, ["top", "section"])
  , (4, ["top", "right", "section"])
  , (5, ["right", "section"])
  , (6, ["bottom", "right", "section"])
  , (7, ["bottom", "section"])
  , (8, ["bottom", "left", "section"])
  , (9, ["title", "bar", "area"])
  ]

e_WindowFrameSection_version = [4, 4]

e_WindowModality =
  makeQtEnum (ident1 "Qt" "WindowModality") qtInclude
  [ (0, ["non", "modal"])
  , (1, ["window", "modal"])
  , (2, ["application", "modal"])
  ]

(e_WindowState, bs_WindowStates) =
  makeQtEnumBitspace (ident1 "Qt" "WindowState") "WindowStates" qtInclude
  [ (0x00, ["window", "no", "state"])
  , (0x01, ["window", "minimized"])
  , (0x02, ["window", "maximized"])
  , (0x04, ["window", "full", "screen"])
  , (0x08, ["window", "active"])
  ]

(e_WindowType, bs_WindowFlags) =
  makeQtEnumBitspace (ident1 "Qt" "WindowType") "WindowFlags" qtInclude $
  let widget = 0x0
      window = 0x1
      dialog = 0x2 .|. window
      sheet = 0x4 .|. window
      drawer = sheet .|. dialog
      popup = 0x8 .|. window
      tool = popup .|. dialog
      toolTip = popup .|. sheet
      splashScreen = toolTip .|. dialog
      desktop = 0x10 .|. window
      subWindow = 0x12 .|. window
      foreignWindow = 0x20 .|. window
      coverWindow = 0x40 .|. window
  in [ (widget, ["widget"])
     , (window, ["window"])
     , (dialog, ["dialog"])
     , (sheet, ["sheet"])
     , (drawer, ["drawer"])
     , (popup, ["popup"])
     , (tool, ["tool"])
     , (toolTip, ["tool", "tip"])
     , (splashScreen, ["splash", "screen"])
     , (desktop, ["desktop"])
     , (subWindow, ["sub", "window"])
     , (foreignWindow, ["foreign", "window"])
     , (coverWindow, ["cover", "window"])
     ]

f_escape =
  addReqIncludes [includeStd "QTextDocument"] $
  makeFn (ident1 "Qt" "escape") Nothing Nonpure [objT c_QString] $ objT c_QString
