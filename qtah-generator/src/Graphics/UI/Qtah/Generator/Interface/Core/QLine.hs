-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

{-# LANGUAGE CPP #-}

module Graphics.UI.Qtah.Generator.Interface.Core.QLine (
  aModule,
  c_QLine,
  ) where

#if !MIN_VERSION_base(4,8,0)
import Data.Monoid (mconcat)
#endif
import Foreign.Hoppy.Generator.Language.Haskell (
  addImports,
  indent,
  sayLn,
  )
import Foreign.Hoppy.Generator.Spec (
  ClassHaskellConversion (
    ClassHaskellConversion,
    classHaskellConversionFromCppFn,
    classHaskellConversionToCppFn,
    classHaskellConversionType
  ),
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetHaskellConversion,
  hsImports,
  hsQualifiedImport,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Foreign.Hoppy.Generator.Types (boolT, intT, objT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QPoint (c_QPoint)
import Graphics.UI.Qtah.Generator.Interface.Imports
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types
import Language.Haskell.Syntax (
  HsName (HsIdent),
  HsQName (UnQual),
  HsType (HsTyCon),
  )

{-# ANN module "HLint: ignore Use camelCase" #-}

aModule =
  AQtModule $
  makeQtModule ["Core", "QLine"]
  [ QtExport $ ExportClass c_QLine ]

c_QLine =
  addReqIncludes [includeStd "QLine"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
    classSetHaskellConversion
    ClassHaskellConversion
    { classHaskellConversionType = do
      addImports $ hsQualifiedImport "Graphics.UI.Qtah.Core.HLine" "HLine"
      return $ HsTyCon $ UnQual $ HsIdent "HLine.HLine"
    , classHaskellConversionToCppFn = do
      addImports $ mconcat [hsImports "Control.Applicative" ["(<$>)", "(<*>)"],
                            hsQualifiedImport "Graphics.UI.Qtah.Core.HLine" "HLine"]
      sayLn "new <$> HLine.p1 <*> HLine.p2"
    , classHaskellConversionFromCppFn = do
      addImports $ mconcat [hsQualifiedImport "Graphics.UI.Qtah.Core.HLine" "HLine",
                            importForPrelude]
      sayLn "\\q -> do"
      indent $ do
        sayLn "p1 <- p1 q"
        sayLn "p2 <- p2 q"
        sayLn "QtahP.return (HLine.HLine p1 p2)"
    } $
  classSetEntityPrefix "" $
  makeClass (ident "QLine") Nothing [] $
  collect
  [ just $ mkCtor "newNull" []
  , just $ mkCtor "new" [objT c_QPoint, objT c_QPoint]
  , just $ mkCtor "newFromRaw" [intT, intT, intT, intT]
  , just $ mkConstMethod "p1" [] $ objT c_QPoint
  , just $ mkConstMethod "p2" [] $ objT c_QPoint
  , just $ mkConstMethod "x1" [] intT
  , just $ mkConstMethod "x2" [] intT
  , just $ mkConstMethod "y1" [] intT
  , just $ mkConstMethod "y2" [] intT
  , just $ mkConstMethod "isNull" [] boolT
  , test (qtVersion >= [4, 4]) $ mkMethod "setP1" [objT c_QPoint] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod "setP2" [objT c_QPoint] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod "setLine" [intT, intT, intT, intT] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod "setPoints" [objT c_QPoint, objT c_QPoint] voidT
  , just $ mkMethod "translate" [objT c_QPoint] voidT
  , just $ mkMethod' "translate" "translateRaw" [intT, intT] voidT
  , test (qtVersion >= [4, 4]) $ mkConstMethod "translated" [objT c_QPoint] $ objT c_QLine
  , test (qtVersion >= [4, 4]) $ mkConstMethod' "translated" "translatedRaw" [intT, intT] $ objT c_QLine
  ]
