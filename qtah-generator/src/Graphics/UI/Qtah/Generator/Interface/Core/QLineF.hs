-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

{-# LANGUAGE CPP #-}

module Graphics.UI.Qtah.Generator.Interface.Core.QLineF (
  aModule,
  c_QLineF,
  ) where

#if !MIN_VERSION_base(4,8,0)
import Data.Monoid (mconcat)
#endif
import Foreign.Hoppy.Generator.Language.Haskell (
  addImports,
  indent,
  sayLn,
  )
import Foreign.Hoppy.Generator.Spec (
  ClassHaskellConversion (
    ClassHaskellConversion,
    classHaskellConversionFromCppFn,
    classHaskellConversionToCppFn,
    classHaskellConversionType
  ),
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetHaskellConversion,
  hsImports,
  hsQualifiedImport,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkStaticMethod
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Foreign.Hoppy.Generator.Types (boolT, enumT, objT, ptrT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QLine (c_QLine)
import Graphics.UI.Qtah.Generator.Interface.Core.QPointF (c_QPointF)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qreal)
import Graphics.UI.Qtah.Generator.Interface.Imports
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types
import Language.Haskell.Syntax (
  HsName (HsIdent),
  HsQName (UnQual),
  HsType (HsTyCon),
  )

{-# ANN module "HLint: ignore Use camelCase" #-}

aModule =
  AQtModule $
  makeQtModule ["Core", "QLineF"]
  [ QtExport $ ExportClass c_QLineF
  , QtExport $ ExportEnum e_IntersectType
  ]

c_QLineF =
  addReqIncludes [includeStd "QLineF"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
    classSetHaskellConversion
    ClassHaskellConversion
    { classHaskellConversionType = do
      addImports $ hsQualifiedImport "Graphics.UI.Qtah.Core.HLineF" "HLineF"
      return $ HsTyCon $ UnQual $ HsIdent "HLineF.HLineF"
    , classHaskellConversionToCppFn = do
      addImports $ mconcat [hsImports "Control.Applicative" ["(<$>)", "(<*>)"],
                            hsQualifiedImport "Graphics.UI.Qtah.Core.HLineF" "HLineF"]
      sayLn "new <$> HLineF.p1 <*> HLineF.p2"
    , classHaskellConversionFromCppFn = do
      addImports $ mconcat [hsQualifiedImport "Graphics.UI.Qtah.Core.HLineF" "HLineF",
                            importForPrelude]
      sayLn "\\q -> do"
      indent $ do
        sayLn "p1 <- p1 q"
        sayLn "p2 <- p2 q"
        sayLn "QtahP.return (HLineF.HLineF p1 p2)"
    } $
  classSetEntityPrefix "" $
  makeClass (ident "QLineF") Nothing [] $
  collect
  [ just $ mkCtor "newNull" []
  , just $ mkCtor "new" [objT c_QPointF, objT c_QPointF]
  , just $ mkCtor "newFromRaw" [qreal, qreal, qreal, qreal]
  , just $ mkConstMethod "p1" [] $ objT c_QPointF
  , just $ mkConstMethod "p2" [] $ objT c_QPointF
  , just $ mkConstMethod "x1" [] qreal
  , just $ mkConstMethod "x2" [] qreal
  , just $ mkConstMethod "y1" [] qreal
  , just $ mkConstMethod "y2" [] qreal
  , test (qtVersion >= [4, 4]) $ mkConstMethod "angle" [] qreal
  , test (qtVersion >= [4, 4]) $ mkConstMethod "angleTo" [objT c_QLineF] qreal
  , just $ mkConstMethod "dx" [] qreal
  , just $ mkConstMethod "dy" [] qreal
  , test (qtVersion >= [4, 4]) $ mkStaticMethod "fromPolar" [qreal, qreal] $ objT c_QLineF
  , just $ mkConstMethod "intersect" [objT c_QLineF, ptrT $ objT c_QPointF] $ enumT e_IntersectType
  , just $ mkConstMethod "isNull" [] boolT
  , just $ mkConstMethod "length" [] boolT
  , just $ mkConstMethod "normalVector" [] $ objT c_QLineF
  , just $ mkConstMethod "pointAt" [qreal] $ objT c_QPointF
  , test (qtVersion >= [4, 4]) $ mkMethod "setP1" [objT c_QPointF] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod "setP2" [objT c_QPointF] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod "setAngle" [qreal] voidT
  , just $ mkMethod "setLength" [qreal] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod "setLine" [qreal, qreal, qreal, qreal] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod "setPoints" [objT c_QPointF, objT c_QPointF] voidT
  , just $ mkMethod "toLine" [] $ objT c_QLine
  , just $ mkMethod "translate" [objT c_QPointF] voidT
  , just $ mkMethod' "translate" "translateRaw" [qreal, qreal] voidT
  , test (qtVersion >= [4, 4]) $ mkConstMethod "translated" [objT c_QPointF] $ objT c_QLineF
  , test (qtVersion >= [4, 4]) $ mkConstMethod' "translated" "translatedRaw" [qreal , qreal] $ objT c_QLineF
  , just $ mkMethod "unitVector" [] $ objT c_QLineF
  ]

e_IntersectType =
  makeQtEnum (ident1 "QLineF" "IntersectType") [includeStd "QLineF"]
  [ (0, ["no", "intersection"])
  , (1, ["bounded", "intersection"])
  , (2, ["unbounded", "intersection"])
  ]
