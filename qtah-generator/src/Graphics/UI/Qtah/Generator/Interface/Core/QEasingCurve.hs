-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QEasingCurve (
  aModule,
  c_QEasingCurve,
  e_Type,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (enumT, fnT, objT, ptrT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QPointF (c_QPointF)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qreal)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 6]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QEasingCurve"] minVersion $
  map QtExport
  [ ExportClass c_QEasingCurve
  , ExportEnum e_Type
  ]

c_QEasingCurve =
  addReqIncludes [includeStd "QEasingCurve"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QEasingCurve") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithType" [enumT e_Type]
  , just $ mkMethod "addCubicBezierSegment" [objT c_QPointF, objT c_QPointF, objT c_QPointF] voidT
  , just $ mkMethod "addTCBSegment" [objT c_QPointF, qreal, qreal, qreal] voidT
  , test (qtVersion >= [5, 0]) $ mkMethod "swap" [refT $ objT c_QEasingCurve] voidT
  -- TODO toCubicSpline
  , just $ mkMethod' "setType" "setCurveType" [enumT e_Type] voidT
  , just $ mkConstMethod' "type" "curveType" [] $ enumT e_Type
  , just $ mkMethod "valueForProgress" [qreal] qreal
  , just $ mkProp "amplitude" qreal
  --, just $ mkProp "customType" $ ptrT $ fnT [qreal] qreal
  , just $ mkProp "overshoot" qreal
  , just $ mkProp "period" qreal
  ]

e_Type =
  makeQtEnum (ident1 "QEasingCurve" "Type") [includeStd "QEasingCurve"]
  [ (0, ["linear"])
  , (1, ["in", "quad"])
  , (2, ["out", "quad"])
  , (3, ["in", "out", "quad"])
  , (4, ["out", "in", "quad"])
  , (5, ["in", "cubic"])
  , (6, ["out", "cubic"])
  , (7, ["in", "out", "cubic"])
  , (8, ["out", "in", "cubic"])
  , (9, ["in", "quart"])
  , (10, ["out", "quart"])
  , (11, ["in", "out", "quart"])
  , (12, ["out", "in", "quart"])
  , (13, ["in", "quint"])
  , (14, ["out", "quint"])
  , (15, ["in", "out", "quint"])
  , (16, ["out", "in", "quint"])
  , (17, ["in", "sine"])
  , (18, ["out", "sine"])
  , (19, ["in", "out", "sine"])
  , (20, ["out", "in", "sine"])
  , (21, ["in", "expo"])
  , (22, ["out", "expo"])
  , (23, ["in", "out", "expo"])
  , (24, ["out", "in", "expo"])
  , (25, ["in", "circ"])
  , (26, ["out", "circ"])
  , (27, ["in", "out", "circ"])
  , (28, ["out", "in", "circ"])
  , (29, ["in", "elastic"])
  , (30, ["out", "elastic"])
  , (31, ["in", "out", "elastic"])
  , (32, ["out", "in", "elastic"])
  , (33, ["in", "back"])
  , (34, ["out", "back"])
  , (35, ["in", "out", "back"])
  , (36, ["out", "in", "back"])
  , (37, ["in", "bounce"])
  , (38, ["out", "bounce"])
  , (39, ["in", "out", "bounce"])
  , (40, ["out", "in", "bounce"])
  , (45, ["bezier", "spline"])
  , (46, ["t", "c", "b", "spline"])
  , (47, ["custom"])
  ]
