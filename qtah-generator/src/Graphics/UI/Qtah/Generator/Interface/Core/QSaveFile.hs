-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QSaveFile (
  aModule,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkCtor,
  mkMethod,
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (boolT, objT, ptrT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QFileDevice (c_QFileDevice)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [5, 1]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Core", "QSaveFile"] minVersion $
  [ QtExport $ ExportClass c_QSaveFile
  ]

c_QSaveFile =
  addReqIncludes [includeStd "QSaveFile"] $
  classSetEntityPrefix "" $
  makeClass (ident "QSaveFile") Nothing [c_QFileDevice]
  [ mkCtor "newWithFileName" [objT c_QString]
  , mkCtor "new" []
  , mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , mkCtor "newWithFileNameAndParent" [objT c_QString, ptrT $ objT c_QObject]
  , mkMethod "cancelWriting" [] voidT
  , mkMethod "commit" [] boolT
  , mkProp "directWriteFallback" boolT
  , mkProp "fileName" $ objT c_QString
  ]
