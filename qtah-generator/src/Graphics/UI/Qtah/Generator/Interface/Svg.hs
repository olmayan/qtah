module Graphics.UI.Qtah.Generator.Interface.Svg (modules) where

import qualified Graphics.UI.Qtah.Generator.Interface.Svg.QGraphicsSvgItem as QGraphicsSvgItem
import qualified Graphics.UI.Qtah.Generator.Interface.Svg.QSvgGenerator as QSvgGenerator
import qualified Graphics.UI.Qtah.Generator.Interface.Svg.QSvgRenderer as QSvgRenderer
import qualified Graphics.UI.Qtah.Generator.Interface.Svg.QSvgWidget as QSvgWidget
import Graphics.UI.Qtah.Generator.Module (AModule)

{-# ANN module "HLint: ignore Use camelCase" #-}

modules :: [AModule]
modules =
  [ QGraphicsSvgItem.aModule
  , QSvgGenerator.aModule
  , QSvgRenderer.aModule
  , QSvgWidget.aModule
  ]
