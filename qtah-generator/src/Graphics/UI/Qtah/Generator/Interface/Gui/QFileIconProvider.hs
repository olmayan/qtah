-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Core.QBuffer (
  aModule,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  )
import Foreign.Hoppy.Generator.Types (constT, charT, intT, objT, ptrT, refT, voidT)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QIODevice (c_QIODevice)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Core", "QBuffer"] $
  [ QtExport $ ExportClass c_QBuffer ]

c_QBuffer =
  addReqIncludes [includeStd "QBuffer"] $
  classSetEntityPrefix "" $
  makeClass (ident "QBuffer") Nothing [c_QIODevice]
  [ mkCtor "new" []
  , mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , mkCtor "newWithBuffer" [ptrT $ objT c_QByteArray]
  , mkCtor "newWithBufferAndParent" [ptrT $ objT c_QByteArray, ptrT $ objT c_QObject]
  , mkMethod "buffer" [] $ refT $ objT c_QByteArray
  , mkConstMethod' "buffer" "constBuffer" [] $ refT $ constT $ objT c_QByteArray
  , mkConstMethod' "data" "data_" [] $ refT $ constT $ objT c_QByteArray
  , mkMethod "setBuffer" [ptrT $ objT c_QByteArray] voidT
  , mkMethod "setData" [refT $ constT $ objT c_QByteArray] voidT
  , mkMethod' "setData" "setDataRaw" [ptrT $ constT charT, intT] voidT
  ]
