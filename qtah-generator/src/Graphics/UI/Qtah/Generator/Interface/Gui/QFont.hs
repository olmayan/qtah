-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QFont (
  aModule,
  c_QFont,
  e_StyleHint,
  e_Style,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  mkStaticMethod,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, ptrT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qreal)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPaintDevice (c_QPaintDevice)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Gui", "QFont"] $ 
  (map QtExport . collect)
  [ just $ ExportClass c_QFont
  , test (qtVersion >= [4, 4]) $ ExportEnum e_Capitalization
  , test (qtVersion >= [4, 8]) $ ExportEnum e_HintingPreference
  , test (qtVersion >= [4, 4]) $ ExportEnum e_SpacingType
  , just $ ExportEnum e_Stretch
  , just $ ExportEnum e_Style
  , just $ ExportEnum e_StyleHint
  , just $ ExportEnum e_StyleStrategy
  , just $ ExportEnum e_Weight
  ]

c_QFont =
  addReqIncludes [includeStd "QFont"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QFont") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithFamily" [objT c_QString]
  , just $ mkCtor "newWithFamilyAndSize" [objT c_QString, intT]
  , just $ mkCtor "newWithFamilyAndSizeAndWeight" [objT c_QString, intT, intT]
  , just $ mkCtor "newWithFamilyAndSizeAndWeightAndItalic" [objT c_QString, intT, intT, boolT]
  , just $ mkCtor "newCopyWithPaintDevice" [objT c_QFont, ptrT $ objT c_QPaintDevice]
  , just $ mkConstMethod "defaultFamily" [] $ objT c_QString
  , just $ mkConstMethod "exactMatch" [] boolT
  , just $ mkMethod "fromString" [objT c_QString] boolT
  , just $ mkConstMethod "isCopyOf" [objT c_QFont] boolT
  , just $ mkConstMethod "key" [] $ objT c_QString
  , just $ mkConstMethod "lastResortFamily" [] $ objT c_QString
  , just $ mkConstMethod "lastResortFont" [] $ objT c_QString
  , test (qtVersion >= [4, 4]) $ mkConstMethod "letterSpacing" [] qreal
  , test (qtVersion >= [4, 4]) $ mkConstMethod "letterSpacingType" [] $ enumT e_SpacingType
  , just $ mkMethod "resolve" [objT c_QFont] $ objT c_QFont
  , test (qtVersion >= [4, 4]) $ mkMethod "setLetterSpacing" [enumT e_SpacingType, qreal] voidT
  , just $ mkMethod' "setStyleHint" "setStyleHintPreferDefault" [enumT e_StyleHint] voidT
  , just $ mkMethod "setStyleHint" [enumT e_StyleHint, enumT e_StyleStrategy] voidT
  , just $ mkMethod "styleHint" [] $ enumT e_StyleHint
  , test (qtVersion >= [5, 0]) $ mkMethod "swap" [refT $ objT c_QFont] voidT
  , just $ mkConstMethod "toString" [] $ objT c_QString
  --operator QVariant
  , just $ mkStaticMethod "insertSubstitution" [objT c_QString, objT c_QString] voidT
  , just $ mkStaticMethod "insertSubstitutions" [objT c_QString, objT c_QStringList] voidT
  , test (qtVersion >= [5, 0]) $ mkStaticMethod "removeSubstitutions" [objT c_QString] voidT
  , just $ mkStaticMethod "substitute" [objT c_QString] $ objT c_QString
  , just $ mkStaticMethod "substitutes" [objT c_QString] $ objT c_QStringList
  , just $ mkStaticMethod "substitutions" [] $ objT c_QStringList
  , just $ mkProp "bold" boolT
  , test (qtVersion >= [4, 4]) $ mkProp "capitalization" $ enumT e_Capitalization
  , just $ mkProp "family" $ objT c_QString
  , just $ mkProp "fixedPitch" boolT
  , test (qtVersion >= [4, 8]) $ mkProp "hintingPreference" $ enumT e_HintingPreference
  , just $ mkProp "italic" boolT
  , just $ mkProp "kerning" boolT
  , just $ mkProp "overline" boolT
  , just $ mkProp "pixelSize" intT
  , just $ mkProp "pointSize" intT
  , just $ mkProp "pointSizeF" qreal
  , just $ mkProp "stretch" intT
  , just $ mkProp "strikeOut" boolT
  , just $ mkProp "style" $ enumT e_Style
  , test (qtVersion >= [4, 8]) $ mkProp "styleName" $ objT c_QString
  , just $ mkProp "styleStrategy" $ enumT e_StyleStrategy
  , just $ mkProp "underline" boolT
  , just $ mkProp "weight" intT
  , test (qtVersion >= [4, 4]) $ mkProp "wordSpacing" intT
  ]

e_Capitalization =
  makeQtEnum (ident1 "QFont" "Capitalization") [includeStd "QFont"]
  [ (0, ["mixed", "case"])
  , (1, ["all", "uppercase"])
  , (2, ["all", "lowercase"])
  , (3, ["small", "caps"])
  , (4, ["capitalize"])
  ]

e_HintingPreference =
  makeQtEnum (ident1 "QFont" "HintingPreference") [includeStd "QFont"]
  [ (0, ["prefer", "default", "hinting"])
  , (1, ["prefer", "no", "hinting"])
  , (2, ["prefer", "vertical", "hinting"])
  , (3, ["prefer", "full", "hinting"])
  ]

e_SpacingType =
  makeQtEnum (ident1 "QFont" "SpacingType") [includeStd "QFont"]
  [ (0, ["percentage", "spacing"])
  , (1, ["absolute", "spacing"])
  ]

e_Stretch =
  makeQtEnum (ident1 "QFont" "Stretch") [includeStd "QFont"]
  [ (50, ["ultra", "condensed"])
  , (62, ["extra", "condensed"])
  , (75, ["condensed"])
  , (87, ["semi", "condensed"])
  , (100, ["unstretched"])
  , (112, ["semi", "expanded"])
  , (125, ["expanded"])
  , (150, ["extra", "expanded"])
  , (200, ["ultra", "expanded"])
  ]

e_Style =
  makeQtEnum (ident1 "QFont" "Style") [includeStd "QFont"]
  [ (0, ["style", "normal"])
  , (1, ["style", "italic"])
  , (2, ["style", "oblique"])
  ]

e_StyleHint =
  makeQtEnum (ident1 "QFont" "StyleHint") [includeStd "QFont"]
  --These values have synonyms
  [ (5, ["any", "style"])
  , (0, ["sans", "serif"])
  , (1, ["times"])
  , (2, ["courier"])
  , (3, ["old", "english"])
  , (7, ["monospace"])
  , (8, ["fantasy"])
  , (6, ["cursive"])
  , (4, ["system"])
  ]

e_StyleStrategy =
  makeQtEnum (ident1 "QFont" "StyleStrategy") [includeStd "QFont"]
  [ (0x0001, ["prefer", "default"])
  , (0x0002, ["prefer", "bitmap"])
  , (0x0004, ["prefer", "device"])
  , (0x0008, ["prefer", "outline"])
  , (0x0010, ["force", "outline"])
  , (0x0100, ["no", "antialias"])
  , (0x0800, ["no", "subpixel", "antialias"])
  , (0x0080, ["prefer", "antialias"])
  , (0x0200, ["open", "g", "l", "compatible"])
  , (0x8000, ["no", "font", "merging"])
  , (0x0020, ["prefer", "match"])
  , (0x0040, ["prefer", "quality"])
  , (0x0400, ["force", "integer", "metrics"])
  ]

e_Weight =
  makeQtEnum (ident1 "QFont" "Weight") [includeStd "QFont"]
  [ (0, ["thin"])
  , (12, ["extra", "light"])
  , (25, ["light"])
  , (50, ["normal"])
  , (57, ["medium"])
  , (63, ["demi", "bold"])
  , (75, ["bold"])
  , (81, ["extra", "bold"])
  , (87, ["black"])
  ]
