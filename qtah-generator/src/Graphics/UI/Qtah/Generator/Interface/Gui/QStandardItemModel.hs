-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QStandardItemModel (
  aModule,
  c_QStandardItemModel,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, constT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QAbstractItemModel (c_QAbstractItemModel)
import Graphics.UI.Qtah.Generator.Interface.Core.QList (c_QListQStandardItem)
import Graphics.UI.Qtah.Generator.Interface.Core.QModelIndex (c_QModelIndex)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (bs_MatchFlags)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Gui.QStandardItem (c_QStandardItem)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (c_ListenerPtrQStandardItem)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Gui", "QStandardItemModel"] $
  [ QtExport $ ExportClass c_QStandardItemModel
  ] ++
  map QtExportSignal signals

c_QStandardItemModel =
  addReqIncludes [includeStd "QStandardItemModel"] $
  classSetEntityPrefix "" $
  makeClass (ident "QStandardItemModel") Nothing [c_QAbstractItemModel] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , just $ mkCtor "newWithRC" [intT, intT]
  , just $ mkCtor "newWithRCAndParent" [intT, intT, ptrT $ objT c_QObject]
  , test (qtVersion >= [4, 2]) $ mkMethod "appendColumn" [objT c_QListQStandardItem] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "appendRow" [objT c_QListQStandardItem] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod' "appendRow" "appendRowSingle" [ptrT $ objT c_QStandardItem] voidT
  , just $ mkMethod "clear" [] voidT
  , test (qtVersion >= [4, 2]) $ mkConstMethod "findItems" [objT c_QString] $ objT c_QListQStandardItem
  , test (qtVersion >= [4, 2]) $ mkConstMethod' "findItems" "findItemsWithFlags" [objT c_QString, bitspaceT bs_MatchFlags] $ objT c_QListQStandardItem
  , test (qtVersion >= [4, 2]) $ mkConstMethod' "findItems" "findItemsWithFlagsAndColumn" [objT c_QString, bitspaceT bs_MatchFlags, intT] $ objT c_QListQStandardItem
  , test (qtVersion >= [4, 2]) $ mkConstMethod "horizontalHeaderItem" [intT] $ ptrT $ objT c_QStandardItem
  , test (qtVersion >= [4, 2]) $ mkConstMethod "indexFromItem" [ptrT $ objT c_QStandardItem] $ objT c_QModelIndex
  , test (qtVersion >= [4, 2]) $ mkMethod "insertColumn" [intT, objT c_QListQStandardItem] voidT
  , just $ mkMethod' "insertColumn" "insertColumnIntoRoot" [intT] boolT
  , just $ mkMethod' "insertColumn" "insertColumnWithParent" [intT, objT c_QModelIndex] boolT
  , test (qtVersion >= [4, 2]) $ mkMethod "insertRow" [intT, objT c_QListQStandardItem] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod' "insertRow" "insertRowSingle" [intT, ptrT $ objT c_QStandardItem] voidT
  , just $ mkMethod' "insertRow" "insertRowIntoRoot" [intT] voidT
  , just $ mkMethod' "insertRow" "insertRowWithParent" [intT, objT c_QModelIndex] voidT
  , test (qtVersion >= [4, 2]) $ mkConstMethod "invisibleRootItem" [] $ ptrT $ objT c_QStandardItem
  , test (qtVersion >= [4, 2]) $ mkConstMethod "item" [intT, intT] $ ptrT $ objT c_QStandardItem
  , test (qtVersion >= [4, 2]) $ mkConstMethod' "item" "itemFirstColumn" [intT] $ ptrT $ objT c_QStandardItem
  , test (qtVersion >= [4, 2]) $ mkConstMethod "itemFromIndex" [objT c_QModelIndex] $ ptrT $ objT c_QStandardItem
  , test (qtVersion >= [4, 2]) $ mkConstMethod "itemPrototype" [] $ ptrT $ constT $ objT c_QStandardItem
  , test (qtVersion >= [4, 2]) $ mkMethod "setColumnCount" [intT] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "setHorizontalHeaderItem" [intT, ptrT $ objT c_QStandardItem] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "setHorizontalHeaderLabels" [objT c_QStringList] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "setItem" [intT, intT, ptrT $ objT c_QStandardItem] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod' "setItem" "setItemFirstColumn" [intT, ptrT $ objT c_QStandardItem] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "setItemPrototype" [ptrT $ objT c_QStandardItem] voidT
  --, just $ mkMethod "setItemRoleNames" [objT c_QHashQByteArray] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "setRowCount" [intT] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "setVerticalHeaderItem" [intT, ptrT $ objT c_QStandardItem] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "setVerticalHeaderLabels" [objT c_QStringList] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "takeColumn" [intT] $ objT c_QListQStandardItem
  , test (qtVersion >= [4, 2]) $ mkMethod "takeHorizontalHeaderItem" [intT] $ ptrT $ objT c_QStandardItem
  , test (qtVersion >= [4, 2]) $ mkMethod "takeItem" [intT, intT] $ ptrT $ objT c_QStandardItem
  , test (qtVersion >= [4, 2]) $ mkMethod' "takeItem" "takeItemFirst" [intT] $ ptrT $ objT c_QStandardItem
  , test (qtVersion >= [4, 2]) $ mkMethod "takeRow" [intT] $ objT c_QListQStandardItem
  , test (qtVersion >= [4, 2]) $ mkMethod "takeVerticalHeaderItem" [intT] $ ptrT $ objT c_QStandardItem
  , test (qtVersion >= [4, 2]) $ mkConstMethod "verticalHeaderItem" [intT] $ ptrT $ objT c_QStandardItem
  , test (qtVersion >= [4, 2]) $ mkProp "sortRole" intT
  ]

signals =
  collect
  [ test (qtVersion >= [4, 2]) $ makeSignal c_QStandardItemModel "itemChanged" c_ListenerPtrQStandardItem
  ]
