-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QTransform (
  aModule,
  c_QTransform
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum, ExportFn),
  Operator (OpAddAssign, OpDivideAssign, OpMultiplyAssign, OpSubtractAssign),
  Purity (Pure),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  ident1,
  includeStd,
  makeClass,
  makeFn,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkStaticMethod,
  operatorPreferredExtName'
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, ptrT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QLine (c_QLine)
import Graphics.UI.Qtah.Generator.Interface.Core.QLineF (c_QLineF)
import Graphics.UI.Qtah.Generator.Interface.Core.QPoint (c_QPoint)
import Graphics.UI.Qtah.Generator.Interface.Core.QPointF (c_QPointF)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  e_Axis,
  qreal
  )
import Graphics.UI.Qtah.Generator.Interface.Gui.QPolygon (c_QPolygon)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPolygonF (c_QPolygonF)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Gui.QRegion (c_QRegion)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 3]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Gui", "QTransform"] minVersion $
  (map QtExport . collect)
  [ just $ ExportClass c_QTransform
  , just $ ExportEnum e_TransformationType
  , test (qtVersion >= [4, 6]) $ ExportFn f_qFuzzyCompare
  ]

c_QTransform =
  addReqIncludes [includeStd "QTransform"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QTransform") Nothing [] $
  collect
  [ just $ mkCtor "newNull" []
  , just $ mkCtor "new" [qreal, qreal, qreal, qreal, qreal, qreal, qreal, qreal, qreal]
  , just $ mkCtor "new_" [qreal, qreal, qreal, qreal, qreal, qreal]
  --, just $ mkCtor "newFromMatrix" [objT c_QMatrix]
  , just $ mkConstMethod "m11" [] qreal
  , just $ mkConstMethod "m12" [] qreal
  , just $ mkConstMethod "m13" [] qreal
  , just $ mkConstMethod "m21" [] qreal
  , just $ mkConstMethod "m22" [] qreal
  , just $ mkConstMethod "m23" [] qreal
  , just $ mkConstMethod "m31" [] qreal
  , just $ mkConstMethod "m32" [] qreal
  , just $ mkConstMethod "m33" [] qreal
  , just $ mkConstMethod "adjoint" [] $ objT c_QTransform
  , just $ mkConstMethod "determinant" [] qreal
  , just $ mkConstMethod "dx" [] qreal
  , just $ mkConstMethod "dy" [] qreal
  , just $ mkConstMethod "inverted" [] $ objT c_QTransform
  , just $ mkConstMethod' "inverted" "invertedCheck" [ptrT boolT] $ objT c_QTransform
  , just $ mkConstMethod "isAffine" [] boolT
  , just $ mkConstMethod "isIdentity" [] boolT
  , just $ mkConstMethod "isInvertible" [] boolT
  , just $ mkConstMethod "isRotating" [] boolT
  , just $ mkConstMethod "isScaling" [] boolT
  , just $ mkConstMethod "isTranslating" [] boolT
  , just $ mkConstMethod "map" [qreal, qreal, ptrT qreal, ptrT qreal] voidT
  , just $ mkConstMethod' "map" "mapPointF" [objT c_QPointF] $ objT c_QPointF
  , just $ mkConstMethod' "map" "mapLine" [objT c_QLine] $ objT c_QLine
  , just $ mkConstMethod' "map" "mapLineF" [objT c_QLineF] $ objT c_QLineF
  , just $ mkConstMethod' "map" "mapPolygonF" [objT c_QPolygonF] $ objT c_QPolygonF
  , just $ mkConstMethod' "map" "mapPolygon" [objT c_QPolygon] $ objT c_QPolygon
  , just $ mkConstMethod' "map" "mapRegion" [objT c_QRegion] $ objT c_QRegion
  --, just $ mkConstMethod' "map" "mapPainterPath" [objT c_QPainterPath] $ objT c_QPainterPath
  , just $ mkConstMethod' "map" "mapInts" [intT, intT, ptrT intT, ptrT intT] voidT
  , just $ mkConstMethod' "map" "mapPoint" [objT c_QPoint] $ objT c_QPoint
  , just $ mkConstMethod' "mapRect" "mapRectF" [objT c_QRectF] $ objT c_QRectF
  , just $ mkConstMethod "mapRect" [objT c_QRect] $ objT c_QRect
  , just $ mkConstMethod "mapToPolygon" [objT c_QRect] $ objT c_QPolygon
  , just $ mkMethod "reset" [] voidT
  , just $ mkMethod' "rotate" "rotateZ" [qreal] $ refT $ objT c_QTransform
  , just $ mkMethod "rotate" [qreal, enumT e_Axis] $ refT $ objT c_QTransform
  , just $ mkMethod' "rotateRadians" "rotateRadiansZ" [qreal] $ refT $ objT c_QTransform
  , just $ mkMethod "rotateRadians" [qreal, enumT e_Axis] $ refT $ objT c_QTransform
  , just $ mkMethod "scale" [qreal, qreal] $ refT $ objT c_QTransform
  , just $ mkMethod "setMatrix" [qreal, qreal, qreal, qreal, qreal, qreal, qreal, qreal, qreal] voidT
  , just $ mkMethod "shear" [qreal, qreal] $ refT $ objT c_QTransform
  --, just $ mkConstMethod "toAffline" [] $ objT c_QMatrix
  , just $ mkMethod "translate" [qreal, qreal] $ refT $ objT c_QTransform
  , just $ mkConstMethod "transposed" [] $ objT c_QTransform
  , just $ mkConstMethod' "type" "type_" [] $ enumT e_TransformationType
  , just $ mkMethod OpAddAssign [qreal] $ refT $ objT c_QTransform
  , just $ mkMethod' OpMultiplyAssign (operatorPreferredExtName' OpMultiplyAssign)
    [objT c_QTransform] $ refT $ objT c_QTransform
  , just $ mkMethod' OpMultiplyAssign (operatorPreferredExtName' OpMultiplyAssign ++ "Real")
    [qreal] $ refT $ objT c_QTransform
  , just $ mkMethod OpSubtractAssign [qreal] $ refT $ objT c_QTransform
  , just $ mkMethod OpDivideAssign [qreal] $ refT $ objT c_QTransform
  , test (qtVersion >= [4, 5]) $ mkStaticMethod "fromScale" [qreal, qreal] $ objT c_QTransform
  , test (qtVersion >= [4, 5]) $ mkStaticMethod "fromTranslate" [qreal, qreal] $ objT c_QTransform
  , just $ mkStaticMethod "quadToQuad" [objT c_QPolygonF, objT c_QPolygonF, refT $ objT c_QTransform] boolT
  , just $ mkStaticMethod "quadToSquare" [objT c_QPolygonF, refT $ objT c_QTransform] boolT
  , just $ mkStaticMethod "squareToQuad" [objT c_QPolygonF, refT $ objT c_QTransform] boolT
  ]

f_qFuzzyCompare =
  addReqIncludes [includeStd "QTransform"] $
  makeFn (ident "qFuzzyCompare") Nothing Pure [objT c_QTransform, objT c_QTransform] boolT

e_TransformationType =
  makeQtEnum (ident1 "QTransform" "TransformationType") [includeStd "QTransform"]
  [ (0x00, ["tx", "none"])
  , (0x01, ["tx", "translate"])
  , (0x02, ["tx", "scale"])
  , (0x04, ["tx", "rotate"])
  , (0x08, ["tx", "shear"])
  , (0x10, ["tx", "project"])
  ]
