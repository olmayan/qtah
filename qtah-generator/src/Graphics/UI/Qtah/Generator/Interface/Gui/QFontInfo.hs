-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QFontInfo (
  aModule,
  c_QFontInfo
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qreal)
import {-# SOURCE #-}Graphics.UI.Qtah.Generator.Interface.Gui.QFont (
  c_QFont,
  e_StyleHint,
  e_Style,
  )
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Gui", "QFontInfo"] $ 
  [ QtExport $ ExportClass c_QFontInfo
  ]

c_QFontInfo =
  addReqIncludes [includeStd "QFontInfo"] $
  classAddFeatures [Assignable, Copyable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QFontInfo") Nothing [] $
  collect
  [ just $ mkCtor "new" [objT c_QFont]
  , just $ mkConstMethod "bold" [] boolT
  , just $ mkConstMethod "exactMatch" [] boolT
  , just $ mkConstMethod "family" [] $ objT c_QString
  , just $ mkConstMethod "fixedPitch" [] boolT
  , just $ mkConstMethod "italic" [] boolT
  , just $ mkConstMethod "pixelSize" [] intT
  , just $ mkConstMethod "pointSize" [] intT
  , just $ mkConstMethod "pointSizeF" [] qreal
  , just $ mkConstMethod "style" [] $ enumT e_Style
  , just $ mkConstMethod "styleHint" [] $ enumT e_StyleHint
  , test (qtVersion >= [4, 8]) $ mkConstMethod "styleName" [] $ objT c_QString
  , test (qtVersion >= [5, 0]) $ mkMethod "swap" [refT $ objT c_QFontInfo] voidT
  , just $ mkConstMethod "weight" [] intT
  ]
