-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QImage (
  aModule,
  c_QImage,
  e_Format
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  mkStaticMethod,
  mkStaticMethod'
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (
  bitspaceT, boolT, charT, constT,
  enumT, intT, objT, ptrT, refT,
  ucharT, uintT, voidT
  )
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QIODevice (c_QIODevice)
import Graphics.UI.Qtah.Generator.Interface.Core.QPoint (c_QPoint)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_ImageConversionFlags,
  e_AspectRatioMode,
  e_GlobalColor,
  e_MaskMode,
  e_TransformationMode,
  qint64,
  qreal
  )
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Gui.QColor (
  c_QColor,
  qRgb
  )
import Graphics.UI.Qtah.Generator.Interface.Gui.QPaintDevice (c_QPaintDevice)
import Graphics.UI.Qtah.Generator.Interface.Gui.QTransform (c_QTransform)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Gui", "QImage"]
  [ QtExport $ ExportClass c_QImage
  , QtExport $ ExportEnum e_Format
  , QtExport $ ExportEnum e_InvertMode
  ]

c_QImage =
  addReqIncludes [includeStd "QImage"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QImage") Nothing [c_QPaintDevice] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithSizeAndFormat" [objT c_QSize, enumT e_Format]
  , just $ mkCtor "newWithSizeRawAndFormat" [intT, intT, enumT e_Format]
  --newFromData
  --newFromXpm
  , just $ mkCtor "newFromFile" [objT c_QString]
  , just $ mkCtor "newFromFileWithFormat" [objT c_QString, ptrT $ constT charT]
  , just $ mkConstMethod "allGray" [] boolT
  , test (qtVersion >= [4, 7]) $ mkConstMethod "bitPlaneCount" [] intT
  , just $ mkMethod "bits" [] $ ptrT ucharT
  , just $ mkConstMethod' "bits" "bitsConst" [] $ ptrT $ constT ucharT
  , test (qtVersion >= [4, 6]) $ mkConstMethod "byteCount" [] intT
  , just $ mkConstMethod "bytesPerLine" [] intT
  , just $ mkConstMethod "cacheKey" [] qint64
  , just $ mkConstMethod "color" [intT] qRgb
  , test (qtVersion >= [4, 7]) $ mkConstMethod "constBits" [] $ ptrT $ constT ucharT
  , test (qtVersion >= [4, 7]) $ mkConstMethod "constScanLine" [intT] $ ptrT $ constT ucharT
  , just $ mkConstMethod "convertToFormat" [enumT e_Format] $ objT c_QImage
  , just $ mkConstMethod' "convertToFormat" "convertToFormatWithFlags" [enumT e_Format, bitspaceT bs_ImageConversionFlags] $ objT c_QImage
  --convertToFormat QVector<QRgb>
  , just $ mkConstMethod "copy" [] $ objT c_QImage
  , just $ mkConstMethod' "copy" "copyRect" [objT c_QRect] $ objT c_QImage
  , just $ mkConstMethod' "copy" "copyRectRaw" [intT, intT, intT, intT] $ objT c_QImage
  , just $ mkConstMethod "createAlphaMask" [] $ objT c_QImage
  , just $ mkConstMethod' "createAlphaMask" "createAlphaMaskWithFlags" [bitspaceT bs_ImageConversionFlags] $ objT c_QImage
  , just $ mkConstMethod' "createHeuristicMask" "createHeuristicMaskClipTight" [] $ objT c_QImage
  , just $ mkConstMethod "createHeuristicMask" [boolT] $ objT c_QImage
  , just $ mkConstMethod' "createMaskFromColor" "createMaskFromColorInColor" [qRgb] $ objT c_QImage
  , just $ mkConstMethod "createMaskFromColor" [qRgb, enumT e_MaskMode] $ objT c_QImage
  , just $ mkConstMethod "depth" [] intT
  , just $ mkMethod "fill" [uintT] voidT
  , test (qtVersion >= [4, 8]) $ mkMethod' "fill" "fillColor" [objT c_QColor] voidT
  , test (qtVersion >= [4, 8]) $ mkMethod' "fill" "fillGC" [enumT e_GlobalColor] voidT
  , just $ mkConstMethod "format" [] $ enumT e_Format
  , just $ mkConstMethod "hasAlphaChannel" [] boolT
  , just $ mkConstMethod "height" [] intT
  , just $ mkMethod' "invertPixels" "invertPixelsRgb" [] voidT
  , just $ mkMethod "invertPixels" [enumT e_InvertMode] voidT
  , just $ mkConstMethod "isGrayscale" [] boolT
  , just $ mkConstMethod "isNull" [] boolT
  , just $ mkMethod "load" [objT c_QString] boolT
  , just $ mkMethod' "load" "loadWithFormat" [objT c_QString, ptrT $ constT charT] boolT
  , just $ mkMethod' "loadFromData" "loadFromDataUChar" [ptrT $ constT ucharT, uintT] boolT
  , just $ mkMethod' "loadFromData" "loadFromDataUCharWithFormat" [ptrT $ constT ucharT, uintT, ptrT $ constT charT] boolT
  , just $ mkMethod "loadFromData" [objT c_QByteArray] boolT
  , just $ mkMethod' "loadFromData" "loadFromDataWithFormat" [objT c_QByteArray, ptrT $ constT charT] boolT
  , just $ mkConstMethod "mirrored" [boolT, boolT] $ objT c_QImage
  , just $ mkConstMethod "pixel" [objT c_QPoint] qRgb
  , just $ mkConstMethod' "pixel" "pixelRaw" [boolT, boolT] qRgb
  , test (qtVersion >= [5, 6]) $ mkConstMethod "pixelColor" [objT c_QPoint] $ objT c_QColor
  , test (qtVersion >= [5, 6]) $ mkConstMethod' "pixelColor" "pixelColorRaw" [intT, intT] $ objT c_QColor
   --pixelFormat
  , just $ mkConstMethod "pixelIndex" [objT c_QPoint] intT
  , just $ mkConstMethod' "pixelIndex" "pixelIndexRaw" [intT, intT] intT
  , just $ mkConstMethod "rect" [] $ objT c_QRect
  , just $ mkConstMethod "rgbSwapped" [] $ objT c_QImage
  , just $ mkConstMethod "save" [objT c_QString] boolT
  , just $ mkConstMethod' "save" "saveWithFormat" [objT c_QString, ptrT $ constT charT] boolT
  , just $ mkConstMethod' "save" "saveWithFormatAndQuality" [objT c_QString, ptrT $ constT charT, intT] boolT
  , just $ mkConstMethod' "save" "saveToIODevice" [ptrT $ objT c_QIODevice] boolT
  , just $ mkConstMethod' "save" "saveToIODeviceWithFormat" [ptrT $ objT c_QIODevice, ptrT $ constT charT] boolT
  , just $ mkConstMethod' "save" "saveToIODeviceWithFormatAndQuality" [ptrT $ objT c_QIODevice, ptrT $ constT charT, intT] boolT
  , just $ mkConstMethod "scaled" [objT c_QSize] $ objT c_QImage
  , just $ mkConstMethod' "scaled" "scaledWithAspectRatio" [objT c_QSize, enumT e_AspectRatioMode] $ objT c_QImage
  , just $ mkConstMethod' "scaled" "scaledWithAspectRatioAndTransformation" [objT c_QSize, enumT e_AspectRatioMode, enumT e_TransformationMode] $ objT c_QImage
  , just $ mkConstMethod' "scaled" "scaledRaw" [intT, intT] $ objT c_QImage
  , just $ mkConstMethod' "scaled" "scaledRawWithAspectRatio" [intT, intT, enumT e_AspectRatioMode] $ objT c_QImage
  , just $ mkConstMethod' "scaled" "scaledRawWithAspectRatioAndTransformation" [intT, intT, enumT e_AspectRatioMode, enumT e_TransformationMode] $ objT c_QImage
  , just $ mkConstMethod "scaledToHeight" [intT] $ objT c_QImage
  , just $ mkConstMethod' "scaledToHeight" "scaledToHeightWithTransformation" [intT, enumT e_TransformationMode] $ objT c_QImage
  , just $ mkConstMethod "scaledToWidth" [intT] $ objT c_QImage
  , just $ mkConstMethod' "scaledToWidth" "scaledToWidthWithTransformation" [intT, enumT e_TransformationMode] $ objT c_QImage
  , just $ mkConstMethod "scanLine" [intT] $ ptrT ucharT
  , just $ mkConstMethod' "scanLine" "scanLineConst" [intT] $ ptrT $ constT ucharT
  , just $ mkMethod "setColor" [intT, qRgb] voidT
  , just $ mkMethod "setPixel" [objT c_QPoint, uintT] voidT
  , just $ mkMethod' "setPixel" "setPixelRaw" [intT, intT, uintT] voidT
  , test (qtVersion >= [5, 6]) $ mkMethod "setPixelColor" [objT c_QPoint, objT c_QColor] voidT
  , test (qtVersion >= [5, 6]) $ mkMethod' "setPixelColor" "setPixelColorRaw" [intT, intT, objT c_QColor] voidT
  , just $ mkMethod "setText" [objT c_QString, objT c_QString] voidT
  , just $ mkConstMethod "size" [] $ objT c_QSize
  , test (qtVersion >= [4, 8]) $ mkMethod "swap" [refT $ objT c_QImage] voidT
  , just $ mkConstMethod "text" [] $ objT c_QString
  , just $ mkConstMethod' "text" "textWithKey" [objT c_QString] $ objT c_QString
  , just $ mkConstMethod "textKeys" [] $ objT c_QStringList
  , just $ mkConstMethod "transformed" [objT c_QTransform] $ objT c_QImage
  , just $ mkConstMethod' "transformed" "transformedWithMode" [objT c_QTransform, enumT e_TransformationMode] $ objT c_QImage
  --, just $ mkConstMethod' "transformed" "transformed_" [objT c_QMatrix] $ objT c_QImage
  --, just $ mkConstMethod' "transformed" "transformedWithMode_" [objT c_QMatrix, enumT e_TransformationMode] $ objT c_QImage
  , just $ mkConstMethod "valid" [objT c_QPoint] boolT
  , just $ mkConstMethod' "valid" "validRaw" [intT, intT] boolT
  , just $ mkConstMethod "width" [] intT
  --operator QVariant
  , just $ mkStaticMethod' "fromData" "fromDataUChar" [ptrT $ constT ucharT, intT] $ objT c_QImage
  , just $ mkStaticMethod' "fromData" "fromDataUCharWithFormat" [ptrT $ constT ucharT, intT, ptrT $ constT charT] $ objT c_QImage
  , just $ mkStaticMethod "fromData" [objT c_QByteArray] $ objT c_QImage
  , just $ mkStaticMethod' "fromData" "fromDataWithFormat" [objT c_QByteArray, ptrT $ constT charT] $ objT c_QImage
  --, just $ mkStaticMethod "toImageFormat" [objT c_QPixelFormat] $ enumT e_Format
  --, just $ mkStaticMethod "toPixelFormat" [enumT e_Format] $ objT c_QPixelFormat
  , test (qtVersion >= [4, 3]) $ mkStaticMethod "trueMatrix" [objT c_QTransform, intT, intT] $ objT c_QTransform
  --, just $ mkStaticMethod' "trueMatrix" "trueMatrix_" [objT c_QMatrix, intT, intT] $ objT c_QMatrix
  , test (qtVersion >= [4, 6]) $ mkProp "colorCount" intT
  --, just $ mkProp "colorTable" --QVector<qRgb>
  , just $ mkProp "devicePixelRatio" qreal
  , just $ mkProp "dotsPerMeterX" intT
  , just $ mkProp "dotsPerMeterY" intT
  , just $ mkProp "offset" $ objT c_QPoint
  ]

e_Format =
  makeQtEnum (ident1 "QImage" "Format") [includeStd "QImage"]
  [ (0, ["format", "_", "invalid"])
  , (1, ["format", "_", "mono"])
  , (2, ["format", "_", "mono", "LSB"])
  , (3, ["format", "_", "indexed8"])
  , (4, ["format", "_", "RGB32"])
  , (5, ["format", "_", "ARGB32"])
  , (6, ["format", "_", "ARGB32", "_", "premultiplied"])
  , (7, ["format", "_", "RGB16"])
  , (8, ["format", "_", "ARGB8565", "_", "premultiplied"])
  , (9, ["format", "_", "RGB666"])
  , (10, ["format", "_", "ARGB6666", "_", "premultiplied"])
  , (11, ["format", "_", "RGB555"])
  , (12, ["format", "_", "ARGB8555", "_", "premultiplied"])
  , (13, ["format", "_", "RGB888"])
  , (14, ["format", "_", "RGB444"])
  , (15, ["format", "_", "ARGB4444", "_", "premultiplied"])
  , (16, ["format", "_", "RGBX8888"])
  , (17, ["format", "_", "RGBA8888"])
  , (18, ["format", "_", "RGBA8888", "_", "premultiplied"])
  , (19, ["format", "_", "BGR30"])
  , (20, ["format", "_", "A2BGR30", "_", "premultiplied"])
  , (21, ["format", "_", "RGB30"])
  , (22, ["format", "_", "A2RGB30", "_", "premultiplied"])
  , (23, ["format", "_", "alpha8"])
  , (24, ["format", "_", "grayscale8"])
  ]

e_InvertMode  =
  makeQtEnum (ident1 "QImage" "InvertMode") [includeStd "QImage"]
  [ (0, ["invert", "rgb"])
  , (1, ["invert", "rgba"])
  ]
