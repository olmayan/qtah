-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QBitmap (
  aModule,
  c_QBitmap
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkStaticMethod,
  mkStaticMethod'
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (
  bitspaceT, charT, constT, enumT,
  intT, objT, ptrT, refT, ucharT,
  voidT
  )
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (bs_ImageConversionFlags)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Gui.QImage (
  c_QImage,
  e_Format
  )
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Gui.QPixmap (c_QPixmap)
import Graphics.UI.Qtah.Generator.Interface.Gui.QTransform (c_QTransform)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Gui", "QBitmap"]
  [ QtExport $ ExportClass c_QBitmap
  ]

c_QBitmap =
  addReqIncludes [includeStd "QBitmap"] $
  classAddFeatures [Assignable, Copyable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QBitmap") Nothing [c_QPixmap] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newFromPixmap" [objT c_QPixmap]
  , just $ mkCtor "newWithSizeRaw" [intT, intT]
  , just $ mkCtor "newWithSize" [objT c_QSize]
  , just $ mkCtor "newFromFile" [objT c_QString]
  , just $ mkCtor "newFromFileWithFormat" [objT c_QString, ptrT $ constT charT]
  , just $ mkMethod "clear" [] voidT
  , test (qtVersion >= [4, 8]) $ mkMethod "swap" [refT $ objT c_QBitmap] voidT
  , just $ mkConstMethod "transformed" [objT c_QTransform] $ objT c_QBitmap
  --operator QVariant
  , just $ mkStaticMethod "fromData" [objT c_QSize, ptrT $ constT ucharT] $ objT c_QBitmap
  , just $ mkStaticMethod' "fromData" "fromDataWithFormat" [objT c_QSize, ptrT $ constT ucharT, enumT e_Format] $ objT c_QBitmap
  , just $ mkStaticMethod "fromImage" [objT c_QImage] $ objT c_QBitmap
  , just $ mkStaticMethod' "fromImage" "fromImageWithFlags" [objT c_QImage, bitspaceT bs_ImageConversionFlags] $ objT c_QBitmap
  ]
