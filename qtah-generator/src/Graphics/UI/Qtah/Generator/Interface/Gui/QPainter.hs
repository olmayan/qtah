-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QPainter (
  aModule,
  bs_RenderHints,
  c_QPainter,
  e_RenderHint,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportBitspace, ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkBoolHasProp,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, ptrT, voidT)
import Graphics.UI.Qtah.Generator.Flag (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QLine (c_QLine)
import Graphics.UI.Qtah.Generator.Interface.Core.QLineF (c_QLineF)
import Graphics.UI.Qtah.Generator.Interface.Core.QPoint (c_QPoint)
import Graphics.UI.Qtah.Generator.Interface.Core.QPointF (c_QPointF)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QRectF (c_QRectF)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_ImageConversionFlags,
  e_BGMode,
  e_BrushStyle,
  e_ClipOperation,
  e_FillRule,
  e_GlobalColor,
  e_LayoutDirection,
  e_SizeMode,
  qreal,
  )
import Graphics.UI.Qtah.Generator.Interface.Gui.QBrush (c_QBrush)
import Graphics.UI.Qtah.Generator.Interface.Gui.QColor (c_QColor)
import Graphics.UI.Qtah.Generator.Interface.Gui.QFontInfo (c_QFontInfo)
import Graphics.UI.Qtah.Generator.Interface.Gui.QImage (c_QImage)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPaintDevice (c_QPaintDevice)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPen (c_QPen)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPixmap (c_QPixmap)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPicture (c_QPicture)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPolygon (c_QPolygon)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPolygonF (c_QPolygonF)
import Graphics.UI.Qtah.Generator.Interface.Gui.QRegion (c_QRegion)
import Graphics.UI.Qtah.Generator.Interface.Gui.QTransform (c_QTransform)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Gui", "QPainter"] $
  (QtExport $ ExportClass c_QPainter) :
  [ QtExport $ ExportBitspace bs_RenderHints
  , QtExport $ ExportEnum e_CompositionMode
  , QtExport $ ExportEnum e_RenderHint
  ]

c_QPainter =
  addReqIncludes [includeStd "QPainter"] $
  classSetEntityPrefix "" $
  makeClass (ident "QPainter") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithDevice" [ptrT $ objT c_QPaintDevice]
  , test (qtVersion >= [4, 6]) $ mkMethod "beginNativePainting" [] voidT
  , just $ mkConstMethod' "boundingRect" "boundingRectF" [objT c_QRectF, intT, objT c_QString] $ objT c_QRectF
  , just $ mkConstMethod "boundingRect" [objT c_QRect, intT, objT c_QString] $ objT c_QRect
  , just $ mkConstMethod' "boundingRect" "boundingRectRaw" [intT, intT, intT, intT, intT, objT c_QString] $ objT c_QRect
  --, just $ mkConstMethod' "boundingRect" "boundingRectFWithTextOption" [objT c_QRectF, intT, objT c_QString, objT c_QTextOption] $ objT c_QRectF
  , just $ mkConstMethod' "brushOrigin" "brushOriginF" [] $ objT c_QPointF
  , test (qtVersion >= [4, 8]) $ mkConstMethod "clipBoundingRect" [] $ objT c_QRectF
  , just $ mkConstMethod "device" [] $ ptrT $ objT c_QPaintDevice
  , just $ mkConstMethod "deviceTransform" [] $ objT c_QTransform
  , just $ mkMethod' "drawArc" "drawArcF" [objT c_QRectF, intT, intT] voidT
  , just $ mkMethod "drawArc" [objT c_QRect, intT, intT] voidT
  , just $ mkMethod' "drawArc" "drawArcRaw" [intT, intT, intT, intT, intT, intT] voidT
  , just $ mkMethod' "drawChord" "drawChordF" [objT c_QRectF, intT, intT] voidT
  , just $ mkMethod' "drawChord" "drawChordRaw" [intT, intT, intT, intT, intT, intT] voidT
  , just $ mkMethod "drawChord" [objT c_QRect, intT, intT] voidT
  , just $ mkMethod' "drawConvexPolygon" "drawConvexPolygonF" [objT c_QPolygonF] voidT
  , just $ mkMethod' "drawConvexPolygon" "drawConvexPolygonWithPtrF" [ptrT $ objT c_QPointF, intT] voidT
  , just $ mkMethod "drawConvexPolygon" [objT c_QPolygon] voidT
  , just $ mkMethod' "drawConvexPolygon" "drawConvexPolygonWithPtr" [ptrT $ objT c_QPoint, intT] voidT
  , just $ mkMethod' "drawEllipse" "drawEllipseF" [objT c_QRectF] voidT
  , just $ mkMethod "drawEllipse" [objT c_QRect] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod' "drawEllipse" "drawEllipseFromCenterF" [objT c_QPointF, qreal, qreal] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod' "drawEllipse" "drawEllipseFromCenter" [objT c_QPoint, intT, intT] voidT
  --, test (qtVersion >= [4, 8]) $ mkMethod "drawGlyphRun" [objT c_QPointF, objT c_QGlyphRun] voidT
  , just $ mkMethod' "drawImage" "drawImagePortionInRectF" [objT c_QRectF, objT c_QImage, objT c_QRectF] voidT
  , just $ mkMethod' "drawImage" "drawImagePortionInRectFWithFlags" [objT c_QRectF, objT c_QImage, objT c_QRectF, bitspaceT bs_ImageConversionFlags] voidT
  , just $ mkMethod' "drawImage" "drawImagePortionInRect" [objT c_QRect, objT c_QImage, objT c_QRect] voidT
  , just $ mkMethod' "drawImage" "drawImagePortionInRectWithFlags" [objT c_QRect, objT c_QImage, objT c_QRect, bitspaceT bs_ImageConversionFlags] voidT
  , just $ mkMethod' "drawImage" "drawImagePortionAtPointF" [objT c_QPointF, objT c_QImage, objT c_QRectF] voidT
  , just $ mkMethod' "drawImage" "drawImagePortionAtPointFWithFlags" [objT c_QPointF, objT c_QImage, objT c_QRectF, bitspaceT bs_ImageConversionFlags] voidT
  , just $ mkMethod' "drawImage" "drawImagePortionAtPoint" [objT c_QPoint, objT c_QImage, objT c_QRect] voidT
  , just $ mkMethod' "drawImage" "drawImagePortionAtPointWithFlags" [objT c_QPoint, objT c_QImage, objT c_QRect, bitspaceT bs_ImageConversionFlags] voidT
  , just $ mkMethod' "drawImage" "drawImageInRectF" [objT c_QRectF, objT c_QImage] voidT
  , just $ mkMethod' "drawImage" "drawImageInRect" [objT c_QRect, objT c_QImage] voidT
  , just $ mkMethod' "drawImage" "drawImageAtPointF" [objT c_QPointF, objT c_QImage] voidT
  , just $ mkMethod "drawImage" [objT c_QPoint, objT c_QImage] voidT
  , just $ mkMethod' "drawImage" "drawImageAtRaw" [intT, intT, objT c_QImage] voidT
  , just $ mkMethod' "drawImage" "drawImageAtRawWithFlags" [intT, intT, objT c_QImage, intT, intT, intT, intT, bitspaceT bs_ImageConversionFlags] voidT
  , just $ mkMethod' "drawLine" "drawLineF" [objT c_QLineF] voidT
  , just $ mkMethod "drawLine" [objT c_QLine] voidT
  , just $ mkMethod' "drawLine" "drawLineRaw" [intT, intT, intT, intT] voidT
  , just $ mkMethod' "drawLine" "drawLinePoints" [objT c_QPoint, objT c_QPoint] voidT
  , just $ mkMethod' "drawLine" "drawLinePointsF" [objT c_QPointF, objT c_QPointF] voidT
  -- TODO drawLines
  --, just $ mkMethod "draPath" [objT c_QPainterPath] voidT
  , just $ mkMethod' "drawPicture" "drawPictureAtPointF" [objT c_QPointF, objT c_QPicture] voidT
  , just $ mkMethod "drawPicture" [objT c_QPoint, objT c_QPicture] voidT
  , just $ mkMethod' "drawPicture" "drawPictureAtRaw" [intT, intT, objT c_QPicture] voidT
  , just $ mkMethod' "drawPie" "drawPieF" [objT c_QRectF, intT, intT] voidT
  , just $ mkMethod' "drawPie" "drawPieRaw" [intT, intT, intT, intT, intT, intT] voidT
  , just $ mkMethod "drawPie" [objT c_QRect, intT, intT] voidT
  , just $ mkMethod' "drawPixmap" "drawPixmapPortionInRectF" [objT c_QRectF, objT c_QPixmap, objT c_QRectF] voidT
  , just $ mkMethod' "drawPixmap" "drawPixmapPortionInRect" [objT c_QRect, objT c_QPixmap, objT c_QRect] voidT
  , just $ mkMethod' "drawPixmap" "drawPixmapPortionInRaw" [intT, intT, intT, intT, objT c_QPixmap, intT, intT, intT, intT] voidT
  , just $ mkMethod' "drawPixmap" "drawPixmapPortionAtRaw" [intT, intT, objT c_QPixmap, intT, intT, intT, intT] voidT
  , just $ mkMethod' "drawPixmap" "drawPixmapPortionAtPointF" [objT c_QPointF, objT c_QPixmap, objT c_QRectF] voidT
  , just $ mkMethod' "drawPixmap" "drawPixmapPortionAtPoint" [objT c_QPoint, objT c_QPixmap, objT c_QRect] voidT
  , just $ mkMethod' "drawPixmap" "drawPixmapInRect" [objT c_QRect, objT c_QPixmap] voidT
  , just $ mkMethod' "drawPixmap" "drawPixmapInRaw" [intT, intT, intT, intT, objT c_QPixmap] voidT
  , just $ mkMethod' "drawPixmap" "drawPixmapAtPointF" [objT c_QPointF, objT c_QPixmap] voidT
  , just $ mkMethod "drawPixmap" [objT c_QPoint, objT c_QPixmap] voidT
  , just $ mkMethod' "drawPixmap" "drawPixmapAtRaw" [intT, intT, objT c_QPixmap] voidT
  -- TODO (>=4.7)drawPixMapFragments
  , just $ mkMethod' "drawPoint" "drawPointF" [objT c_QPointF] voidT
  , just $ mkMethod "drawPoint" [objT c_QPoint] voidT
  , just $ mkMethod' "drawPoint" "drawPointRaw" [intT, intT] voidT
  -- TODO drawPoints
  , just $ mkMethod' "drawPolygon" "drawPolygonF" [objT c_QPolygonF] voidT
  , just $ mkMethod' "drawPolygon" "drawPolygonFWithFillRule" [objT c_QPolygonF, enumT e_FillRule] voidT
  , just $ mkMethod "drawPolygon" [objT c_QPolygon] voidT
  , just $ mkMethod' "drawPolygon" "drawPolygonWithFillRule" [objT c_QPolygon, enumT e_FillRule] voidT
  , just $ mkMethod' "drawPolyline" "drawPolylineF" [objT c_QPolygonF] voidT
  , just $ mkMethod "drawPolyline" [objT c_QPolygon] voidT
  , just $ mkMethod' "drawRect" "drawRectF" [objT c_QRectF] voidT
  , just $ mkMethod "drawRect" [objT c_QRect] voidT
  , just $ mkMethod' "drawRect" "drawRectRaw" [intT, intT, intT, intT] voidT
  -- TODO drawRects
  , test (qtVersion >= [4, 4]) $ mkMethod' "drawRoundedRect" "drawRoundedRectF" [objT c_QRectF, qreal, qreal] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod' "drawRoundedRect" "drawRoundedRectFWithSizeMode" [objT c_QRectF, qreal, qreal, enumT e_SizeMode] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod "drawRoundedRect" [objT c_QRect, qreal, qreal] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod' "drawRoundedRect" "drawRoundedRectWithSizeMode" [objT c_QRect, qreal, qreal, enumT e_SizeMode] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod' "drawRoundedRect" "drawRoundedRectRaw" [intT, intT, intT, intT, qreal, qreal] voidT
  , test (qtVersion >= [4, 4]) $ mkMethod' "drawRoundedRect" "drawRoundedRectRawWithSizeMode" [intT, intT, intT, intT, qreal, qreal, enumT e_SizeMode] voidT
  -- TODO drawStaticText
  , just $ mkMethod' "drawText" "drawTextAtPointF" [objT c_QPointF, objT c_QString] voidT
  , just $ mkMethod "drawText" [objT c_QPoint, objT c_QString] voidT
  , just $ mkMethod' "drawText" "drawTextAtRaw" [intT, intT, objT c_QString] voidT
  , just $ mkMethod' "drawText" "drawTextInRectF" [objT c_QRectF, intT, objT c_QString] voidT
  , just $ mkMethod' "drawText" "drawTextInRectFWithBounding" [objT c_QRectF, intT, objT c_QString, ptrT $ objT c_QRectF] voidT
  , just $ mkMethod' "drawText" "drawTextInRect" [objT c_QRect, intT, objT c_QString] voidT
  , just $ mkMethod' "drawText" "drawTextInRectWithBounding" [objT c_QRect, intT, objT c_QString, ptrT $ objT c_QRect] voidT
  , just $ mkMethod' "drawText" "drawTextInRaw" [intT, intT, intT, intT, intT, objT c_QString] voidT
  , just $ mkMethod' "drawText" "drawTextInRawWithBounding" [intT, intT, intT, intT, intT, objT c_QString, ptrT $ objT c_QRect] voidT
  --, just $ mkMethod' "drawText" "drawText" [objT c_QRectF, objT c_QString, objT c_QTextOption] voidT
  , just $ mkMethod' "drawTiledPixmap" "drawTiledPixmapF" [objT c_QRectF, objT c_QPixmap] voidT
  , just $ mkMethod' "drawTiledPixmap" "drawTiledPixmapFFromPos" [objT c_QRectF, objT c_QPixmap, objT c_QPointF] voidT
  , just $ mkMethod' "drawTiledPixmap" "drawTiledPixmapRaw" [intT, intT, intT, intT, objT c_QPixmap] voidT
  , just $ mkMethod' "drawTiledPixmap" "drawTiledPixmapRawFromPos" [intT, intT, intT, intT, objT c_QPixmap, intT, intT] voidT
  , just $ mkMethod "drawTiledPixmap" [objT c_QRect, objT c_QPixmap] voidT
  , just $ mkMethod' "drawTiledPixmap" "drawTiledPixmapFromPos" [objT c_QRect, objT c_QPixmap, objT c_QPoint] voidT
  , just $ mkMethod "end" [] boolT
  , test (qtVersion >= [4, 6]) $ mkMethod "endNativePainting" [] voidT
  , just $ mkMethod' "eraseRect" "eraseRectF" [objT c_QRectF] voidT
  , just $ mkMethod' "eraseRect" "eraseRectRaw" [intT, intT, intT, intT] voidT
  , just $ mkMethod "eraseRect" [objT c_QRect] voidT
  --, just $ mkMethod "fillPath" [objT c_QPainterPath, objT c_QBrush] voidT
  , just $ mkMethod' "fillRect" "fillRectF" [objT c_QRectF, objT c_QBrush] voidT
  , just $ mkMethod' "fillRect" "fillRectRaw" [intT, intT, intT, intT, objT c_QBrush] voidT
  , just $ mkMethod "fillRect" [objT c_QRect, objT c_QBrush] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod' "fillRect" "fillRectFWithColor" [objT c_QRectF, objT c_QColor] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod' "fillRect" "fillRectRawWithColor" [intT, intT, intT, intT, objT c_QColor] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod' "fillRect" "fillRectWithColor" [objT c_QRect, objT c_QColor] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod' "fillRect" "fillRectFWithGC" [objT c_QRectF, enumT e_GlobalColor] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod' "fillRect" "fillRectRawWithGC" [intT, intT, intT, intT, enumT e_GlobalColor] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod' "fillRect" "fillRectWithGC" [objT c_QRect, enumT e_GlobalColor] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod' "fillRect" "fillRectFWithBS" [objT c_QRectF, enumT e_BrushStyle] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod' "fillRect" "fillRectRawWithBS" [intT, intT, intT, intT, enumT e_BrushStyle] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod' "fillRect" "fillRectWithBS" [objT c_QRect, enumT e_BrushStyle] voidT
  , just $ mkConstMethod "fontInfo" [] $ objT c_QFontInfo
  --, just $ mkConstMethod "fontMetrics" [] $ objT c_QFontMetrics
  , just $ mkConstMethod "isActive" [] boolT
  --, just $ mkConstMethod "paintEngine" [] $ ptrT $ objT c_QPaintEngine
  , just $ mkConstMethod "renderHints" [] $ bitspaceT bs_RenderHints
  , just $ mkMethod "resetTransform" [] voidT
  , just $ mkMethod "restore" [] voidT
  , just $ mkMethod "rotate" [qreal] voidT
  , just $ mkMethod "save" [] voidT
  , just $ mkMethod "scale" [qreal, qreal] voidT
  , just $ mkMethod' "setBrushOrigin" "setBrushOriginF" [objT c_QPointF] voidT
  --, just $ mkMethod' "setClipPath" "PathWithOperation" [objT c_QPainterPath, enumT e_ClipOperation] voidT
  , just $ mkMethod' "setClipRect" "setClipRectFWithOperation" [objT c_QRectF, enumT e_ClipOperation] voidT
  , just $ mkMethod' "setClipRect" "setClipRectF" [objT c_QRectF] voidT
  , just $ mkMethod' "setClipRect" "setClipRectWithOperation" [objT c_QRect, enumT e_ClipOperation] voidT
  , just $ mkMethod "setClipRect" [objT c_QRect] voidT
  , just $ mkMethod' "setClipRect" "setClipRectRaw" [intT, intT, intT, intT] voidT
  , just $ mkMethod' "setClipRect" "setClipRectRawWithOperation" [intT, intT, intT, intT, enumT e_ClipOperation] voidT
  , just $ mkMethod' "setClipRegion" "setClipRegionWithOperation" [objT c_QRegion, enumT e_ClipOperation] voidT
  , just $ mkMethod' "setRenderHint" "setRenderHintOn" [enumT e_RenderHint] voidT
  , just $ mkMethod "setRenderHint" [enumT e_RenderHint, boolT] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod' "setRenderHints" "setRenderHintsOn" [bitspaceT bs_RenderHints] voidT
  , test (qtVersion >= [4, 2]) $ mkMethod "setRenderHints" [bitspaceT bs_RenderHints, boolT] voidT
  , test (qtVersion >= [4, 3]) $ mkMethod' "setTransform" "setTransformCombine" [objT c_QTransform, boolT] voidT
  , just $ mkMethod' "setViewport" "setViewportRaw" [intT, intT, intT, intT] voidT
  , test (qtVersion >= [4, 3]) $ mkMethod' "setWorldTransform" "setWorldTransformCombine" [objT c_QTransform, boolT] voidT
  , just $ mkMethod "shear" [qreal, qreal] voidT
  --, just $ mkMethod "strokePath" [objT c_QPainterPath, objT c_QPen] voidT
  , test (qtVersion >= [4, 3]) $ mkConstMethod "testRenderHint" [enumT e_RenderHint] boolT
  , just $ mkMethod' "translate" "translateF" [objT c_QPointF] voidT
  , just $ mkMethod' "translate" "translateRaw" [qreal, qreal] voidT
  , just $ mkMethod "translate" [objT c_QPoint] voidT
  , just $ mkProp "background" $ objT c_QBrush
  , just $ mkProp "backgroundMode" $ enumT e_BGMode
  , just $ mkProp "brush" $ objT c_QBrush
  , just $ mkProp "brushOrigin" $ objT c_QPoint
  --, just $ mkProp "clipPath" $ objT c_QPainterPath
  , just $ mkBoolHasProp "clipping"
  , just $ mkProp "clipRegion" $ objT c_QRegion
  , just $ mkProp "compositionMode" $ enumT e_CompositionMode
  --, just $ mkProp "font" $ objT c_QFont
  , just $ mkProp "layoutDirection" $ enumT e_LayoutDirection
  , test (qtVersion >= [4, 2]) $ mkProp "opacity" qreal
  , just $ mkProp "pen" $ objT c_QPen
  , test (qtVersion >= [4, 3]) $ mkProp "transform" $ objT c_QTransform
  , just $ mkProp "viewTransformEnabled" boolT
  , just $ mkProp "viewport" $ objT c_QRect
  , just $ mkProp "window" $ objT c_QRect
  , test (qtVersion >= [4, 2]) $ mkProp "worldMatrixEnabled" boolT
  , test (qtVersion >= [4, 3]) $ mkProp "worldTransform" $ objT c_QTransform
  ]

e_CompositionMode =
  makeQtEnum (ident1 "QPainter" "CompositionMode") [includeStd "QPainter"]
  [ (0, ["source", "over"])
  , (1, ["destination", "over"])
  , (2, ["clear"])
  , (3, ["source"])
  , (4, ["destination"])
  , (5, ["source", "in"])
  , (6, ["destination", "in"])
  , (7, ["source", "out"])
  , (8, ["destination", "out"])
  , (9, ["source", "atop"])
  , (10, ["destination", "atop"])
  , (11, ["xor"])
  , (12, ["plus"])
  , (13, ["multiply"])
  , (14, ["screen"])
  , (15, ["overlay"])
  , (16, ["darken"])
  , (17, ["lighten"])
  , (18, ["color", "dodge"])
  , (19, ["color", "burn"])
  , (20, ["hard", "light"])
  , (21, ["soft", "light"])
  , (22, ["difference"])
  , (23, ["exclusion"])
  , (24, ["source", "or", "destination"])
  , (25, ["source", "and", "destination"])
  , (26, ["source", "xor", "destination"])
  , (27, ["not", "source", "and", "not", "destination"])
  , (28, ["not", "source", "or", "not", "destination"])
  , (29, ["not", "source", "xor", "destination"])
  , (30, ["not", "source"])
  , (31, ["not", "source", "and", "destination"])
  , (32, ["source", "and", "not", "destination"])
  , (33, ["not", "source", "or", "destination"])
  , (35, ["clear", "destination"])
  , (36, ["set", "destination"])
  , (37, ["not", "destination"])
  , (34, ["source", "or", "not", "destination"])
  ]

(e_RenderHint, bs_RenderHints) =
  makeQtEnumBitspace (ident1 "QPainter" "RenderHint") "RenderHints" [includeStd "QPainter"]
  [ (0x01, ["antialiasing"])
  , (0x02, ["text", "antialiasing"])
  , (0x04, ["smooth", "pixmap", "transform"])
  , (0x08, ["high", "quality", "antialiasing"])
  , (0x10, ["non", "cosmetic", "default", "pen"])
  , (0x20, ["qt4", "compatible", "painting"])
  ]
