-- This file is part of Qtah.
--
-- Copyright 2016 Bryan Gardiner <bog@khumba.net>
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QMovie (
  aModule,
  c_QMovie,
  e_MovieState
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QIODevice (c_QIODevice)
import Graphics.UI.Qtah.Generator.Interface.Core.QObject (c_QObject)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Gui.QColor (c_QColor)
import Graphics.UI.Qtah.Generator.Interface.Gui.QImage (c_QImage)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPixmap (c_QPixmap)
import Graphics.UI.Qtah.Generator.Interface.Internal.Listener (
  c_Listener,
  c_ListenerInt,
  c_ListenerQMovieState,
  c_ListenerQRect,
  c_ListenerQSize
  )
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

{-# ANN module "HLint: ignore Use camelCase" #-}

aModule =
  AQtModule $
  makeQtModule ["Gui", "QMovie"] $
  map QtExport
  [ ExportClass c_QMovie
  , ExportEnum e_CacheMode
  , ExportEnum e_MovieState
  ] ++
  map QtExportSignal signals

c_QMovie =
  addReqIncludes [includeStd "QMovie"] $
  classSetEntityPrefix "" $
  makeClass (ident "QMovie") Nothing [c_QObject] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithParent" [ptrT $ objT c_QObject]
  , just $ mkCtor "newFromIODevice" [ptrT $ objT c_QIODevice]
  , just $ mkCtor "newFromIODeviceWithFormat" [ptrT $ objT c_QIODevice, objT c_QByteArray]
  , just $ mkCtor "newFromIODeviceWithFormatAndParent" [ptrT $ objT c_QIODevice, objT c_QByteArray, ptrT $ objT c_QObject]
  , just $ mkCtor "newFromFile" [objT c_QString]
  , just $ mkCtor "newFromFileWithFormat" [objT c_QString, objT c_QByteArray]
  , just $ mkCtor "newFromFileWithFormatAndParent" [objT c_QString, objT c_QByteArray, ptrT $ objT c_QObject]
  , just $ mkConstMethod "currentFrameNumber" [] intT
  , just $ mkConstMethod "currentImage" [] $ objT c_QImage
  , just $ mkConstMethod "currentPixmap" [] $ objT c_QPixmap
  , just $ mkConstMethod "frameCount" [] intT
  , just $ mkConstMethod "frameRect" [] $ objT c_QRect
  , just $ mkConstMethod "isValid" [] boolT
  , just $ mkMethod "jumpToFrame" [intT] boolT
  , just $ mkMethod "jumpToNextFrame" [] boolT
  , just $ mkConstMethod "loopCount" [] intT
  , just $ mkConstMethod "nextFrameDelay" [] intT
  , just $ mkMethod "setPaused" [boolT] voidT
  , just $ mkMethod "start" [] voidT
  , just $ mkConstMethod "state" [] $ enumT e_MovieState
  , just $ mkMethod "stop" [] voidT
  --supportedFormats
  , just $ mkProp "backgroundColor" $ objT c_QColor
  , just $ mkProp "cacheMode" $ enumT e_CacheMode
  , just $ mkProp "device" $ ptrT $ objT c_QIODevice
  , just $ mkProp "fileName" $ objT c_QString
  , just $ mkProp "format" $ objT c_QByteArray
  , test (qtVersion >= [4, 1]) $ mkProp "scaledSize" $ objT c_QSize
  , just $ mkProp "speed" intT
  ]

signals =
  [ --error
    makeSignal c_QMovie "finished" c_Listener
  , makeSignal c_QMovie "frameChanged" c_ListenerInt
  , makeSignal c_QMovie "resized" c_ListenerQSize
  , makeSignal c_QMovie "started" c_Listener
  , makeSignal c_QMovie "stateChanged" c_ListenerQMovieState
  , makeSignal c_QMovie "updated" c_ListenerQRect
  ]

e_CacheMode =
  makeQtEnum (ident1 "QMovie" "CacheMode") [includeStd "QMovie"]
  [ (0, ["cache", "none"])
  , (1, ["cache", "all"])
  ]

e_MovieState =
  makeQtEnum (ident1 "QMovie" "MovieState") [includeStd "QMovie"]
  [ (0, ["not", "running"])
  , (1, ["paused"])
  , (2, ["running"])
  ]
