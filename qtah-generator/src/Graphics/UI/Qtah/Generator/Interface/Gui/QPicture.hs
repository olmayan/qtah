module Graphics.UI.Qtah.Generator.Interface.Gui.QPicture (
  aModule,
  c_QPicture,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (
  boolT, charT, constT, intT,
  objT, ptrT, refT, uintT, voidT
  )
import Graphics.UI.Qtah.Generator.Flag (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QIODevice (c_QIODevice)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPaintDevice (c_QPaintDevice)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Gui.QPainter (c_QPainter)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Gui", "QPicture"] $
  [ QtExport $ ExportClass c_QPicture
  ]

c_QPicture =
  addReqIncludes [includeStd "QPicture"] $
  classAddFeatures [Assignable, Copyable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QPicture") Nothing [c_QPaintDevice] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithVersion" [intT]
  , just $ mkConstMethod' "data" "data_" [] $ ptrT $ constT charT
  , just $ mkConstMethod "isNull" [] boolT
  , just $ mkMethod "load" [objT c_QString] boolT
  , just $ mkMethod' "load" "loadWithFormat" [objT c_QString, ptrT $ constT charT] boolT
  , just $ mkMethod' "load" "loadFromIODevice" [ptrT $ objT c_QIODevice] boolT
  , just $ mkMethod' "load" "loadFromIODeviceWithFormat" [ptrT $ objT c_QIODevice, ptrT $ constT charT] boolT
  , just $ mkMethod "play" [ptrT $ objT c_QPainter] boolT
  , just $ mkMethod "save" [objT c_QString] boolT
  , just $ mkMethod' "save" "saveWithFormat" [objT c_QString, ptrT $ constT charT] boolT
  , just $ mkMethod' "save" "saveToIODevice" [ptrT $ objT c_QIODevice] boolT
  , just $ mkMethod' "save" "saveToIODeviceWithFormat" [ptrT $ objT c_QIODevice, ptrT $ constT charT] boolT
  , just $ mkMethod "setData" [ptrT $ constT charT, uintT] voidT
  , just $ mkMethod "size" [] uintT
  , test (qtVersion >= [4, 8]) $ mkMethod "swap" [refT $ objT c_QPicture] voidT
  , just $ mkProp "boundingRect" $ objT c_QRect
  ]
