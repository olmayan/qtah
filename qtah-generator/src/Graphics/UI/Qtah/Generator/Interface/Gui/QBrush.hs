-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QBrush (
  aModule,
  c_QBrush
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, objT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  e_BrushStyle,
  e_GlobalColor
  )
import Graphics.UI.Qtah.Generator.Interface.Gui.QColor (c_QColor)
import Graphics.UI.Qtah.Generator.Interface.Gui.QImage (c_QImage)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Gui.QPixmap (c_QPixmap)
import Graphics.UI.Qtah.Generator.Interface.Gui.QTransform (c_QTransform)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Gui", "QBrush"]
  [ QtExport $ ExportClass c_QBrush
  ]

c_QBrush =
  addReqIncludes [includeStd "QBrush"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QBrush") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithStyle" [enumT e_BrushStyle]
  , just $ mkCtor "newWithColor" [objT c_QColor]
  , just $ mkCtor "newWithColorAndStyle" [objT c_QColor, enumT e_BrushStyle]
  , just $ mkCtor "newWithGC" [enumT e_GlobalColor]
  , just $ mkCtor "newWithGCAndStyle" [enumT e_GlobalColor, enumT e_BrushStyle]
  , just $ mkCtor "newWithColorAndPixmap" [objT c_QColor, objT c_QPixmap]
  , just $ mkCtor "newWithGCAndPixmap" [enumT e_GlobalColor, objT c_QPixmap]
  , just $ mkCtor "newWithPixmap" [objT c_QPixmap]
  , just $ mkCtor "newWithImage" [objT c_QImage]
  --, mkCtor "newWithGradient" [objT c_QGradient]
  --just $ mkConstMethod "gradient" [] $ ptrT $ objT c_QGradient
  , just $ mkConstMethod "isOpaque" [] boolT
  , just $ mkMethod' "setColor" "setColorGC" [enumT e_GlobalColor] voidT
  , test (qtVersion >= [4, 8]) $ mkMethod "swap" [refT $ objT c_QBrush] voidT
  , just $ mkProp "color" $ objT c_QColor
  --, just $ mkProp "matrix" $ objT c_QMatrix
  , just $ mkProp "style" $ enumT e_BrushStyle
  , just $ mkProp "texture" $ objT c_QPixmap
  , just $ mkProp "textureImage" $ objT c_QImage
  , test (qtVersion >= [4, 3]) $ mkProp "transform" $ objT c_QTransform
  ]
