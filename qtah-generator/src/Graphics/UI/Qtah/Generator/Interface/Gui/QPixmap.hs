-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QPixmap (
  aModule,
  c_QPixmap
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  mkStaticMethod,
  mkStaticMethod'
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (
  bitspaceT, boolT, charT, constT,
  enumT, intT, objT, ptrT, refT,
  ucharT, uintT, voidT
  )
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QByteArray (c_QByteArray)
import Graphics.UI.Qtah.Generator.Interface.Core.QIODevice (c_QIODevice)
import Graphics.UI.Qtah.Generator.Interface.Core.QRect (c_QRect)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_ImageConversionFlags,
  e_AspectRatioMode,
  e_MaskMode,
  e_TransformationMode,
  qint64,
  qreal
  )
import Graphics.UI.Qtah.Generator.Interface.Gui.QBitmap (c_QBitmap)
import Graphics.UI.Qtah.Generator.Interface.Gui.QColor (c_QColor)
import Graphics.UI.Qtah.Generator.Interface.Gui.QImage (c_QImage)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPaintDevice (c_QPaintDevice)
import Graphics.UI.Qtah.Generator.Interface.Gui.QRegion (c_QRegion)
import Graphics.UI.Qtah.Generator.Interface.Gui.QTransform (c_QTransform)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Gui", "QPixmap"]
  [ QtExport $ ExportClass c_QPixmap
  ]

c_QPixmap =
  addReqIncludes [includeStd "QPixmap"] $
  classAddFeatures [Assignable, Copyable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QPixmap") Nothing [c_QPaintDevice] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithSizeRaw" [intT, intT]
  , just $ mkCtor "newWithSize" [objT c_QSize]
  , just $ mkCtor "newFromFile" [objT c_QString]
  , just $ mkCtor "newFromFileWithFormat" [objT c_QString, ptrT $ constT charT]
  , just $ mkCtor "newFromFileWithFormatAndFlags" [objT c_QString, ptrT $ constT charT, bitspaceT bs_ImageConversionFlags]
  --newFromXpm
  , just $ mkConstMethod "cacheKey" [] qint64
  , test (qtVersion >= [4, 7]) $ mkMethod "convertFromImage" [objT c_QImage] boolT
  , test (qtVersion >= [4, 7]) $ mkMethod' "convertFromImage" "convertFromImageWithFlags" [objT c_QImage, bitspaceT bs_ImageConversionFlags] boolT
  , just $ mkConstMethod "copy" [] $ objT c_QPixmap
  , just $ mkConstMethod' "copy" "copyRect" [objT c_QRect] $ objT c_QPixmap
  , just $ mkConstMethod' "copy" "copyRectRaw" [intT, intT, intT, intT] $ objT c_QPixmap
  , just $ mkConstMethod' "createHeuristicMask" "createHeuristicMaskClipTight" [] $ objT c_QBitmap
  , just $ mkConstMethod "createHeuristicMask" [boolT] $ objT c_QBitmap
  , just $ mkConstMethod' "createMaskFromColor" "createMaskFromColorInColor" [objT c_QColor] $ objT c_QBitmap
  , just $ mkConstMethod "createMaskFromColor" [objT c_QColor, enumT e_MaskMode] $ objT c_QBitmap
  , just $ mkConstMethod "depth" [] intT
  , just $ mkMethod "detach" [] voidT
  , just $ mkMethod' "fill" "fillWhite" [] voidT
  , just $ mkMethod "fill" [objT c_QColor] voidT
  , just $ mkConstMethod "hasAlpha" [] boolT
  , just $ mkConstMethod "hasAlphaChannel" [] boolT
  , just $ mkConstMethod "height" [] intT
  , just $ mkConstMethod "isNull" [] boolT
  , just $ mkConstMethod "isQBitmap" [] boolT
  , just $ mkMethod "load" [objT c_QString] boolT
  , just $ mkMethod' "load" "loadWithFormat" [objT c_QString, ptrT $ constT charT] boolT
  , just $ mkMethod' "load" "loadWithFormatAndFlags" [objT c_QString, ptrT $ constT charT, bitspaceT bs_ImageConversionFlags] boolT
  , just $ mkMethod' "loadFromData" "loadFromDataUChar" [ptrT $ constT ucharT, uintT] boolT
  , just $ mkMethod' "loadFromData" "loadFromDataUCharWithFormat" [ptrT $ constT ucharT, uintT, ptrT $ constT charT] boolT
  , just $ mkMethod' "loadFromData" "loadFromDataUCharWithFormatAndFlags" [ptrT $ constT ucharT, uintT, ptrT $ constT charT, bitspaceT bs_ImageConversionFlags] boolT
  , just $ mkMethod "loadFromData" [objT c_QByteArray] boolT
  , just $ mkMethod' "loadFromData" "loadFromDataWithFormat" [objT c_QByteArray, ptrT $ constT charT] boolT
  , just $ mkMethod' "loadFromData" "loadFromDataWithFormatAndFlags" [objT c_QByteArray, ptrT $ constT charT, bitspaceT bs_ImageConversionFlags] boolT
  , just $ mkConstMethod "rect" [] $ objT c_QRect
  , just $ mkConstMethod "save" [objT c_QString] boolT
  , just $ mkConstMethod' "save" "saveWithFormat" [objT c_QString, ptrT $ constT charT] boolT
  , just $ mkConstMethod' "save" "saveWithFormatAndQuality" [objT c_QString, ptrT $ constT charT, intT] boolT
  , just $ mkConstMethod' "save" "saveToIODevice" [ptrT $ objT c_QIODevice] boolT
  , just $ mkConstMethod' "save" "saveToIODeviceWithFormat" [ptrT $ objT c_QIODevice, ptrT $ constT charT] boolT
  , just $ mkConstMethod' "save" "saveToIODeviceWithFormatAndQuality" [ptrT $ objT c_QIODevice, ptrT $ constT charT, intT] boolT
  , just $ mkConstMethod "scaled" [objT c_QSize] $ objT c_QPixmap
  , just $ mkConstMethod' "scaled" "scaledWithAspectRatio" [objT c_QSize, enumT e_AspectRatioMode] $ objT c_QPixmap
  , just $ mkConstMethod' "scaled" "scaledWithAspectRatioAndTransformation" [objT c_QSize, enumT e_AspectRatioMode, enumT e_TransformationMode] $ objT c_QPixmap
  , just $ mkConstMethod' "scaled" "scaledRaw" [intT, intT] $ objT c_QPixmap
  , just $ mkConstMethod' "scaled" "scaledRawWithAspectRatio" [intT, intT, enumT e_AspectRatioMode] $ objT c_QPixmap
  , just $ mkConstMethod' "scaled" "scaledRawWithAspectRatioAndTransformation" [intT, intT, enumT e_AspectRatioMode, enumT e_TransformationMode] $ objT c_QPixmap
  , just $ mkConstMethod "scaledToHeight" [intT] $ objT c_QPixmap
  , just $ mkConstMethod' "scaledToHeight" "scaledToHeightWithTransformation" [intT, enumT e_TransformationMode] $ objT c_QPixmap
  , just $ mkConstMethod "scaledToWidth" [intT] $ objT c_QPixmap
  , just $ mkConstMethod' "scaledToWidth" "scaledToWidthWithTransformation" [intT, enumT e_TransformationMode] $ objT c_QPixmap
  , test (qtVersion >= [4, 6]) $ mkMethod' "scroll" "scrollRaw" [intT, intT, intT, intT, intT, intT] voidT
  , test (qtVersion >= [4, 6]) $ mkMethod' "scroll" "scrollRawWithRegion" [intT, intT, intT, intT, intT, intT, ptrT $ objT c_QRegion] voidT
  , test (qtVersion >= [4, 6]) $ mkMethod "scroll" [intT, intT, objT c_QRect] voidT
  , test (qtVersion >= [4, 6]) $ mkMethod' "scroll" "scrollWithRegion" [intT, intT, objT c_QRect, ptrT $ objT c_QRegion] voidT
  , just $ mkConstMethod "size" [] $ objT c_QSize
  , test (qtVersion >= [4, 8]) $ mkMethod "swap" [refT $ objT c_QPixmap] voidT
  , just $ mkConstMethod "toImage" [] $ objT c_QImage
  , just $ mkConstMethod "transformed" [objT c_QTransform] $ objT c_QPixmap
  , just $ mkConstMethod' "transformed" "transformedWithMode" [objT c_QTransform, enumT e_TransformationMode] $ objT c_QPixmap
  --, just $ mkConstMethod' "transformed" "transformed_" [objT c_QMatrix] $ objT c_QPixmap
  --, just $ mkConstMethod' "transformed" "transformedWithMode_" [objT c_QMatrix, enumT e_TransformationMode] $ objT c_QPixmap
  , just $ mkConstMethod "width" [] intT
  --operator QVariant
  --operator!
  , just $ mkStaticMethod "defaultDepth" [] intT
  , test (qtVersion >= [5, 3]) $ mkStaticMethod "fromImage" [objT c_QImage] $ objT c_QPixmap
  , test (qtVersion >= [5, 3]) $ mkStaticMethod' "fromImage" "fromImageWithFlags" [objT c_QImage, bitspaceT bs_ImageConversionFlags] $ objT c_QPixmap
  --, just $ mkStaticMethod "fromImageReader" [ptrT $ objT c_QImageReader] $ objT c_QPixmap
  --, just $ mkStaticMethod' "fromImageReader" "fromImageReaderWithFlags" [ptrT $ objT c_QImageReader, bitspaceT bs_ImageConversionFlags] $ objT c_QPixmap
  , test (qtVersion >= [4, 3]) $ mkStaticMethod "trueMatrix" [objT c_QTransform, intT, intT] $ objT c_QTransform
  --, just $ mkStaticMethod' "trueMatrix" "trueMatrix_" [objT c_QMatrix, intT, intT] $ objT c_QMatrix
  --just $ mkProp "alphaChannel" $ objT c_QPixmap
  , just $ mkProp "devicePixelRatio" qreal
  , just $ mkProp "mask" $ objT c_QBitmap
  ]
