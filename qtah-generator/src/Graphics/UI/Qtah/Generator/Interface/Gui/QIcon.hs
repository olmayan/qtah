-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QIcon (
  aModule,
  c_QIcon,
  e_Mode
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  mkStaticMethod,
  mkStaticMethod'
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, ptrT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.QList (c_QListQSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QStringList (c_QStringList)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qint64)
import Graphics.UI.Qtah.Generator.Interface.Gui.QPixmap (c_QPixmap)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Gui", "QIcon"]
  [ QtExport $ ExportClass c_QIcon
  , QtExport $ ExportEnum e_Mode
  , QtExport $ ExportEnum e_State
  ]

c_QIcon =
  addReqIncludes [includeStd "QIcon"] $
  classAddFeatures [Assignable, Copyable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QIcon") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newFromPixmap" [objT c_QPixmap]
  , just $ mkCtor "newFromFile" [objT c_QString]
  --, just $ mkCtor "newFromIconEngine" [ptrT $ objT c_QIconEngine]
  , just $ mkConstMethod "actualSize" [objT c_QSize] $ objT c_QSize
  , just $ mkConstMethod' "actualSize" "actualSizeWithMode" [objT c_QSize, enumT e_Mode] $ objT c_QSize
  , just $ mkConstMethod' "actualSize" "actualSizeWithModeAndState" [objT c_QSize, enumT e_Mode, enumT e_State] $ objT c_QSize
  --, test (qtVersion >= [5, 1]) $ mkConstMethod' "actualSize" "actualSizeFromWindow" [ptrT $ objT c_QWindow, objT c_QSize] $ objT c_QSize
  --, test (qtVersion >= [5, 1]) $ mkConstMethod' "actualSize" "actualSizeFromWindowWithMode" [ptrT $ objT c_QWindow, objT c_QSize, enumT e_Mode] $ objT c_QSize
  --, test (qtVersion >= [5, 1]) $ mkConstMethod' "actualSize" "actualSizeFromWindowWithModeAndState" [ptrT $ objT c_QWindow, objT c_QSize, enumT e_Mode, enumT e_State] $ objT c_QSize
  , just $ mkMethod "addFile" [objT c_QString, objT c_QSize] voidT
  , just $ mkMethod' "addFile" "addFileWithMode" [objT c_QString, objT c_QSize, enumT e_Mode] voidT
  , just $ mkMethod' "addFile" "addFileWithModeAndState" [objT c_QString, objT c_QSize, enumT e_Mode, enumT e_State] voidT
  , just $ mkMethod "addPixmap" [objT c_QPixmap] voidT
  , just $ mkMethod' "addPixmap" "addPixmapWithMode" [objT c_QPixmap, enumT e_Mode] voidT
  , just $ mkMethod' "addPixmap" "addPixmapWithModeAndState" [objT c_QPixmap, enumT e_Mode, enumT e_State] voidT
  , test (qtVersion >= [4, 5]) $ mkMethod "availableSizes" [] $ objT c_QListQSize
  , test (qtVersion >= [4, 5]) $ mkMethod' "availableSizes" "availableSizesWithMode" [enumT e_Mode] $ objT c_QListQSize
  , test (qtVersion >= [4, 5]) $ mkMethod' "availableSizes" "availableSizesWithModeAndState" [enumT e_Mode, enumT e_State] $ objT c_QListQSize
  , test (qtVersion >= [4, 3]) $ mkConstMethod "cacheKey" [] qint64
  , test (qtVersion >= [4, 6]) $ mkStaticMethod "fromTheme" [objT c_QString] $ objT c_QIcon
  , test (qtVersion >= [4, 6]) $ mkStaticMethod' "fromTheme" "fromThemeWithFallback" [objT c_QString, objT c_QIcon] $ objT c_QIcon
  , just $ mkConstMethod "isNull" [] boolT
  , test (qtVersion >= [4, 7]) $ mkConstMethod "name" [] $ objT c_QString
  --paint
  , just $ mkConstMethod "pixmap" [objT c_QSize] $ objT c_QPixmap
  , just $ mkConstMethod' "pixmap" "pixmapWithMode" [objT c_QSize, enumT e_Mode] $ objT c_QPixmap
  , just $ mkConstMethod' "pixmap" "pixmapWithModeAndState" [objT c_QSize, enumT e_Mode, enumT e_State] $ objT c_QPixmap
  , just $ mkConstMethod' "pixmap" "pixmapRaw" [intT, intT] $ objT c_QPixmap
  , just $ mkConstMethod' "pixmap" "pixmapRawWithMode" [intT, intT, enumT e_Mode] $ objT c_QPixmap
  , just $ mkConstMethod' "pixmap" "pixmapRawWithModeAndState" [intT, intT, enumT e_Mode, enumT e_State] $ objT c_QPixmap
  , just $ mkConstMethod' "pixmap" "pixmapWithExtent" [intT] $ objT c_QPixmap
  , just $ mkConstMethod' "pixmap" "pixmapWithExtentAndMode" [intT, enumT e_Mode] $ objT c_QPixmap
  , just $ mkConstMethod' "pixmap" "pixmapWithExtentAndModeAndState" [intT, enumT e_Mode, enumT e_State] $ objT c_QPixmap
  --, just $ mkConstMethod' "pixmap" "pixmapFromWindow" [ptrT $ objT c_QWindow, objT c_QSize] $ objT c_QPixmap
  --, just $ mkConstMethod' "pixmap" "pixmapFromWindowWithMode" [ptrT $ objT c_QWindow, objT c_QSize, enumT e_Mode] $ objT c_QPixmap
  --, just $ mkConstMethod' "pixmap" "pixmapFromWindowWithModeAndState" [ptrT $ objT c_QWindow, objT c_QSize, enumT e_Mode, enumT e_State] $ objT c_QPixmap
  , test (qtVersion >= [4, 8]) $ mkMethod "swap" [refT $ objT c_QIcon] voidT
  --operator QVariant
  , test (qtVersion >= [5, 6]) $ mkProp "isMask" boolT
  , test (qtVersion >= [4, 6]) $ mkProp "themeName" $ objT c_QString
  , test (qtVersion >= [4, 6]) $ mkProp "themeSearchPaths" $ objT c_QStringList
  ]

e_Mode =
  makeQtEnum (ident1 "QIcon" "Mode") [includeStd "QIcon"]
  [ (0, ["normal"])
  , (1, ["disabled"])
  , (2, ["active"])
  , (3, ["selected"])
  ]

e_State =
  makeQtEnum (ident1 "QIcon" "State") [includeStd "QIcon"]
  [ (0, ["off"])
  , (1, ["on"])
  ]
