-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QPen (
  aModule,
  c_QPen
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkCtor,
  mkMethod,
  mkProp,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, intT, objT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  e_PenCapStyle,
  e_PenJoinStyle,
  e_PenStyle,
  qreal
  )
import Graphics.UI.Qtah.Generator.Interface.Gui.QBrush (c_QBrush)
import Graphics.UI.Qtah.Generator.Interface.Gui.QColor (c_QColor)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Gui", "QPen"]
  [ QtExport $ ExportClass c_QPen
  ]

c_QPen =
  addReqIncludes [includeStd "QPen"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QPen") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithStyle" [enumT e_PenStyle]
  , just $ mkCtor "newWithColor" [objT c_QColor]
  , just $ mkCtor "newWithBrush" [objT c_QBrush, qreal]
  , just $ mkCtor "newWithBrushAndStyles" [objT c_QBrush, qreal, enumT e_PenStyle, enumT e_PenCapStyle, enumT e_PenJoinStyle]
  , just $ mkConstMethod "isSolid" [] boolT
  , test (qtVersion >= [4, 8]) $ mkMethod "swap" [refT $ objT c_QPen] voidT
  --operator QVariant
  , just $ mkProp "brush" $ objT c_QBrush
  , just $ mkProp "capStyle" $ enumT e_PenCapStyle
  , just $ mkProp "color" $ objT c_QColor
  , just $ mkProp "dashOffset" qreal
  --dashPattern
  , just $ mkBoolIsProp "cosmetic"
  , just $ mkProp "joinStyle" $ enumT e_PenJoinStyle
  , just $ mkProp "miterLimit" qreal
  , just $ mkProp "width" intT
  , just $ mkProp "widthF" qreal
  ]
