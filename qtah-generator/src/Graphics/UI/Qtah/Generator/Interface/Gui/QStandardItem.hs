-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QStandardItem (
  aModule,
  c_QStandardItem,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkBoolIsProp,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Types (bitspaceT, boolT, enumT, intT, objT, ptrT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Core.QList (c_QListQStandardItem)
import Graphics.UI.Qtah.Generator.Interface.Core.QModelIndex (c_QModelIndex)
import Graphics.UI.Qtah.Generator.Interface.Core.QSize (c_QSize)
import Graphics.UI.Qtah.Generator.Interface.Core.QString (c_QString)
import Graphics.UI.Qtah.Generator.Interface.Core.QVariant (c_QVariant)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  bs_Alignment,
  bs_ItemFlags,
  e_CheckState,
  e_SortOrder,
  )
import Graphics.UI.Qtah.Generator.Interface.Gui.QStandardItemModel (c_QStandardItemModel)
import Graphics.UI.Qtah.Generator.Interface.Gui.QBrush (c_QBrush)
import Graphics.UI.Qtah.Generator.Interface.Gui.QFont (c_QFont)
import Graphics.UI.Qtah.Generator.Interface.Gui.QIcon (c_QIcon)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModuleWithMinVersion)
import Graphics.UI.Qtah.Generator.Types

minVersion = [4, 2]

aModule =
  AQtModule $
  makeQtModuleWithMinVersion ["Gui", "QStandardItem"] minVersion $
  map QtExport
  [ ExportClass c_QStandardItem
  , ExportEnum e_ItemType
  ]

c_QStandardItem =
  addReqIncludes [includeStd "QStandardItem"] $
  classSetEntityPrefix "" $
  makeClass (ident "QStandardItem") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithText" [objT c_QString]
  , just $ mkCtor "newWithIconAndText" [objT c_QIcon, objT c_QString]
  , just $ mkCtor "newWithRC" [intT, intT]
  , just $ mkMethod "appendColumn" [objT c_QListQStandardItem] voidT
  , just $ mkMethod "appendRow" [objT c_QListQStandardItem] voidT
  , just $ mkMethod' "appendRow" "appendRowSingle" [ptrT $ objT c_QStandardItem] voidT
  , just $ mkMethod "appendRows" [objT c_QListQStandardItem] voidT
  , just $ mkConstMethod' "child" "childFirstColumn" [intT] $ ptrT $ objT c_QStandardItem
  , just $ mkConstMethod "child" [intT, intT] $ ptrT $ objT c_QStandardItem
  , just $ mkConstMethod "clone" [] $ ptrT $ objT c_QStandardItem
  , just $ mkConstMethod "column" [] intT
  , just $ mkConstMethod' "data" "dataUserRole" [] $ objT c_QVariant
  , just $ mkConstMethod' "data" "data_" [intT] $ objT c_QVariant
  , just $ mkConstMethod "hasChildren" [] boolT
  , just $ mkConstMethod "index" [] $ objT c_QModelIndex
  , just $ mkMethod "insertColumn" [intT, objT c_QListQStandardItem] voidT
  , just $ mkMethod "insertColumns" [intT, intT] voidT
  , just $ mkMethod "insertRow" [intT, objT c_QListQStandardItem] voidT
  , just $ mkMethod' "insertRow" "insertRowSingle" [intT, ptrT $ objT c_QStandardItem] voidT
  , just $ mkMethod "insertRows" [intT, intT] voidT
  , just $ mkMethod' "insertRows" "insertRowsSingle" [intT, intT] voidT
  , just $ mkConstMethod "model" [] $ ptrT $ objT c_QStandardItemModel
  , just $ mkConstMethod "parent" [] $ ptrT $ objT c_QStandardItem
  --read
  , just $ mkMethod "removeColumn" [intT] voidT
  , just $ mkMethod "removeColumns" [intT, intT] voidT
  , just $ mkMethod "removeRow" [intT] voidT
  , just $ mkMethod "removeRows" [intT, intT] voidT
  , just $ mkConstMethod "row" [] intT
  , just $ mkMethod "setChild" [intT, intT, ptrT $ objT c_QStandardItem] voidT
  , just $ mkMethod' "setChild" "setChildFirstColumn" [intT, ptrT $ objT c_QStandardItem] voidT
  , just $ mkMethod' "setData" "setDataUserRole" [objT c_QVariant] voidT
  , just $ mkMethod "setData" [objT c_QVariant, intT] voidT
  , just $ mkMethod' "sortChildren" "sortChildrenAscending" [intT] voidT
  , just $ mkMethod "sortChildren" [intT, enumT e_SortOrder] voidT
  , just $ mkMethod' "takeChild" "takeChildFirstColumn" [intT] $ ptrT $ objT c_QStandardItem
  , just $ mkMethod "takeChild" [intT, intT] $ ptrT $ objT c_QStandardItem
  , just $ mkMethod "takeColumn" [intT] $ objT c_QListQStandardItem
  , just $ mkMethod "takeRow" [intT] $ objT c_QListQStandardItem
  , just $ mkConstMethod' "type" "type_" [] intT
  --write
  , test (qtVersion >= [5, 6]) $ mkBoolIsProp "autoTristate"
  , just $ mkProp "accessibleDescription" $ objT c_QString
  , just $ mkProp "accessibleText" $ objT c_QString
  , just $ mkProp "background" $ objT c_QBrush
  , just $ mkProp "checkState" $ enumT e_CheckState
  , just $ mkBoolIsProp "checkable"
  , just $ mkProp "columnCount" intT
  , just $ mkBoolIsProp "dragEnabled"
  , just $ mkBoolIsProp "dropEnabled"
  , just $ mkBoolIsProp "editable"
  , just $ mkBoolIsProp "enabled"
  , just $ mkProp "flags" $ bitspaceT bs_ItemFlags
  , just $ mkProp "font" $ objT c_QFont
  , just $ mkProp "foreground" $ objT c_QBrush
  , just $ mkProp "icon" $ objT c_QIcon
  , just $ mkProp "rowCount" intT
  , just $ mkBoolIsProp "selectable"
  , just $ mkProp "sizeHint" $ objT c_QSize
  , just $ mkProp "statusTip" $ objT c_QString
  , just $ mkProp "text" $ objT c_QString
  , just $ mkProp "textAlignment" $ bitspaceT bs_Alignment
  , just $ mkProp "toolTip" $ objT c_QString
  , test (qtVersion >= [5, 6]) $ mkBoolIsProp "userTristate"
  , just $ mkProp "whatsThis" $ objT c_QString
  ]

e_ItemType =
  makeQtEnum (ident1 "QStandardItem" "ItemType") [includeStd "QStandardItem"]
  [ (0, ["type"])
  , (1000, ["user", "type"])
  ]
