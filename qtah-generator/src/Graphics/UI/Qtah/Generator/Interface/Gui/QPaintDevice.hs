module Graphics.UI.Qtah.Generator.Interface.Gui.QPaintDevice (
  aModule,
  c_QPaintDevice,
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  )
import Foreign.Hoppy.Generator.Types (boolT, intT, objT, ptrT)
import Graphics.UI.Qtah.Generator.Flag (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (qreal)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Gui", "QPaintDevice"] $
  [ QtExport $ ExportClass c_QPaintDevice
  , QtExport $ ExportEnum e_PaintDeviceMetric
  ]

c_QPaintDevice =
  addReqIncludes [includeStd "QPaintDevice"] $
  classSetEntityPrefix "" $
  makeClass (ident "QPaintDevice") Nothing [] $
  collect
  [ just $ mkConstMethod "colorCount" [] intT
  , just $ mkConstMethod "depth" [] intT
  , just $ mkConstMethod "devicePixelRatio" [] intT
  , test (qtVersion >= [5, 6]) $ mkConstMethod "devicePixelRatioF" [] qreal
  , just $ mkConstMethod "height" [] intT
  , just $ mkConstMethod "heightMM" [] intT
  , just $ mkConstMethod "logicalDpiX" [] intT
  , just $ mkConstMethod "logicalDpiY" [] intT
  --, just $ mkConstMethod "paintEngine" [] $ ptrT $ objT c_QPaintEngine
  , just $ mkConstMethod "paintingActive" [] boolT
  , just $ mkConstMethod "physicalDpiX" [] intT
  , just $ mkConstMethod "physicalDpiY" [] intT
  , just $ mkConstMethod "width" [] intT
  , just $ mkConstMethod "widthMM" [] intT
  ]

e_PaintDeviceMetric =
  makeQtEnum (ident1 "QPaintDevice" "PaintDeviceMetric") [includeStd "QPaintDevice"]
  [ (1, ["pdm", "width"])
  , (2, ["pdm", "height"])
  , (3, ["pdm", "width", "m", "m"])
  , (4, ["pdm", "height", "m", "m"])
  , (5, ["pdm", "num", "colors"])
  , (6, ["pdm", "depth"])
  , (7, ["pdm", "dpi", "x"])
  , (8, ["pdm", "dpi", "y"])
  , (9, ["pdm", "physical", "dpi", "x"])
  , (10, ["pdm", "physical", "dpi", "y"])
  , (11, ["pdm", "device", "pixel", "ratio"])
  , (12, ["pdm", "device", "pixel", "ratio", "scaled"])
  ]
