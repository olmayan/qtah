-- This file is part of Qtah.
--
-- Copyright 2015-2016 Bryan Gardiner <bog@khumba.net>
--
-- This particular file has been created by olmayan (gitlab.com/olmayan)
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU Lesser General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- (at your option) any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU Lesser General Public License for more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program.  If not, see <http://www.gnu.org/licenses/>.

module Graphics.UI.Qtah.Generator.Interface.Gui.QPalette (
  aModule,
  c_QPalette
  ) where

import Foreign.Hoppy.Generator.Spec (
  Export (ExportClass, ExportEnum),
  addReqIncludes,
  classSetEntityPrefix,
  classSetConversionToGc,
  ident,
  ident1,
  includeStd,
  makeClass,
  mkConstMethod,
  mkConstMethod',
  mkCtor,
  mkMethod,
  mkMethod',
  mkProp,
  )
import Foreign.Hoppy.Generator.Spec.ClassFeature (
  ClassFeature (Assignable, Copyable, Equatable),
  classAddFeatures,
  )
import Foreign.Hoppy.Generator.Types (boolT, enumT, objT, refT, voidT)
import Foreign.Hoppy.Generator.Version (collect, just, test)
import Graphics.UI.Qtah.Generator.Flags (qtVersion)
import Graphics.UI.Qtah.Generator.Interface.Core.Types (
  e_GlobalColor,
  qint64
  )
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Gui.QBrush (c_QBrush)
import {-# SOURCE #-} Graphics.UI.Qtah.Generator.Interface.Gui.QColor (c_QColor)
import Graphics.UI.Qtah.Generator.Module (AModule (AQtModule), makeQtModule)
import Graphics.UI.Qtah.Generator.Types

aModule =
  AQtModule $
  makeQtModule ["Gui", "QPalette"]
  [ QtExport $ ExportClass c_QPalette
  , QtExport $ ExportEnum e_ColorGroup
  , QtExport $ ExportEnum e_ColorRole
  ]

c_QPalette =
  addReqIncludes [includeStd "QPalette"] $
  classAddFeatures [Assignable, Copyable, Equatable] $
  classSetConversionToGc $
  classSetEntityPrefix "" $
  makeClass (ident "QPalette") Nothing [] $
  collect
  [ just $ mkCtor "new" []
  , just $ mkCtor "newWithButtonColor" [objT c_QColor]
  , just $ mkCtor "newWithButtonGC" [enumT e_GlobalColor]
  , just $ mkCtor "newWithButtonAndWindow" [objT c_QColor, objT c_QColor]
  , just $ mkCtor "newMain" [objT c_QBrush, objT c_QBrush, objT c_QBrush,
                             objT c_QBrush, objT c_QBrush, objT c_QBrush,
                             objT c_QBrush, objT c_QBrush, objT c_QBrush]
  , just $ mkConstMethod "alternateBase" [] $ objT c_QBrush
  , just $ mkConstMethod "base" [] $ objT c_QBrush
  , just $ mkConstMethod "brightText" [] $ objT c_QBrush
  , just $ mkConstMethod' "brush" "brushForGroup" [enumT e_ColorGroup, enumT e_ColorRole] $ objT c_QBrush
  , just $ mkConstMethod "brush" [enumT e_ColorRole] $ objT c_QBrush
  , just $ mkConstMethod "button" [] $ objT c_QBrush
  , just $ mkConstMethod "buttonText" [] $ objT c_QBrush
  , just $ mkConstMethod "cacheKey" [] qint64
  , just $ mkConstMethod' "color" "colorForGroup" [enumT e_ColorGroup, enumT e_ColorRole] $ objT c_QColor
  , just $ mkConstMethod "color" [enumT e_ColorRole] $ objT c_QColor
  , just $ mkConstMethod "dark" [] $ objT c_QBrush
  , just $ mkConstMethod "highlight" [] $ objT c_QBrush
  , just $ mkConstMethod "highlightedText" [] $ objT c_QBrush
  , test (qtVersion >= [4, 2]) $ mkConstMethod "isBrushSet" [enumT e_ColorGroup, enumT e_ColorRole] boolT
  , just $ mkConstMethod "isCopyOf" [objT c_QPalette] boolT
  , just $ mkConstMethod "isEqual" [enumT e_ColorGroup, enumT e_ColorGroup] boolT
  , just $ mkConstMethod "light" [] $ objT c_QBrush
  , just $ mkConstMethod "link" [] $ objT c_QBrush
  , just $ mkConstMethod "linkVisited" [] $ objT c_QBrush
  , just $ mkConstMethod "mid" [] $ objT c_QBrush
  , just $ mkConstMethod "midlight" [] $ objT c_QBrush
  , just $ mkConstMethod "resolve" [objT c_QPalette] $ objT c_QPalette
  , just $ mkMethod "setBrush" [enumT e_ColorRole, objT c_QBrush] voidT
  , just $ mkMethod' "setBrush" "setBrushForGroup" [enumT e_ColorGroup, enumT e_ColorRole, objT c_QBrush] voidT
  , just $ mkMethod "setColor" [enumT e_ColorRole, objT c_QColor] voidT
  , just $ mkMethod' "setColor" "setColorForGroup" [enumT e_ColorGroup, enumT e_ColorRole, objT c_QColor] voidT
  , just $ mkMethod "setColorGroup" [enumT e_ColorGroup, objT c_QBrush, objT c_QBrush, objT c_QBrush,
                                                         objT c_QBrush, objT c_QBrush, objT c_QBrush,
                                                         objT c_QBrush, objT c_QBrush, objT c_QBrush] voidT
  , just $ mkConstMethod "shadow" [] $ objT c_QBrush
  , test (qtVersion >= [5, 0]) $ mkConstMethod "swap" [refT $ objT c_QPalette] voidT
  , just $ mkConstMethod "text" [] $ objT c_QBrush
  , test (qtVersion >= [4, 4]) $ mkConstMethod "toolTipBase" [] $ objT c_QBrush
  , test (qtVersion >= [4, 4]) $ mkConstMethod "toolTipText" [] $ objT c_QBrush
  , just $ mkConstMethod "window" [] $ objT c_QBrush
  , just $ mkConstMethod "windowText" [] $ objT c_QBrush
    --operator QVariant
  , just $ mkProp "currentColorGroup" $ enumT e_ColorGroup
  ]

e_ColorGroup =
  makeQtEnum (ident1 "QPalette" "ColorGroup") [includeStd "QPalette"]
  [ (1, ["disabled"])
  , (0, ["active"])
  , (2, ["inactive"])
  ]

e_ColorRole =
  makeQtEnum (ident1 "QPalette" "ColorRole") [includeStd "QPalette"]
  [ (10, ["window"])
  , (0, ["window", "text"])
  , (9, ["base"])
  , (16, ["alternate", "base"])
  , (18, ["tool", "tip", "base"])
  , (19, ["tool", "tip", "text"])
  , (6, ["text"])
  , (1, ["button"])
  , (8, ["button", "text"])
  , (7, ["bright", "text"])
  , (2, ["light"])
  , (3, ["midlight"])
  , (4, ["dark"])
  , (5, ["mid"])
  , (11, ["shadow"])
  , (12, ["highlight"])
  , (13, ["highlighted", "text"])
  , (14, ["link"])
  , (15, ["link", "visited"])
  , (17, ["no", "role"])
  ]
